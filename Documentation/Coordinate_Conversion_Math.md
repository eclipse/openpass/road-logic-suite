# Coordinate Conversions

This document describes the basic math that is used for the different coordinate conversions.

## Line Geometries

To convert a road coordinate to inertial coordinates, we simply use the following formula:

![equation](./img/line_reference_line_to_inertial.png)

where:

- `(x_in,y_in)`: inertial coordinates, aka the result.
- `(x_0,y_0)`: the starting point of the line in inertial coordinates.
- `(s,t)`: Road coordinates
- `s_0`: The starting point of the line in road coordinates.
- `\theta_0`: The heading of the line.

![drawing](./img/line_reference_line_to_inertial_drawing.png)

To calculate the other way around, we are using the following setup as a basis:

![drawing2](./img/line_inertial_to_reference_line_drawing.png)

The vector `P_Q -> P_S` from the query point `P_Q` to the closest point on the line `P_S` is orthogonal to the
directional vector `u` of the line. This means that the cross product between the two vectors must be zero.

![equation2](./img/line_inertial_to_reference_line.png)

`P_S` is defined as:

![equation3](./img/line_inertial_to_reference_line_2.png)

Calculating the s and t values is done in the following way:

1. Extract `Lambda` using the above two equations.
2. If Lambda is < 0 or longer than the length of the line, we stop and try the next geometry.
3. Calculate `P_S`
4. `s = s0 + lambda` and `t = distance(P_S, P_Q)`

## Arc Geometries

Converting road coordinates to inertial coordinates in an arc geometry works in the following way:

![drawing3](./img/arc_reference_line_to_inertial_drawing.png)

1. Calculate the center point of the arc using the start heading `\theta_0`, the start position `(x_0, y_0)` and the
   radius of the arc.
2. Calculate the angle to which our target point lies from the values of `s` and the curvature.
3. Calculate the target point using that angle and (adding or subtracting) the `t` offset.

The complete equation:

![equation4](./img/arc_reference_line_to_inertial.png)

To convert inertial coordinates to road coordinates, we do the following:

1. Find the center point of the arc.
2. Project the query point onto a circle with the same radius as the arc.
3. Check, if the arc length from the start point to the projected point is bigger than 0 and smaller than the length of
   the arc. Otherwise, skip this geometry.
4. The arc length from the start point to the projected point yields `s`.
5. The distance from the center point to the projected point subtracted from the radius of the arc yields `t`.

![drawing4](./img/arc_inertial_to_reference_line_drawing.png)

## Spiral Geometries

Spirals in OpenDRIVE are defined as pieces of an euler spiral:

![drawing5](./img/spiral_naming_conventions.png)

Thus, we can define our spiral in terms of the following parameters:

- Road Geometry Origin
    - $s_r$: Starting value on the road. Useful, if a road contains multiple geometries
    - $(x_r, y_r)$: Start offset in inertial coordinates
    - $\theta_r$: Start heading
- Spiral Origin
    - $s_s$: The arc length between $s_r$ and the spiral origin.
    - $(x_s(0), y_s(0))$: Offset of the spiral center.
    - $\theta_s$: Start heading of the spiral.
- Spiral Parameters:
    - $\Delta c$: Curvature change rate.
    - $a = \sqrt{\frac{2}{\Delta c}}$: Scaling parameter of the spiral.

The spiral itself is defined as a parametric function:

$$ x_s(u) = \int_{0}^{s_{in}}{\cos((\frac{u}{a})^2) du} $$

$$ y_s(u) = \int_{0}^{s_{in}}{\sin((\frac{u}{a})^2) du} $$

$(x_s(u), y_s(u)) defines a position in the spiral coordinates system. The origin is at the spiral origin point.
$s_{in}$ is defined as the arc length from the spiral origin to the query point and is defined as $s_{in} = s_q - s_r +
s_s$.

To convert from s-t coordinates to x-y-z coordinates we first determine the position in spiral coordinates and then
convert the spiral coordinates into inertial coordinates, via the following transformation:

$$ x(u) = \cos(\theta_r - \theta_s) (x_s(u) - x_s(0)) - \sin(\theta_r - \theta_s) (y_s(u) - y_s(0)) + x_r $$

$$ y(u) = \sin(\theta_r - \theta_s) (x_s(u) - x_s(0)) + \cos(\theta_r - \theta_s) (y_s(u) - y_s(0)) + x_r $$

Then the t offset is added just like for the other geometries.

To convert back from x-y-z coordinates to s-t coordinates, we use the Newton-Rhapson method to find the minimal distance
from the query point to the spiral.

$$ s_{i+1} = s_i - \frac{f'(s_i)}{f''(s_i)} $$

The function we try to minimize is the function of the squared distance to the query point $(x_q, y_q):

$$ f(s_i) = (x_q - x_s(s_i))^2 + (y_q - y_s(s_i))^2 $$

The first and second derivative are

$$ f'(s_i) = -2 ((x_q - x(s_i)) x'(s_i) + (y_q - y(s_i)) y'(s_i)) $$

$$ f''(s_i) = 2 (-(x_q - x(s_i)) x''(s_i) + x'(s_i)^2 - (y_q - y(i)) y''(s) + y'(s_i)^2) $$

The derivatives of the $x_s$ and $y_s$ function of the spiral are:

$$ x_s'(s_i) = \cos( (\frac{s}{a})^2 ) $$

$$ y_s'(s_i) = \sin( (\frac{s}{a})^2 ) $$

When converting them from spiral coordinates into inertial coordinates, this yields:

$$ x'(s_i) = \cos(\theta_r - \theta_s) \cos( (\frac{s}{a})^2 ) - \sin(\theta_r - \theta_s) \sin( (\frac{s}{a})^2 ) $$

$$ y'(s_i) = \sin(\theta_r - \theta_s) \cos( (\frac{s}{a})^2 ) + \cos(\theta_r - \theta_s) \sin( (\frac{s}{a})^2 ) $$

This can be simplified to:

$$ x'(s_i) = \cos( \frac{s^2}{a^2} + (\theta_r - \theta_s) ) $$

$$ y'(s_i) = \sin( \frac{s^2}{a^2} + (\theta_r - \theta_s) ) $$

The second derivative of $x_s$ and $y_s$ are:

$$ x''(s_i) = - \frac{2 s}{a^2} \sin( \frac{s^2}{a^2} + (\theta_r - \theta_s) ) $$

$$ y''(s_i) = \frac{2 s}{a^2} \cos( \frac{s^2}{a^2} + (\theta_r - \theta_s) ) $$

The code itself contains some additional checks:

- A broad search for a starting point for the Newton-Rhapson method. This ensures, that there is no accidental
  convergence towards a maxima or another local minima.
- A maximum amount of steps in case of non-convergence.
- A bounds check to make sure that only s values in between s0 and s0+length are valid.