# *******************************************************************************
# Copyright (C) 2023-2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

FROM ubuntu:24.04 AS build-base

RUN apt-get --quiet update && \
    DEBIAN_FRONTEND=noninteractive apt-get --quiet install -y --no-install-recommends \
    build-essential \
    git \
    git-lfs \
    curl \
    ca-certificates \
    clang-format-15 \
    clang-tidy \
    gnupg \
    apt-transport-https \
    # required for code coverage :
    lcov \
    default-jre default-jdk \
    # reuqired by doxygen:
    doxygen \
    graphviz \
    aha \
    # required by test output generation :
    xsltproc

ARG BAZELISK_VERSION=1.19.0
ARG BAZELISK_SHA=d28b588ac0916abd6bf02defb5433f6eddf7cba35ffa808eabb65a44aab226f7
RUN curl -fSsL -L -o /usr/local/bin/bazel \
    "https://github.com/bazelbuild/bazelisk/releases/download/v${BAZELISK_VERSION}/bazelisk-linux-amd64" \
    && echo "${BAZELISK_SHA} */usr/local/bin/bazel" | sha256sum --check - \
    && chmod 0755 /usr/local/bin/bazel


ARG BUILDIFIER_VERSION=6.0.0
ARG BUILDIFIER_SHA=7ff82176879c0c13bc682b6b0e482d670fbe13bbb20e07915edb0ad11be50502
RUN curl -fSsL -L -o /usr/local/bin/buildifier \
    "https://github.com/bazelbuild/buildtools/releases/download/${BUILDIFIER_VERSION}/buildifier-linux-amd64" \
    && echo "${BUILDIFIER_SHA} */usr/local/bin/buildifier" | sha256sum --check - \
    && chmod 0755 /usr/local/bin/buildifier

ARG SHFMT_VERSION=v3.6.0
ARG SHFMT_SHA=5741a02a641de7e56b8da170e71a97e58050d66a3cf485fb268d6a5a8bb74afb
RUN curl -fSsL -L -o /usr/local/bin/shfmt \
    "https://github.com/mvdan/sh/releases/download/${SHFMT_VERSION}/shfmt_${SHFMT_VERSION}_linux_amd64" \
    && echo "${SHFMT_SHA} */usr/local/bin/shfmt" | sha256sum --check - \
    && chmod 0755 /usr/local/bin/shfmt
