# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/usr/bin/env bash

function log::success()
{
    echo -e "[ $(_ansi_bold)$(_ansi_green)OK$(_ansi_reset) ] $*"
}

function log::warn()
{
    echo -e "$(_ansi_yellow)WARNING: $*$(_ansi_reset)"
}

function log::error()
{
    echo >&2 -e "$(_ansi_bold)$(_ansi_red)ERROR: $*$(_ansi_reset)"
}

function log::failure()
{
    echo >&2 -e "[$(_ansi_bold)$(_ansi_red)FAIL$(_ansi_reset)] $*"
}

function log::sub_failure()
{
    echo >&2 -e "   [$(_ansi_red)x$(_ansi_reset)] $*"
}

function log::fix()
{
    echo -e "[ $(_ansi_bold)$(_ansi_yellow)FIX$(_ansi_reset)] $*"
}

function log::sub_fix()
{
    echo -e "   [$(_ansi_yellow)+$(_ansi_reset)] $*"
}

function log::run()
{
    echo -e "[$(_ansi_bold)$(_ansi_yellow)RUN $(_ansi_reset)] $*"
}

function log::summary()
{
    local title=${1}
    local failed=${2}
    local perform_dry_run=${3}

    local mode="found"
    if [[ ${perform_dry_run} -eq 0 ]]; then
        mode="fixed"
    fi

    local msg="${title}: ${failed} errors ${mode}"

    if [[ ${failed} -ne 0 ]]; then
        if [[ ${perform_dry_run} -eq 1 ]]; then
            log::failure "${msg}"
        else
            log::fix "${msg}"
        fi
    else
        log::success "${msg}"
    fi
}

# -------------- private section ------------------------

_color_enabled=1

_ansi_reset()
{
    if [[ "${_color_enabled}" -eq 1 ]]; then
        echo -e "\e[0m"
    else
        echo ""
    fi
}

_ansi_bold()
{
    if [[ "${_color_enabled}" -eq 1 ]]; then
        echo -e "\e[1m"
    else
        echo ""
    fi
}

_ansi_green()
{
    if [[ "${_color_enabled}" -eq 1 ]]; then
        echo -e "\e[32m"
    else
        echo ""
    fi
}

_ansi_yellow()
{
    if [[ "${_color_enabled}" -eq 1 ]]; then
        echo -e "\e[33m"
    else
        echo ""
    fi
}

_ansi_red()
{
    if [[ "${_color_enabled}" -eq 1 ]]; then
        echo -e "\e[91m"
    else
        echo ""
    fi
}
