# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/usr/bin/env bash

source "$(dirname "${BASH_SOURCE[0]}")/log.sh"
