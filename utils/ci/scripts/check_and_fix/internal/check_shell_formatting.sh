# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u

if [[ -z "${CURRENT_DIRECTORY+x}" ]]; then
    readonly CURRENT_DIRECTORY=$(readlink -f "$(dirname "$0")")
fi
source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

perform_dry_run="${1-0}"
failed=0
shfmt_location=$(which shfmt)

function main()
{
    title="Check shell script formatting"

    log::run "${title}"

    ensure_shfmt_installed
    check_shell_script_formatting

    log::summary "${title}" "${failed}" "${perform_dry_run}"
    return "${failed}"
}

function ensure_shfmt_installed()
{
    if [[ -z ${shfmt_location} ]]; then
        log::warn "shfmt was not found on your system, so formatting cannot be checked"
        exit 1
    fi
}

function remove_third_party_fails()
{
    local prefix_to_remove="ThirdParty/"
    local list=("$@")

    local new_list=()
    for item in "${list[@]}"; do
        if [[ ! "$item" =~ ^"$prefix_to_remove" ]]; then
            new_list+=("$item")
        fi
    done

    echo "${new_list[@]}"
}

function check_shell_script_formatting()
{
    local general_options=(-i 4 -ci -fn -bn)

    local dry_run=(-w -l)
    if [[ ${perform_dry_run} -eq 1 ]]; then
        dry_run=(-l)
    fi

    local all_failing_files=($(${shfmt_location} "${general_options[@]}" "${dry_run[@]}" "."))
    local failing_files=($(remove_third_party_fails "${all_failing_files[@]}"))

    failed=${#failing_files[@]}

    for ((i = 0; i < failed; i += 1)); do
        if [[ ${perform_dry_run} -eq 1 ]]; then
            log::sub_failure "Invalid shell script formatting: ${failing_files[i]} "
        else
            log::sub_fix "Formatted shell script: ${failing_files[i]}"
        fi
    done
}

main "${@}"
