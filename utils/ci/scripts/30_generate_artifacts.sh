# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

echo "Code coverage and doxygen ..."

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "${BASEDIR}/.cache")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

git_tag=$(git tag --points-at HEAD)

# Only create code coverage and doxygen report for the main branch or tagged commit
if [[ "$BRANCH_NAME" == "main" || -n "$git_tag" ]]; then
    echo "Create a navigation page"
    mkdir _site
    echo "<!DOCTYPE html>" >>_site/index.html
    echo "<html>" >>_site/index.html
    echo "<head><title>Ansys Road Logic Suite</title><style>h1 {text-align: center;}</style></head> " >>_site/index.html
    echo "<body>" >>_site/index.html
    echo "<h1><a href="DoxygenAPI/documentation/index.html">User Documentation</a></h1>" >>_site/index.html
    echo "<h1><a href="Doxygen/documentation/index.html">Developer Documentation</a></h1>" >>_site/index.html
    echo "<h1><a href="bazel-coverage/index.html">Code Coverage</a></h1>" >>_site/index.html
    echo "<h1><a href="test_results.html">Test Results</a></h1>" >>_site/index.html
    echo "</body>" >>_site/index.html
    echo "</html>" >>_site/index.html
    ls -l _site/index.html

    echo "Run test and generate test report ..."
    "${MYDIR}"/build_and_test/build_and_test.sh

    ls -l test_results.html
    mv test_results.html _site/

    echo "Run code coverage generation ..."
    "${MYDIR}"/check_and_fix/code_coverage.sh
    ls -l bazel-coverage/index.html
    mv bazel-coverage/ _site/

    echo "Run doxygen generation (fully) ..."
    doxygen Documentation/Doxyfile
    ls -l Doxygen/documentation/index.html
    mv Doxygen/ _site/

    echo "Run doxygen generation (API Only) ..."
    doxygen Documentation/DoxyfileAPI
    ls -l DoxygenAPI/documentation/index.html
    mv DoxygenAPI/ _site/

    echo "Copy results to artifacts folder ..."
    mkdir -p "${BASEDIR}/artifacts"
    cp -r _site/* "${BASEDIR}/artifacts"
fi

# Only create pre-built artifact if current commit is tagged
if [ -n "$git_tag" ]; then
    echo "Creating pre-built artifact for tag: $git_tag"
    bazel build --config=rls_release //Packaging/...
    mkdir -p "${BASEDIR}/artifacts"
    cp -r bazel-bin/Packaging/tar-road-logic-suite-lib.tar.gz "${BASEDIR}/artifacts/road-logic-suite-${git_tag}-linux-x86_64.tar.gz"
fi
