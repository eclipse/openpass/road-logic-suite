/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

void ValidateMap(const std::string& map_path, int expected_errors)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath(map_path));

    // When
    auto runner = suite.GetMapValidator();
    const auto result = runner->Validate();

    // Then
    EXPECT_EQ(result.size(), expected_errors);
}

TEST(MapValidatorTest, GivenValidMap_WhenValidateMap_ThenNoErrors)
{
    ValidateMap("3kmStraight.xodr", 0);
}

TEST(MapLoaderTest, GivenMapGeometryShorterThanRoad_WhenValidateMap_ThenOneError)
{
    // TODO: Add test content with Xodr Map
}

TEST(MapLoaderTest, GivenMapGeometryLongerThanRoad_WhenValidateMap_ThenOneError)
{
    // TODO: Add test content with Xodr Map
}

}  // namespace road_logic_suite::test
