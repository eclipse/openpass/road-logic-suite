/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithConvertedRoadmarkContainsOnlyOnePoint_WhenParsingMap_ThenDownsamplingIsSkippedAndPrintWarning)
{

    testing::internal::CaptureStdout();

    const auto expected_warning_output =
        "[RoadLogicSuite] WARN: Cannot simplify a polyline with less than two points. Downsampling is skipped!\n";

    EXPECT_NO_THROW(GetMap(GetResolvedMapPathInternal("roadmarks_in_edge_case.xodr")));

    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_warning_output);
}

}  // namespace road_logic_suite::test
