/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
TEST_F(MapConvertIntegrationTestFixture, GivenRoadWithMultipleGeometries_WhenConverting_ThenGeometryMustBeCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath("road_with_multiple_geometries.xodr"), 1, 0.01, false);

    const auto left_lane = GetLaneWithFrontAt(0, -1.5);

    // Test some sample points going along the three geometries
    // s=0 (line)
    EXPECT_THAT(left_lane.centerline[0],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(0), units::length::meter_t(-1.5), units::length::meter_t(0)}));
    EXPECT_THAT(left_lane.centerline[250],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(250), units::length::meter_t(-1.5), units::length::meter_t(0)}));
    // s=500 (line -> spiral)
    EXPECT_THAT(left_lane.centerline[500],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(500), units::length::meter_t(-1.5), units::length::meter_t(0)}));
    EXPECT_THAT(left_lane.centerline[750],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(748.813), units::length::meter_t(19.287), units::length::meter_t(0)}));
    // s=1000 (spiral -> arc)
    EXPECT_THAT(left_lane.centerline[1000],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(953.524), units::length::meter_t(154.324), units::length::meter_t(0)}));
    EXPECT_THAT(left_lane.centerline[1250],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(970.583), units::length::meter_t(394.871), units::length::meter_t(0)}));
    EXPECT_THAT(left_lane.centerline[1500],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(777.386), units::length::meter_t(539.193), units::length::meter_t(0)}));

    const auto opposite_lane = left_lane.left_adjacent_lanes[0].get();
    // s=1500 (arc)
    EXPECT_THAT(opposite_lane.centerline[0],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(776.963), units::length::meter_t(536.223), units::length::meter_t(0)}));
    EXPECT_THAT(opposite_lane.centerline[250],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(967.855), units::length::meter_t(393.622), units::length::meter_t(0)}));
    // s=1000 (arc -> spiral)
    EXPECT_THAT(opposite_lane.centerline[500],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(951), units::length::meter_t(155.945), units::length::meter_t(0)}));
    EXPECT_THAT(opposite_lane.centerline[750],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(748.071), units::length::meter_t(22.194), units::length::meter_t(0)}));
    // s=500 (spiral -> lane)
    EXPECT_THAT(opposite_lane.centerline[1000],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(500), units::length::meter_t(1.5), units::length::meter_t(0)}));
    EXPECT_THAT(opposite_lane.centerline[1250],
                MantlePositionIsNear(MantlePosition{
                    units::length::meter_t(250), units::length::meter_t(1.5), units::length::meter_t(0)}));
    EXPECT_THAT(opposite_lane.centerline[1500],
                MantlePositionIsNear(
                    MantlePosition{units::length::meter_t(0), units::length::meter_t(1.5), units::length::meter_t(0)}));

    // Check boundary elements
    // note: All points of a lane boundary will always have the same width/height after conversion as a change in
    // width/height also triggers the creation of a separate lane boundary.
    EXPECT_EQ(left_lane.left_lane_boundaries[0].get().boundary_line.front().width.value(), 0.2);
    EXPECT_EQ(opposite_lane.left_lane_boundaries[0].get().boundary_line.front().width.value(), 0.2);
    EXPECT_EQ(left_lane.left_lane_boundaries[0].get().boundary_line.front().height.value(), 0.02);
    EXPECT_EQ(opposite_lane.left_lane_boundaries[0].get().boundary_line.front().height.value(), 0.02);
}
}  // namespace road_logic_suite::test