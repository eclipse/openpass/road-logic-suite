/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using ::testing::ElementsAre;
using units::literals::operator""_m;
/////////////////////////////////////////////////////////////////////////////////////////
/// @verbatim
///
/// length="1060"
/// curvature="0.003
///
/// origin = (140.0, 0.0)
///         s=1060
///         |___________________
///         |                   \
///         |____________        \
///         |            \        \
///         |_____        \        \
///         |     \        \        \
///         |      \        \        \
///         |       \        \        \
///         o   (lane-1)  (lane-2)  (lane-3)
///         |       /        /        /
/// y=0.0   |      /        /        /
/// y=-1.75 |_____/        /        /
///         |             /        /
/// y= -5.25|____________/        /
///         |                    /
/// y=-8.75 |___________________/
///         s=0
///
/// IDs:
/// |LaneGroup: 9                 |
/// |-----------------------------|
/// |Left LaneBoundary: 1         |
/// |Lane: 3                      |
/// |Right LaneBoundary: 6        |
/// |-----------------------------|
/// |Left LaneBoundary: 6         |
/// |Lane: 4                      |
/// |Right LaneBoundary:7         |
/// |-----------------------------|
/// |Left LaneBoundary: 7         |
/// |Lane: 5                      |
/// |Right LaneBoundary:8         |
/// |-----------------------------|
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenRoadWithArcGeometry_WhenParsingMap_ThenConvertedLaneAndLanBoundaryAreCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath("CurvedRoad1kmRightArcGeometry.xodr"));

    // Lane group
    const auto& lanes = map.lanes;
    ASSERT_EQ(lanes.size(), 3);

    const auto& lane_3 = lanes.at(0);
    const auto& lane_4 = lanes.at(1);
    const auto& lane_5 = lanes.at(2);
    const auto& lane_boundary_4 = lane_3->left_lane_boundaries[0];
    const auto& lane_boundary_6 = lane_3->right_lane_boundaries[0];
    const auto& lane_boundary_7 = lane_4->right_lane_boundaries[0];
    const auto& lane_boundary_8 = lane_5->right_lane_boundaries[0];

    // Lane  1
    EXPECT_EQ(lane_3->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_3->successor_lanes.size(), 0);
    EXPECT_THAT(lane_3->left_adjacent_lanes, ::testing::IsEmpty());
    EXPECT_THAT(lane_3->right_adjacent_lanes, ElementsAre(*lane_4));
    EXPECT_EQ(lane_3->type, Lane::Type::kDriving);
    EXPECT_THAT(lane_3->centerline.front(), MantlePositionIsNear(MantlePosition{140.0_m, -1.75_m, 0.0_m}));
    EXPECT_THAT(lane_3->centerline.back(), MantlePositionIsNear(MantlePosition{127.133502_m, 668.1695522_m, 0.0_m}));
    ASSERT_THAT(lane_3->left_lane_boundaries, ElementsAre(lane_boundary_4.get()));
    ASSERT_THAT(lane_3->right_lane_boundaries, ElementsAre(lane_boundary_6.get()));

    // Lane 2
    EXPECT_EQ(lane_4->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_4->successor_lanes.size(), 0);
    EXPECT_THAT(lane_4->left_adjacent_lanes, ElementsAre(*lane_3));
    EXPECT_THAT(lane_4->right_adjacent_lanes, ElementsAre(*lane_5));
    EXPECT_EQ(lane_4->type, Lane::Type::kDriving);
    EXPECT_THAT(lane_4->centerline.front(), MantlePositionIsNear(MantlePosition{140.0_m, -5.25_m, 0.0_m}));
    EXPECT_THAT(lane_4->centerline.back(), MantlePositionIsNear(MantlePosition{126.999109_m, 671.666971_m, 0.0_m}));
    ASSERT_THAT(lane_4->left_lane_boundaries, ElementsAre(lane_boundary_6.get()));
    ASSERT_THAT(lane_4->right_lane_boundaries, ElementsAre(lane_boundary_7.get()));

    // Lane 3
    EXPECT_EQ(lane_5->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_5->successor_lanes.size(), 0);
    EXPECT_THAT(lane_5->left_adjacent_lanes, ElementsAre(*lane_4));
    EXPECT_THAT(lane_5->right_adjacent_lanes, ElementsAre());
    EXPECT_EQ(lane_5->type, Lane::Type::kDriving);
    EXPECT_THAT(lane_5->centerline.front(), MantlePositionIsNear(MantlePosition{140.0_m, -8.75_m, 0.0_m}));
    EXPECT_THAT(lane_5->centerline.back(), MantlePositionIsNear(MantlePosition{126.864716_m, 675.1643899_m, 0.0_m}));
    ASSERT_THAT(lane_5->left_lane_boundaries, ElementsAre(lane_boundary_7.get()));
    ASSERT_THAT(lane_5->right_lane_boundaries, ElementsAre(lane_boundary_8.get()));

    // Lane boundary 0
    EXPECT_EQ(lane_boundary_4.get().type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(lane_boundary_4.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_4.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {140.0_m, 0.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_4.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {127.200698_m, 666.420842_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 4
    EXPECT_EQ(lane_boundary_6.get().type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(lane_boundary_6.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_6.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {140.0_m, -3.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_6.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {127.066305_m, 669.918261_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 5
    EXPECT_EQ(lane_boundary_7.get().type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(lane_boundary_7.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_7.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {140.0_m, -7.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_7.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {126.931913_m, 673.415680_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 6
    EXPECT_EQ(lane_boundary_8.get().type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(lane_boundary_8.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_8.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {140.0_m, -10.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_8.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {126.797520_m, 676.913099_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
}

}  // namespace road_logic_suite::test
