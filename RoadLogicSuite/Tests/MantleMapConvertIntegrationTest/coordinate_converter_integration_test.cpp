/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"
#include "RoadLogicSuite/Tests/Utils/assertions.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace road_logic_suite::test
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using ::testing::Optional;
using ::testing::Values;

using InertialPosition = mantle_api::Vec3<units::length::meter_t>;
using LanePosition = mantle_api::OpenDriveLanePosition;
using RoadPosition = mantle_api::OpenDriveRoadPosition;

TEST(CoordinateConverterTest,
     GivenMapWithInertialQueryOffRoad_WhenConvertInertialToLaneCoordinates_ThenConversionFailed)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));
    const auto inertial_position = InertialPosition(-5.0_m, 30.0_m, 0.0_m);

    // When
    const auto actual_lane_position = suite.ConvertInertialToLaneCoordinates(inertial_position);

    // Then
    EXPECT_THAT(actual_lane_position, testing::Eq(std::nullopt));
}

TEST(CoordinateConverterTest, GivenMapWithLaneQueryOffRoad_WhenConvertLaneToInertialCoordinates_ThenConversionFailed)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));
    const auto lane_postion = LanePosition{.road = "0", .lane = -1, .s_offset = 6100.0_m, .t_offset = 0.0_m};

    // When
    const auto actual_inertial_position = suite.ConvertLaneToInertialCoordinates(lane_postion);

    // Then
    EXPECT_THAT(actual_inertial_position, testing::Eq(std::nullopt));
}

class CoordinateConverterParametersTests
    : public ::testing::TestWithParam<std::tuple<std::string, InertialPosition, LanePosition>>
{
  protected:
    void SetUp(const std::string& test_case)
    {
        [[maybe_unused]] auto map = suite_.LoadMap(GetResolvedMapPath(test_case));
    }

    RoadLogicSuite suite_{};
};

TEST_P(CoordinateConverterParametersTests, GivenRoad_WhenConvertLaneToInertialCoordinates_ThenConversionIsCorrect)
{
    // Given
    const auto map_file = std::get<0>(GetParam());
    const auto expected_inertial_position = std::get<1>(GetParam());
    const auto expected_lane_postion = std::get<2>(GetParam());
    SetUp(map_file);

    // When
    const auto actual_inertial_position = suite_.ConvertLaneToInertialCoordinates(expected_lane_postion);

    // Then
    ASSERT_THAT(actual_inertial_position, testing::Ne(std::nullopt));
    EXPECT_THAT(actual_inertial_position, Optional(Vec3IsNear(expected_inertial_position)));
}

TEST_P(CoordinateConverterParametersTests,
       GivenMapWithLineGeometry_WhenConvertInertialToLaneCoordinates_ThenConversionIsCorrect)
{
    // Given
    const auto map_file = std::get<0>(GetParam());
    const auto expected_inertial_position = std::get<1>(GetParam());
    const auto expected_lane_position = std::get<2>(GetParam());
    SetUp(map_file);

    // When
    const auto actual_lane_position = suite_.ConvertInertialToLaneCoordinates(expected_inertial_position);

    // Then
    ASSERT_THAT(actual_lane_position, testing::Ne(std::nullopt));
    EXPECT_THAT(actual_lane_position.value(), (LanePositionIsNear(expected_lane_position)));
}

INSTANTIATE_TEST_CASE_P(
    CoordinateConverterTests,
    CoordinateConverterParametersTests,
    Values(
        // Line Geometry, lane -1, start position
        std::make_tuple("Highway6kmStraightRight.xodr",
                        InertialPosition(-5.0_m, -11.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 0.0_m, .t_offset = 0.0_m}),
        // Line Geometry, lane -1, end position
        std::make_tuple("Highway6kmStraightRight.xodr",
                        InertialPosition(5995.0_m, -11.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 6000.0_m, .t_offset = 0.0_m}),
        // Line Geometry, lane -2, with s and t offset
        std::make_tuple("Highway6kmStraightRight.xodr",
                        InertialPosition(4995.0_m, -16.25_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -2, .s_offset = 5000.0_m, .t_offset = -1.0_m}),
        // Line Geometry, lane -3, with s and t offset
        std::make_tuple("Highway6kmStraightRight.xodr",
                        InertialPosition(3995.0_m, -17.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -3, .s_offset = 4000.0_m, .t_offset = 1.0_m}),
        // Arc Geometry, lane -1, with t offset
        std::make_tuple("CurvedRoad1kmRightArcGeometry.xodr",
                        InertialPosition(140.0_m, -0.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 0.0_m, .t_offset = 1.0_m}),
        // Arc Geometry, lane -1, end position
        std::make_tuple("CurvedRoad1kmRightArcGeometry.xodr",
                        InertialPosition(127.13350216537071_m, 668.16955226917253_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 1060.0_m, .t_offset = 0.0_m}),
        // Arc geometry,lane -1 with t offset
        std::make_tuple("CurvedRoad1kmRightArcGeometry.xodr",
                        InertialPosition(127.0951042609_m, 669.1688147977_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 1060.0_m, .t_offset = -1.0_m}),
        // Arc Geometry, lane -2, start position
        std::make_tuple("CurvedRoad1kmRightArcGeometry.xodr",
                        InertialPosition(140.0_m, -5.25_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -2, .s_offset = 0.0_m, .t_offset = 0.0_m}),
        // Arc Geometry, lane -3, end position, with t offset
        std::make_tuple("CurvedRoad1kmRightArcGeometry.xodr",
                        InertialPosition(126.90311474_m, 674.16512744_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -3, .s_offset = 1060.0_m, .t_offset = 1.0_m}),
        // Spiral Geometry,lane -1, start position
        std::make_tuple("CurvedRoad1kmRightSpiralGeometry.xodr",
                        InertialPosition(-5.0_m, -11.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 0.0_m, .t_offset = 0.0_m}),
        std::make_tuple("CurvedRoad1kmRightSpiralGeometry.xodr",
                        InertialPosition(-5.0_m, -10.75_m, 0.0_m),
                        LanePosition{.road = "0", .lane = -1, .s_offset = 0.0_m, .t_offset = 1.0_m})
        /// @todo add more tests for Spiral geometry when it is fully supported
        ));

TEST(CoordinateConverterTest,
     GivenRoadWithParamPolynomialGeometry_WhenConvertLaneToInertialCoordinates_ThenConversionIsCorrect)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPathInternal("road_with_param_poly3_geometry.xodr"));
    const auto lane_position = LanePosition{.road = "0", .lane = -2, .s_offset = 25.0_m, .t_offset = 0.0_m};
    const auto expected_inertial_position = InertialPosition(24.9474_m, -5.374_m, 0.0_m);

    // When
    const auto actual_inertial_position = suite.ConvertLaneToInertialCoordinates(lane_position);

    // Then
    ASSERT_THAT(actual_inertial_position, testing::Ne(std::nullopt));
    EXPECT_THAT(actual_inertial_position, Optional(Vec3IsNear(expected_inertial_position)));
}

TEST(CoordinateConverterTest,
     GivenRoadWithParamPolynomialGeometry_WhenConvertInertialToLaneCoordinates_ThenConversionIsCorrect)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPathInternal("road_with_param_poly3_geometry.xodr"));
    const auto inertial_position = InertialPosition(24.9474_m, -5.374_m, 0.0_m);
    const auto expected_lane_position = LanePosition{.road = "0", .lane = -2, .s_offset = 25.0_m, .t_offset = 0.0_m};

    // When
    const auto actual_lane_position = suite.ConvertInertialToLaneCoordinates(inertial_position);

    // Then
    ASSERT_THAT(actual_lane_position, testing::Ne(std::nullopt));
    EXPECT_THAT(actual_lane_position.value(), (LanePositionIsNear(expected_lane_position)));
}

TEST(CoordinateConverterTest,
     GivenRoadWithParamPolynomialGeometryAndInvalidInput_WhenConvertInertialToLaneCoordinates_ThenConversionFails)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPathInternal("road_with_param_poly3_geometry.xodr"));
    const auto inertial_position = InertialPosition(24.9474_m, -15.374_m, 0.0_m);

    // When
    const auto actual_lane_position = suite.ConvertInertialToLaneCoordinates(inertial_position);

    // Then
    ASSERT_THAT(actual_lane_position, std::nullopt);
}

TEST(CoordinateConverterTest,
     GivenMapWithValidRoadPosition_WhenCallConvertRoadPositionToInertial_ThenConversionIsCorrect)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));
    const auto road_position = RoadPosition{.road = "0", .s_offset = 100.0_m, .t_offset = -5.0_m};

    // When
    const auto inertial_position = suite.ConvertRoadPositionToInertialCoordinates(road_position);

    // Then
    ASSERT_THAT(inertial_position, testing::Ne(std::nullopt));
    const auto expected_inertial_position = InertialPosition(95_m, -15_m, 0.0_m);
    EXPECT_THAT(inertial_position.value(), InertialPositionIsNear(expected_inertial_position));
}

TEST(CoordinateConverterTest, GivenMapWithValidRoadPosition_WhenCallGetRoadPositionHeading_ThenOrientationReturned)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));
    const auto road_position = RoadPosition{.road = "0", .s_offset = 100.0_m, .t_offset = 0.0_m};

    // When
    const auto road_heading = suite.GetRoadPositionHeading(road_position.road, road_position.s_offset);

    // Then
    EXPECT_THAT(road_heading.value(), 0_rad);
}

TEST(CoordinateConverterTest, GivenMapWithInvalidRoadPosition_WhenCallGetRoadPositionHeading_ThenNulloptReturned)
{
    // Given
    RoadLogicSuite suite{};
    [[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));
    const auto road_position = RoadPosition{.road = "12345", .s_offset = 100.0_m, .t_offset = 0.0_m};

    // When
    const auto road_heading = suite.GetRoadPositionHeading(road_position.road, road_position.s_offset);

    // Then
    EXPECT_THAT(road_heading, std::nullopt);
}

}  // namespace road_logic_suite::test
