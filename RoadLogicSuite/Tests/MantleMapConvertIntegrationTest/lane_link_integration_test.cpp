/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
TEST_F(MapConvertIntegrationTestFixture, GivenMapWithConnectedRoad_WhenParsingMap_ThenConversionOfLaneLinkIsCorrect)
{
    const auto expected_road0_lane_minus1_successors_size = 1;
    const auto expected_road1_lane_minus1_antecessors_size = 1;

    const auto& map = GetMap(GetResolvedMapPathInternal("two_roads.xodr"));
    const auto road0_lane_minus1 = GetLaneWithFrontAt(0.0, -1.5);
    const auto road1_lane_minus1 = GetLaneWithFrontAt(500.0, -1.5);

    ASSERT_EQ(road0_lane_minus1.successor_lanes.size(), expected_road0_lane_minus1_successors_size);
    EXPECT_THAT(road0_lane_minus1.successor_lanes, ::testing::ElementsAre(road1_lane_minus1));
    ASSERT_EQ(road1_lane_minus1.antecessor_lanes.size(), expected_road1_lane_minus1_antecessors_size);
    EXPECT_THAT(road1_lane_minus1.antecessor_lanes, ::testing::ElementsAre(road0_lane_minus1));
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithNonExistingPredecessor_WhenParsingMap_ThenPrintWarning)
{

    const auto expected_road1_lane_minus1_antecessors_size = 0;
    const auto expected_console_output = "Road predecessor with ID 0 not found!\n";
    ::testing::internal::CaptureStdout();

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_non_existing_predecessor.xodr"));
    const auto road1_lane_minus1 = GetLaneWithFrontAt(500.0, -1.5);

    const auto console_output = ::testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_console_output);
    ASSERT_EQ(road1_lane_minus1.successor_lanes.size(), expected_road1_lane_minus1_antecessors_size);
}
}  // namespace road_logic_suite::test