/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using units::literals::operator""_m;
using ::testing::ElementsAre;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///   |-------------------|---------------------| ref line
///   | x x (Lane -1) x x | x x (Lane -1) x x   | y: -1.75
///   |-------------------|---------------------|
///   | x x (Lane -2) x x | x x (Lane -2) x x   | y: -5.25
///   |-------------------|---------------------|
///   ^                   ^                   ^
/// s(0)                (300)               (2000)
///        section 0             section 1
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithLaneSectionsWithinTheRoad_WhenParsingMap_ThenConversionLaneSectionsIsCorrect)
{
    // Given
    const auto& map = GetMap(GetResolvedMapPath("Straight2LanesSections.xodr"));

    // When
    const auto& lanes = map.lanes;

    // Then
    // Lane -1 in section 0
    EXPECT_EQ(lanes[0]->antecessor_lanes.size(), 0);
    EXPECT_EQ(lanes[0]->successor_lanes.size(), 1);
    EXPECT_EQ(lanes[0]->left_adjacent_lanes.size(), 0);
    EXPECT_EQ(lanes[0]->right_adjacent_lanes.size(), 1);
    EXPECT_THAT(lanes[0]->right_adjacent_lanes, ElementsAre(*lanes[1]));
    EXPECT_EQ(lanes[0]->centerline.front().x, 0.0_m);
    EXPECT_EQ(lanes[0]->centerline.back().x, 300.0_m);

    ASSERT_EQ(lanes[0]->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lanes[0]->right_lane_boundaries.size(), 1);

    // Lane -2 in section 0
    EXPECT_EQ(lanes[1]->antecessor_lanes.size(), 0);
    EXPECT_EQ(lanes[1]->successor_lanes.size(), 1);
    EXPECT_EQ(lanes[1]->left_adjacent_lanes.size(), 1);
    EXPECT_EQ(lanes[1]->right_adjacent_lanes.size(), 0);
    EXPECT_THAT(lanes[1]->left_adjacent_lanes, ElementsAre(*lanes[0]));
    EXPECT_EQ(lanes[1]->centerline.front().x, 0.0_m);
    EXPECT_EQ(lanes[1]->centerline.back().x, 300.0_m);

    ASSERT_EQ(lanes[1]->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lanes[1]->right_lane_boundaries.size(), 1);

    // Lane -1 in section 1
    EXPECT_EQ(lanes[2]->antecessor_lanes.size(), 1);
    EXPECT_EQ(lanes[2]->successor_lanes.size(), 0);
    EXPECT_EQ(lanes[2]->left_adjacent_lanes.size(), 0);
    EXPECT_EQ(lanes[2]->right_adjacent_lanes.size(), 1);
    EXPECT_THAT(lanes[2]->right_adjacent_lanes, ElementsAre(*lanes[3]));
    EXPECT_EQ(lanes[2]->centerline.front().x, 300.0_m);
    EXPECT_EQ(lanes[2]->centerline.back().x, 2000.0_m);

    ASSERT_EQ(lanes[2]->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lanes[2]->right_lane_boundaries.size(), 1);

    // Lane -2 in section 1
    EXPECT_EQ(lanes[3]->antecessor_lanes.size(), 1);
    EXPECT_EQ(lanes[3]->successor_lanes.size(), 0);
    EXPECT_EQ(lanes[3]->left_adjacent_lanes.size(), 1);
    EXPECT_EQ(lanes[3]->right_adjacent_lanes.size(), 0);
    EXPECT_THAT(lanes[3]->left_adjacent_lanes, ElementsAre(*lanes[2]));
    EXPECT_EQ(lanes[3]->centerline.front().x, 300.0_m);
    EXPECT_EQ(lanes[3]->centerline.back().x, 2000.0_m);

    ASSERT_EQ(lanes[3]->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lanes[3]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[0]->right_lane_boundaries[0].get().id, lanes[1]->left_lane_boundaries[0].get().id);
    EXPECT_EQ(lanes[2]->right_lane_boundaries[0].get().id, lanes[3]->left_lane_boundaries[0].get().id);
}

}  // namespace road_logic_suite::test
