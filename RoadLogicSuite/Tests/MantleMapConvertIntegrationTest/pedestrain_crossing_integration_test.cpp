/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithCrossPaths_WhenParsingMap_ThenConversionOfCrossPathsIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("cross_path_example.xodr"));
    const auto expected_cross_path_with_road_id_77_type = map_api::Lane::Type::kIntersection;
    const auto expected_cross_path_with_road_id_77_subtype = map_api::Lane::Subtype::kUnknown;

    // cross path lane
    const auto cross_path_with_road_id_77 = GetLaneWithFrontAt(106.637, 44.2235);
    EXPECT_THAT(cross_path_with_road_id_77.local_id, -1);
    EXPECT_THAT(cross_path_with_road_id_77.type, expected_cross_path_with_road_id_77_type);
    EXPECT_THAT(cross_path_with_road_id_77.sub_type, expected_cross_path_with_road_id_77_subtype);
    EXPECT_THAT(cross_path_with_road_id_77.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(cross_path_with_road_id_77.successor_lanes, ::testing::IsEmpty());
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithCrossing_WhenParsingMap_ThenConversionOfCrossingIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("crossing_example.xodr"));
    const auto expected_crossing_lane_type = map_api::Lane::Type::kUnknown;
    const auto expected_crossing_lane_subtype = map_api::Lane::Subtype::kUnknown;

    // cross path lane
    const auto crossing_lane = GetLaneWithFrontAt(51.5, -5.0);
    EXPECT_THAT(crossing_lane.local_id, -1);
    EXPECT_THAT(crossing_lane.type, expected_crossing_lane_type);
    EXPECT_THAT(crossing_lane.sub_type, expected_crossing_lane_subtype);
    EXPECT_THAT(crossing_lane.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(crossing_lane.successor_lanes, ::testing::IsEmpty());
}

}  // namespace road_logic_suite::test