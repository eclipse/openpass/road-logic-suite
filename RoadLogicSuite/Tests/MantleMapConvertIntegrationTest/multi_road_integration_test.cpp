/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using ::testing::ElementsAre;
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///          Road "0"            Road "1"
///   |-------------------|-------------------|
///   | x x (Lane  2) x x | x x (Lane  2) x x | y:  4.8
///   |-------------------|-------------------|
///   | x x (Lane  1) x x | x x (Lane  1) x x | y:  1.6
///   |===================|===================| ref line (Lane 0) direction ->
///   | x x (Lane -1) x x | x x (Lane -1) x x | y: -1.6
///   |-------------------|-------------------|
///   | x x (Lane -2) x x | x x (Lane -2) x x | y: -4.8
///   |-------------------|-------------------|
///   ^                   ^                   ^
/// s(0)                (3750)               (7500)
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithMultipleRoads_WhenParsingMap_ThenConversionOfRoadsIsCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath("3750mStraightMultipleRoads.xodr"));

    // basic amounts checking
    ASSERT_EQ(map.lanes.size(), 8);

    // Check predecessors and successors
    const auto road_0_lane_minus_1 = GetLaneWithFrontAt(0, -1.6);
    const auto road_1_lane_minus_1 = GetLaneWithFrontAt(3750, -1.6);
    ASSERT_THAT(road_0_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_0_lane_minus_1.successor_lanes, ::testing::ElementsAre(road_1_lane_minus_1));
    ASSERT_THAT(road_1_lane_minus_1.antecessor_lanes, ::testing::ElementsAre(road_0_lane_minus_1));
    ASSERT_THAT(road_1_lane_minus_1.successor_lanes, ::testing::IsEmpty());

    const auto road_0_lane_minus_2 = GetLaneWithFrontAt(0, -4.8);
    const auto road_1_lane_minus_2 = GetLaneWithFrontAt(3750, -4.8);
    ASSERT_THAT(road_0_lane_minus_2.antecessor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_0_lane_minus_2.successor_lanes, ::testing::ElementsAre(road_1_lane_minus_2));
    ASSERT_THAT(road_1_lane_minus_2.antecessor_lanes, ::testing::ElementsAre(road_0_lane_minus_2));
    ASSERT_THAT(road_1_lane_minus_2.successor_lanes, ::testing::IsEmpty());

    const auto road_0_lane_1 = GetLaneWithFrontAt(3750, 1.6);
    const auto road_1_lane_1 = GetLaneWithFrontAt(7500, 1.6);
    ASSERT_THAT(road_0_lane_1.antecessor_lanes, ::testing::ElementsAre(road_1_lane_1));
    ASSERT_THAT(road_0_lane_1.successor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_1_lane_1.antecessor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_1_lane_1.successor_lanes, ::testing::ElementsAre(road_0_lane_1));

    const auto road_0_lane_2 = GetLaneWithFrontAt(3750, 4.8);
    const auto road_1_lane_2 = GetLaneWithFrontAt(7500, 4.8);
    ASSERT_THAT(road_0_lane_2.antecessor_lanes, ::testing::ElementsAre(road_1_lane_2));
    ASSERT_THAT(road_0_lane_2.successor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_1_lane_2.antecessor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road_1_lane_2.successor_lanes, ::testing::ElementsAre(road_0_lane_2));

    // Check directly adjacent lanes
    ASSERT_THAT(road_0_lane_minus_1.left_adjacent_lanes, ::testing::ElementsAre(road_0_lane_1));
    ASSERT_THAT(road_0_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_0_lane_minus_2));

    ASSERT_THAT(road_0_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_0_lane_minus_1));
    ASSERT_THAT(road_0_lane_minus_2.right_adjacent_lanes, ::testing::IsEmpty());

    ASSERT_THAT(road_1_lane_minus_1.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_1));
    ASSERT_THAT(road_1_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_2));

    ASSERT_THAT(road_1_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_1));
    ASSERT_THAT(road_1_lane_minus_2.right_adjacent_lanes, ::testing::IsEmpty());

    ASSERT_THAT(road_0_lane_1.left_adjacent_lanes, ::testing::ElementsAre(road_0_lane_minus_1));
    ASSERT_THAT(road_0_lane_1.right_adjacent_lanes, ::testing::ElementsAre(road_0_lane_2));

    ASSERT_THAT(road_0_lane_2.left_adjacent_lanes, ::testing::ElementsAre(road_0_lane_1));
    ASSERT_THAT(road_0_lane_2.right_adjacent_lanes, ::testing::IsEmpty());

    ASSERT_THAT(road_1_lane_1.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_1));
    ASSERT_THAT(road_1_lane_1.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_2));

    ASSERT_THAT(road_1_lane_2.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_1));
    ASSERT_THAT(road_1_lane_2.right_adjacent_lanes, ::testing::IsEmpty());
}

}  // namespace road_logic_suite::test