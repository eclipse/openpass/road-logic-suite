/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

constexpr double kAbsError = 1e-6;

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithSingleObject_WhenParsingMap_ThenConversionOfObjectIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_single_object.xodr"));
    const auto& lanes = map.lanes;

    ASSERT_EQ(lanes.size(), 4);
    const auto& lane_minus_1 = GetLaneWithFrontAt(0, -1.5);
    const auto& lane_minus_2 = GetLaneWithFrontAt(0, -4.5);

    const auto& objects = map.stationary_objects;
    ASSERT_EQ(objects.size(), 1);
    const auto& object = objects[0];

    const auto expected_object_position_x = 30;
    const auto expected_object_position_y = -6.0;
    const auto expected_object_position_z = 6.0;

    const auto expected_object_rotation_roll = 0.0;
    const auto expected_object_rotation_pitch = 0.0;
    const auto expected_object_rotation_yaw = 1.57;
    const auto expected_object_length = 12.10;
    const auto expected_object_width = 22.40;
    const auto expected_object_height = 11.00;

    const auto expected_object_type = map_api::StationaryObject::Type::kBuilding;

    const auto& object_base = object->base;

    EXPECT_NEAR(object_base.position.x(), expected_object_position_x, kAbsError);
    EXPECT_NEAR(object_base.position.y(), expected_object_position_y, kAbsError);
    EXPECT_NEAR(object_base.position.z(), expected_object_position_z, kAbsError);

    EXPECT_NEAR(object_base.orientation.yaw(), expected_object_rotation_yaw, kAbsError);
    EXPECT_NEAR(object_base.orientation.pitch(), expected_object_rotation_pitch, kAbsError);
    EXPECT_NEAR(object_base.orientation.roll(), expected_object_rotation_roll, kAbsError);

    ASSERT_EQ(object_base.base_polygon.size(), 4);

    ASSERT_NEAR(object_base.dimension.length(), expected_object_length, kAbsError);
    ASSERT_NEAR(object_base.dimension.width(), expected_object_width, kAbsError);
    ASSERT_NEAR(object_base.dimension.height(), expected_object_height, kAbsError);

    EXPECT_EQ(object->type, expected_object_type);
    ASSERT_EQ(object->assigned_lanes.size(), 2);
    EXPECT_THAT(object->assigned_lanes, ::testing::ElementsAre(lane_minus_2, lane_minus_1));
}

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithMultipleObjects_WhenParsingMap_ThenNumberOfConvertedObjectsIsCorrect)
{
    const auto& map = GetMap(GetResolvedMapPathInternal("simple_road_with_objects.xodr"));
    const auto expected_objects_num = 41;

    const auto& objects = map.stationary_objects;
    ASSERT_EQ(objects.size(), expected_objects_num);
}

}  // namespace road_logic_suite::test
