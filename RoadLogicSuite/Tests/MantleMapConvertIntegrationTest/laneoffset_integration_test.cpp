/*******************************************************************************
 * Copyright (C) 2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using namespace units::literals;
static const double kAbsErrorMax = 1e-6;

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithLaneOffset_WhenParsingMap_ThenLanesAreConvertedCorrectly)
{

    const std::string test_map = GetResolvedMapPathInternal("road_with_laneoffset.xodr");
    const auto& map = GetMap(test_map, 1.0, 0.01, false);

    ASSERT_EQ(map.lanes.size(), 3);
    ASSERT_EQ(map.logical_lanes.size(), 3);
    const auto& lane_1 = GetLaneWithFrontAt(0.0, -1.6);
    const auto& lane_2 = GetLaneWithFrontAt(100.0, -1.6);
    const auto& lane_3 = GetLaneWithFrontAt(150.0, -1.5);

    ASSERT_EQ(lane_1.right_lane_boundaries.size(), 1);
    const auto& lane_boundary_1 = lane_1.right_lane_boundaries[0];
    const auto& centerline_1 = lane_1.centerline;
    const auto& logical_lane_1 = map.logical_lanes[2];
    ASSERT_EQ(lane_2.right_lane_boundaries.size(), 1);
    const auto& lane_boundary_2 = lane_2.right_lane_boundaries[0];
    const auto& centerline_2 = lane_2.centerline;
    const auto& logical_lane_2 = map.logical_lanes[0];
    ASSERT_EQ(lane_3.right_lane_boundaries.size(), 1);
    const auto& lane_boundary_3 = lane_3.right_lane_boundaries[0];
    const auto& centerline_3 = lane_3.centerline;
    const auto& logical_lane_3 = map.logical_lanes[1];

    // lane_boundary_1
    EXPECT_THAT(lane_boundary_1.get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{0.0_m, -3.2_m, 0.0_m}));
    EXPECT_THAT(lane_boundary_1.get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{100.0_m, -3.2_m, 0.0_m}));

    // centerline 1
    EXPECT_THAT(centerline_1.front(), MantlePositionIsNear(MantlePosition{0.0_m, -1.6_m, 0.0_m}));
    EXPECT_THAT(centerline_1.back(), MantlePositionIsNear(MantlePosition{100.0_m, -1.6_m, 0.0_m}));

    // logical lane 1
    ASSERT_EQ(logical_lane_1->right_boundaries.size(), 1);
    EXPECT_THAT(logical_lane_1->right_boundaries[0].get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{0.0_m, -3.2_m, 0.0_m}));
    EXPECT_THAT(logical_lane_1->right_boundaries[0].get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{100.0_m, -3.2_m, 0.0_m}));
    EXPECT_NEAR_UNITS(logical_lane_1->right_boundaries[0].get().boundary_line.front().s_position, 0.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_1->right_boundaries[0].get().boundary_line.front().t_position, -3.2_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_1->right_boundaries[0].get().boundary_line.back().s_position, 100.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_1->right_boundaries[0].get().boundary_line.back().t_position, -3.2_m, kAbsErrorMax);

    // lane_boundary_2
    EXPECT_THAT(lane_boundary_2.get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{100.0_m, -3.2_m, 0.0_m}));
    EXPECT_THAT(lane_boundary_2.get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{150.0_m, -3.1_m, 0.0_m}));

    // centerline 2
    EXPECT_THAT(centerline_2.front(), MantlePositionIsNear(MantlePosition{100.0_m, -1.6_m, 0.0_m}));
    EXPECT_THAT(centerline_2.back(), MantlePositionIsNear(MantlePosition{150.0_m, -1.5_m, 0.0_m}));

    // logical lane 2
    ASSERT_EQ(logical_lane_2->right_boundaries.size(), 1);
    EXPECT_THAT(logical_lane_2->right_boundaries[0].get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{100.0_m, -3.2_m, 0.0_m}));
    EXPECT_THAT(logical_lane_2->right_boundaries[0].get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{150.0_m, -3.1_m, 0.0_m}));
    EXPECT_NEAR_UNITS(logical_lane_2->right_boundaries[0].get().boundary_line.front().s_position, 0.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_2->right_boundaries[0].get().boundary_line.front().t_position, -3.2_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_2->right_boundaries[0].get().boundary_line.back().s_position, 50.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_2->right_boundaries[0].get().boundary_line.back().t_position, -3.1_m, kAbsErrorMax);

    // lane_boundary_3
    EXPECT_THAT(lane_boundary_3.get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{150.0_m, -3.1_m, 0.0_m}));
    EXPECT_THAT(lane_boundary_3.get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{200.0_m, 0.15_m, 0.0_m}));

    // centerline 3
    EXPECT_THAT(centerline_3.front(), MantlePositionIsNear(MantlePosition{150.0_m, -1.5_m, 0.0_m}));
    EXPECT_THAT(centerline_3.back(), MantlePositionIsNear(MantlePosition{200.0_m, 1.75_m, 0.0_m}));

    // logical lane 3
    ASSERT_EQ(logical_lane_3->right_boundaries.size(), 1);
    EXPECT_THAT(logical_lane_3->right_boundaries[0].get().boundary_line.front().position,
                MantlePositionIsNear(MantlePosition{150.0_m, -3.1_m, 0.0_m}));
    EXPECT_THAT(logical_lane_3->right_boundaries[0].get().boundary_line.back().position,
                MantlePositionIsNear(MantlePosition{200.0_m, 0.15_m, 0.0_m}));
    EXPECT_NEAR_UNITS(logical_lane_3->right_boundaries[0].get().boundary_line.front().s_position, 50.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_3->right_boundaries[0].get().boundary_line.front().t_position, -3.1_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_3->right_boundaries[0].get().boundary_line.back().s_position, 100.0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(logical_lane_3->right_boundaries[0].get().boundary_line.back().t_position, 0.15_m, kAbsErrorMax);
}

}  // namespace road_logic_suite::test
