/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

TEST(MapLoaderTest, GivenInvalidMap_WhenLoadMap_ThenThrow)
{
    // Given
    const auto invalid_map_file = "invalid_map.xodr";

    // When
    RoadLogicSuite suite{};

    // Then
    EXPECT_THROW([[maybe_unused]] auto map = suite.LoadMap(GetResolvedMapPath(invalid_map_file)), std::runtime_error);
}

}  // namespace road_logic_suite::test
