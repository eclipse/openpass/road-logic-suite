/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (103.0, 120.0)
///
///                                          Road "2"
///                                         /      /
///          Road "0"       Road "1"       /      / (Lane -1)
///   |<=================|===============>\      /
///   | x x (Lane 1) x x | (Lane -1)       \    /
///   |------------------|------------------\---
///   | x x (Lane 2) x x | (Lane -2)    \    \
///   |------------------|-----------    \    \
///   | x x (Lane 3) x x | (Lane -3) \    \    \
///   |------------------|--------    \    \    \
///                               \    \    \    \
///                                \    \    \    \
///   ^                  ^
///  s(103)             s(0)
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithJunction_WhenParsingMap_ThenConversionOfJunctionRoadsIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("junction_example.xodr"), 1.0, 0.01, false);

    const auto& lanes = map.lanes;

    // calculation of expected number of points of road2 lane -1: 29.9(s value) / 1.0(default sampling_distance) +
    // 2(start point + end point) = 31
    const auto& expected_num_road2_first_lane_points = 31;
    const auto& expected_num_road2_first_lane_left_boundary_points = 31;
    const auto& expected_num_road2_first_lane_right_boundary_points = 31;
    const auto& expected_num_road1_second_lane_points = 30;
    const auto& expected_num_road1_second_lane_left_boundary_points = 30;
    const auto& expected_num_road1_second_lane_right_boundary_points = 30;
    const auto& expected_num_road0_second_lane_points = 104;
    const auto& expected_num_road0_second_lane_left_boundary_points = 104;
    const auto& expected_num_road0_second_lane_right_boundary_points = 104;

    ASSERT_EQ(lanes.size(), 7);

    // road 2
    EXPECT_EQ(lanes[0]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[0]->right_lane_boundaries.size(), 1);

    const auto& road2_first_lane_left_boundary = lanes[0]->left_lane_boundaries[0].get();
    const auto& road2_first_lane_right_boundary = lanes[0]->right_lane_boundaries[0].get();

    EXPECT_EQ(road2_first_lane_left_boundary.type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(road2_first_lane_right_boundary.type, LaneBoundary::Type::kNoLine);

    EXPECT_THAT(road2_first_lane_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 120.0_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road2_first_lane_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {120.0_m, 137.0_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road2_first_lane_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road2_first_lane_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {123.75_m, 137.0_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));

    const auto road2_first_lane = GetLaneWithFrontAt(103.0, 118.125);
    const auto road0_first_lane = GetLaneWithFrontAt(0.0, 118.125);
    ASSERT_THAT(road2_first_lane.antecessor_lanes, ::testing::ElementsAre(road0_first_lane));
    ASSERT_THAT(road2_first_lane.successor_lanes, ::testing::IsEmpty());

    EXPECT_EQ(road2_first_lane.centerline.size(), expected_num_road2_first_lane_points);
    EXPECT_EQ(road2_first_lane_left_boundary.boundary_line.size(), expected_num_road2_first_lane_left_boundary_points);
    EXPECT_EQ(road2_first_lane_right_boundary.boundary_line.size(),
              expected_num_road2_first_lane_right_boundary_points);

    // road 1
    EXPECT_EQ(lanes[1]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[1]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[2]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[2]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[3]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[3]->right_lane_boundaries.size(), 1);

    const auto& road1_second_lane_left_boundary = lanes[2]->left_lane_boundaries[0].get();
    const auto& road1_second_lane_right_boundary = lanes[2]->right_lane_boundaries[0].get();

    EXPECT_EQ(road1_second_lane_left_boundary.type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(road1_second_lane_right_boundary.type, LaneBoundary::Type::kNoLine);

    EXPECT_THAT(road1_second_lane_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road1_second_lane_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {116.25_m, 103.0_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road1_second_lane_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 115.9_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road1_second_lane_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {115.9_m, 103.0_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));

    const auto road1_second_lane = GetLaneWithFrontAt(103.0, 116.075);
    const auto road0_second_lane = GetLaneWithFrontAt(0.0, 116.075);
    ASSERT_THAT(road1_second_lane.antecessor_lanes, ::testing::ElementsAre(road0_second_lane));
    ASSERT_THAT(road1_second_lane.successor_lanes, ::testing::IsEmpty());

    EXPECT_EQ(road1_second_lane.centerline.size(), expected_num_road1_second_lane_points);
    EXPECT_EQ(road1_second_lane_left_boundary.boundary_line.size(),
              expected_num_road1_second_lane_left_boundary_points);
    EXPECT_EQ(road1_second_lane_right_boundary.boundary_line.size(),
              expected_num_road1_second_lane_right_boundary_points);

    // road 0
    EXPECT_EQ(lanes[4]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[4]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[5]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[5]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[6]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[6]->right_lane_boundaries.size(), 1);

    const auto& road0_second_lane_left_boundary = lanes[5]->left_lane_boundaries[0].get();
    const auto& road0_second_lane_right_boundary = lanes[5]->right_lane_boundaries[0].get();

    EXPECT_EQ(road0_second_lane_left_boundary.type, LaneBoundary::Type::kNoLine);
    EXPECT_EQ(road0_second_lane_right_boundary.type, LaneBoundary::Type::kNoLine);

    EXPECT_THAT(road0_second_lane_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {0.0_m, 116.25_m, 0.0_m}, .width = 0.12_m, .height = 0.02_m}));
    EXPECT_THAT(road0_second_lane_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .width = 0.12_m, .height = 0.02_m}));
    EXPECT_THAT(road0_second_lane_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {0.0_m, 115.9_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));
    EXPECT_THAT(road0_second_lane_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {103.0_m, 115.9_m, 0.0_m}, .width = 0.0_m, .height = 0.0_m}));

    ASSERT_THAT(road0_second_lane.antecessor_lanes, ::testing::IsEmpty());
    ASSERT_THAT(road0_second_lane.successor_lanes, ::testing::ElementsAre(road1_second_lane));
    const auto road1_first_lane = *lanes[1];
    ASSERT_THAT(road0_first_lane.successor_lanes, ::testing::ElementsAre(road1_first_lane, road2_first_lane));

    EXPECT_EQ(road0_second_lane.centerline.size(), expected_num_road0_second_lane_points);
    EXPECT_EQ(road0_second_lane_left_boundary.boundary_line.size(),
              expected_num_road0_second_lane_left_boundary_points);
    EXPECT_EQ(road0_second_lane_right_boundary.boundary_line.size(),
              expected_num_road0_second_lane_right_boundary_points);
}

/// @verbatim
///
/// OpenDrive Map
/// Parse the given road correctly which has exit lanes on either side of straight road
/// Expectation :
///  1. Lane relationship which includes successor, adjacent lanes are parsed correctly
///  2. Here all the lane links are defined explicitly, no warnings expected from test
///                                      /         /
///                                     /         /
///                                    /         /
///                                   /R 2 : L-1/
///                                  /         /
///                                 /         /
/// -------------------------------/         /
/// R 1 : L -1                    |         /
/// --------------------------------------------------------------------
/// R 1 : L -2                    |           R 3:L -1
/// --------------------------------------------------------------------
/// R 1 : L -3                    |           R 3:L -2
/// --------------------------------------------------------------------
/// R 1 : L -4                    |         \
/// --------------------------------         \
///                                 \         \     
///                                  \         \     
///                                   \         \     
///                                    \         \     
///                                     \R 4:L -1 \     
///                                      \         \     
///                                       \         \     
///                                        \         \     
/// @endverbatim
TEST_F(MapConvertIntegrationTestFixture, GivenMapWithYJunction_WhenMapIsParsed_ThenAllLanesOfRoadAreProperlyConnected)
{
    const auto& map =
        GetMap(GetResolvedMapPathInternal("y_exit_either_sides_via_junction_and_lane_links.xodr"), 1.0, 0.01, false);
    // validate number of lanes to be 8
    ASSERT_EQ(map.lanes.size(), 8);
    const char* expected_projection_str = "";
    EXPECT_STREQ(map.projection_string.c_str(), expected_projection_str);
    // number of reference lines == no of roads
    ASSERT_EQ(map.reference_lines.size(), 4);

    // checking now if each of the lanes in the scenario are exactly parsed and related correctly

    // get the lanes first from known road geometry
    const auto road_1_lane_minus_1 = GetLaneWithFrontAt(0, -1.75);
    const auto road_1_lane_minus_2 = GetLaneWithFrontAt(0, -5.25);
    const auto road_1_lane_minus_3 = GetLaneWithFrontAt(0, -8.75);
    const auto road_1_lane_minus_4 = GetLaneWithFrontAt(0, -12.25);

    const auto road_2_lane_minus_1 = GetLaneWithFrontAt(100, -1.75);

    const auto road_3_lane_minus_1 = GetLaneWithFrontAt(100, -5.25);
    const auto road_3_lane_minus_2 = GetLaneWithFrontAt(100, -8.75);

    const auto road_4_lane_minus_1 = GetLaneWithFrontAt(100, -12.25);

    EXPECT_THAT(road_1_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_1.successor_lanes, ::testing::ElementsAre(road_2_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_1_lane_minus_1.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_2));

    EXPECT_THAT(road_1_lane_minus_2.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_2.successor_lanes, ::testing::ElementsAre(road_3_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_2.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_2.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_2.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_3));

    EXPECT_THAT(road_1_lane_minus_3.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_3.successor_lanes, ::testing::ElementsAre(road_3_lane_minus_2));
    EXPECT_EQ(road_1_lane_minus_3.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_3.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_2));
    EXPECT_EQ(road_1_lane_minus_3.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_3.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_4));

    EXPECT_THAT(road_1_lane_minus_4.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_4.successor_lanes, ::testing::ElementsAre(road_4_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_4.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_4.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_3));
    EXPECT_EQ(road_1_lane_minus_4.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_2_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_2_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_2_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_2_lane_minus_1.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_3_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_3_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_3_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_3_lane_minus_1.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_3_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_3_lane_minus_2));

    EXPECT_THAT(road_3_lane_minus_2.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_3_lane_minus_2.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_3_lane_minus_2.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_3_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_3_lane_minus_1));
    EXPECT_EQ(road_3_lane_minus_2.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_4_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_4_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_4_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_4_lane_minus_1.right_adjacent_lanes.size(), 0);
}

/// @verbatim
///
/// OpenDrive Map
/// Parse the given road correctly which has exit lanes on either side of straight road
/// Expectation :
///  1. Lane relationship which includes successor, adjacent lanes are parsed correctly
///  2. If incoming road is having more lanes and one of the lanes is connected to single lane road of connected road
///     and if lane links are missing within junction specification, it should still be able to parse the lane
///     relationship eg :- R1 : L-1 connecting to R2 : L-1 Warnings should be issued during this case
///                                      /         /
///                                     /         /
///                                    /         /
///                                   /R 2 : L-1/
///                                  /         /
///                                 /         /
/// -------------------------------/         /
/// R 1 : L -1                    |         /
/// --------------------------------------------------------------------
/// R 1 : L -2                    |           R 3:L -1
/// --------------------------------------------------------------------
/// R 1 : L -3                    |           R 3:L -2
/// --------------------------------------------------------------------
/// R 1 : L -4                    |         \
/// --------------------------------         \
///                                 \         \ 
///                                  \         \   
///                                   \         \     
///                                    \         \     
///                                     \R 4:L -1 \     
///                                      \         \     
///                                       \         \     
///                                        \         \    
/// @endverbatim
TEST_F(
    MapConvertIntegrationTestFixture,
    DISABLED_GivenMapWithYJunction_WhenMapIsParsed_ThenAllLanesOfRoadAreProperlyConnectedLaneLinkMissingWarningsIssued)
{
    ::testing::internal::CaptureStdout();
    const auto& map =
        GetMap(GetResolvedMapPathInternal("y_exit_either_sides_via_junction_no_lane_links_for_one_laners.xodr"),
               1.0,
               0.01,
               false);
    auto expected_warning = ::testing::internal::GetCapturedStdout();
    /* Once the map is parsed warnings are expected as the obvious lane links for one on one connection are not
     * explicitly specified in the xodr, check point 2 */
    EXPECT_EQ(expected_warning,
              "WARNING: Lane links for one on one lane connections are missing in junction specified!\n");
    ASSERT_EQ(map.lanes.size(), 8);

    const auto road_1_lane_minus_1 = GetLaneWithFrontAt(0, -1.75);
    const auto road_1_lane_minus_2 = GetLaneWithFrontAt(0, -5.25);
    const auto road_1_lane_minus_3 = GetLaneWithFrontAt(0, -8.75);
    const auto road_1_lane_minus_4 = GetLaneWithFrontAt(0, -12.25);

    const auto road_2_lane_minus_1 = GetLaneWithFrontAt(100, -1.75);

    const auto road_3_lane_minus_1 = GetLaneWithFrontAt(100, -5.25);
    const auto road_3_lane_minus_2 = GetLaneWithFrontAt(100, -8.75);

    const auto road_4_lane_minus_1 = GetLaneWithFrontAt(100, -12.25);

    EXPECT_THAT(road_1_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_1.successor_lanes, ::testing::ElementsAre(road_2_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_1_lane_minus_1.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_2));

    EXPECT_THAT(road_1_lane_minus_2.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_2.successor_lanes, ::testing::ElementsAre(road_3_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_2.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_2.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_2.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_3));

    EXPECT_THAT(road_1_lane_minus_3.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_3.successor_lanes, ::testing::ElementsAre(road_3_lane_minus_2));
    EXPECT_EQ(road_1_lane_minus_3.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_3.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_2));
    EXPECT_EQ(road_1_lane_minus_3.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_3.right_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_4));

    EXPECT_THAT(road_1_lane_minus_4.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_1_lane_minus_4.successor_lanes, ::testing::ElementsAre(road_4_lane_minus_1));
    EXPECT_EQ(road_1_lane_minus_4.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_1_lane_minus_4.left_adjacent_lanes, ::testing::ElementsAre(road_1_lane_minus_3));
    EXPECT_EQ(road_1_lane_minus_4.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_2_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_2_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_2_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_2_lane_minus_1.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_3_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_3_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_3_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_3_lane_minus_1.right_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_3_lane_minus_1.right_adjacent_lanes, ::testing::ElementsAre(road_3_lane_minus_2));

    EXPECT_THAT(road_3_lane_minus_2.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_3_lane_minus_2.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_3_lane_minus_2.left_adjacent_lanes.size(), 1);
    EXPECT_THAT(road_3_lane_minus_2.left_adjacent_lanes, ::testing::ElementsAre(road_3_lane_minus_1));
    EXPECT_EQ(road_3_lane_minus_2.right_adjacent_lanes.size(), 0);

    EXPECT_THAT(road_4_lane_minus_1.antecessor_lanes, ::testing::IsEmpty());
    EXPECT_THAT(road_4_lane_minus_1.successor_lanes, ::testing::IsEmpty());
    EXPECT_EQ(road_4_lane_minus_1.left_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_4_lane_minus_1.right_adjacent_lanes.size(), 0);
}

/// @verbatim
///
/// OpenDrive Map
/// Parse the given road correctly which has exit lanes on either side of straight road
/// Expectation :
///  Junction specification is missing and will throw a warning.
///                                      /         /
///                                     /         /
///                                    /         /
///                                   /R 2 : L-1/
///                                  /         /
///                                 /         /
/// -------------------------------/         /
/// R 1 : L -1                    |         /
/// --------------------------------------------------------------------
/// R 1 : L -2                    |           R 3:L -1
/// --------------------------------------------------------------------
/// R 1 : L -3                    |           R 3:L -2
/// --------------------------------------------------------------------
/// R 1 : L -4                    |         \
/// --------------------------------         \
///                                 \         \     
///                                  \         \     
///                                   \         \     
///                                    \         \     
///                                     \R 4:L -1 \     
///                                      \         \     
///                                       \         \     
///                                        \         \    
/// @endverbatim
TEST_F(MapConvertIntegrationTestFixture, GivenMapWithYJunctionWithoutLaneLinkElements_WhenMapIsParsed_ThenOutputWarning)
{

    ::testing::internal::CaptureStdout();
    const auto& map = GetMap(GetResolvedMapPathInternal("y_exit_either_sides_via_junction_no_lane_links.xodr"));
    const std::string expected_warning_message =
        "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID 0 and connection ID 0 has no laneLink "
        "elements. "
        "Lane relations (successor/predecessor) cannot be deduced. Please check the map file!\n"
        "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID 0 and connection ID 1 has no laneLink "
        "elements. "
        "Lane relations (successor/predecessor) cannot be deduced. Please check the map file!\n"
        "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID 0 and connection ID 2 has no laneLink "
        "elements. "
        "Lane relations (successor/predecessor) cannot be deduced. Please check the map file!\n";
    const std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expected_warning_message);
}

}  // namespace road_logic_suite::test
