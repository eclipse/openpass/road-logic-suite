/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <OpenDriveMap.h>
#include <gtest/gtest.h>

namespace road_logic_suite::test
{

/**
 * This test is just to make sure that the libOpenDRIVE depedency is working correctly and capable of parsing simple
 * openDRIVE files. This test might become obsolete in the future. The test has been setup to work with bazel runfiles,
 * but on a Windows machine there can be issues with symlinks, see: https://bazel.build/configure/windows#symlink
 * If that happens, the current workaround would be to first build the tests and then run them from the same directory
 * where the WORKSPACE file is, i.e. by executing `./bazel-bin/RoadLogicSuite/test/unit_test.exe`.
 */
TEST(OpenDriveTest, GivenValidOpenDriveMap_WhenParsingMap_ThenParsedDataIsCorrect)
{

    // arrange
    const auto filepath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    const auto expected_roads_size = 1;
    const auto expected_road_length = 1000.0;
    const auto expected_lanesections_size = 1;
    const auto expected_lane_size = 5;
    const auto expected_center_lane_type = "none";
    const auto expected_center_line_roadmark_type = "solid";
    const auto expected_center_line_roadmark_width = 0.2;
    const auto expected_center_line_roadmark_soffset = 0.0;

    // act
    const odr::OpenDriveMap map(filepath);

    // assert
    ASSERT_EQ(expected_roads_size, map.get_roads().size());
    EXPECT_EQ(expected_road_length, map.get_roads()[0].length);

    ASSERT_EQ(expected_lanesections_size, map.id_to_road.at("0").get_lanesections().size());
    ASSERT_EQ(expected_lane_size, map.id_to_road.at("0").get_lanesections()[0].get_lanes().size());

    const auto center_lane = map.id_to_road.at("0").get_lanesections()[0].id_to_lane.at(0);
    EXPECT_EQ(expected_center_lane_type, center_lane.type);

    const auto& center_line_roadmarkgroups = center_lane.roadmark_groups;
    ASSERT_EQ(1, center_line_roadmarkgroups.size());
    const auto& center_line_roadmark = *center_line_roadmarkgroups.begin();
    EXPECT_EQ(expected_center_line_roadmark_type, center_line_roadmark.type);
    EXPECT_EQ(expected_center_line_roadmark_width, center_line_roadmark.width);
    EXPECT_EQ(expected_center_line_roadmark_soffset, center_line_roadmark.s_offset);
}

}  // namespace road_logic_suite::test
