/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include "tools/cpp/runfiles/runfiles.h"

#include <filesystem>

using bazel::tools::cpp::runfiles::Runfiles;

namespace road_logic_suite::test::utils
{

std::string Resolve(const std::string& path)
{
    if (std::filesystem::exists(path))
    {
        return path;
    }

    std::string error{};
    std::unique_ptr<Runfiles> runfiles(Runfiles::CreateForTest(&error));

    if (runfiles == nullptr)
    {
        throw std::runtime_error("Resolve file failed: " + error);
    }

    auto full_path = runfiles->Rlocation(path);

    if (std::filesystem::exists(full_path))
    {
        return full_path;
    }

    full_path = runfiles->Rlocation("road-logic-suite/" + path);
    if (std::filesystem::exists(full_path))
    {
        return full_path;
    }

    throw std::runtime_error("File not found: " + path);
}

}  // namespace road_logic_suite::test::utils
