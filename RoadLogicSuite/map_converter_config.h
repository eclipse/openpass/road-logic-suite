/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MAPCONVERTERCONFIG_H
#define ROADLOGICSUITE_MAPCONVERTERCONFIG_H

#include <units.h>

namespace road_logic_suite
{
struct MapConverterConfig
{
    /// @brief Used to determine the sampling rate. The first and last point of a geometry are always sampled.
    units::length::meter_t sampling_distance{1.0};

    /// @brief When true, the converter will try to optimize the polyline to have less points.
    bool downsampling{false};

    /// @brief When \p downsampling is true, this is used as the maximum offset value for the Douglas-Peucker algorithm.
    units::length::meter_t downsampling_epsilon{0};
};

}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_MAPCONVERTERCONFIG_H
