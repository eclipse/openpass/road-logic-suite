/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/param_poly_3.h"

#include <cmath>

namespace road_logic_suite
{

ParamPoly3::ParamPoly3(const units::length::meter_t& s,
                       const Vec2<units::length::meter_t>& position,
                       const units::angle::radian_t& hdg,
                       const units::length::meter_t& length,
                       const std::string& road_id,
                       const types::Poly3& u_poly,
                       const types::Poly3& v_poly,
                       bool is_p_range_normalized)
    : IGeometry(s, position, hdg, length, road_id),
      u_poly(u_poly),
      v_poly(v_poly),
      is_p_range_normalized(is_p_range_normalized)
{
}

std::optional<Vec2<units::length::meter_t>> ParamPoly3::ConvertInertialCoordinatesToST(
    const Vec2<units::length::meter_t>& query_point) const
{
    // transform the query point into the local coordinate system of the curve
    auto shifted_query_point = query_point - position;
    // after rotating the query point with the negative heading of the curve, the relative position of the query point
    // to the curve should be the same as if we rotated the parametric curve with its positive heading
    shifted_query_point = shifted_query_point.Rotate(-hdg);
    return ConvertInertialCoordinatesToSTLocal(shifted_query_point);
}

std::optional<Vec2<units::length::meter_t>> ParamPoly3::ConvertInertialCoordinatesToSTLocal(
    const Vec2<units::length::meter_t>& query_point) const
{
    auto p0 =
        u_poly.a * u_poly.b - u_poly.b * query_point.x.value() + v_poly.a * v_poly.b - v_poly.b * query_point.y.value();
    auto p1 = u_poly.b * u_poly.b + 2 * u_poly.a * u_poly.c - 2 * u_poly.c * query_point.x.value() +
              v_poly.b * v_poly.b + 2 * v_poly.a * v_poly.c - 2 * v_poly.c * query_point.y.value();
    auto p2 = 3 * u_poly.b * u_poly.c + 3 * u_poly.a * u_poly.d - 3 * u_poly.d * query_point.x.value() +
              3 * v_poly.b * v_poly.c + 3 * v_poly.a * v_poly.d - 3 * v_poly.d * query_point.y.value();
    auto p3 = 4 * u_poly.b * u_poly.d + 2 * u_poly.c * u_poly.c + 4 * v_poly.b * v_poly.d + 2 * v_poly.c * v_poly.c;
    auto p4 = 5 * u_poly.c * u_poly.d + 5 * v_poly.c * v_poly.d;
    auto p5 = 3 * u_poly.d * u_poly.d + 3 * v_poly.d * v_poly.d;

    Eigen::VectorXd coefficients(6);
    coefficients << p0, p1, p2, p3, p4, p5;

    Eigen::PolynomialSolver<double, Eigen::Dynamic> solver;
    solver.compute(coefficients);
    const Eigen::PolynomialSolver<double, Eigen::Dynamic>::RootsType& r = solver.roots();

    return ExtractBestSolutionST(r, query_point);
}

std::optional<Vec2<units::length::meter_t>> ParamPoly3::ExtractBestSolutionST(
    const Eigen::PolynomialSolver<double, Eigen::Dynamic>::RootsType& roots,
    const Vec2<units::length::meter_t>& query_point) const
{
    std::optional<Vec2<units::length::meter_t>> return_val = std::nullopt;
    double min_length = std::numeric_limits<double>::max();

    for (auto result : roots)
    {
        if (result.imag() != 0)
        {
            // there is no real root at this point, if it has an imaginary part
            continue;
        }

        if (result.real() < 0)
        {
            // No solution is desired if the value is less than 0.
            continue;
        }

        auto point_on_curve =
            ConvertSTCoordinatesToInertialLocal(units::length::meter_t(result.real()) + s, units::length::meter_t(0));
        auto point_to_curve = point_on_curve - query_point;
        auto length_2 = point_to_curve.LengthSquared();
        if (length_2 >= min_length)
        {
            // we already found a better fitting point
            continue;
        }

        auto s_coordinate = units::length::meter_t(result.real());
        auto forward = CalcDirectionAt(s_coordinate + s);
        auto tDirection = forward.Rotate90Left();
        auto tSign = tDirection.Dot(point_to_curve) < 0 ? 1.0 : -1.0;

        min_length = length_2;
        return_val =
            Vec2<units::length::meter_t>(s_coordinate + s, units::length::meter_t(tSign * std::sqrt(length_2)));
    }

    return return_val;
}

Vec2<units::length::meter_t> ParamPoly3::ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                        const units::length::meter_t& query_t) const
{
    auto local_uv = ConvertSTCoordinatesToInertialLocal(query_s, query_t);
    auto local_uv_rotated = local_uv.Rotate(hdg);
    return local_uv_rotated + position;
}

Vec2<units::length::meter_t> ParamPoly3::ConvertSTCoordinatesToInertialLocal(
    const units::length::meter_t& query_s,
    const units::length::meter_t& query_t) const
{
    auto distance_query = is_p_range_normalized ? ((query_s - s) / length).value() : (query_s - s).value();

    auto local_uv = Vec2<units::length::meter_t>{units::length::meter_t(u_poly.CalcAt(distance_query)),
                                                 units::length::meter_t(v_poly.CalcAt(distance_query))};
    auto direction_norm = CalcDirectionAt(query_s);
    auto t_direction_norm = direction_norm.Rotate90Left();

    return local_uv + query_t.value() * t_direction_norm;
}

custom_units::curvature_t ParamPoly3::CalcCurvatureAt(const units::length::meter_t& query_s) const
{
    auto distance_query = is_p_range_normalized ? ((query_s - s) / length).value() : (query_s - s).value();

    // https://en.wikipedia.org/wiki/Curvature#In_terms_of_a_general_parametrization
    auto u_d1 = u_poly.CalcDerivativeAt(distance_query);
    auto u_d2 = u_poly.CalcDerivative2At(distance_query);
    auto v_d1 = v_poly.CalcDerivativeAt(distance_query);
    auto v_d2 = v_poly.CalcDerivative2At(distance_query);
    auto numerator = u_d1 * v_d2 - v_d1 * u_d2;
    auto denominator = std::pow(u_d1 * u_d1 + v_d1 * v_d1, 3.0 / 2.0);
    if (denominator == 0)
    {
        /// Handle special case. If demonitator is zero then return NAN.
        return custom_units::curvature_t(NAN);
    }

    return custom_units::curvature_t(numerator / denominator);
}

Vec2<units::length::meter_t> ParamPoly3::CalcDirectionAt(const units::length::meter_t& query_s) const
{
    auto distance_query = is_p_range_normalized ? ((query_s - s) / length).value() : (query_s - s).value();

    auto local_heading = Vec2<units::length::meter_t>{units::length::meter_t(u_poly.CalcDerivativeAt(distance_query)),
                                                      units::length::meter_t(v_poly.CalcDerivativeAt(distance_query))};
    return local_heading / local_heading.Length().value();
}

units::angle::radian_t ParamPoly3::GetHeadingAt(const units::length::meter_t& s) const
{
    const auto direction = CalcDirectionAt(s);
    return units::math::atan2(direction.y, direction.x);
}

}  // namespace road_logic_suite
