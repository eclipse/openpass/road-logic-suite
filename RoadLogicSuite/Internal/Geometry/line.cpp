/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/line.h"

namespace road_logic_suite
{

Line::Line(const units::length::meter_t& s,
           const Vec2<units::length::meter_t>& position,
           const units::angle::radian_t& hdg,
           const units::length::meter_t& length,
           const std::string& road_id)
    : IGeometry(s, position, hdg, length, road_id)
{
}

std::optional<Vec2<units::length::meter_t>> road_logic_suite::Line::ConvertInertialCoordinatesToST(
    const Vec2<units::length::meter_t>& query_point) const
{
    auto lambda = OffsetOfClosestPointOnLine(query_point);
    if (lambda.value() < 0 || lambda > length)
    {
        return std::nullopt;
    }
    units::length::meter_t distance_to_line = SignedDistanceToLine(query_point);
    return Vec2{lambda + s, distance_to_line};
}

Vec2<units::length::meter_t> Line::ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                  const units::length::meter_t& query_t) const
{
    Vec2<units::length::meter_t> offset = {query_s - s, query_t};
    auto rotated = offset.Rotate(hdg);
    return position + rotated;
}

custom_units::curvature_t Line::CalcCurvatureAt([[maybe_unused]] const units::length::meter_t& query_s) const
{
    return custom_units::curvature_t(0);
}

types::Rect<units::length::meter_t> Line::GetBoundingBox() const
{
    const auto position_end = ConvertSTCoordinatesToInertial(s + length, units::length::meter_t(0));
    return {{std::min(position_end.x, position.x), std::min(position_end.y, position.y)},
            {std::max(position_end.x, position.x), std::max(position_end.y, position.y)}};
}

units::length::meter_t road_logic_suite::Line::OffsetOfClosestPointOnLine(
    const Vec2<units::length::meter_t>& query_point) const
{
    Vec2<units::dimensionless::scalar_t> line_direction(units::math::cos(hdg), units::math::sin(hdg));
    auto offset = query_point - position;
    return (line_direction.x * offset.x + line_direction.y * offset.y) /
           (line_direction.x * line_direction.x + line_direction.y * line_direction.y);
}

units::length::meter_t road_logic_suite::Line::SignedDistanceToLine(
    const Vec2<units::length::meter_t>& query_point) const
{
    Vec2<units::length::meter_t> line_direction(units::length::meter_t(units::math::cos(hdg).value()),
                                                units::length::meter_t(units::math::sin(hdg).value()));
    auto lambda = OffsetOfClosestPointOnLine(query_point);
    auto line_direction_lambda = line_direction * lambda.value();
    auto point_on_line = line_direction_lambda + position;
    units::length::meter_t distance_query_to_line = (query_point - point_on_line).Length();
    units::angle::radian_t signed_angle = units::math::atan2(query_point.y - position.y, line_direction_lambda.x) -
                                          units::math::atan2(line_direction_lambda.y, line_direction_lambda.x);
    if (signed_angle < units::angle::radian_t(0))
    {
        return -distance_query_to_line;
    }
    return distance_query_to_line;
}
units::angle::radian_t Line::GetHeadingAt([[maybe_unused]] const units::length::meter_t& s) const
{
    return hdg;
}

}  // namespace road_logic_suite
