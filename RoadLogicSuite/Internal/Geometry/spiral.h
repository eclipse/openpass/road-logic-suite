/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_SPIRAL_H
#define ROADLOGICSUITE_SPIRAL_H

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"

namespace road_logic_suite
{

class Spiral : public IGeometry
{
  public:
    /// @brief Curvature at the start of the element. A positive value denotes a left turn. A negative value denotes a
    /// right turn.
    custom_units::curvature_t curvature_start{0};

    /// @brief Curvature at the end of the element. A positive value denotes a left turn. A negative value denotes a
    /// right turn.
    custom_units::curvature_t curvature_end{0};

    Spiral(const units::length::meter_t& s,
           const Vec2<units::length::meter_t>& position,
           const units::angle::radian_t& hdg,
           const units::length::meter_t& length,
           const std::string& road_id,
           const custom_units::curvature_t& curvature_start,
           const custom_units::curvature_t& curvature_end);

    [[nodiscard]] std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToST(
        const Vec2<units::length::meter_t>& query_point) const override;
    [[nodiscard]] Vec2<units::length::meter_t> ConvertSTCoordinatesToInertial(
        const units::length::meter_t& query_s,
        const units::length::meter_t& query_t) const override;

    [[nodiscard]] custom_units::curvature_t CalcCurvatureAt(
        [[maybe_unused]] const units::length::meter_t& query_s) const override;

    [[nodiscard]] units::angle::radian_t GetHeadingAt(const units::length::meter_t& s) const override;

  private:
    [[nodiscard]] double GetStartingValueForNewtonsMethod(double xq, double yq) const;
    [[nodiscard]] std::pair<double, double> GetSpiralPositionAtS(double query_s) const;
    [[nodiscard]] std::pair<double, double> GetFirstDerivativeAtS(double query_s) const;
    [[nodiscard]] std::pair<double, double> GetSecondDerivativeAtS(double query_s) const;

    double curvature_change_rate_{};
    double x0_spiral_{};
    double y0_spiral_{};
    double s0_spiral_{};
    double hdg0_spiral_{};
    double clothoid_scale_{};
};

}  // namespace road_logic_suite
#endif  // ROADLOGICSUITE_SPIRAL_H
