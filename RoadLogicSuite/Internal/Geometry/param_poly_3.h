/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_PARAM_POLY_3_H
#define ROADLOGICSUITE_PARAM_POLY_3_H

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"
#include "RoadLogicSuite/Internal/Types/poly3.h"
#include "unsupported/Eigen/Polynomials"

namespace road_logic_suite
{

class ParamPoly3 : public IGeometry
{
  public:
    road_logic_suite::types::Poly3 u_poly;
    road_logic_suite::types::Poly3 v_poly;
    bool is_p_range_normalized;

    ParamPoly3(const units::length::meter_t& s,
               const Vec2<units::length::meter_t>& position,
               const units::angle::radian_t& hdg,
               const units::length::meter_t& length,
               const std::string& road_id,
               const road_logic_suite::types::Poly3& u_poly,
               const road_logic_suite::types::Poly3& v_poly,
               bool is_p_range_normalized);

    std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToST(
        const Vec2<units::length::meter_t>& query_point) const override;
    Vec2<units::length::meter_t> ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                const units::length::meter_t& query_t) const override;

    custom_units::curvature_t CalcCurvatureAt(const units::length::meter_t& query_s) const override;

    Vec2<units::length::meter_t> CalcDirectionAt(const units::length::meter_t& query_s) const;

    [[nodiscard]] units::angle::radian_t GetHeadingAt(const units::length::meter_t& s) const override;

  private:
    std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToSTLocal(
        const Vec2<units::length::meter_t>& query_point) const;

    Vec2<units::length::meter_t> ConvertSTCoordinatesToInertialLocal(const units::length::meter_t& query_s,
                                                                     const units::length::meter_t& query_t) const;

    std::optional<Vec2<units::length::meter_t>> ExtractBestSolutionST(
        const Eigen::PolynomialSolver<double, Eigen::Dynamic>::RootsType& roots,
        const Vec2<units::length::meter_t>& query_point) const;
};

}  // namespace road_logic_suite
#endif  // ROADLOGICSUITE_PARAM_POLY_3_H
