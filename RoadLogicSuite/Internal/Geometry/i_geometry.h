/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_GEOMETRY_H
#define ROADLOGICSUITE_GEOMETRY_H

#include "RoadLogicSuite/Internal/Types/custom_units.h"
#include "RoadLogicSuite/Internal/Types/rect.h"
#include "RoadLogicSuite/Internal/Types/vec2.h"

#include <units.h>

#include <array>
#include <optional>
#include <string>

namespace road_logic_suite
{

/// @brief Holds geometrical information, i.e. about a road reference line.

class IGeometry
{
  public:
    /// @brief s coordinate of the starting position. Must be >= 0.
    units::length::meter_t s{0};

    /// @brief starting position in inertial coordinates.
    Vec2<units::length::meter_t> position{units::length::meter_t(0), units::length::meter_t(0)};

    /// @brief Start orientation (aka inertial heading). The inertial x-axis has a heading of 0.
    units::angle::radian_t hdg{0};

    /// @brief The length of the geometry. Must be bigger than 0.
    units::length::meter_t length{0};

    /// @brief Road identifier that this geometry belongs to.
    std::string road_id{};

    IGeometry(const units::length::meter_t& s,
              const Vec2<units::length::meter_t>& position,
              const units::angle::radian_t& hdg,
              const units::length::meter_t& length,
              const std::string& road_id);

    virtual ~IGeometry() = default;

    /// @brief Converts a given point from the global inertial coordinate system to the local ST coordinate system of
    /// the geometry element.
    /// @param query_point the inertial coordinates
    /// @return std::nullopt if there is no valid st coordinate for the given point, otherwise the valid st coordinates
    virtual std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToST(
        const Vec2<units::length::meter_t>& query_point) const = 0;

    /// @brief Converts a given point from the local ST coordinate system of the geometry element to
    /// the global inertial coordinate system.
    /// @param query_s the road s coordinate. Should usually be in the range of (s, s+length). But other points will
    /// also be calculated.
    /// @param query_t the road t coordinate
    /// @return The global inertial coordinates
    virtual Vec2<units::length::meter_t> ConvertSTCoordinatesToInertial(
        const units::length::meter_t& query_s,
        const units::length::meter_t& query_t) const = 0;

    /// @brief Calculates the curvature of the curve at the given local s coordinate of the element.
    /// @param query_s the local s coordinate. Should usually be in the range of (s, s+length). But other points will
    /// also be calculated.
    /// @return The curvature at the point s
    virtual custom_units::curvature_t CalcCurvatureAt(const units::length::meter_t& query_s) const = 0;

    /// @brief Determines the heading of the geometry at `s`.
    /// @param s the local s coordinate. Should be in the range of (s, s+length).
    /// @return The heading at the point s.
    [[nodiscard]] virtual units::angle::radian_t GetHeadingAt(const units::length::meter_t& s) const = 0;

    /// @brief Calculates an axis aligned bounding box for the geometry. A default implementation is provided.
    /// @return The bounding box
    [[nodiscard]] virtual types::Rect<units::length::meter_t> GetBoundingBox() const;
};

}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_GEOMETRY_H
