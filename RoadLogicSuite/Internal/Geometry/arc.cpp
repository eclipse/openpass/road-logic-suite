/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/arc.h"

#include "RoadLogicSuite/Internal/Utils/math_utils.h"

namespace road_logic_suite
{

Arc::Arc(const units::length::meter_t& s,
         const Vec2<units::length::meter_t>& position,
         const units::angle::radian_t& hdg,
         const units::length::meter_t& length,
         const custom_units::curvature_t& curvature,
         const std::string& road_id)
    : IGeometry(s, position, hdg, length, road_id), curvature(curvature)
{
}

std::optional<Vec2<units::length::meter_t>> Arc::ConvertInertialCoordinatesToST(
    const Vec2<units::length::meter_t>& query_point) const
{
    Vec2<units::length::meter_t> center_point = CenterPoint();
    if (!ArcIsValidQuery(Vec2(query_point), center_point))
    {
        return std::nullopt;
    }
    units::length::meter_t distance_m_to_p = (query_point - center_point).Length();
    units::length::meter_t radius = 1 / curvature;
    units::length::meter_t out_t = math_utils::Signum(curvature) * (units::math::fabs(radius) - distance_m_to_p);

    units::length::meter_t out_s = s + ArcLengthToQueryProjectedToCircle(Vec2(query_point), center_point);
    return Vec2{out_s, out_t};
}

Vec2<units::length::meter_t> Arc::ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                 const units::length::meter_t& query_t) const
{
    Vec2<units::length::meter_t> center_point = CenterPoint();
    units::angle::radian_t theta = Angle(query_s - s, curvature);
    units::length::meter_t r = 1 / curvature;
    return {center_point.x + (r - query_t) * units::math::sin(theta + hdg),
            center_point.y - (r - query_t) * units::math::cos(theta + hdg)};
}

custom_units::curvature_t Arc::CalcCurvatureAt([[maybe_unused]] const units::length::meter_t& query_s) const
{
    return curvature;
}

units::length::meter_t Arc::ArcLengthToQueryProjectedToCircle(const Vec2<units::length::meter_t>& query,
                                                              const Vec2<units::length::meter_t>& center_point) const
{
    units::angle::radian_t min_angle = hdg - math_utils::Signum(curvature) * math_utils::kPiOver2;
    units::angle::radian_t angle = units::math::atan2(query.y - center_point.y, query.x - center_point.x);
    return ArcLength(angle - min_angle, 1 / curvature);
}

Vec2<units::length::meter_t> Arc::CenterPoint() const
{
    return {
        position.x - units::math::sin(hdg) / curvature,
        position.y + units::math::cos(hdg) / curvature,
    };
}

units::length::meter_t Arc::ArcLength(const units::angle::radian_t& angle, const units::length::meter_t& radius)
{
    return angle.value() * radius;
}

units::angle::radian_t Arc::Angle(const units::length::meter_t& arc_length, const custom_units::curvature_t& curvature)
{
    return units::angle::radian_t((arc_length * curvature).value());
}

bool Arc::ArcIsValidQuery(const Vec2<units::length::meter_t>& query,
                          const Vec2<units::length::meter_t>& center_point) const
{
    units::length::meter_t arc_length_to_query = ArcLengthToQueryProjectedToCircle(query, center_point);
    return units::length::meter_t(0) <= arc_length_to_query && arc_length_to_query <= length;
}

units::angle::radian_t Arc::GetHeadingAt(const units::length::meter_t& s) const
{
    return hdg + Angle(s, curvature);
}

}  // namespace road_logic_suite
