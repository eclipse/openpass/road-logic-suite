/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/open_drive_data.h"

namespace road_logic_suite
{

bool OpenDriveData::IsEmpty() const
{
    return roads.empty() && geometries.empty();
}

}  // namespace road_logic_suite
