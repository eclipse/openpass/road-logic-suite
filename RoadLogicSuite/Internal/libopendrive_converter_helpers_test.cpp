/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_converter_helpers.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"

#include <gtest/gtest.h>

namespace road_logic_suite::test
{

using namespace road_logic_suite::helpers;

TEST(CreateRoadLinksTest, GivenInvalidContactType_WhenGetContactType_ThenThrowError)
{
    odr::RoadLink::ContactPoint invalid_contact_point = static_cast<odr::RoadLink::ContactPoint>(3);

    // Check if calling GetContactType with the invalid ContactPoint throws an error
    EXPECT_THROW(GetContactType(invalid_contact_point), std::runtime_error);
}

TEST(CreateRoadLinksTest, GivenValidContactType_WhenGetContactType_ThenParseCorrectly)
{
    odr::RoadLink::ContactPoint contact_point_start = odr::RoadLink::ContactPoint_Start;
    odr::RoadLink::ContactPoint contact_point_end = odr::RoadLink::ContactPoint_End;

    EXPECT_EQ(LinkContactType::kStart, GetContactType(contact_point_start));
    EXPECT_EQ(LinkContactType::kEnd, GetContactType(contact_point_end));
}

TEST(CreateRoadLinksTest, GivenRoadWithInvalidContactType_WhenCreateRoadLinks_ThenThrowError)
{
    // Construct a road with an invalid ContactPoint type
    odr::Road road_with_invalid_contact_type("", 100.0, "", "");
    road_with_invalid_contact_type.predecessor.type = odr::RoadLink::Type_Road;
    road_with_invalid_contact_type.predecessor.contact_point = static_cast<odr::RoadLink::ContactPoint>(3);

    EXPECT_THROW(CreateRoadLinks(road_with_invalid_contact_type), std::runtime_error);
}

TEST(CreateRoadLinksTest, GivenRoadWithValidContactType_WhenCreateRoadLinks_ThenParseCorrectly)
{
    // Construct a road with an valid ContactPoint type
    odr::Road road_with_valid_contact_type("", 100.0, "", "");
    road_with_valid_contact_type.predecessor.type = odr::RoadLink::Type_Road;
    road_with_valid_contact_type.predecessor.contact_point = odr::RoadLink::ContactPoint_Start;
    road_with_valid_contact_type.successor.type = odr::RoadLink::Type_Road;
    road_with_valid_contact_type.successor.contact_point = odr::RoadLink::ContactPoint_End;
    const auto expected_road_links_size = 2;

    const auto& road_links = CreateRoadLinks(road_with_valid_contact_type);

    ASSERT_EQ(expected_road_links_size, road_links.size());
    // check if the contact type of predecessor parsed correctly
    EXPECT_EQ(LinkContactType::kStart, road_links[0].contact_type);
    // check if the contact type of successor parsed correctly
    EXPECT_EQ(LinkContactType::kEnd, road_links[1].contact_type);
}

TEST(CreateJunctionLinksTest, GivenInvalidContactType_WhenGetContactType_ThenThrowError)
{
    const auto invalid_contact_point = static_cast<odr::JunctionConnection::ContactPoint>(3);

    // Check if calling GetContactType with the invalid ContactPoint throws an error
    EXPECT_THROW(GetContactType(invalid_contact_point), std::runtime_error);
}

TEST(CreateJunctionLinksTest, GivenValidContactType_WhenGetContactType_ThenParseCorrectly)
{
    const auto contact_point_start = odr::JunctionConnection::ContactPoint::ContactPoint_Start;
    const auto contact_point_end = odr::JunctionConnection::ContactPoint::ContactPoint_End;

    EXPECT_EQ(types::JunctionContactType::kStart, GetContactType(contact_point_start));
    EXPECT_EQ(types::JunctionContactType::kEnd, GetContactType(contact_point_end));
}

TEST(CreateJunctionLinksTest, GivenOrdJunctionConnection_WhenCreateJunctionLaneLinks_ThenParseCorrectly)
{
    odr::JunctionConnection xml_junction_connection(
        "0", "1", "2", odr::JunctionConnection::ContactPoint::ContactPoint_Start);
    std::set<odr::JunctionLaneLink> xml_junction_lane_links{odr::JunctionLaneLink{1, -1}, odr::JunctionLaneLink{2, -2}};
    xml_junction_connection.lane_links = xml_junction_lane_links;

    const auto& junction_lane_links = CreateJunctionLinks(xml_junction_connection);

    EXPECT_EQ(junction_lane_links[0].from, 1);
    EXPECT_EQ(junction_lane_links[0].to, -1);
    EXPECT_EQ(junction_lane_links[1].from, 2);
    EXPECT_EQ(junction_lane_links[1].to, -2);
}

TEST(ConvertSignalValidityTest, GivenValidInput_WhenConverting_ThenLaneValidityIsConvertedCorrectly)
{
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("root");
    pugi::xml_node validity = root.append_child("validity");
    validity.append_attribute("fromLane") = -1;
    validity.append_attribute("toLane") = 3;

    std::vector<types::LaneId> expected_lane_validity = {-1, 1, 2, 3};
    auto converted_lane_validity = ConvertLaneValidity(root);

    ASSERT_EQ(converted_lane_validity, expected_lane_validity);
}

TEST(ConvertSignalValidityTest, GivenValidInputReverse_WhenConverting_ThenLaneValidityIsConvertedCorrectly)
{
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("root");
    pugi::xml_node validity = root.append_child("validity");
    validity.append_attribute("fromLane") = 3;
    validity.append_attribute("toLane") = 1;

    std::vector<types::LaneId> expected_lane_validity = {3, 2, 1};
    auto converted_lane_validity = ConvertLaneValidity(root);

    ASSERT_EQ(converted_lane_validity, expected_lane_validity);
}

TEST(ConvertSignalValidityTest, GivenValidInputContainingZero_WhenConverting_ThenLaneZeroIsExcluded)
{
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("root");
    pugi::xml_node validity = root.append_child("validity");
    validity.append_attribute("fromLane") = 0;
    validity.append_attribute("toLane") = 3;

    std::vector<types::LaneId> expected_lane_validity = {1, 2, 3};
    auto converted_lane_validity = ConvertLaneValidity(root);

    ASSERT_EQ(converted_lane_validity, expected_lane_validity);
}

TEST(ConvertSignalValidityTest, GivenInValidNode_WhenConverting_ThenReturnNull)
{
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("root");

    std::vector<types::LaneId> expected = {};
    auto result = ConvertLaneValidity(root);

    ASSERT_EQ(result, expected);
}

TEST(ConvertSignalValidityTest, GivenInputOnlyContainingZero_WhenConverting_ThenLaneZeroIsExcluded)
{
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("root");
    pugi::xml_node validity = root.append_child("validity");
    validity.append_attribute("fromLane") = 0;
    validity.append_attribute("toLane") = 0;

    std::vector<types::LaneId> expected = {};
    auto result = ConvertLaneValidity(root);

    ASSERT_EQ(result, expected);
}

class GetInertialPosFromReferenceLinePosTest : public ::testing::Test
{
  protected:
    road_logic_suite::OpenDriveData data;
    std::string road_id;

    void SetUp() override
    {
        road_id = "1";
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
            lane_sections{};
        road_logic_suite::types::LaneSection lane_section{};
        lane_sections.Insert(units::length::meter_t(0), lane_section);

        const road_logic_suite::Road road{
            .id = road_id,
            .index_geometries_start = 0,
            .index_geometries_end = 1,
            .lane_sections = lane_sections,
        };
        data.roads.insert_or_assign(road_id, road);

        auto geometry = std::make_unique<road_logic_suite::Line>(
            units::length::meter_t(0),
            road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(10.0), units::length::meter_t(5.0)),
            units::angle::radian_t(0),
            units::length::meter_t(100.0),
            road_id);

        data.geometries.push_back(std::move(geometry));
    }
};

TEST_F(GetInertialPosFromReferenceLinePosTest,
       GivenValidInput_WhenFunctionCalled_TheCoordinatePositionIsConvertedCorrectly)
{
    const double s = 50.0;
    const double t = -3.0;
    const double z_offset = 2.0;

    const auto expected_position_x = 60.0;
    const auto expected_position_y = 2.0;
    const auto expected_position_z = 2.0;

    const CoordinateConverter converter(data);
    const auto position =
        GetInertialPosFromReferenceLinePos(converter, odr::Road{road_id, 100.0, "-1", "myroad"}, s, t, z_offset);

    EXPECT_EQ(position.x.value(), expected_position_x);
    EXPECT_EQ(position.y.value(), expected_position_y);
    EXPECT_EQ(position.z.value(), expected_position_z);
}

TEST_F(GetInertialPosFromReferenceLinePosTest, GivenInvalidS_WhenFunctionCalled_ThenRuntimeErrorIsThrown)
{
    const double invalid_s = 150.0;  // s is greater than the length of the road
    const double t = -3.0;
    const double z_offset = 2.0;

    const CoordinateConverter converter(data);
    const odr::Road xml_road = {road_id, 100.0, "-1", "myroad"};

    EXPECT_THROW({ GetInertialPosFromReferenceLinePos(converter, xml_road, invalid_s, t, z_offset); },
                 std::runtime_error);
}

}  // namespace road_logic_suite::test
