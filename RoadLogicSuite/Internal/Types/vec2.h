/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_VEC2_H
#define ROADLOGICSUITE_VEC2_H

#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <type_traits>

namespace road_logic_suite
{

/// @brief This struct is similar to Mantle APIs Vec3<T> struct, but without a z value.
/// @tparam T a unit type.
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
struct Vec2
{
    /// @brief Default constructor.
    Vec2() = default;

    /// @brief Construct a new 2-dimensional vector using explicit x and y values.
    /// @param x_in the x value.
    /// @param y_in the y value.
    Vec2(T x_in, T y_in) : x{x_in}, y{y_in} {}

    /// @brief Construct a new 2-dimensional vector by truncating the z value of an existing 2-dimensional vector.
    /// @param in the 3-dimensional vector.
    explicit Vec2(mantle_api::Vec3<T> in) : x{in.x}, y{in.y} {}

    T x{0};
    T y{0};

    /// @brief The length (aka magnitude) of the vector.
    /// @return the length in the given unit.
    inline T Length() const { return units::math::sqrt((x * x) + (y * y)); }

    /// @brief The squared length (aka magnitude) of the vector.
    /// This function is much faster than the Length() function because it don't have to calculate the square root.
    /// @return the squared length.
    inline double LengthSquared() const { return ((x * x) + (y * y)).value(); }

    /// @brief Calculates the dot product between this vector and a given one.
    /// @param rhs The given other vector
    /// @return the dot product
    inline double Dot(const Vec2<T>& rhs) const { return ((x * rhs.x) + (y * rhs.y)).value(); }

    /// @brief Rotates this vector around the origin by a given angle
    /// @param angle The angle in radiant
    /// @return The rotated vector
    inline Vec2<T> Rotate(const units::angle::radian_t& angle) const
    {
        return {x * units::math::cos(angle) - y * units::math::sin(angle),
                y * units::math::cos(angle) + x * units::math::sin(angle)};
    }

    /// @brief Rotates this vector counter-clockwise around the origin by 90 degrees.
    /// This special case is much faster than the general Rotate-function.
    /// @return The rotated vector
    inline Vec2<T> Rotate90Left() const { return {-y, x}; }

    /// @brief Rotates this vector clockwise around the origin by 90 degrees.
    /// This special case is much faster than the general Rotate-function.
    /// @return The rotated vector
    inline Vec2<T> Rotate90Right() const { return {y, -x}; }

    /// @brief Converts the Vec2 to a mantle API compatible Vec3.
    /// @param z_in the optional z value. Otherwise it is initialized to zero.
    /// @return the Vec3 interpretation.
    inline mantle_api::Vec3<T> ToVec3(T z_in = T(0)) const { return mantle_api::Vec3<T>(x, y, z_in); }

    /// @brief The negative value of the vector.
    /// @return
    inline Vec2<T> operator-() const noexcept { return {-x, -y}; }
};

/// @brief Checks for equality of the two vectors.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return true, when the two vectors are equal. false, otherwise.
template <typename T>
inline bool operator==(const Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    return IsEqual(lhs.x, rhs.x) && IsEqual(lhs.y, rhs.y);
}

/// @brief Checks for inequality of the two vectors.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return true, when the two vectors are not equal. false, otherwise.
template <typename T>
inline bool operator!=(const Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    return !(lhs == rhs);
}

/// @brief Subtract one vector from the other.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return the difference between the two vectors.
template <typename T>
inline Vec2<T> operator-(const Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    return {lhs.x - rhs.x, lhs.y - rhs.y};
}

/// @brief Add one vector to the other.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return the sum of the two vectors.
template <typename T>
inline Vec2<T> operator+(const Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    return {lhs.x + rhs.x, lhs.y + rhs.y};
}

/// @brief Multiplication of a vector with a scalar value.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param d the scalar value.
/// @return the resulting vector.
template <typename T>
inline Vec2<T> operator*(const Vec2<T>& lhs, double d) noexcept
{
    return {lhs.x * d, lhs.y * d};
}

/// @brief Multiplication of a vector with a scalar value.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param d the scalar value.
/// @param rhs right hand side.
/// @return the resulting vector.
template <typename T>
inline Vec2<T> operator*(double d, const Vec2<T>& rhs) noexcept
{
    return rhs * d;
}

/// @brief Division of a vector with a scalar value.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param d the scalar value.
/// @return the resulting vector.
template <typename T>
inline Vec2<T> operator/(const Vec2<T>& lhs, double d) noexcept
{
    return lhs * (1 / d);
}

/// @brief Addition assignment operator.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return the left hand side.
template <typename T>
inline Vec2<T> operator+=(Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    lhs.x += rhs.x;
    lhs.y += rhs.y;
    return lhs;
}

/// @brief Subtraction assignment operator.
/// @tparam T the type (must be a unit, i.e. meters)
/// @param lhs left hand side.
/// @param rhs right hand side.
/// @return the left hand side.
template <typename T>
inline Vec2<T> operator-=(Vec2<T>& lhs, const Vec2<T>& rhs) noexcept
{
    lhs.x -= rhs.x;
    lhs.y -= rhs.y;
    return lhs;
}

}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_VEC2_H
