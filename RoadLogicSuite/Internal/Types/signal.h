/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_SIGNAL_H
#define ROADLOGICSUITE_SIGNAL_H

#include <units.h>

#include <vector>

namespace road_logic_suite
{

enum class SignalUnit : std::uint8_t
{
    kUnknown = 0,
    kOther = 1,
    kNoUnit = 2,
    kKilometerPerHour = 3,
    kMeterPerSecond = 4,
    kMilePerHour = 5,
    kMeter = 6,
    kKilometer = 7,
    kFeet = 8,
    kMile = 9,
    kMetricTon = 10,
    kKilo = 11,
    kPercentage = 12,
};

enum class SignalOrientation
{
    // valid in positive s-direction.
    kPostive = 0,

    // valid in negative s-direction.
    kNegative = 1,

    // valid in both directions.
    kNone = 2,

    // unknown orientation.
    kUnknown = 3,
};

struct Signal
{
    using LaneId = std::int32_t;

    /// @brief Name of the signal. Equal to OpenDrive definition.
    std::string name{};

    /// @brief Id of the road it belongs to.
    std::string road_id{};

    /// @brief Id of the signal.
    std::string id{};

    /// @brief Indicates whether the signal is dynamic or static. Example: traffic light is dynamic.
    bool is_dynamic{false};

    /// @brief Country code of the road.
    std::string country{};

    /// @brief Defines the year of the applied traffic rules.
    std::string country_revision{};

    /// @brief Type identifier according to country code or "-1" / "none". See extra document.
    std::string type{};

    /// @brief  Subtype identifier according to country code or "-1" / "none".
    std::string subtype{};

    /// @brief Value of the signal, if value is given, unit is mandatory.
    double value{};

    /// @brief Unit of @value.
    SignalUnit unit{};

    /// @brief Length of the signal’s bounding box. Length is defined in the local coordinate system u/v along the
    /// u-axis.
    units::length::meter_t length{0.0};

    /// @brief Width of the signal’s bounding box @width is defined in the local coordinate system u/v along the v-axis.
    units::length::meter_t width{0.0};

    /// @brief Height of the signal, measured from bottom edge of the signal.
    units::length::meter_t height{0.0};

    /// @brief Yaw angle of the signal, relative to the inertial system (xy-plane), which is converted from heading
    /// offset (relative to @orientation, if orientation is equal to "+ or "-", relative to road reference line, if
    /// orientation is equal to "none" ).
    units::angle::radian_t yaw{0.0};

    /// @brief Pitch angle of the signal, relative to the inertial system (xy-plane).
    units::angle::radian_t pitch{0.0};

    /// @brief Roll angle of the signal after applying pitch, relative to the inertial system (xy-plane).
    units::angle::radian_t roll{0.0};

    /// @brief Orientation along s-direction.
    SignalOrientation orientation{};

    /// @brief Additional text associated with the signal, for example, text on city limit "City\nBadAibling".
    std::string text{};

    /// @brief Position of the signal in inertial coordinates.
    mantle_api::Vec3<units::length::meter_t> position{};

    /// @brief Two-dimensional shape of the signal.
    std::vector<Vec2<units::length::meter_t>> shape{};

    /// @brief Defines the lanes for which a signal is valid.
    std::vector<LaneId> valid_lanes{};

    /// @brief One signal controls the output of another signal.
    std::vector<std::string> dependencies{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_SIGNAL_H
