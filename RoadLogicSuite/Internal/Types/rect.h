/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_RECT_H
#define ROADLOGICSUITE_RECT_H

#include "RoadLogicSuite/Internal/Types/vec2.h"

#include <units.h>

#include <type_traits>

namespace road_logic_suite::types
{

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
struct Rect
{
    Vec2<T> lower_left;
    Vec2<T> upper_right;

    /// @brief Creates a square rect around a specific point with a specified size.
    /// @param center the center point.
    /// @param size the size in each direction.
    /// @return the new rect.
    static Rect CreateRectAroundPoint(const Vec2<T>& center, const T size)
    {
        return Rect<T>{{center.x - size, center.y - size}, {center.x + size, center.y + size}};
    }

    /// @brief Expands the bounding box by the exact amount that is necessary to include the given point.
    /// @param point the given point.
    void Encapsulate(const Vec2<T> point)
    {
        lower_left.x = units::math::min(lower_left.x, point.x);
        lower_left.y = units::math::min(lower_left.y, point.y);
        upper_right.x = units::math::max(upper_right.x, point.x);
        upper_right.y = units::math::max(upper_right.y, point.y);
    }

    /// @brief Expands the bounding box by the given amount.
    /// @param amount the given amount.
    void Expand(T amount)
    {
        lower_left -= {amount, amount};
        upper_right += {amount, amount};
    }
};

}  // namespace road_logic_suite::types
#endif  // ROADLOGICSUITE_RECT_H
