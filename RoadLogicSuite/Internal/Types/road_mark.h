/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_MARK_H
#define ROADLOGICSUITE_ROAD_MARK_H

#include <units.h>

namespace road_logic_suite::types
{

enum class RoadMarkType
{
    UNKNOWN,
    BOTTS_DOTS,     // https://en.wikipedia.org/wiki/Botts%27_dots
    BROKEN_BROKEN,  // double lines. Left Broken / Right Broken
    BROKEN_SOLID,   // double lines. Left Broken / Right Solid
    BROKEN,
    CURB,
    CUSTOM,
    EDGE,
    GRASS,
    NONE,
    SOLID_BROKEN,  // double lines. Left Solid / Right Broken
    SOLID_SOLID,   // double lines. Left Solid / Right Solid
    SOLID
};

enum class RoadMarkWeight
{
    UNKNOWN,
    BOLD,
    STANDARD
};

enum class RoadMarkColor
{
    UNKNOWN,
    BLUE,
    GREEN,
    ORANGE,
    RED,
    STANDARD,
    VIOLET,
    WHITE,
    YELLOW
};

enum class RoadMarkLaneChange
{
    UNKNOWN,
    BOTH,
    DECREASE,
    INCREASE,
    NONE
};

struct RoadMark
{
    // Fields correspond to the XML definition:
    RoadMarkType type{};
    RoadMarkWeight weight{};
    RoadMarkColor color{};
    RoadMarkLaneChange laneChange{};
    units::length::meter_t height{};
    units::length::meter_t width{};
};
}  // namespace road_logic_suite::types

#endif  // ROADLOGICSUITE_ROAD_MARK_H
