/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LANE_SECTION_H
#define ROADLOGICSUITE_LANE_SECTION_H

#include "RoadLogicSuite/Internal/Types/lane.h"

#include <map>

namespace road_logic_suite::types
{

template <class Iter>
class Range
{
    Iter b;
    Iter e;

  public:
    Range(Iter b, Iter e) : b(b), e(e) {}

    Iter begin() { return b; }
    Iter end() { return e; }
};

template <class Container>
Range<typename Container::iterator> make_range(Container& c, size_t b, size_t e)
{
    return Range<typename Container::iterator>(c.begin() + b, c.begin() + e);
}

struct LaneSection
{
    /// @brief Start position of this lane section in reference line coordinates.
    units::length::meter_t s_start{0};

    /// @brief End position of this lane section in reference line coordinates.
    units::length::meter_t s_end{0};

    /// @brief vector of lanes in this lane section.
    std::vector<types::Lane> left_lanes;
    std::vector<types::Lane> right_lanes;
    types::Lane center_lane;

    /// @brief Returns a Lane by given Id
    /// @param id LaneId
    /// @return Lane
    [[nodiscard]] const types::Lane& GetLaneById(types::Lane::LaneId id) const;

    /// @brief Returns a lane according to the following rule:
    /// The boundary is closest to the given s-t coordinate.
    ///
    /// Example:
    /// A lane section consisting of the center lane (0) and two driving lanes with a width of 3 meters (-1, -2).
    /// -> A t-value of -1.5m or bigger yields the center lane @ t=0m
    /// -> A t-value between -1.5m and -4.5m yields the first right lane (-1).
    /// -> A t-value smaller than -4.5m yields the right-most lane (-2).
    /// @param s the s coordinate.
    /// @param t the t coordinate.
    /// @return the lane whose boundary is closest to the given s-t coordinate.
    types::Lane& GetLaneByBoundaryOffset(units::length::meter_t s, units::length::meter_t t);

    /// @brief Returns the count of all Lanes
    /// @return number of all Lanes
    [[nodiscard]] unsigned int GetLaneCount() const;

    /// @brief Returns a range of lanes from the reference line to the given lane id, which simplifies iterating until a
    /// specific lane.
    /// @param id the given id.
    /// @return the range.
    Range<std::vector<types::Lane>::iterator> GetLaneRangeFromReferenceLineToId(types::Lane::LaneId id);

    /// @brief Return true when given lane id is in Range from right to left lanes
    /// @return true or false
    [[nodiscard]] bool IsValidLaneId(types::Lane::LaneId id) const;
};
}  // namespace road_logic_suite::types

#endif  // ROADLOGICSUITE_LANE_SECTION_H
