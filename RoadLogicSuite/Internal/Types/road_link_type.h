/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_LINK_TYPE_H
#define ROADLOGICSUITE_ROAD_LINK_TYPE_H

namespace road_logic_suite
{
enum class RoadLinkType
{
    /// @brief The linked road is a successor of the current road.
    kSuccessor,

    /// @brief The linked road is a predecessor of the current road.
    kPredecessor,
};
}

#endif  // ROADLOGICSUITE_ROAD_LINK_TYPE_H
