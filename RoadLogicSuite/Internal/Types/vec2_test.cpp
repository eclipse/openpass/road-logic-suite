/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/vec2.h"

#include <gtest/gtest.h>

namespace road_logic_suite::types
{
static const double kAbsError = 1e-3;

TEST(Vec2Test, GivenAVec2_WhenLengthIsQueried_ThenCorrectLengthIsReturnedWithUnits)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    auto result = vec.Length();

    ASSERT_NEAR(result.value(), 5.83095, kAbsError);
}

TEST(Vec2Test, GivenAVec2_WhenSquaredLengthIsQueried_ThenCorrectSquaredLengthIsReturned)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    auto result = vec.LengthSquared();

    ASSERT_EQ(result, 34);
}

TEST(Vec2Test, GivenTwoVec2_WhenDotProductIsQueried_ThenCorrectDotProductIsReturned)
{
    units::length::meter_t x1{5};
    units::length::meter_t y1{3};
    Vec2 vec{x1, y1};

    units::length::meter_t x2{7};
    units::length::meter_t y2{1};
    Vec2 vecc{x2, y2};

    auto result = vec.Dot(vecc);

    ASSERT_EQ(result, 38);
}

TEST(Vec2Test, GivenAVec2_WhenRotateIsQueried_ThenCorrectRotateVec2IsReturned)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    units::angle::radian_t angle{0.5};

    auto result = vec.Rotate(angle);

    ASSERT_NEAR(result.x.value(), 2.94964, kAbsError);
    ASSERT_NEAR(result.y.value(), 5.02988, kAbsError);
}

TEST(Vec2Test, GivenAVec2_WhenRotateLeftIsQueried_ThenCorrectRotateLeftVec2IsReturned)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    auto result = vec.Rotate90Left();

    ASSERT_EQ(result.x.value(), -3.0);
    ASSERT_EQ(result.y.value(), 5.0);
}

TEST(Vec2Test, GivenAVec2_WhenRotateRightIsQueried_ThenCorrectRotateRightVec2IsReturned)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    auto result = vec.Rotate90Right();

    ASSERT_EQ(result.x.value(), 3.0);
    ASSERT_EQ(result.y.value(), -5.0);
}

TEST(Vec2Test, GivenAVec2WithoutAZValue_WhenConvertToVec3tIsQueried_ThenCorrectVec3)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    Vec2 vec{x, y};

    auto result = vec.ToVec3();

    ASSERT_EQ(result.x.value(), 5);
    ASSERT_EQ(result.y.value(), 3);
    ASSERT_EQ(result.z.value(), 0);
}

TEST(Vec2Test, GivenAVec2WithAZValue_WhenConvertToVec3tIsQueried_ThenCorrectVec3)
{
    units::length::meter_t x{5};
    units::length::meter_t y{3};
    units::length::meter_t z{1};
    Vec2 vec{x, y};

    auto result = vec.ToVec3(z);

    ASSERT_EQ(result.x.value(), 5);
    ASSERT_EQ(result.y.value(), 3);
    ASSERT_EQ(result.z.value(), 1);
}

}  // namespace road_logic_suite::types
