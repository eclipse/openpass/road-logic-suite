/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/road_mark_utils.h"

#include "RoadLogicSuite/Internal/Utils/converter_utils.h"

#include <iostream>
#include <unordered_map>

namespace road_logic_suite::types::utils
{

RoadMarkType ParseRoadMarkType(const std::string& type)
{
    const static std::unordered_map<std::string, RoadMarkType> string_to_odr_road_mark_type_map = {
        {"botts dots", RoadMarkType::BOTTS_DOTS},
        {"broken broken", RoadMarkType::BROKEN_BROKEN},
        {"broken solid", RoadMarkType::BROKEN_SOLID},
        {"broken", RoadMarkType::BROKEN},
        {"curb", RoadMarkType::CURB},
        {"custom", RoadMarkType::CUSTOM},
        {"edge", RoadMarkType::EDGE},
        {"grass", RoadMarkType::GRASS},
        {"none", RoadMarkType::NONE},
        {"solid broken", RoadMarkType::SOLID_BROKEN},
        {"solid solid", RoadMarkType::SOLID_SOLID},
        {"solid", RoadMarkType::SOLID}};

    if (auto it = string_to_odr_road_mark_type_map.find(road_logic_suite::utils::ToLower(type));
        it != string_to_odr_road_mark_type_map.end())
    {
        return it->second;
    }
    std::cout << "WARNING: Invalid road mark type input: " << type << ", please check your map file." << std::endl;

    return RoadMarkType::UNKNOWN;
}

RoadMarkWeight ParseRoadMarkWeight(const std::string& weight)
{
    const static std::unordered_map<std::string, RoadMarkWeight> string_to_odr_road_mark_weight_map = {
        {"bold", RoadMarkWeight::BOLD},
        {"standard", RoadMarkWeight::STANDARD},
    };

    if (auto it = string_to_odr_road_mark_weight_map.find(road_logic_suite::utils::ToLower(weight));
        it != string_to_odr_road_mark_weight_map.end())
    {
        return it->second;
    }

    std::cout << "WARNING: Invalid road mark weight input: " << weight << ", please check your map file." << std::endl;

    return RoadMarkWeight::UNKNOWN;
}

RoadMarkColor ParseRoadMarkColor(const std::string& color)
{
    const static std::unordered_map<std::string, RoadMarkColor> string_to_odr_road_mark_color_map = {
        {"blue", RoadMarkColor::BLUE},
        {"green", RoadMarkColor::GREEN},
        {"orange", RoadMarkColor::ORANGE},
        {"red", RoadMarkColor::RED},
        {"standard", RoadMarkColor::STANDARD},
        {"violet", RoadMarkColor::VIOLET},
        {"white", RoadMarkColor::WHITE},
        {"yellow", RoadMarkColor::YELLOW}};

    if (auto it = string_to_odr_road_mark_color_map.find(road_logic_suite::utils::ToLower(color));
        it != string_to_odr_road_mark_color_map.end())
    {
        return it->second;
    }

    std::cout << "WARNING: Invalid road mark color input: " << color << ", please check your map file." << std::endl;

    return RoadMarkColor::UNKNOWN;
}

RoadMarkLaneChange ParseRoadMarkLaneChange(const std::string& lane_change)
{
    const static std::unordered_map<std::string, RoadMarkLaneChange> string_to_odr_road_mark_lane_change_map = {
        {"both", RoadMarkLaneChange::BOTH},
        {"decrease", RoadMarkLaneChange::DECREASE},
        {"increase", RoadMarkLaneChange::INCREASE},
        {"none", RoadMarkLaneChange::NONE}};

    if (auto it = string_to_odr_road_mark_lane_change_map.find(road_logic_suite::utils::ToLower(lane_change));
        it != string_to_odr_road_mark_lane_change_map.end())
    {
        return it->second;
    }

    std::cout << "WARNING: Invalid road mark Lane Change input: " << lane_change << ", please check your map file."
              << std::endl;

    return RoadMarkLaneChange::UNKNOWN;
}

}  // namespace road_logic_suite::types::utils
