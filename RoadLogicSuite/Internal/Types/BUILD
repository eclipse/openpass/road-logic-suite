load("@rules_cc//cc:defs.bzl", "cc_library", "cc_test")

cc_library(
    name = "custom_units",
    hdrs = ["custom_units.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
)

cc_library(
    name = "lane",
    srcs = ["lane.cpp"],
    hdrs = ["lane.h"],
    visibility = ["//RoadLogicSuite/Internal/MapConversion:__pkg__"],
    deps = [
        ":poly3",
        ":range_based_map",
        ":road_barrier",
        ":road_mark",
    ],
)

cc_test(
    name = "lane_test",
    timeout = "short",
    srcs = ["lane_test.cpp"],
    deps = [
        ":lane",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "lane_section",
    srcs = ["lane_section.cpp"],
    hdrs = ["lane_section.h"],
    visibility = [
        "//RoadLogicSuite/Internal:__pkg__",
        "//RoadLogicSuite/Internal/Utils:__pkg__",
    ],
    deps = [":lane"],
)

cc_test(
    name = "lane_section_test",
    timeout = "short",
    srcs = ["lane_section_test.cpp"],
    deps = [
        ":lane_section",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "link_contact_type",
    hdrs = ["link_contact_type.h"],
    visibility = ["//RoadLogicSuite/Internal:__pkg__"],
)

cc_library(
    name = "object",
    hdrs = ["object.h"],
    visibility = ["//RoadLogicSuite/Internal:__pkg__"],
)

cc_library(
    name = "poly3",
    srcs = ["poly3.cpp"],
    hdrs = ["poly3.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
)

cc_test(
    name = "poly3_test",
    timeout = "short",
    srcs = ["poly3_test.cpp"],
    deps = [
        ":poly3",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "range_based_map",
    hdrs = ["range_based_map.h"],
)

cc_test(
    name = "range_based_map_test",
    timeout = "short",
    srcs = ["range_based_map_test.cpp"],
    deps = [
        ":range_based_map",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_library(
    name = "road_barrier",
    hdrs = ["road_barrier.h"],
)

cc_library(
    name = "road_link",
    hdrs = ["road_link.h"],
    visibility = ["//RoadLogicSuite/Internal:__pkg__"],
    deps = [
        ":link_contact_type",
        ":road_link_type",
    ],
)

cc_library(
    name = "road_link_type",
    hdrs = ["road_link_type.h"],
    visibility = ["//RoadLogicSuite/Internal:__pkg__"],
)

cc_library(
    name = "road_mark",
    hdrs = ["road_mark.h"],
    deps = ["@units_nhh"],
)

cc_library(
    name = "vec2",
    hdrs = ["vec2.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
    deps = ["@units_nhh"],
)

cc_test(
    name = "vec2_test",
    timeout = "short",
    srcs = ["vec2_test.cpp"],
    deps = [
        ":vec2",
        "@googletest//:gtest_main",
        "@mantle_api",
    ],
)

cc_library(
    name = "utils",
    srcs = ["road_mark_utils.cpp"],
    hdrs = ["road_mark_utils.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
    deps = [
        ":road_mark",
        "//RoadLogicSuite/Internal/Utils:converter_utils",
    ],
)

cc_test(
    name = "utils_test",
    timeout = "short",
    srcs = ["road_mark_utils_test.cpp"],
    deps = [
        ":utils",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "rect",
    hdrs = ["rect.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
    deps = [
        ":vec2",
        "@units_nhh",
    ],
)

cc_library(
    name = "junction",
    hdrs = ["junction.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
)

cc_library(
    name = "signal",
    hdrs = ["signal.h"],
    visibility = ["//RoadLogicSuite:__subpackages__"],
)
