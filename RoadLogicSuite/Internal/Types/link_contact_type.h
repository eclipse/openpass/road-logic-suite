/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LINK_CONTACT_TYPE_H
#define ROADLOGICSUITE_LINK_CONTACT_TYPE_H

namespace road_logic_suite
{
enum class LinkContactType
{
    /// @brief The road link points to the start of the linked road (s=0)
    kStart,

    /// @brief The road link points to the end of the linked road (s=road_length)
    kEnd,
};
}

#endif  // ROADLOGICSUITE_LINK_CONTACT_TYPE_H
