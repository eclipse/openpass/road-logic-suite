/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/lane.h"

namespace road_logic_suite::types
{
units::length::meter_t road_logic_suite::types::Lane::GetWidthAt(const units::length::meter_t& s) const
{
    const auto width_poly = width_polys.At(s);
    return units::length::meter_t(width_poly.CalcAt(s()));
}
}  // namespace road_logic_suite::types