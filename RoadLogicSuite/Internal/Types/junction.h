/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_JUNCTION_H
#define ROADLOGICSUITE_JUNCTION_H

#include <cstdint>
#include <string>
#include <vector>

namespace road_logic_suite::types
{
using LaneId = std::int32_t;

enum class JunctionType
{
    kStandard,
    kCrossing,
    kOther
};

enum class JunctionContactType
{
    /// @brief The connecting road connects the incoming road at the start point(s=0)
    kStart,

    /// @brief The connecting road connects the incoming road at the end point(s=road_length)
    kEnd,
};

struct JunctionLaneLink
{
    /// @brief ID of the incoming lane.
    LaneId from{};

    /// @brief ID of the connecting lane.
    LaneId to{};
};

struct JunctionConnection
{
    /// @brief Identifier according to OpenDRIVE specification.
    std::string id{};

    /// @brief Incoming roads contain lanes that lead into a junction.
    std::string incoming_road{};

    /// @brief Connecting roads link the roads that meet in a junction.
    std::string connecting_road{};

    /// @brief Type of contact point (start/end).
    JunctionContactType contact_type{};

    /// @brief The vector of junction lane links in this junction connection.
    std::vector<JunctionLaneLink> junction_lane_links;
};

struct JunctionPriority
{
    std::string high{};
    std::string low{};
};

struct JunctionController
{
    std::string id{};
    std::string type{};
    std::uint32_t sequence = 0;
};

struct StartLaneLink
{
    /// @brief s-coordinate of start point in linked road.
    double s;

    /// @brief Lane ID of roadAtStart for <startLaneLink>.
    LaneId from{};

    /// @brief Lane ID of crossing road.
    LaneId to{};
};

struct EndLaneLink
{
    /// @brief s-coordinate of end point in linked road.
    double s;

    /// @brief Lane ID of roadAtEnd for <endLaneLink>.
    LaneId from{};

    /// @brief Lane ID of crossing road.
    LaneId to{};
};

struct JunctionCrossPath
{
    /// @brief Unique ID within the junction according to OpenDRIVE specification.
    std::string id{};

    /// @brief ID of road defining the cross path.
    std::string crossing_road{};

    /// @brief ID of road at start point of the crossing road.
    std::string road_at_start{};

    /// @brief ID of road at end point of the crossing road.
    std::string road_at_end{};

    /// @brief Start lane link of cross paths.
    StartLaneLink start_lane_link{};

    /// @brief End lane link of cross paths.
    EndLaneLink end_lane_link{};
};

struct JunctionRoadSection
{
    /// @brief Unique ID within the junction.
    std::string id{};

    /// @brief ID of the road of this roadSection element.
    std::string road_id{};

    /// @brief Start position of the crossing junction in the road reference line coordinate system.
    double s_start{};

    /// @brief End position of the crossing junction in the road reference line coordinate system.
    double s_end{};
};

struct Junction
{
    /// @brief Identifier according to OpenDRIVE specification.
    std::string id{};

    /// @brief The name of junction.
    std::string name{};

    /// @brief The type of junction.
    JunctionType type{};

    /// @brief The vector of junction connections in this junction.
    std::vector<JunctionConnection> junction_connections{};

    /// @brief The vector of junction cross paths in this junction.
    std::vector<JunctionCrossPath> junction_cross_paths{};

    /// @brief The vector of junction controllers in this junction.
    std::vector<JunctionController> junction_controllers{};

    /// @brief The vector of junction priorities in this junction.
    std::vector<JunctionPriority> junction_priorities{};

    /// @brief The vector of junction priorities in this junction.
    std::vector<JunctionRoadSection> junction_road_sections{};
};

}  // namespace road_logic_suite::types
#endif  // ROADLOGICSUITE_JUNCTION_H