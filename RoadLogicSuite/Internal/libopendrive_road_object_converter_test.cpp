/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_road_object_converter.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "RoadLogicSuite/Tests/Utils/assertions.h"

#include <gmock/gmock.h>

namespace
{
constexpr double kAbsError = 1e-6;
constexpr double kSampleRoadLength = 100;
constexpr double kSampleRoadOriginPointX = -0.8;
constexpr double kSampleRoadOriginPointY = 14;
constexpr double kSampleRoadHeading = 3.349;
constexpr double kSampleSingleObjectSOffset = 78.2;
constexpr double kSampleSingleObjectTOffset = -3.52;

road_logic_suite::OpenDriveData CreateDataStorageWithSimpleStraightRoad()
{
    road_logic_suite::OpenDriveData data{};
    road_logic_suite::Road road{.id = "0",
                                .length = units::length::meter_t(kSampleRoadLength),
                                .index_geometries_start = 0,
                                .index_geometries_end = 1};
    data.roads.insert_or_assign("0", road);
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(kSampleRoadOriginPointX),
                                                       units::length::meter_t(kSampleRoadOriginPointY)),
        units::angle::radian_t(kSampleRoadHeading),
        units::length::meter_t(kSampleRoadLength),
        "0"));
    return data;
}

odr::Road CreateObjectsInRoad()
{
    odr::Road road("0", kSampleRoadLength, "-1", "TestRoad");
    odr::RoadObject singleObject("0",
                                 "0",
                                 kSampleSingleObjectSOffset,
                                 kSampleSingleObjectTOffset,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 "",
                                 "singleObject",
                                 "",
                                 "",
                                 false);
    road.id_to_object.insert_or_assign("0", singleObject);
    return road;
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenMapWithSimpleObject_WhenConverting_ThenObjectMustBeCorrectlyStored)
{
    road_logic_suite::OpenDriveData data = CreateDataStorageWithSimpleStraightRoad();
    const auto xml_road = CreateObjectsInRoad();

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto single_object = std::find_if(
        data.objects.begin(), data.objects.end(), [](const auto& obj) { return obj.name == "singleObject"; });
    ASSERT_TRUE(single_object != data.objects.end()) << "The object was not found in data storage!";
    // Geometry is a line, so expected position can be easily calculated.
    const mantle_api::Vec3<units::length::meter_t> expected_position(
        units::length::meter_t(kSampleRoadOriginPointX + kSampleSingleObjectSOffset * std::cos(kSampleRoadHeading) -
                               kSampleSingleObjectTOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(kSampleRoadOriginPointY + kSampleSingleObjectTOffset * std::cos(kSampleRoadHeading) +
                               kSampleSingleObjectSOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(0));
    ASSERT_THAT(single_object->position, road_logic_suite::test::InertialPositionIsNear(expected_position));
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRoadObjectOutsideOfRefLineBounds_WhenConverting_ThenErrorMustBeThrown)
{
    road_logic_suite::OpenDriveData data{};
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false));

    ASSERT_THROW(road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road), std::runtime_error);
}

std::unique_ptr<road_logic_suite::IGeometry> CreateLine(double s0,
                                                        double x0,
                                                        double y0,
                                                        double heading,
                                                        double length,
                                                        const std::string& road_id)
{
    return std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(s0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(x0), units::length::meter_t(y0)),
        units::angle::radian_t(heading),
        units::length::meter_t(length),
        road_id);
}

TEST(LibOpenDriveRoadObjectConverterTest,
     GivenRoadObjectWithOrientation_WhenConverting_ThenOrientationMustBeSetCorrectly)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 0, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    const double hdg = -1.759;
    const double pitch = -5.404;
    const double roll = 1.885;
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, hdg, pitch, roll, "", "", "", "", false));

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 1);
    EXPECT_NEAR(data.objects[0].hdg(), hdg, kAbsError);
    EXPECT_NEAR(data.objects[0].pitch(), pitch, kAbsError);
    EXPECT_NEAR(data.objects[0].roll(), roll, kAbsError);
}

TEST(LibOpenDriveroadObjectConverterTest,
     GivenRoadObjectWithOrientationOnRoadWithOrientation_WhenConverting_ThenOrientationMustBeSetCorrectly)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    const double road_hdg = 1;
    data.geometries.push_back(CreateLine(0, 0, 0, road_hdg, 0, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    const double hdg = -1.759;
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, hdg, 0, 0, "", "", "", "", false));

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 1);
    EXPECT_NEAR(data.objects[0].hdg(), road_hdg + hdg, kAbsError);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRectangularRoadObject_WhenConverting_ThenObjectShapeMustBeSetCorrectly)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 0, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 0, 0, 0, 6.49, 0, 8.09, 0, 0, 0, 0, 0, "", "", "", "", false));

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 1);
    ASSERT_EQ(data.objects[0].shape.size(), 4);
    const std::vector<std::array<double, 2>> expected_shape_points{{
        {3.245, 4.045},
        {-3.245, 4.045},
        {-3.245, -4.045},
        {3.245, -4.045},
    }};
    road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(data.objects[0].shape),
                                                expected_shape_points);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenCircularRoadObject_WhenConverting_ThenObjectShapeMustBeSetCorrectly)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 0, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 0, 0, 0, 0, 0, 0, 7.7, 0, 0, 0, 0, "", "", "", "", false));

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 1);
    ASSERT_EQ(data.objects[0].shape.size(), 8);
    const std::vector<std::array<double, 2>> expected_shape_points{{
        {7.7, 0},
        {5.444722, 5.444722},
        {0, 7.7},
        {-5.444722, 5.444722},
        {-7.7, 0},
        {-5.444722, -5.444722},
        {0, -7.7},
        {5.444722, -5.444722},
    }};
    road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(data.objects[0].shape),
                                                expected_shape_points);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRoadObjectWithType_WhenConverting_ThenTypeIsCopiedIntoDataStructure)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    xml_road.id_to_object.insert_or_assign(
        "0", odr::RoadObject("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Pole", "", "", "", false));

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 1);
    ASSERT_EQ(data.objects[0].type, "pole");
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenMinimalRepeatingObject_WhenConverting_ThenObjectIsCopiedIntoStructure)
{
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    // Object @ s=0, s=10, .... s=100 -> 11 Objects
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), 11);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithName_WhenConverting_ThenObjectIsCopiedIntoStructure)
{
    std::string object_name = "MyObject";
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", object_name, "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (const auto& objs : data.objects)
    {
        ASSERT_EQ(object_name, objs.name);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenSimpleRepeatedObject_WhenConverting_ThenPositionIsCorrect)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> expected_positions{
        {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(10), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(20), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(30), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(40), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(50), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(60), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(70), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(80), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(90), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(100), units::length::meter_t(0), units::length::meter_t(0)},
    };
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (std::size_t i = 0; i < data.objects.size(); i++)
    {
        ASSERT_THAT(data.objects[i].position, road_logic_suite::test::InertialPositionIsNear(expected_positions[i]))
            << "Element: " << i << "\n";
    }
}

TEST(LibOpenDriveRoadObjectConverterTest,
     GivenRepeatingObjectWithLengthOverDistanceNonInteger_WhenConverting_CorrectAmountMustBeCreated)
{
    const auto distance = 50;
    const auto length = 90;  // -> Causes two objects to be created @ 0 and 50.
    std::vector<mantle_api::Vec3<units::length::meter_t>> expected_positions{
        {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(distance), units::length::meter_t(0), units::length::meter_t(0)},
    };
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, length, "0"));
    odr::Road xml_road("0", length, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, length, distance, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_EQ(data.objects.size(), expected_positions.size());
    for (std::size_t i = 0; i < data.objects.size(); i++)
    {
        ASSERT_THAT(data.objects[i].position, road_logic_suite::test::InertialPositionIsNear(expected_positions[i]))
            << "Element: " << i << "\n";
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithHeading_WhenConverting_ThenObjectHeadingIsSet)
{
    const double hdg = 5.975;
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, hdg, 0, 0, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (const auto& obj : data.objects)
    {
        ASSERT_NEAR(hdg, obj.hdg(), kAbsError);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithPitch_WhenConverting_ThenObjectPitchIsSet)
{
    const double pitch = 4.291;
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, pitch, 0, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (const auto& obj : data.objects)
    {
        ASSERT_NEAR(pitch, obj.pitch(), kAbsError);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithRoll_WhenConverting_ThenObjectRollIsSet)
{
    const double roll = 4.637;
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, roll, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (const auto& obj : data.objects)
    {
        ASSERT_NEAR(roll, obj.roll(), kAbsError);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest,
     GivenRepeatingObjectWithChangingTOffset_WhenConverting_ThenObjectPositionIsSet)
{
    const double t_start = -20;
    const double t_end = -10;
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_positions{
        {units::length::meter_t(0), units::length::meter_t(-20), units::length::meter_t(0)},
        {units::length::meter_t(50), units::length::meter_t(-15), units::length::meter_t(0)},
        {units::length::meter_t(100), units::length::meter_t(-10), units::length::meter_t(0)},
    };
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 50, t_start, t_end, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (std::size_t i = 0; i < data.objects.size(); i++)
    {
        ASSERT_THAT(data.objects[i].position, road_logic_suite::test::InertialPositionIsNear(expected_positions[i]))
            << "Element: " << i << "\n";
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithType_WhenConverting_ThenObjectTypeIsSet)
{
    const std::string type = "Pole";
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, type, "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (const auto& obj : data.objects)
    {
        // Pole lowercase
        ASSERT_EQ("pole", obj.type);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest,
     GivenRepeatingObjectWithRectangularShape_WhenConverting_ThenShapeIsCorrectlySet)
{
    const std::vector<std::vector<std::array<double, 2>>> expected_shapes{
        {
            {0.5, 1.0},
            {-0.5, 1.0},
            {-0.5, -1.0},
            {0.5, -1.0},
        },
        {
            {0.75, 0.75},
            {-0.75, 0.75},
            {-0.75, -0.75},
            {0.75, -0.75},
        },
        {
            {1.0, 0.5},
            {-1.0, 0.5},
            {-1.0, -0.5},
            {1.0, -0.5},
        },
    };
    const auto width_start = 2.0;
    const auto width_end = 1.0;
    const auto length_start = 1.0;
    const auto length_end = 2.0;
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    odr::RoadObjectRepeat xml_obj_rep(0, 100, 50, 0, 0, width_start, width_end, 0, 0, 0, 0);
    // This is outside of scope for libOpenDrive, but the xml node itself allows us to access attributes:
    pugi::xml_document doc;
    xml_obj_rep.xml_node = doc.append_child("MockRepeatObject");
    xml_obj_rep.xml_node.append_attribute("lengthStart") = length_start;
    xml_obj_rep.xml_node.append_attribute("lengthEnd") = length_end;
    xml_obj.repeats.push_back(xml_obj_rep);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (std::size_t i = 0; i < data.objects.size(); i++)
    {
        road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(data.objects[i].shape),
                                                    expected_shapes[i]);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenRepeatingObjectWithCircularShape_WhenConverting_ThenShapeIsCorrectlySet)
{
    const std::vector<std::vector<std::array<double, 2>>> expected_shapes{
        {
            {1.0, 0.0},
            {0.707107, 0.707107},
            {0, 1.0},
            {-0.707107, 0.707107},
            {-1.0, 0.0},
            {-0.707107, -0.707107},
            {0.0, -1.0},
            {0.707107, -0.707107},
        },
        {
            {1.5, 0.0},
            {1.060660, 1.060660},
            {0, 1.5},
            {-1.060660, 1.060660},
            {-1.5, 0.0},
            {-1.060660, -1.060660},
            {0.0, -1.5},
            {1.060660, -1.060660},
        },
        {
            {2.0, 0.0},
            {1.414214, 1.414214},
            {0, 2.0},
            {-1.414214, 1.414214},
            {-2.0, 0.0},
            {-1.414214, -1.414214},
            {0.0, -2.0},
            {1.414214, -1.414214},
        },
    };
    const auto radius_start = 1.0;
    const auto radius_end = 2.0;
    road_logic_suite::OpenDriveData data{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data.geometries.push_back(CreateLine(0, 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", false);
    odr::RoadObjectRepeat xml_obj_rep(0, 100, 50, 0, 0, 0, 0, 0, 0, 0, 0);
    // This is outside of scope for libOpenDrive, but the xml node itself allows us to access attributes:
    pugi::xml_document doc;
    xml_obj_rep.xml_node = doc.append_child("MockRepeatObject");
    xml_obj_rep.xml_node.append_attribute("radiusStart") = radius_start;
    xml_obj_rep.xml_node.append_attribute("radiusEnd") = radius_end;
    xml_obj.repeats.push_back(xml_obj_rep);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    for (std::size_t i = 0; i < data.objects.size(); i++)
    {
        road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(data.objects[i].shape),
                                                    expected_shapes[i]);
    }
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObject_WhenConverting_ThenLaneBoundaryIsCreated)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = 0},
                                                           {.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_FALSE(data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers.empty());
}

TEST(LibOpenDriveRoadObjectConverterTest,
     GivenContinuousObjectWithTOffset_WhenConverting_ThenLaneBoundaryIsCreatedOnTheCorrectLane)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, -2.95, -2.95, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    ASSERT_FALSE(data.roads["0"].lane_sections.At(road_s0).GetLaneById(-1).road_barriers.empty());
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObject_WhenConverting_ThenSStartAndEndareRecordedInDataStorage)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(20, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].s_start(), 20);
    EXPECT_EQ(road_barriers[0].s_end(), 100);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObject_WhenConverting_ThenOffsetsAreRecordedInDataStorage)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, -0.05, 0.05, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].t_start(), -0.05);
    EXPECT_EQ(road_barriers[0].t_end(), 0.05);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObjectWithName_WhenConverting_ThenNameIsRecordedInStorage)
{
    const std::string& obj_name = "myname";
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", obj_name, "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, -0.05, 0.05, 0, 0, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].name, obj_name);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObjectZOffset_WhenConverting_ThenZOffsetIsRecordedInStorage)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, 0, 0, 0, 0, 0, 0, 0.79, 0.57);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].z_offset_start(), 0.79);
    EXPECT_EQ(road_barriers[0].z_offset_end(), 0.57);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObjectHeight_WhenConverting_ThenHeightIsRecordedInStorage)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, 0, 0, 0, 0, 8.4, 3.9, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].height_start(), 8.4);
    EXPECT_EQ(road_barriers[0].height_end(), 3.9);
}

TEST(LibOpenDriveRoadObjectConverterTest, GivenContinuousObjectWidth_WhenConverting_ThenWidthIsRecordedInStorage)
{
    const units::length::meter_t road_s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    const std::vector<road_logic_suite::types::Lane> lanes{{.id = -1, .width_polys = {{road_s0, constant_width}}}};
    const road_logic_suite::types::LaneSection section{.right_lanes = lanes, .center_lane = {.id = 0}};
    road_logic_suite::OpenDriveData data{
        .roads = {{"0",
                   {
                       .index_geometries_start = 0,
                       .index_geometries_end = 1,
                       .lane_sections = {{road_s0, section}},
                   }}},
    };
    data.geometries.push_back(CreateLine(road_s0(), 0, 0, 0, 100, "0"));
    odr::Road xml_road("0", 100, "-1", "TestRoad");
    odr::RoadObject xml_obj("0", "0", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "barrier", "", "", "", false);
    xml_obj.repeats.emplace_back(0, 100, 0, 0, 0, 0.8, 0.5, 0, 0, 0, 0);
    xml_road.id_to_object.insert_or_assign("0", xml_obj);

    road_logic_suite::LibOpenDriveRoadObjectConverter::ConvertForRoad(data, xml_road);

    const auto road_barriers = data.roads["0"].lane_sections.At(road_s0).GetLaneById(0).road_barriers;
    ASSERT_FALSE(road_barriers.empty());
    EXPECT_EQ(road_barriers[0].width_start(), 0.8);
    EXPECT_EQ(road_barriers[0].width_end(), 0.5);
}

}  // namespace
