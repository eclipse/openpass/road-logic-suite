/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/default_validation_config.h"

#include "Validators/road_length_validator.h"

#include <gtest/gtest.h>

#include <iostream>
#include <typeinfo>

namespace road_logic_suite::map_validation::validators
{
TEST(DefaultValidationConfigTest, GivenDefaultValidationConfig_WhenReturningRoadValidators_ThenSizeShouldBeOne)
{
    DefaultValidationConfig config{};

    const auto roadValidators = config.GetRoadValidators();

    ASSERT_EQ(1, roadValidators.size());

    auto roadValidator = roadValidators.at(0).get();

    ASSERT_FALSE(dynamic_cast<road_logic_suite::map_validation::RoadLengthValidator*>(roadValidator) == nullptr);
}
}  // namespace road_logic_suite::map_validation::validators