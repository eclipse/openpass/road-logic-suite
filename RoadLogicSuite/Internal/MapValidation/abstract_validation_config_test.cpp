/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/abstract_validation_config.h"

#include <gtest/gtest.h>

namespace road_logic_suite::map_validation::validators
{
// Requirement: Configurations should return an empty list, so that a new configuration does not need to implement all
// methods of the configuration.
TEST(AbstractValidationConfigTest, GivenAbstractValidationConfig_WhenReturningRoadValidators_ThenTheyMustBeEmpty)
{
    AbstractValidationConfig config{};

    const auto roadValidators = config.GetRoadValidators();

    ASSERT_EQ(0, roadValidators.size());
}
}  // namespace road_logic_suite::map_validation::validators