/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_DEFAULTVALIDATIONCONFIG_H
#define ROADLOGICSUITE_DEFAULTVALIDATIONCONFIG_H

#include "RoadLogicSuite/Internal/MapValidation/abstract_validation_config.h"

namespace road_logic_suite::map_validation
{
/// @brief A ValidationConfig which contains default validators
class DefaultValidationConfig : public AbstractValidationConfig
{
  public:
    /// @brief Get the default validators for the type Road
    std::vector<std::unique_ptr<IValidator<Road>>> GetRoadValidators() override;
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_DEFAULTVALIDATIONCONFIG_H
