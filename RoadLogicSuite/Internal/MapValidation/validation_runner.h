/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_VALIDATION_RUNNER_H
#define ROADLOGICSUITE_VALIDATION_RUNNER_H

#include "RoadLogicSuite/Internal/MapValidation/abstract_validation_config.h"
#include "RoadLogicSuite/Internal/MapValidation/default_validation_config.h"
#include "RoadLogicSuite/Internal/MapValidation/map_error.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <memory>
#include <vector>

namespace road_logic_suite::map_validation
{
/// @brief This class is responsible for running the validations and returning the findings.
class ValidationRunner
{
  public:
    explicit ValidationRunner(const OpenDriveData& data_storage)
        : data_storage_(data_storage), config_(std::make_unique<DefaultValidationConfig>())
    {
    }

    /// @brief Sets up a non-standard configuration for the validation runner.
    /// @note The existing config will not be considered anymore.
    /// @remark This moves the ownership of the configuration into the validation runner class.
    void SetConfiguration(std::unique_ptr<AbstractValidationConfig> config);

    /// @brief Validates the map with all validators defined in the validation config.
    /// @return All errors that were found related to the map.
    [[nodiscard]] std::vector<MapError> Validate() const;

  private:
    const OpenDriveData& data_storage_{};
    std::unique_ptr<AbstractValidationConfig> config_{};
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_VALIDATION_RUNNER_H
