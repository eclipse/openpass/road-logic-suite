/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_LENGTH_VALIDATOR_H
#define ROADLOGICSUITE_ROAD_LENGTH_VALIDATOR_H

#include "RoadLogicSuite/Internal/MapValidation/Validators/i_validator.h"
#include "RoadLogicSuite/Internal/road.h"

#include <units.h>

#include <numeric>

namespace road_logic_suite::map_validation
{
/// @brief This simple validator checks, whether the sum of the planview geometries in the OpenDRIVE file are equal to
/// the total length of the road.
class RoadLengthValidator : public IValidator<Road>
{
  public:
    /// @brief Validates the map with all validators defined in the validation config.
    /// @return All errors that were found related to the map.
    [[nodiscard]] std::vector<MapError> Validate(const Road& road,
                                                 const OpenDriveData& data,
                                                 const std::string& object_identifier) override;
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_ROAD_LENGTH_VALIDATOR_H
