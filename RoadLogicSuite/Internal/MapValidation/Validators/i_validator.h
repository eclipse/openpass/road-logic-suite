/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_I_VALIDATOR_H
#define ROADLOGICSUITE_I_VALIDATOR_H

#include "RoadLogicSuite/Internal/MapValidation/map_error.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <memory>
#include <vector>

namespace road_logic_suite::map_validation
{
/// @brief Validator interface.
/// @tparam TObject The object that is being validated.
template <typename TObject>
class IValidator
{
  public:
    virtual ~IValidator() = default;

    /// @brief Validates the given object.
    /// @param tested_object The object under test.
    /// @param data_storage The data storage.
    /// @param object_identifier An optional id that can be used to construct helpful error messages.
    /// @return A collection of map errors.
    virtual std::vector<MapError> Validate(const TObject& test_object,
                                           const OpenDriveData& data_storage,
                                           const std::string& object_identifier) = 0;

  protected:
    IValidator() = default;
    IValidator(const IValidator&) = default;
    IValidator(IValidator&&) = default;
    IValidator& operator=(const IValidator&) = default;
    IValidator& operator=(IValidator&&) = default;
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_I_VALIDATOR_H
