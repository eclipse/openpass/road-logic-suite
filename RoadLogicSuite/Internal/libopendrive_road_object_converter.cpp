/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "libopendrive_road_object_converter.h"

#include "RoadLogicSuite/Internal/Utils/converter_utils.h"
#include "RoadLogicSuite/Internal/Utils/math_utils.h"
#include "RoadLogicSuite/Internal/libopendrive_converter_helpers.h"

namespace road_logic_suite
{

Object CreateObject(const CoordinateConverter& converter,
                    const odr::Road& xml_road,
                    const odr::RoadObject& xml_obj,
                    const std::vector<Vec2<units::length::meter_t>>& shape,
                    double s,
                    double t)
{
    const auto road_heading = converter.GetRoadPositionHeading(xml_obj.road_id, units::length::meter_t(s));
    const auto position = helpers::GetInertialPosFromReferenceLinePos(converter, xml_road, s, t, xml_obj.z0);
    auto valid_lanes = helpers::ConvertLaneValidity(xml_obj.xml_node);
    return {
        .name = xml_obj.name,
        .road_id = xml_road.id,
        .position = position,
        .hdg = units::angle::radian_t(xml_obj.hdg) + road_heading.value(),
        .pitch = units::angle::radian_t(xml_obj.pitch),
        .roll = units::angle::radian_t(xml_obj.roll),
        .shape = shape,
        .height = units::length::meter_t(xml_obj.height),
        .type = utils::ToLower(xml_obj.type),
        .valid_lanes = std::move(valid_lanes),
    };
}

std::vector<Vec2<units::length::meter_t>> CreateShapeForSingleObject(const odr::RoadObject& xml_obj)
{
    if (xml_obj.length != 0 && xml_obj.width != 0)
    {
        return math_utils::CreateRectangularShape(xml_obj.length, xml_obj.width);
    }
    else if (xml_obj.radius != 0)
    {
        return math_utils::CreateCircularShape(xml_obj.radius);
    }
    return {};
}

auto IsRectangularShape(const odr::RoadObjectRepeat& xml_obj_rep)
{
    return xml_obj_rep.width_start != 0 && xml_obj_rep.width_end != 0 &&
           xml_obj_rep.xml_node.attribute("lengthStart").as_double() != 0 &&
           xml_obj_rep.xml_node.attribute("lengthEnd").as_double() != 0;
}

auto IsCircularShape(const odr::RoadObjectRepeat& xml_obj_rep)
{
    return xml_obj_rep.xml_node.attribute("radiusStart").as_double() != 0 &&
           xml_obj_rep.xml_node.attribute("radiusEnd").as_double() != 0;
}

int GetAmountRepeatedObjects(const odr::RoadObjectRepeat& xml_obj_rep)
{
    return static_cast<int>(std::floor(xml_obj_rep.length / xml_obj_rep.distance)) + 1;
}

auto CreateShapesForRepeatedObjects(const odr::RoadObjectRepeat& xml_obj_rep)
{
    const int amount_objects = GetAmountRepeatedObjects(xml_obj_rep);
    std::vector<std::vector<Vec2<units::length::meter_t>>> shapes(amount_objects,
                                                                  std::vector<Vec2<units::length::meter_t>>{});
    if (IsRectangularShape(xml_obj_rep))
    {
        const auto linspaced_width =
            math_utils::LinSpace(xml_obj_rep.width_start, xml_obj_rep.width_end, amount_objects);
        const auto linspaced_length = math_utils::LinSpace(xml_obj_rep.xml_node.attribute("lengthStart").as_double(),
                                                           xml_obj_rep.xml_node.attribute("lengthEnd").as_double(),
                                                           amount_objects);
        for (int i = 0; i < amount_objects; i++)
        {
            shapes[i] = math_utils::CreateRectangularShape(linspaced_length[i], linspaced_width[i]);
        }
    }
    else if (IsCircularShape(xml_obj_rep))
    {
        const auto linspaced_radius = math_utils::LinSpace(xml_obj_rep.xml_node.attribute("radiusStart").as_double(),
                                                           xml_obj_rep.xml_node.attribute("radiusEnd").as_double(),
                                                           amount_objects);
        for (int i = 0; i < amount_objects; i++)
        {
            shapes[i] = math_utils::CreateCircularShape(linspaced_radius[i]);
        }
    }
    return shapes;
}

void ConvertRepeatedObjects(OpenDriveData& data_storage,
                            const odr::Road& xml_road,
                            const odr::RoadObject& xml_obj,
                            const CoordinateConverter& converter)
{
    for (const auto& xml_obj_rep : xml_obj.repeats)
    {
        if (xml_obj_rep.distance == 0)
        {
            const units::length::meter_t s0_meters(xml_obj_rep.s0);
            const RoadBarrier barrier{
                .s_start = s0_meters,
                .s_end = s0_meters + units::length::meter_t(xml_obj_rep.length),
                .t_start = units::length::meter_t(xml_obj_rep.t_start),
                .t_end = units::length::meter_t(xml_obj_rep.t_end),
                .z_offset_start = units::length::meter_t(xml_obj_rep.z_offset_start),
                .z_offset_end = units::length::meter_t(xml_obj_rep.z_offset_end),
                .height_start = units::length::meter_t(xml_obj_rep.height_start),
                .height_end = units::length::meter_t(xml_obj_rep.height_end),
                .width_start = units::length::meter_t(xml_obj_rep.width_start),
                .width_end = units::length::meter_t(xml_obj_rep.width_end),
                .name = xml_obj.name,
            };
            data_storage.roads[xml_road.id]
                .lane_sections.Find(s0_meters)
                ->second.GetLaneByBoundaryOffset(s0_meters, units::length::meter_t(xml_obj_rep.t_start))
                .road_barriers.push_back(barrier);
        }
        else
        {
            const int amount_objects = GetAmountRepeatedObjects(xml_obj_rep);
            const auto t_diff = (xml_obj_rep.t_end - xml_obj_rep.t_start) / (amount_objects - 1);
            const auto shapes = CreateShapesForRepeatedObjects(xml_obj_rep);
            for (auto i = 0; i < amount_objects; i++)
            {
                const auto s = xml_obj_rep.s0 + i * xml_obj_rep.distance;
                const auto t = xml_obj_rep.t_start + i * t_diff;
                const auto obj = CreateObject(converter, xml_road, xml_obj, shapes[i], s, t);
                data_storage.objects.push_back(obj);
            }
        }
    }
}

void ConvertSingleObject(OpenDriveData& data_storage,
                         const odr::Road& xml_road,
                         const odr::RoadObject& xml_obj,
                         const CoordinateConverter& converter)
{
    const auto shape = CreateShapeForSingleObject(xml_obj);
    const auto obj = CreateObject(converter, xml_road, xml_obj, shape, xml_obj.s0, xml_obj.t0);
    data_storage.objects.push_back(obj);
}

void LibOpenDriveRoadObjectConverter::ConvertForRoad(OpenDriveData& data_storage, const odr::Road& xml_road)
{
    const CoordinateConverter converter(data_storage);
    for (const auto& xml_obj : xml_road.get_road_objects())
    {
        if (!xml_obj.repeats.empty())
        {
            ConvertRepeatedObjects(data_storage, xml_road, xml_obj, converter);
        }
        else
        {
            ConvertSingleObject(data_storage, xml_road, xml_obj, converter);
        }
    }
}
}  // namespace road_logic_suite
