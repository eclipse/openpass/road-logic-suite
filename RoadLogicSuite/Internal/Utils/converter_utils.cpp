/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "converter_utils.h"

#include <algorithm>

namespace road_logic_suite::utils
{
std::string ToLower(const std::string& str)
{
    std::string result{};
    result.resize(str.size());

    std::transform(str.begin(), str.end(), result.begin(), ::tolower);
    return result;
}

}  // namespace road_logic_suite::utils

namespace road_logic_suite
{
units::length::meter_t GetBoundaryTValueAt(const units::length::meter_t& s,
                                           const road_logic_suite::types::LaneSection& section,
                                           const road_logic_suite::types::Lane::LaneId& lane_id)
{
    const auto abs_lane_id = llabs(lane_id);
    const auto lane_id_sign = (lane_id > 0) - (lane_id < 0);
    units::length::meter_t t(0);
    // When using the sign, we can always loop upwards and just re-insert the sign of the lane id when fetching the
    // width from the data structure.
    for (auto i = 1; i <= abs_lane_id; i++)
    {
        t += section.GetLaneById(lane_id_sign * i).GetWidthAt(s);
    }
    return lane_id_sign * t;
}

units::length::meter_t GetLaneCenterTValueAt(const units::length::meter_t& s,
                                             const road_logic_suite::types::LaneSection& section,
                                             const road_logic_suite::types::Lane::LaneId& lane_id)
{
    const auto lane_id_sign = (lane_id > 0) - (lane_id < 0);
    const auto width_at_boundary_towards_ref_line =
        road_logic_suite::GetBoundaryTValueAt(s, section, lane_id - lane_id_sign);
    const auto half_lane_width = section.GetLaneById(lane_id).GetWidthAt(s) / 2;
    return width_at_boundary_towards_ref_line + lane_id_sign * half_lane_width;
}
}  // namespace road_logic_suite
