/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef ROADLOGICSUITE_MATH_UTILS_H
#define ROADLOGICSUITE_MATH_UTILS_H

#include "RoadLogicSuite/Internal/Types/vec2.h"

#include <units.h>

#include <vector>

namespace road_logic_suite::math_utils
{
inline constexpr units::angle::radian_t kPi = units::angle::radian_t(M_PI);
inline constexpr units::angle::radian_t kPiOver2 = units::angle::radian_t(M_PI_2);
inline constexpr units::angle::radian_t kPiOver4 = units::angle::radian_t(M_PI_4);
inline constexpr units::length::meter_t kSqrt1_2 = units::length::meter_t(M_SQRT1_2);

/// @brief Returns the value of the signum function for any unit type.
/// @tparam T the unit type, i.e. meters.
/// @param val The value.
/// @return -1 if the value is negative. <br>
///          1 if the value is positive. <br>
///          0 if the value is 0.
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
units::dimensionless::scalar_t Signum(const T& val)
{
    return (T(0) < val) - (val < T(0));
}

/// @brief Returns the fraction of the value \p val relative to the range \p min and \p max.
/// @param min The min value of the range.
/// @param max The max value of the range.
/// @result The fraction relative to the range.
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
units::dimensionless::scalar_t InverseLerp(const T& min, const T& max, const T& val)
{
    return (val - min) / (max - min);
}

/// @brief Returns a value relative to the range \p min and \p max based on the fraction \p val.
/// @param min The min value of the range.
/// @param max The max value of the range.
/// @result The value between min and max.
template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
T Lerp(const T& min, const T& max, const units::dimensionless::scalar_t& val)
{
    return min + val * (max - min);
}
/// @brief Return evenly spaced numbers over a specified interval. Returns \p num_entries evenly spaced samples,
/// calculated over the interval [\p start, \p end].
/// @param start The start of the interval.
/// @param end The end of the interval.
/// @param num_entries The amount of entries.
/// @return Evenly spaced numbers.
std::vector<double> LinSpace(double start, double end, int num_entries);

/// @brief Returns a rectangular shape from the given length and width. The shape contains the four corner points of the
/// rectangle.
/// @param length The length
/// @param width The width
/// @return The rectangular shape.
std::vector<Vec2<units::length::meter_t>> CreateRectangularShape(double length, double width);

/// @breif Returns a circular shape from the given radius. The shape contains 8 sample points.
/// @param radius The radius of the circle.
/// @return The circular shape.
std::vector<Vec2<units::length::meter_t>> CreateCircularShape(double radius);

}  // namespace road_logic_suite::math_utils

#endif  // ROADLOGICSUITE_MATH_UTILS_H
