/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Utils/math_utils.h"

#include "RoadLogicSuite/Tests/Utils/assertions.h"

#include <gmock/gmock.h>

namespace
{

TEST(MathUtilsTest, GivenARangeFrom0To100ToBeSplitInto11Values_WhenCallingLinSpace_ValuesMustBeCorrect)
{
    const auto actual_result = road_logic_suite::math_utils::LinSpace(0, 100, 11);
    ASSERT_THAT(actual_result,
                ::testing::ElementsAre(0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0));
}

TEST(MathUtilsTest, GivenALengthAndWidth_WhenCreatingARectangularShape_TheCorrectValuesAreReturned)
{
    const auto length = 2.0;
    const auto width = 1.0;
    const std::vector<std::array<double, 2>> exp_shp{{1.0, 0.5}, {-1.0, 0.5}, {-1.0, -0.5}, {1.0, -0.5}};

    const auto shp = road_logic_suite::math_utils::CreateRectangularShape(length, width);

    road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(shp), exp_shp);
}

TEST(MathUtilsTest, GivenARadius_WhenCreatingACircularShape_TheCorrectValuesAreReturned)
{
    const double kSqrt1_2 = road_logic_suite::math_utils::kSqrt1_2.value();
    const auto radius = 1.0;

    const std::vector<std::array<double, 2>> exp_shp{{1.0, 0.0},
                                                     {kSqrt1_2, kSqrt1_2},
                                                     {0.0, 1.0},
                                                     {-kSqrt1_2, kSqrt1_2},
                                                     {-1.0, 0.0},
                                                     {-kSqrt1_2, -kSqrt1_2},
                                                     {0.0, -1.0},
                                                     {kSqrt1_2, -kSqrt1_2}};

    const auto shp = road_logic_suite::math_utils::CreateCircularShape(radius);

    road_logic_suite::test::AssertShapeEquality(road_logic_suite::test::ConvertToDouble(shp), exp_shp);
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
void TestSignumFunction(const T& val, units::dimensionless::scalar_t exp)
{
    auto res = road_logic_suite::math_utils::Signum(val);
    ASSERT_EQ(res, exp);
}

TEST(MathUtilsTest, GivenSomeValues_WhenUsingSignum_TheCorrectValuesAreReturned)
{
    const units::dimensionless::scalar_t kNeg = -1;
    const units::dimensionless::scalar_t kZero = 0;
    const units::dimensionless::scalar_t kPos = 1;

    TestSignumFunction(units::length::meter_t(-2.0), kNeg);
    TestSignumFunction(units::length::meter_t(-1.0), kNeg);
    TestSignumFunction(units::length::meter_t(0.0), kZero);
    TestSignumFunction(units::length::meter_t(1.0), kPos);
    TestSignumFunction(units::length::meter_t(2.0), kPos);

    TestSignumFunction(-units::angle::radian_t(road_logic_suite::math_utils::kPi), kNeg);
    TestSignumFunction(units::angle::radian_t(0.0), kZero);
    TestSignumFunction(units::angle::radian_t(road_logic_suite::math_utils::kPi), kPos);
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
void TestLerpFunction(const T& min, const T& max, units::dimensionless::scalar_t val, const T& exp)
{
    auto res = road_logic_suite::math_utils::Lerp(min, max, val);
    ASSERT_EQ(res, exp);
}

TEST(MathUtilsTest, GivenSomeValues_WhenUsingLerp_TheCorrectValuesAreReturned)
{
    TestLerpFunction(units::length::meter_t(-2.0),
                     units::length::meter_t(8.0),
                     units::dimensionless::scalar_t(0.5),
                     units::length::meter_t(3));

    TestLerpFunction(units::length::meter_t(8.0),
                     units::length::meter_t(-2.0),
                     units::dimensionless::scalar_t(0.75),
                     units::length::meter_t(0.5));

    TestLerpFunction(-units::angle::radian_t(road_logic_suite::math_utils::kPi),
                     units::angle::radian_t(road_logic_suite::math_utils::kPi),
                     units::dimensionless::scalar_t(0.75),
                     units::angle::radian_t(road_logic_suite::math_utils::kPiOver2.value()));

    TestLerpFunction(units::angle::radian_t(road_logic_suite::math_utils::kPi),
                     -units::angle::radian_t(road_logic_suite::math_utils::kPi),
                     units::dimensionless::scalar_t(0.75),
                     -units::angle::radian_t(road_logic_suite::math_utils::kPiOver2.value()));
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
void TestInverseLerpFunction(const T& min, const T& max, const T& val, units::dimensionless::scalar_t exp)
{
    auto res = road_logic_suite::math_utils::InverseLerp(min, max, val);
    ASSERT_EQ(res, exp);
}

TEST(MathUtilsTest, GivenSomeValues_WhenUsingInverseLerp_TheCorrectValuesAreReturned)
{
    TestInverseLerpFunction(units::length::meter_t(-2.0),
                            units::length::meter_t(8.0),
                            units::length::meter_t(3),
                            units::dimensionless::scalar_t(0.5));

    TestInverseLerpFunction(units::length::meter_t(8.0),
                            units::length::meter_t(-2.0),
                            units::length::meter_t(0.5),
                            units::dimensionless::scalar_t(0.75));

    TestInverseLerpFunction(-units::angle::radian_t(road_logic_suite::math_utils::kPi),
                            units::angle::radian_t(road_logic_suite::math_utils::kPi),
                            units::angle::radian_t(road_logic_suite::math_utils::kPiOver2.value()),
                            units::dimensionless::scalar_t(0.75));

    TestInverseLerpFunction(units::angle::radian_t(road_logic_suite::math_utils::kPi),
                            -units::angle::radian_t(road_logic_suite::math_utils::kPi),
                            -units::angle::radian_t(road_logic_suite::math_utils::kPiOver2.value()),
                            units::dimensionless::scalar_t(0.75));
}

}  // namespace
