/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_I_GEOMETRY_LOOKUP_H
#define ROADLOGICSUITE_I_GEOMETRY_LOOKUP_H

#include "Geometry/i_geometry.h"

#include <units.h>

#include <functional>

namespace road_logic_suite
{

class IGeometryLookup
{
  public:
    virtual ~IGeometryLookup() = default;

    /// @brief May optimize the geometry with an internal RTree for a faster search.
    /// Can influence the processing speed of subsequent calls of VisitGeometry.
    virtual void OptimizeStorage() = 0;

    /// @brief Executes the given function for all stored geometries.
    /// If the function returns false, the execution is aborted.
    /// @param search_callback Function which is called for the geometry
    virtual void VisitGeometry(const std::function<bool(const IGeometry*)>& search_callback) const = 0;

    /// @brief Visits only some geometries which are near to the given search point. Should only be called if the
    /// minimum distance between the given point and the geometry is searched.
    /// @param search_point A point near to the geometry
    /// @param search_callback Function which is called for every near geometry
    virtual void VisitGeometry(const Vec2<units::length::meter_t>& search_point,
                               const std::function<bool(const IGeometry*)>& search_callback) const = 0;
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_I_GEOMETRY_LOOKUP_H
