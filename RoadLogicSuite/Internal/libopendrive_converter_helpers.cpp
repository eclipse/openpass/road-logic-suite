/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_converter_helpers.h"

namespace road_logic_suite
{
namespace helpers
{

LinkContactType GetContactType(odr::RoadLink::ContactPoint xml_contact_point)
{
    switch (xml_contact_point)
    {
        case odr::RoadLink::ContactPoint_End:
            return LinkContactType::kEnd;
        case odr::RoadLink::ContactPoint_Start:
            return LinkContactType::kStart;
        default:
            throw std::runtime_error("Invalid Contact Point Type");
    }
}

types::JunctionContactType GetContactType(odr::JunctionConnection::ContactPoint xml_connection_contact_point)
{
    switch (xml_connection_contact_point)
    {
        case odr::JunctionConnection::ContactPoint::ContactPoint_End:
            return types::JunctionContactType::kEnd;
        case odr::JunctionConnection::ContactPoint::ContactPoint_Start:
            return types::JunctionContactType::kStart;
        default:
            throw std::runtime_error("Invalid Contact Point Type");
    }
}

std::vector<RoadLink> CreateRoadLinks(const odr::Road& xml_road)
{
    std::vector<RoadLink> road_links{};
    if (xml_road.predecessor.type == odr::RoadLink::Type_Road)
    {
        road_links.push_back(RoadLink{
            .target_id = xml_road.predecessor.id,
            .link_type = RoadLinkType::kPredecessor,
            .contact_type = GetContactType(xml_road.predecessor.contact_point),
        });
    }
    if (xml_road.successor.type == odr::RoadLink::Type_Road)
    {
        road_links.push_back(RoadLink{
            .target_id = xml_road.successor.id,
            .link_type = RoadLinkType::kSuccessor,
            .contact_type = GetContactType(xml_road.successor.contact_point),
        });
    }
    return road_links;
}

std::vector<types::JunctionLaneLink> CreateJunctionLinks(const odr::JunctionConnection& xml_junction_connection)
{
    const auto& xml_junction_lane_links = xml_junction_connection.lane_links;

    std::vector<types::JunctionLaneLink> junction_lane_links{};
    junction_lane_links.reserve(xml_junction_lane_links.size());

    for (const auto& element : xml_junction_lane_links)
    {
        junction_lane_links.push_back(types::JunctionLaneLink{
            .from = element.from,
            .to = element.to,
        });
    }

    return junction_lane_links;
}

std::vector<types::LaneId> ConvertLaneValidity(const pugi::xml_node& xml_node)
{
    if (!xml_node.child("validity"))
    {
        return {};
    }

    const auto& validity_node = xml_node.child("validity");

    const auto from_lane_id = validity_node.attribute("fromLane").as_int(0);
    const auto to_lane_id = validity_node.attribute("toLane").as_int(0);

    const auto num_elements = std::abs(to_lane_id - from_lane_id) + 1;
    std::vector<types::LaneId> result;
    result.reserve(num_elements);

    const auto increment = (from_lane_id <= to_lane_id) ? 1 : -1;
    for (int i = from_lane_id; i != to_lane_id + increment; i += increment)
    {
        if (i == 0)
        {
            continue;
        }
        result.push_back(i);
    }

    return result;
}

mantle_api::Vec3<units::length::meter_t> GetInertialPosFromReferenceLinePos(const CoordinateConverter& converter,
                                                                            const odr::Road& xml_road,
                                                                            double s,
                                                                            double t,
                                                                            double z_offset)
{
    auto position = converter.ConvertRoadPositionToInertialCoordinates({
        .road = xml_road.id,
        .s_offset = units::length::meter_t(s),
        .t_offset = units::length::meter_t(t),
    });
    if (!position)
    {
        const auto msg = "Error: Object found @ s=" + std::to_string(s) + ", but length of road is " +
                         std::to_string(xml_road.length);
        throw std::runtime_error(msg);
    }
    position->z += units::length::meter_t(z_offset);
    return position.value();
}

}  // namespace helpers
}  // namespace road_logic_suite
