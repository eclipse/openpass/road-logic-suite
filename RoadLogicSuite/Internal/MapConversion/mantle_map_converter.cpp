/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_barrier_converter.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_junction_converter.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_lane_linker.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_road_object_converter.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_road_signal_converter.h"
#include "RoadLogicSuite/Internal/Utils/converter_utils.h"

#include <cassert>

namespace road_logic_suite::map_conversion
{
map_api::Map MantleMapConverter::Convert(const OpenDriveData& data_storage)
{
    map_.projection_string = data_storage.projection;

    for (const auto& road : data_storage.roads)
    {
        ConvertRoad(road.second, data_storage);
    }

    MantleJunctionConverter::DeduceLaneRelationFromJunctions(data_storage, id_provider_, id_to_lane_relation_);
    CreateLaneRelations();

    MantleRoadObjectConverter::ConvertObjects(data_storage, map_, id_provider_);

    MantleRoadSignalConverter::ConvertSignals(data_storage, map_, id_provider_);

    return std::move(map_);
}

void MantleMapConverter::ConvertRoad(const Road& road, const OpenDriveData& data_storage)
{
    CoordinateConverter converter(data_storage);

    for (const auto& lane_section : road.lane_sections)
    {
        auto reference_line =
            CreateReferenceLine(converter, road, lane_section.second.s_start, lane_section.second.s_end);

        const auto& lane = lane_section.second.center_lane;

        auto [physical_center_boundaries_right, logical_center_boundaries_right] =
            CreateLaneBoundariesForLane(converter, road, lane_section.second, lane, reference_line);
        auto [physical_center_boundaries_left, logical_center_boundaries_left] =
            CreateLaneBoundariesForLane(converter, road, lane_section.second, lane, reference_line);
        std::reverse(physical_center_boundaries_left.begin(), physical_center_boundaries_left.end());

        bool is_left_side = false;
        auto [right_lanes, right_logical_lanes] =
            CreateLanes(data_storage, converter, road, lane_section.second, is_left_side);
        if (!right_lanes.empty())
        {
            auto [physical_lane_boundaries, logical_lane_boundaries] =
                CreateLaneBoundaries(converter,
                                     road,
                                     lane_section.second,
                                     right_lanes,
                                     right_logical_lanes,
                                     physical_center_boundaries_right,
                                     logical_center_boundaries_right,
                                     reference_line,
                                     is_left_side);

            // Create lane_group and set this in map_api::map
            std::unique_ptr<map_api::LaneGroup> lane_group = std::make_unique<map_api::LaneGroup>();
            lane_group->id = id_provider_lane_group_.GetNewId();
            lane_group->type = utils::IsJunction(road.road_assigned_junction_id) ? map_api::LaneGroup::Type::kJunction
                                                                                 : map_api::LaneGroup::Type::kOneWay;
            auto ref_right_lanes = utils::GetLanesRefs(right_lanes);
            lane_group->lanes.insert(lane_group->lanes.end(),
                                     std::make_move_iterator(ref_right_lanes.begin()),
                                     std::make_move_iterator(ref_right_lanes.end()));

            auto ref_center_boundaries_right = utils::GetLaneBoundariesRefs(physical_center_boundaries_right);
            lane_group->lane_boundaries.insert(lane_group->lane_boundaries.end(),
                                               std::make_move_iterator(ref_center_boundaries_right.begin()),
                                               std::make_move_iterator(ref_center_boundaries_right.end()));

            auto ref_lane_boundaries = utils::GetLaneBoundariesRefs(physical_lane_boundaries);
            lane_group->lane_boundaries.insert(lane_group->lane_boundaries.end(),
                                               std::make_move_iterator(ref_lane_boundaries.begin()),
                                               std::make_move_iterator(ref_lane_boundaries.end()));

            map_.lane_groups.push_back(std::move(lane_group));

            map_.lane_boundaries.insert(map_.lane_boundaries.end(),
                                        std::make_move_iterator(physical_lane_boundaries.begin()),
                                        std::make_move_iterator(physical_lane_boundaries.end()));

            map_.lane_boundaries.insert(map_.lane_boundaries.end(),
                                        std::make_move_iterator(physical_center_boundaries_right.begin()),
                                        std::make_move_iterator(physical_center_boundaries_right.end()));

            map_.logical_lane_boundaries.insert(map_.logical_lane_boundaries.end(),
                                                std::make_move_iterator(logical_lane_boundaries.begin()),
                                                std::make_move_iterator(logical_lane_boundaries.end()));
            map_.logical_lane_boundaries.insert(map_.logical_lane_boundaries.end(),
                                                std::make_move_iterator(logical_center_boundaries_right.begin()),
                                                std::make_move_iterator(logical_center_boundaries_right.end()));
            std::move(right_lanes.begin(), right_lanes.end(), std::back_inserter(map_.lanes));
            std::move(right_logical_lanes.begin(), right_logical_lanes.end(), std::back_inserter(map_.logical_lanes));
        }

        is_left_side = true;
        auto [left_lanes, left_logical_lanes] =
            CreateLanes(data_storage, converter, road, lane_section.second, is_left_side);
        if (!left_lanes.empty())
        {

            auto [physical_lane_boundaries, logical_lane_boundaries] =
                CreateLaneBoundaries(converter,
                                     road,
                                     lane_section.second,
                                     left_lanes,
                                     left_logical_lanes,
                                     physical_center_boundaries_left,
                                     logical_center_boundaries_left,
                                     reference_line,
                                     is_left_side);

            // Create lane_group and set this in map_api::map
            std::unique_ptr<map_api::LaneGroup> lane_group = std::make_unique<map_api::LaneGroup>();
            lane_group->id = id_provider_lane_group_.GetNewId();
            lane_group->type = utils::IsJunction(road.road_assigned_junction_id) ? map_api::LaneGroup::Type::kJunction
                                                                                 : map_api::LaneGroup::Type::kOneWay;

            auto ref_left_lanes = utils::GetLanesRefs(left_lanes);
            lane_group->lanes.insert(lane_group->lanes.end(),
                                     std::make_move_iterator(ref_left_lanes.begin()),
                                     std::make_move_iterator(ref_left_lanes.end()));

            auto ref_center_boundaries_left = utils::GetLaneBoundariesRefs(physical_center_boundaries_left);
            lane_group->lane_boundaries.insert(lane_group->lane_boundaries.end(),
                                               std::make_move_iterator(ref_center_boundaries_left.begin()),
                                               std::make_move_iterator(ref_center_boundaries_left.end()));

            auto ref_lane_boundaries = utils::GetLaneBoundariesRefs(physical_lane_boundaries);
            lane_group->lane_boundaries.insert(lane_group->lane_boundaries.end(),
                                               std::make_move_iterator(ref_lane_boundaries.begin()),
                                               std::make_move_iterator(ref_lane_boundaries.end()));

            map_.lane_groups.push_back(std::move(lane_group));

            map_.lane_boundaries.insert(map_.lane_boundaries.end(),
                                        std::make_move_iterator(physical_lane_boundaries.begin()),
                                        std::make_move_iterator(physical_lane_boundaries.end()));

            map_.lane_boundaries.insert(map_.lane_boundaries.end(),
                                        std::make_move_iterator(physical_center_boundaries_left.begin()),
                                        std::make_move_iterator(physical_center_boundaries_left.end()));

            map_.logical_lane_boundaries.insert(map_.logical_lane_boundaries.end(),
                                                std::make_move_iterator(logical_lane_boundaries.begin()),
                                                std::make_move_iterator(logical_lane_boundaries.end()));
            map_.logical_lane_boundaries.insert(map_.logical_lane_boundaries.end(),
                                                std::make_move_iterator(logical_center_boundaries_left.begin()),
                                                std::make_move_iterator(logical_center_boundaries_left.end()));

            std::move(left_lanes.begin(), left_lanes.end(), std::back_inserter(map_.lanes));
            std::move(left_logical_lanes.begin(), left_logical_lanes.end(), std::back_inserter(map_.logical_lanes));
        }

        map_.reference_lines.push_back(std::move(reference_line));
    }
}

map_api::Lane::Polyline MantleMapConverter::CreateCenterLineGeometry(
    const CoordinateConverter& converter,
    const Road& road,
    const types::LaneSection& section,
    const road_logic_suite::types::Lane::LaneId& lane_id) const
{
    map_api::Lane::Polyline center_line;
    for (auto s = section.s_start; s < section.s_end; s += config_.sampling_distance)
    {
        const auto lane_offset = utils::CalculateLaneOffsetAt(road, s);
        if (auto position = converter.ConvertLaneToInertialCoordinates({road.id, lane_id, s, lane_offset}))
        {
            const auto center_lane_point = position.value();
            center_line.emplace_back(center_lane_point.x, center_lane_point.y, center_lane_point.z);
        }
    }

    const auto lane_offset_at_s_end = utils::CalculateLaneOffsetAt(road, section.s_end);
    if (auto position =
            converter.ConvertLaneToInertialCoordinates({road.id, lane_id, section.s_end, lane_offset_at_s_end}))
    {
        const auto last_center_lane_point = position.value();
        center_line.emplace_back(last_center_lane_point.x, last_center_lane_point.y, last_center_lane_point.z);
    }

    if (config_.downsampling)
    {
        center_line = DecimatePolyline(center_line.begin(), center_line.end(), config_.downsampling_epsilon.value());
    }

    return center_line;
}

std::pair<map_api::Map::Lanes, map_api::Map::LogicalLanes> MantleMapConverter::CreateLanes(
    const OpenDriveData& data_storage,
    const CoordinateConverter& converter,
    const Road& road,
    const types::LaneSection& section,
    bool is_left_side)
{
    map_api::Map::Lanes physical_lanes{};
    map_api::Map::LogicalLanes logical_lanes{};
    const auto lanes = is_left_side ? section.left_lanes : section.right_lanes;
    physical_lanes.reserve(lanes.size());
    logical_lanes.reserve(lanes.size());

    // First pass to store IDs
    const auto is_junction = utils::IsJunction(road.road_assigned_junction_id);
    const auto section_id = road.lane_sections.FindIndex(section.s_start);

    for (const auto& lane : lanes)
    {
        const LaneInfo lane_info{.road_id = road.id, .lane_section_id = section_id, .lane_id = lane.id};

        auto& physical_lane = physical_lanes.emplace_back(
            std::make_unique<map_api::Lane>(map_api::Lane{.id = id_provider_.GetOrGenerateLaneId(lane_info)}));

        auto& logical_lane = logical_lanes.emplace_back(
            std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = id_provider_.GetNewId()}));

        physical_lane_id_to_logical_lane_id_[physical_lane->id] = logical_lane->id;
        logical_lane_id_to_physical_lane_id_[logical_lane->id] = physical_lane->id;

        // Set logical lane properties
        utils::SetLogicalLaneType(*logical_lane, lane.type);
        utils::SetLogicalLaneMovingDirection(*logical_lane, is_left_side);
        utils::SetLogicalLaneSValue(*logical_lane, section.s_start, section.s_end);
        utils::SetPhysicalLaneRefs(*logical_lane, *physical_lane, section.s_start, section.s_end);

        // Set physical lane properties
        physical_lane->local_id = lane.id;
        physical_lane->centerline = CreateCenterLineGeometry(converter, road, section, lane.id);
        physical_lane->centerline_is_driving_direction = true;
        if (is_left_side)
        {
            std::reverse(physical_lane->centerline.begin(), physical_lane->centerline.end());
        }
        if (is_junction)
        {
            utils::SetLaneToIntersectionType(*physical_lane);
        }
        else
        {
            utils::SetLaneType(*physical_lane, lane.type);
        }
        utils::SetLaneSubtype(*physical_lane, lane.type);
        id_to_lane_relation_.insert({physical_lane->id, {}});
        id_to_logical_lane_relation_.insert({physical_lane->id, {}});
        /// @todo: need mantle_lane->road_condition?
        /// @todo: need mantle_lane->source_references?
        MantleMapLaneLinker::StoreLinks(
            data_storage, road, section, lane, id_provider_, id_to_lane_relation_.at(physical_lane->id));
        StoreNeighbourLanes(road, section, lane, id_to_lane_relation_.at(physical_lane->id));
        StoreNeighbourLanesForLogicalLane(road, section, lane, id_to_logical_lane_relation_.at(physical_lane->id));
    }

    return {std::move(physical_lanes), std::move(logical_lanes)};
}

std::pair<map_api::Map::LaneBoundaries, map_api::Map::LogicalLaneBoundaries>
MantleMapConverter::CreateLaneBoundariesForLane(const CoordinateConverter& coordinate_converter,
                                                const Road& road,
                                                const types::LaneSection& section,
                                                const types::Lane& lane,
                                                const std::unique_ptr<map_api::ReferenceLine>& reference_line)
{
    map_api::Map::LaneBoundaries physical_boundaries;
    map_api::Map::LogicalLaneBoundaries logical_boundaries;

    if (!reference_line)
    {
        throw std::invalid_argument("[RoadLogicSuite]: reference_line cannot be null!");
    }

    for (auto road_mark_it = lane.road_mark.begin(); road_mark_it != lane.road_mark.end(); ++road_mark_it)
    {
        auto [created_physical_boundaries, logical_boundary_line] =
            CreateLaneBoundariesForRoadMark(coordinate_converter, road, section, lane, road_mark_it);

        // create logic lane boundary for each roadmark
        const auto physical_boundaries_refs = utils::GetLaneBoundariesRefs(created_physical_boundaries);
        const auto passing_rule = utils::ConvertPassingRule(road_mark_it->second.type, lane.id);

        auto logical_lane_boundary = std::make_unique<map_api::LogicalLaneBoundary>(
            map_api::LogicalLaneBoundary{.id = id_provider_.GetNewId(),
                                         .boundary_line = std::move(logical_boundary_line),
                                         .passing_rule = passing_rule,
                                         .reference_line = reference_line.get(),
                                         .physical_boundaries = physical_boundaries_refs});

        logical_boundaries.push_back(std::move(logical_lane_boundary));

        physical_boundaries.insert(physical_boundaries.end(),
                                   std::make_move_iterator(created_physical_boundaries.begin()),
                                   std::make_move_iterator(created_physical_boundaries.end()));
    }

    for (const auto& road_barrier : lane.road_barriers)
    {
        if (road_barrier.s_end > road.length)
        {
            std::cout
                << "[RoadLogicSuite: Barrier Conversion] WARNING: The s_end of the road barrier is larger than the "
                   "road length (ID: "
                << road.id << "). Points outside the range will be discarded during conversion!" << std::endl;
        }
        physical_boundaries.push_back(
            MantleBarrierConverter::Convert(road.id, road_barrier, coordinate_converter, config_, id_provider_));
    }
    return {std::move(physical_boundaries), std::move(logical_boundaries)};
}

std::vector<map_api::LogicalBoundaryPoint> MantleMapConverter::CreateLogicalBoundaryLine(
    const CoordinateConverter& converter,
    const Road& road,
    const types::LaneSection& section,
    const types::Lane::LaneId& lane_id,
    const units::length::meter_t& s_start,
    const units::length::meter_t& s_end,
    const units::length::meter_t& t_offset) const
{
    std::vector<map_api::LogicalBoundaryPoint> boundary_line{};
    for (auto s = s_start; s < s_end; s += config_.sampling_distance)
    {
        const auto lane_offset = utils::CalculateLaneOffsetAt(road, s);
        if (auto boundary_line_point =
                utils::CreateLogicBoundaryPointAt(converter, s, t_offset + lane_offset, road.id, section, lane_id))
        {
            boundary_line.push_back(boundary_line_point.value());
        }
    }

    const auto lane_offset_at_s_end = utils::CalculateLaneOffsetAt(road, s_end);
    if (auto last_point = utils::CreateLogicBoundaryPointAt(
            converter, s_end, t_offset + lane_offset_at_s_end, road.id, section, lane_id))
    {
        boundary_line.push_back(last_point.value());
    }

    if (lane_id > 0)
    {
        std::reverse(boundary_line.begin(), boundary_line.end());
    }
    return boundary_line;
}

std::pair<map_api::Map::LaneBoundaries, std::vector<map_api::LogicalBoundaryPoint>>
MantleMapConverter::CreateLaneBoundariesForRoadMark(
    const CoordinateConverter& converter,
    const Road& road,
    const types::LaneSection& section,
    const types::Lane& lane,
    const types::RangeBasedMap<units::length::meter_t, types::RoadMark>::ConstIterator& road_mark_it)
{
    map_api::Map::LaneBoundaries created_physical_boundaries;
    created_physical_boundaries.reserve(2);

    units::length::meter_t s_start = road_mark_it->first;
    auto next_it = std::next(road_mark_it);
    auto s_end = next_it != lane.road_mark.end() ? next_it->first : section.s_end;

    const bool is_double_lined = utils::IsRoadMarkTypeDoubleLined(road_mark_it->second.type);

    // magic offset number 0.12; no offset for single line
    const units::length::meter_t t_offset = is_double_lined ? units::length::meter_t(0.12) : units::length::meter_t(0);

    if (is_double_lined)
    {
        created_physical_boundaries.push_back(
            CreateBoundary(road, converter, s_start, s_end, t_offset, section, road_mark_it->second, lane.id));
        created_physical_boundaries.push_back(
            CreateBoundary(road, converter, s_start, s_end, -t_offset, section, road_mark_it->second, lane.id));
    }
    else
    {
        created_physical_boundaries.push_back(
            CreateBoundary(road, converter, s_start, s_end, t_offset, section, road_mark_it->second, lane.id));
    }

    auto logical_boundary_line =
        CreateLogicalBoundaryLine(converter, road, section, lane.id, s_start, s_end, units::length::meter_t(0));

    return {std::move(created_physical_boundaries), std::move(logical_boundary_line)};
}

std::pair<map_api::Map::LaneBoundaries, map_api::Map::LogicalLaneBoundaries> MantleMapConverter::CreateLaneBoundaries(
    const CoordinateConverter& converter,
    const Road& road,
    const types::LaneSection& section,
    map_api::Map::Lanes& physical_lanes,
    map_api::Map::LogicalLanes& logical_lanes,
    const map_api::Map::LaneBoundaries& center_boundaries,
    const map_api::Map::LogicalLaneBoundaries& logical_center_boundaries,
    const std::unique_ptr<map_api::ReferenceLine>& reference_line,
    const bool is_left_side)
{
    map_api::Map::LaneBoundaries physical_lane_boundaries{};
    map_api::Map::LogicalLaneBoundaries logical_lane_boundaries{};
    const auto lanes = is_left_side ? section.left_lanes : section.right_lanes;
    auto physical_lanes_it = physical_lanes.begin();
    auto logical_lanes_it = logical_lanes.begin();
    auto last_physical_boundaries_to_copy = utils::GetLaneBoundariesRefs(center_boundaries);
    auto last_logical_boundaries_to_copy = utils::GetLogicalLaneBoundariesRefs(logical_center_boundaries);
    for (const auto& lane : lanes)
    {
        auto& physical_lane = *physical_lanes_it++;
        auto& logical_lane = *logical_lanes_it++;

        logical_lane->reference_line = reference_line.get();

        physical_lane->left_lane_boundaries.insert(physical_lane->left_lane_boundaries.end(),
                                                   last_physical_boundaries_to_copy.begin(),
                                                   last_physical_boundaries_to_copy.end());

        logical_lane->left_boundaries.insert(logical_lane->left_boundaries.end(),
                                             last_logical_boundaries_to_copy.begin(),
                                             last_logical_boundaries_to_copy.end());

        auto [created_physical_boundaries, created_logical_boundaries] =
            CreateLaneBoundariesForLane(converter, road, section, lane, reference_line);

        if (is_left_side)
        {
            std::reverse(created_physical_boundaries.begin(), created_physical_boundaries.end());
            std::reverse(created_logical_boundaries.begin(), created_logical_boundaries.end());
        }

        for (auto& boundary : created_physical_boundaries)
        {
            physical_lane->right_lane_boundaries.push_back(std::ref(*boundary));
        }

        for (auto& logical_boundary : created_logical_boundaries)
        {
            logical_lane->right_boundaries.push_back(std::ref(*logical_boundary));
        }

        physical_lane_boundaries.insert(physical_lane_boundaries.end(),
                                        std::make_move_iterator(created_physical_boundaries.begin()),
                                        std::make_move_iterator(created_physical_boundaries.end()));

        logical_lane_boundaries.insert(logical_lane_boundaries.end(),
                                       std::make_move_iterator(created_logical_boundaries.begin()),
                                       std::make_move_iterator(created_logical_boundaries.end()));

        last_physical_boundaries_to_copy = physical_lane->right_lane_boundaries;
        last_logical_boundaries_to_copy = logical_lane->right_boundaries;
    }
    return {std::move(physical_lane_boundaries), std::move(logical_lane_boundaries)};
}

std::unique_ptr<map_api::LaneBoundary> MantleMapConverter::CreateBoundary(const Road& road,
                                                                          const CoordinateConverter& converter,
                                                                          const units::length::meter_t& s_start,
                                                                          const units::length::meter_t& s_end,
                                                                          const units::length::meter_t& t_offset,
                                                                          const types::LaneSection& section,
                                                                          const types::RoadMark& road_mark,
                                                                          const types::Lane::LaneId& lane_id)
{
    std::vector<map_api::LaneBoundary::BoundaryPoint> boundary_points{};

    for (auto s = s_start; s < s_end; s += config_.sampling_distance)
    {
        const auto lane_offset = utils::CalculateLaneOffsetAt(road, s);

        if (auto boundary_point = utils::CreateBoundaryPointAt(
                converter, s, t_offset + lane_offset, road.id, road_mark, section, lane_id))
        {
            boundary_points.push_back(boundary_point.value());
        }
    }

    const auto lane_offset_at_s_end = utils::CalculateLaneOffsetAt(road, s_end);
    if (auto boundary_point = utils::CreateBoundaryPointAt(
            converter, s_end, t_offset + lane_offset_at_s_end, road.id, road_mark, section, lane_id))
    {
        boundary_points.push_back(boundary_point.value());
    }

    if (config_.downsampling)
    {
        boundary_points =
            DecimatePolyline(boundary_points.begin(), boundary_points.end(), config_.downsampling_epsilon.value());
    }
    if (lane_id > 0)
    {
        std::reverse(boundary_points.begin(), boundary_points.end());
    }

    bool is_left_side =
        (lane_id <= 0 && t_offset > units::length::meter_t(0)) || (lane_id > 0 && t_offset < units::length::meter_t(0));

    const auto boundary_type = utils::ConvertRoadMarkingsType(road_mark.type, is_left_side);
    const auto boundary_color = utils::ConvertRoadMarkingsColor(road_mark.color);
    auto lane_boundary = std::make_unique<map_api::LaneBoundary>(
        map_api::LaneBoundary{id_provider_.GetNewId(), boundary_points, boundary_type, boundary_color});

    return lane_boundary;
}

void MantleMapConverter::CreateLaneReferenceFromId(std::vector<map_api::RefWrapper<map_api::Lane>>& lanes,
                                                   const std::vector<uint64_t>& lane_ids) const
{
    for (const auto& lane_id : lane_ids)
    {
        auto lane_ref = utils::FindLaneWithId(map_.lanes, lane_id);
        if (lane_ref)
        {
            lanes.push_back(lane_ref.value());
        }
        else
        {
            std::cout << "[MapConversion] WARNING: An expected lane with id " + std::to_string(lane_id) +
                             " was not found in the mantle map lanes vector."
                      << std::endl;
        }
    }
}

void MantleMapConverter::CreateLogicalLaneReferenceFromId(
    std::vector<map_api::RefWrapper<map_api::LogicalLane>>& logical_lanes,
    const std::vector<uint64_t>& lane_ids) const
{
    for (const auto& lane_id : lane_ids)
    {
        auto it = physical_lane_id_to_logical_lane_id_.find(lane_id);
        if (it != physical_lane_id_to_logical_lane_id_.end())
        {
            auto logical_lane_ref = utils::CreateLogicalLaneRefWithId(map_.logical_lanes, it->second);
            if (logical_lane_ref)
            {
                logical_lanes.push_back(logical_lane_ref.value());
            }
        }
        else
        {
            std::cout << "[MapConversion] WARNING: An expected logical lane with id " + std::to_string(lane_id) +
                             " was not found in the mantle map logical lanes vector."
                      << std::endl;
        }
    }
}

void MantleMapConverter::CreateLogicalLaneRelationFromId(std::vector<map_api::LogicalLaneRelation>& logical_lanes,
                                                         map_api::LogicalLane& logical_lane,
                                                         const std::vector<uint64_t>& lane_ids) const
{
    for (const auto& lane_id : lane_ids)
    {
        auto it = physical_lane_id_to_logical_lane_id_.find(lane_id);
        if (it != physical_lane_id_to_logical_lane_id_.end())
        {
            auto logical_lane_relation =
                utils::CreateLogicalLaneRelationWithId(map_.logical_lanes, logical_lane, it->second);
            if (logical_lane_relation)
            {
                logical_lanes.push_back(logical_lane_relation.value());
            }
        }
        else
        {
            std::cout << "[MapConversion] WARNING: An expected logical lane with id " + std::to_string(lane_id) +
                             " was not found in the mantle map logical lanes vector."
                      << std::endl;
        }
    }
}

/// @todo Any antecessors or successor which do not exist in the current map are ignored with a warning. This might
/// need to be changed for cases where map is loaded in several small pieces.
void MantleMapConverter::CreateLaneRelations()
{
    // Second pass to correspond lane IDs to the now constructed lane objects
    for (auto& mantle_lane : map_.lanes)
    {
        const auto& lane_relation = id_to_lane_relation_.at(mantle_lane->id);

        CreateLaneReferenceFromId(mantle_lane->antecessor_lanes, lane_relation.antecessor_lane_ids);
        CreateLaneReferenceFromId(mantle_lane->successor_lanes, lane_relation.successor_lane_ids);
        CreateLaneReferenceFromId(mantle_lane->left_adjacent_lanes, lane_relation.left_adjacent_lane_ids);
        CreateLaneReferenceFromId(mantle_lane->right_adjacent_lanes, lane_relation.right_adjacent_lane_ids);
    }

    for (auto& logical_lane : map_.logical_lanes)
    {
        const auto& lane_relation = id_to_lane_relation_.at(logical_lane_id_to_physical_lane_id_.at(logical_lane->id));
        const auto& logical_lane_relation =
            id_to_logical_lane_relation_.at(logical_lane_id_to_physical_lane_id_.at(logical_lane->id));
        CreateLogicalLaneReferenceFromId(logical_lane->predecessor_lanes, lane_relation.antecessor_lane_ids);
        CreateLogicalLaneReferenceFromId(logical_lane->successor_lanes, lane_relation.successor_lane_ids);
        CreateLogicalLaneRelationFromId(
            logical_lane->left_adjacent_lanes, *logical_lane, logical_lane_relation.left_adjacent_lane_ids);
        CreateLogicalLaneRelationFromId(
            logical_lane->right_adjacent_lanes, *logical_lane, logical_lane_relation.right_adjacent_lane_ids);
    }
}

void MantleMapConverter::StoreNeighbourLanes(const Road& road,
                                             const types::LaneSection& section,
                                             const types::Lane& lane,
                                             utils::LaneRelationIds& lane_relation_ids)
{
    if (lane.id == 0)
    {
        return;
    }

    const auto lane_id_sign = (0 < lane.id) - (lane.id < 0);
    const auto right_lane_id = lane.id + lane_id_sign;
    auto left_lane_id = lane.id - lane_id_sign;
    if (left_lane_id == 0)
    {
        left_lane_id -= lane_id_sign;
    }

    const auto section_id = road.lane_sections.FindIndex(section.s_start);

    if (section.IsValidLaneId(right_lane_id))
    {
        const LaneInfo lane_info{.road_id = road.id, .lane_section_id = section_id, .lane_id = right_lane_id};
        const auto reserved_lane_id = id_provider_.GetOrGenerateLaneId(lane_info);
        lane_relation_ids.right_adjacent_lane_ids.push_back(reserved_lane_id);
    }
    if (section.IsValidLaneId(left_lane_id))
    {
        const LaneInfo lane_info{.road_id = road.id, .lane_section_id = section_id, .lane_id = left_lane_id};
        const auto reserved_lane_id = id_provider_.GetOrGenerateLaneId(lane_info);
        lane_relation_ids.left_adjacent_lane_ids.push_back(reserved_lane_id);
    }
}

//  The adjacent lane for logical lane is in definition direction (not driving direction).
void MantleMapConverter::StoreNeighbourLanesForLogicalLane(const Road& road,
                                                           const types::LaneSection& section,
                                                           const types::Lane& lane,
                                                           utils::LogicalLaneRelationIds& lane_relation_ids)
{
    if (lane.id == 0)
    {
        return;
    }

    const auto section_id = road.lane_sections.FindIndex(section.s_start);
    const auto add_adjacent_lane = [&](int adjacent_lane_id, auto& adjacent_lane_ids) {
        if (section.IsValidLaneId(adjacent_lane_id))
        {
            const LaneInfo lane_info{road.id, section_id, adjacent_lane_id};
            adjacent_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info));
        }
    };

    // Right adjacent lane
    int right_lane_id = lane.id - 1;
    if (right_lane_id == 0)
    {
        right_lane_id = -1;
    }
    add_adjacent_lane(right_lane_id, lane_relation_ids.right_adjacent_lane_ids);

    // Left adjacent lane
    int left_lane_id = lane.id + 1;
    if (left_lane_id == 0)
    {
        left_lane_id = 1;
    }
    add_adjacent_lane(left_lane_id, lane_relation_ids.left_adjacent_lane_ids);
}

mantle_api::UniqueId MantleMapConverter::GetLaneId(const LaneInfo& info)
{
    return id_provider_.GetOrGenerateLaneId(info);
}

std::unique_ptr<map_api::ReferenceLine> MantleMapConverter::CreateReferenceLine(const CoordinateConverter& converter,
                                                                                const Road& road,
                                                                                const units::length::meter_t& s_start,
                                                                                const units::length::meter_t& s_end)
{
    std::vector<map_api::ReferenceLinePoint> reference_line_points{};
    for (auto s = s_start; s < s_end; s += config_.sampling_distance)
    {
        if (auto reference_line_point = utils::CreateReferenceLinePointAt(converter, s, road.id))
        {
            reference_line_points.push_back(reference_line_point.value());
        }
    }

    if (auto last_point = utils::CreateReferenceLinePointAt(converter, s_end, road.id))
    {
        reference_line_points.push_back(last_point.value());
    }

    auto reference_line = std::make_unique<map_api::ReferenceLine>(
        map_api::ReferenceLine{id_provider_.GetNewId(), reference_line_points});

    return reference_line;
}

}  // namespace road_logic_suite::map_conversion
