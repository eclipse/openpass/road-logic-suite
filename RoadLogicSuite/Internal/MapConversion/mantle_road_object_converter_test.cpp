/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_road_object_converter.h"

#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <MapAPI/map.h>
#include <gmock/gmock.h>

namespace
{

using road_logic_suite::map_conversion::MantleIdProvider;

constexpr double kAbsError = 1e-6;

TEST(MantleRoadObjectConverterTest, GivenDataStorageWithSingularObject_WhenConvertingToIMap_ThenObjectIsCorrectlyNamed)
{
    const auto object_type = "pole";
    road_logic_suite::OpenDriveData data{.objects = {{.type = object_type}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);

    ASSERT_EQ(map.stationary_objects.size(), 1);
    EXPECT_EQ(map.stationary_objects[0]->type, map_api::StationaryObject::Type::kPole);
}

TEST(MantleRoadObjectConverterTest, GivenObjectWithPosition_WhenConvertingToIMap_ThenObjectPositionIsCopiedCorrectly)
{
    const mantle_api::Vec3<units::length::meter_t> obj_position(
        units::length::meter_t(79.36), units::length::meter_t(-9.36), units::length::meter_t(34.96));
    road_logic_suite::OpenDriveData data{.objects = {{.position = obj_position}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);

    ASSERT_EQ(map.stationary_objects.size(), 1);
    EXPECT_NEAR(map.stationary_objects[0]->base.position.x(), obj_position.x(), kAbsError);
    EXPECT_NEAR(map.stationary_objects[0]->base.position.y(), obj_position.y(), kAbsError);
    EXPECT_NEAR(map.stationary_objects[0]->base.position.z(), obj_position.z(), kAbsError);
}

TEST(MantleRoadObjectConverterTest, GivenObjectWithRotation_WhenConvertingToIMap_ThenObjectRotationIsCopiedCorrectly)
{
    const double yaw = 4.649;
    const double pitch = 1.193;
    const double roll = 4.147;

    road_logic_suite::OpenDriveData data{.objects = {{.hdg = units::angle::radian_t(yaw),
                                                      .pitch = units::angle::radian_t(pitch),
                                                      .roll = units::angle::radian_t(roll)}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);

    ASSERT_EQ(map.stationary_objects.size(), 1);
    EXPECT_NEAR(map.stationary_objects[0]->base.orientation.yaw(), yaw, kAbsError);
    EXPECT_NEAR(map.stationary_objects[0]->base.orientation.pitch(), pitch, kAbsError);
    EXPECT_NEAR(map.stationary_objects[0]->base.orientation.roll(), roll, kAbsError);
}

TEST(MantleRoadObjectConverterTest, GivenObjectWithShape_WhenConvertingToIMap_ThenObjectShapeIsConvertedToPolygon)
{
    const std::vector<road_logic_suite::Vec2<units::length::meter_t>> triangle{
        {units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(2), units::length::meter_t(0)},
        {units::length::meter_t(1), units::length::meter_t(1)},
    };
    road_logic_suite::OpenDriveData data{.objects = {{.shape = triangle}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);

    ASSERT_EQ(map.stationary_objects.size(), 1);
    ASSERT_EQ(map.stationary_objects[0]->base.base_polygon.size(), 3);
    for (int i = 0; i < 3; i++)
    {
        EXPECT_NEAR(map.stationary_objects[0]->base.base_polygon[i].x(), triangle[i].x(), kAbsError);
        EXPECT_NEAR(map.stationary_objects[0]->base.base_polygon[i].y(), triangle[i].y(), kAbsError);
    }
}

TEST(MantleRoadObjectConverterTest, GivenObjectWithShape_WhenConvertingToIMap_ThenDimensionsCanBeProperlyCalculated)
{
    // triangle with base = 2m and height = 1m -> length = 2m (x), width = 1m (y)
    const std::vector<road_logic_suite::Vec2<units::length::meter_t>> triangle{
        {units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(2), units::length::meter_t(0)},
        {units::length::meter_t(1), units::length::meter_t(1)},
    };
    road_logic_suite::OpenDriveData data{.objects = {{.shape = triangle}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);

    ASSERT_EQ(map.stationary_objects.size(), 1);
    ASSERT_NEAR(map.stationary_objects[0]->base.dimension.length(), 2, kAbsError);
    ASSERT_NEAR(map.stationary_objects[0]->base.dimension.width(), 1, kAbsError);
    ASSERT_NEAR(map.stationary_objects[0]->base.dimension.height(), 0, kAbsError);
}

class MantleRoadObjectConverterObjectEntityTypeParameterTest
    : public ::testing::TestWithParam<std::pair<std::string, map_api::StationaryObject::Type>>
{
};

TEST_P(MantleRoadObjectConverterObjectEntityTypeParameterTest,
       GivenObjectWithType_WhenConvertingToIMap_ThenCorrectTypeIsSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{.objects = {{.type = param.first}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);
    ASSERT_EQ(map.stationary_objects.size(), 1);
    ASSERT_EQ(map.stationary_objects[0]->type, param.second);
}

const std::vector<std::pair<std::string, map_api::StationaryObject::Type>> kTestCases{
    {"barrier", map_api::StationaryObject::Type::kBarrier},
    {"building", map_api::StationaryObject::Type::kBuilding},
    {"crosswalk", map_api::StationaryObject::Type::kOther},
    {"gantry", map_api::StationaryObject::Type::kOverheadStructure},
    {"none", map_api::StationaryObject::Type::kOther},
    {"obstacle", map_api::StationaryObject::Type::kOther},
    {"parkingspace", map_api::StationaryObject::Type::kOther},
    {"patch", map_api::StationaryObject::Type::kOther},
    {"pole", map_api::StationaryObject::Type::kPole},
    {"railing", map_api::StationaryObject::Type::kBarrier},
    {"roadmark", map_api::StationaryObject::Type::kOther},
    {"trafficisland", map_api::StationaryObject::Type::kOther},
    {"tree", map_api::StationaryObject::Type::kTree},
    {"vegetation", map_api::StationaryObject::Type::kVegetation},
    {"soundbarrier", map_api::StationaryObject::Type::kWall},
    {"streetlamp", map_api::StationaryObject::Type::kPole},
    {"railing", map_api::StationaryObject::Type::kBarrier},
    {"somethingunknown", map_api::StationaryObject::Type::kUnknown},
};

INSTANTIATE_TEST_SUITE_P(MantleRoadObjectConverterTest,
                         MantleRoadObjectConverterObjectEntityTypeParameterTest,
                         ::testing::ValuesIn(kTestCases));

TEST(MantleRoadObjectConverterTest, GivenAFewObjects_WhenConvertingToMantleMap_ThenUniqueIdsAreAscending)
{
    road_logic_suite::OpenDriveData data{.objects = {{}, {}, {}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadObjectConverter::ConvertObjects(data, map, id_provider);
    ASSERT_EQ(map.stationary_objects.size(), 3);
    EXPECT_EQ(map.stationary_objects[0]->id, mantle_api::UniqueId{1});
    EXPECT_EQ(map.stationary_objects[1]->id, mantle_api::UniqueId{2});
    EXPECT_EQ(map.stationary_objects[2]->id, mantle_api::UniqueId{3});
}
}  // namespace