/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LANE_INFO_H
#define ROADLOGICSUITE_LANE_INFO_H

#include "RoadLogicSuite/Internal/Types/lane.h"

#include <units.h>

#include <string>

namespace road_logic_suite::map_conversion
{
struct LaneInfo
{
    /// Road ID according to OpenDRIVE standard.
    std::string road_id;

    /// Lane Section index in the above road after sorting the lane sections via s0.
    std::int64_t lane_section_id;

    /// Lane ID, according to OpenDRIVE standard.
    types::Lane::LaneId lane_id;

    /// to allow using this struct for ordered maps
    bool operator<(const LaneInfo& other) const;
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_LANE_INFO_H
