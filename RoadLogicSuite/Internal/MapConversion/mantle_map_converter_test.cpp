/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter.h"

#include "RoadLogicSuite/Internal/Geometry/arc.h"
#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/Utils/math_utils.h"

#include <MantleAPI/Common/vector.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <numeric>

namespace map_api
{
// Used for printing values from within GTest macros. Must be in map_api namespace
void PrintTo(const std::vector<RefWrapper<Lane>>& lanes, std::ostream* os)
{
    bool comma = false;
    *os << "Lane IDs as mantle_api::UniqueId : { ";
    for (auto& lane : lanes)
    {
        if (comma)
        {
            *os << ", ";
        }
        *os << lane.get().id;
        comma = true;
    }
    *os << " }";
}
}  // namespace map_api

namespace
{
using road_logic_suite::map_conversion::MantleMapConverter;
constexpr double kAbsError = 1e-6;

auto CreateDefaultSolidRoadMark()
{
    const units::length::meter_t road_start_s(0);
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::RoadMark>
        default_solid_road_mark{};
    const units::length::meter_t standardHeight(0.02);
    const units::length::meter_t standardWidth(0.2);
    const road_logic_suite::types::RoadMark default_solid = {
        .type = road_logic_suite::types::RoadMarkType::SOLID,
        .weight = road_logic_suite::types::RoadMarkWeight::STANDARD,
        .color = road_logic_suite::types::RoadMarkColor::STANDARD,
        .laneChange = road_logic_suite::types::RoadMarkLaneChange::NONE,
        .height = standardHeight,
        .width = standardWidth,
    };
    default_solid_road_mark.Insert(road_start_s, default_solid);
    return default_solid_road_mark;
}

auto AddSolidRoadMarkToData(
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::RoadMark>&
        default_solid_road_mark,
    const double roadmark_soffset)
{
    const units::length::meter_t road_start_s(roadmark_soffset);

    const units::length::meter_t standard_height(0.02);
    const units::length::meter_t standard_width(0.2);
    const road_logic_suite::types::RoadMark default_solid = {
        .type = road_logic_suite::types::RoadMarkType::SOLID,
        .weight = road_logic_suite::types::RoadMarkWeight::STANDARD,
        .color = road_logic_suite::types::RoadMarkColor::STANDARD,
        .laneChange = road_logic_suite::types::RoadMarkLaneChange::NONE,
        .height = standard_height,
        .width = standard_width,
    };
    default_solid_road_mark.Insert(road_start_s, default_solid);
}

auto CreateDoubleLinedRedRoadMark()
{
    const units::length::meter_t road_start_s(0);
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::RoadMark>
        default_solid_road_mark{};
    const units::length::meter_t standardHeight(0.02);
    const units::length::meter_t standardWidth(0.2);
    const road_logic_suite::types::RoadMark default_solid = {
        .type = road_logic_suite::types::RoadMarkType::SOLID_BROKEN,
        .weight = road_logic_suite::types::RoadMarkWeight::BOLD,
        .color = road_logic_suite::types::RoadMarkColor::RED,
        .laneChange = road_logic_suite::types::RoadMarkLaneChange::DECREASE,
        .height = standardHeight,
        .width = standardWidth,
    };
    default_solid_road_mark.Insert(road_start_s, default_solid);
    return default_solid_road_mark;
}

auto CreateSimpleCenterLane()
{
    return road_logic_suite::types::Lane{.id = 0, .road_mark = CreateDefaultSolidRoadMark()};
}

auto CreateDoubleLinedCenterLane()
{
    return road_logic_suite::types::Lane{.id = 0, .road_mark = CreateDoubleLinedRedRoadMark()};
}

auto CreateSimpleDrivingLane(const road_logic_suite::types::Lane::LaneId& id, const std::string& lane_type = "")
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::Poly3> constant_width{};
    constant_width.Insert(units::length::meter_t(0), road_logic_suite::types::Poly3(3, 0, 0, 0));
    return road_logic_suite::types::Lane{
        .id = id,
        .road_mark = CreateDefaultSolidRoadMark(),
        .width_polys = constant_width,
        .type = lane_type,
    };
}

auto CreateSimpleDrivingLaneWithMultiRoadmarks(const road_logic_suite::types::Lane::LaneId& id,
                                               const std::vector<double>& roadmark_soffsets_to_add,
                                               const std::string& lane_type = "")
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::Poly3> constant_width{};
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::RoadMark> road_marks{};
    for (auto soffset : roadmark_soffsets_to_add)
    {
        AddSolidRoadMarkToData(road_marks, soffset);
    }
    constant_width.Insert(units::length::meter_t(0), road_logic_suite::types::Poly3(3, 0, 0, 0));
    return road_logic_suite::types::Lane{
        .id = id,
        .road_mark = road_marks,
        .width_polys = constant_width,
        .type = lane_type,
    };
}

auto CreateDoubleLinedDrivingLane(const road_logic_suite::types::Lane::LaneId& id)
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::Poly3> constant_width{};
    constant_width.Insert(units::length::meter_t(0), road_logic_suite::types::Poly3(3, 0, 0, 0));
    return road_logic_suite::types::Lane{
        .id = id,
        .road_mark = CreateDoubleLinedRedRoadMark(),
        .width_polys = constant_width,
    };
}

auto CreateSimpleArcGeometry(const std::string& road_id,
                             const units::length::meter_t& length,
                             const units::length::meter_t& radius)
{
    return std::make_unique<road_logic_suite::Arc>(units::length::meter_t(0),
                                                   road_logic_suite::Vec2<units::length::meter_t>(),
                                                   units::angle::radian_t(0),
                                                   length,
                                                   1 / radius,
                                                   road_id);
}

auto CreateSimpleLineGeometry(const std::string& road_id, units::length::meter_t length = units::length::meter_t(100))
{
    return std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                    road_logic_suite::Vec2<units::length::meter_t>(),
                                                    units::angle::radian_t(0),
                                                    length,
                                                    road_id);
}

auto CreateLaneSection(double s0,
                       double length,
                       const road_logic_suite::types::Lane& center_lane,
                       const std::vector<road_logic_suite::types::Lane>& right_lanes = {},
                       const std::vector<road_logic_suite::types::Lane>& left_lanes = {})
{
    return road_logic_suite::types::LaneSection{
        .s_start = units::length::meter_t(s0),
        .s_end = units::length::meter_t(s0 + length),
        .left_lanes = left_lanes,
        .right_lanes = right_lanes,
        .center_lane = center_lane,
    };
}

auto CreateRoadDefinition(const std::string& road_id,
                          const std::vector<road_logic_suite::types::LaneSection>& lane_sections)
{
    const auto road_length = lane_sections.rbegin()->s_end;
    road_logic_suite::Road road{
        .id = road_id,
        .length = road_length,
        .index_geometries_start = 0,
        .index_geometries_end = 1,
    };
    std::for_each(
        lane_sections.begin(), lane_sections.end(), [&road](const road_logic_suite::types::LaneSection& section) {
            road.lane_sections.Insert(section.s_start, section);
        });
    return road;
}

auto CreateJunctionRoadDefinition(const std::string& road_id,
                                  const std::string& junction_id,
                                  const std::vector<road_logic_suite::types::LaneSection>& lane_sections)
{
    const auto road_length = lane_sections.rbegin()->s_end;
    road_logic_suite::Road road{
        .id = road_id,
        .road_assigned_junction_id = junction_id,
        .length = road_length,
        .index_geometries_start = 0,
        .index_geometries_end = 1,
    };
    std::for_each(
        lane_sections.begin(), lane_sections.end(), [&road](const road_logic_suite::types::LaneSection& section) {
            road.lane_sections.Insert(section.s_start, section);
        });
    return road;
}

auto CreateMapWithArcRoad(const std::string& road_id,
                          const std::vector<road_logic_suite::types::LaneSection>& lane_sections,
                          const units::length::meter_t& radius)
{
    const auto road = CreateRoadDefinition(road_id, lane_sections);
    road_logic_suite::OpenDriveData data;
    data.roads[road_id] = road;
    data.geometries.push_back(CreateSimpleArcGeometry(road_id, road.length, radius));
    return data;
}

auto CreateMapWithLinearRoad(const std::string& road_id,
                             const std::vector<road_logic_suite::types::LaneSection>& lane_sections)
{
    const auto road = CreateRoadDefinition(road_id, lane_sections);
    road_logic_suite::OpenDriveData data;
    data.roads[road_id] = road;
    data.geometries.push_back(CreateSimpleLineGeometry(road_id, road.length));
    return data;
}

auto CreateMapWithLinearRoadAndLaneOffset(const std::string& road_id,
                                          const std::vector<road_logic_suite::types::LaneSection>& lane_sections,
                                          double lane_offset_a,
                                          double lane_offset_b,
                                          double lane_offset_c,
                                          double lane_offset_d)
{
    auto road = CreateRoadDefinition(road_id, lane_sections);
    road.laneoffsets = {{units::length::meter_t(0.0),
                         road_logic_suite::types::Poly3{lane_offset_a, lane_offset_b, lane_offset_c, lane_offset_d}}};
    road_logic_suite::OpenDriveData data;
    data.roads[road_id] = road;
    data.geometries.push_back(CreateSimpleLineGeometry(road_id, road.length));
    return data;
}

auto CreateMapWithLinearJunctionRoad(const std::string& road_id,
                                     const std::string& junction_id,
                                     const std::vector<road_logic_suite::types::LaneSection>& lane_sections)
{
    const auto road = CreateJunctionRoadDefinition(road_id, junction_id, lane_sections);
    road_logic_suite::OpenDriveData data;
    data.roads[road_id] = road;
    data.geometries.push_back(CreateSimpleLineGeometry(road_id, road.length));
    return data;
}

auto CreateQuarterCircleOneWayRoad(const std::string& road_id, const units::length::meter_t radius)
{
    auto center_lane = CreateSimpleCenterLane();
    auto driving_lane = CreateSimpleDrivingLane(-1);
    const auto lane_section_a = CreateLaneSection(
        0, (radius * road_logic_suite::math_utils::kPiOver2).value(), center_lane, {driving_lane}, {});
    return CreateMapWithArcRoad(road_id, {lane_section_a}, radius);
}

auto CreateStraightOneWayRoad(const std::string& road_id = "0", const std::string& lane_type = "")
{
    auto center_lane = CreateSimpleCenterLane();
    auto driving_lane = CreateSimpleDrivingLane(-1, lane_type);
    const auto lane_section_a = CreateLaneSection(0, 100, center_lane, {driving_lane}, {});
    return CreateMapWithLinearRoad(road_id, {lane_section_a});
}

auto CreateStraightOneWayRoadWithLaneOffset(const std::string& road_id = "0",
                                            const std::string& lane_type = "",
                                            double lane_offset_a = 0.1,
                                            double lane_offset_b = 0.0,
                                            double lane_offset_c = 0.0,
                                            double lane_offset_d = 0.0)
{
    auto center_lane = CreateSimpleCenterLane();
    auto driving_lane = CreateSimpleDrivingLane(-1, lane_type);
    const auto lane_section_a = CreateLaneSection(0, 100, center_lane, {driving_lane}, {});
    return CreateMapWithLinearRoadAndLaneOffset(
        road_id, {lane_section_a}, lane_offset_a, lane_offset_b, lane_offset_c, lane_offset_d);
}

auto CreateStraightOneWayRoadWithMultiRoadmarks(const std::string& road_id = "0",
                                                const std::vector<double>& roadmark_soffsets_to_add = {0},
                                                const std::string& lane_type = "")
{
    auto center_lane = CreateSimpleCenterLane();
    auto driving_lane = CreateSimpleDrivingLaneWithMultiRoadmarks(-1, roadmark_soffsets_to_add, lane_type);
    const auto lane_section_a = CreateLaneSection(0, 100, center_lane, {driving_lane}, {});
    return CreateMapWithLinearRoad(road_id, {lane_section_a});
}

auto CreateDoubleLinedOneWayRoad(const std::string& road_id = "0")
{
    auto center_lane = CreateDoubleLinedCenterLane();
    auto driving_lane = CreateDoubleLinedDrivingLane(-1);
    const auto lane_section_a = CreateLaneSection(0, 100, center_lane, {driving_lane}, {});
    return CreateMapWithLinearRoad(road_id, {lane_section_a});
}

auto CreateStraightOneWayJunctionRoad(const std::string& road_id = "0",
                                      const std::string& junction_id = "1",
                                      const std::string& lane_type = "")
{
    auto center_lane = CreateSimpleCenterLane();
    auto driving_lane = CreateSimpleDrivingLane(-1, lane_type);
    const auto lane_section_a = CreateLaneSection(0, 100, center_lane, {driving_lane}, {});
    return CreateMapWithLinearJunctionRoad(road_id, junction_id, {lane_section_a});
}

TEST(MantleConverterTest, GivenSimpleRoadWithBoundaries_WhenConvertingToMantleMap_ThenMantleMapShouldHaveCorrectValues)
{
    const auto data_storage = CreateStraightOneWayRoad();
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    // asserts to make sure there are no segmentation faults.
    ASSERT_EQ(1, mantle_map.lanes.size());

    ASSERT_EQ(2, mantle_map.lane_boundaries.size());
    ASSERT_EQ(1, mantle_map.lanes[0]->left_lane_boundaries.size());
    ASSERT_EQ(1, mantle_map.lanes[0]->right_lane_boundaries.size());

    const auto center_boundary = mantle_map.lanes[0]->left_lane_boundaries[0];
    EXPECT_NEAR(27, center_boundary.get().boundary_line[27].position.x.value(), kAbsError);
    EXPECT_NEAR(0, center_boundary.get().boundary_line[27].position.y.value(), kAbsError);
    EXPECT_NEAR(0, center_boundary.get().boundary_line[27].position.z.value(), kAbsError);

    const auto right_boundary = mantle_map.lanes[0]->right_lane_boundaries[0];
    EXPECT_NEAR(27, right_boundary.get().boundary_line[27].position.x.value(), kAbsError);
    EXPECT_NEAR(-3, right_boundary.get().boundary_line[27].position.y.value(), kAbsError);
    EXPECT_NEAR(0, right_boundary.get().boundary_line[27].position.z.value(), kAbsError);

    const auto lane_center_line = mantle_map.lanes[0]->centerline;
    EXPECT_NEAR(27, lane_center_line[27].x(), kAbsError);
    EXPECT_NEAR(-1.5, lane_center_line[27].y(), kAbsError);
    EXPECT_NEAR(0, lane_center_line[27].z(), kAbsError);
}

void CreateRoadWithRightAndLeftLane(road_logic_suite::OpenDriveData& data)
{
    constexpr auto road_id = "0";
    constexpr auto road_length = units::length::meter_t(100);
    constexpr auto line_s_start = units::length::meter_t(0);
    constexpr auto line_pos = road_logic_suite::Vec2<units::length::meter_t>();
    constexpr auto line_hdg = units::angle::radian_t(0);
    constexpr auto lane_width = 3;
    constexpr auto road_mark_width = units::length::meter_t(0.2);
    constexpr auto road_mark_height = units::length::meter_t(0.02);
    constexpr auto road_mark_color = road_logic_suite::types::RoadMarkColor::STANDARD;
    constexpr auto road_mark_type = road_logic_suite::types::RoadMarkType::SOLID;
    constexpr auto road_mark_weight = road_logic_suite::types::RoadMarkWeight::STANDARD;
    constexpr auto road_mark_lane_change = road_logic_suite::types::RoadMarkLaneChange::NONE;
    const auto lane_width_poly = road_logic_suite::types::Poly3(lane_width, 0, 0, 0);

    const road_logic_suite::types::RoadMark solidLine{
        .type = road_mark_type,
        .weight = road_mark_weight,
        .color = road_mark_color,
        .laneChange = road_mark_lane_change,
        .height = road_mark_height,
        .width = road_mark_width,
    };
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::RoadMark> solid_line_mark{};
    solid_line_mark.Insert(line_s_start, solidLine);

    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::Poly3>
        constant_lane_width{};
    constant_lane_width.Insert(line_s_start, lane_width_poly);

    road_logic_suite::types::Lane right_hand_line{
        .id = -1, .road_mark = solid_line_mark, .width_polys = constant_lane_width};
    road_logic_suite::types::Lane center_line{.id = 0, .road_mark = solid_line_mark};
    road_logic_suite::types::Lane left_hand_line{
        .id = 1, .road_mark = solid_line_mark, .width_polys = constant_lane_width};

    road_logic_suite::types::LaneSection lane_section{};
    lane_section.right_lanes.push_back(right_hand_line);
    lane_section.center_lane = center_line;
    lane_section.left_lanes.push_back(left_hand_line);

    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
        lane_sections{};
    lane_sections.Insert(line_s_start, lane_section);

    const road_logic_suite::Road road{
        .id = road_id,
        .length = road_length,
        .index_geometries_start = 0,
        .index_geometries_end = 1,
        .lane_sections = lane_sections,
    };
    data.roads.insert_or_assign(road_id, road);
    auto geometry = std::make_unique<road_logic_suite::Line>(line_s_start, line_pos, line_hdg, road_length, road_id);
    data.geometries.push_back(std::move(geometry));
}

TEST(MantleConverterTest, GivenRoadWithRightAndLeftLanes_WhenConverting_ThenBothShouldExist)
{
    road_logic_suite::OpenDriveData data_storage{};
    CreateRoadWithRightAndLeftLane(data_storage);
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 2);

    const auto& left_lane = mantle_map.lanes[1];
    const auto& right_lane = mantle_map.lanes[0];

    EXPECT_EQ(left_lane->local_id, 1);
    EXPECT_EQ(right_lane->local_id, -1);

    ASSERT_EQ(left_lane->left_lane_boundaries.size(), 1);
    ASSERT_EQ(left_lane->left_lane_boundaries.size(), 1);
    ASSERT_EQ(right_lane->right_lane_boundaries.size(), 1);

    // Check if the y values are correct:
    // +----------------> (y > 0)
    // Left Lane
    // +────────────────> (y = 0)
    // Right Lane
    // +----------------> (y < 0)
    EXPECT_EQ(3, left_lane->right_lane_boundaries[0].get().boundary_line[0].position.y.value());
    EXPECT_EQ(1.5, left_lane->centerline[0].y());
    EXPECT_EQ(0, left_lane->left_lane_boundaries[0].get().boundary_line[0].position.y.value());
    EXPECT_EQ(-1.5, right_lane->centerline[0].y());
    EXPECT_EQ(-3, right_lane->right_lane_boundaries[0].get().boundary_line[0].position.y.value());
}

TEST(MantleConverterTest, GivenRoadWithRightAndLeftLanes_WhenConverting_ThenLaneGroupsShouldBeCorrect)
{
    road_logic_suite::OpenDriveData data_storage{};
    CreateRoadWithRightAndLeftLane(data_storage);
    MantleMapConverter converter{};

    const auto expected_lane_group_size = 2;
    const auto expected_lane_group_type = map_api::LaneGroup::Type::kOneWay;
    const auto expected_single_lane_local_id_in_lanegroup_1 = -1;
    const auto expected_single_lane_local_id_in_lanegroup_2 = 1;

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lane_groups.size(), expected_lane_group_size);
    EXPECT_EQ(mantle_map.lane_groups.at(0)->type, expected_lane_group_type);
    EXPECT_EQ(mantle_map.lane_groups.at(1)->type, expected_lane_group_type);

    EXPECT_EQ(mantle_map.lane_groups.at(0)->id, 1);
    EXPECT_EQ(mantle_map.lane_groups.at(1)->id, 2);
    ASSERT_EQ(mantle_map.lane_groups.at(0)->lanes.size(), 1);
    ASSERT_EQ(mantle_map.lane_groups.at(1)->lanes.size(), 1);
    ASSERT_EQ(mantle_map.lane_groups.at(0)->lane_boundaries.size(), 2);
    ASSERT_EQ(mantle_map.lane_groups.at(1)->lane_boundaries.size(), 2);

    // make sure every lane and lane boundary inside a lane group is accessible
    EXPECT_EQ(mantle_map.lane_groups.at(0)->lanes[0].get().local_id, expected_single_lane_local_id_in_lanegroup_1);
    EXPECT_EQ(mantle_map.lane_groups.at(1)->lanes[0].get().local_id, expected_single_lane_local_id_in_lanegroup_2);

    EXPECT_EQ(mantle_map.lane_groups.at(0)->lane_boundaries[0].get().type, map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(mantle_map.lane_groups.at(0)->lane_boundaries[1].get().type, map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(mantle_map.lane_groups.at(1)->lane_boundaries[0].get().type, map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(mantle_map.lane_groups.at(1)->lane_boundaries[1].get().type, map_api::LaneBoundary::Type::kSolidLine);
}

TEST(MantleConverterTest, GivenRoadWithArcGeometry_WhenConvertingRoad_ThenValuesMustBeCorrect)
{
    const units::length::meter_t arc_radius(1000);
    const auto data_storage = CreateQuarterCircleOneWayRoad("0", arc_radius);
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 1);
    const auto first_point = mantle_map.lanes[0]->centerline.begin();
    const auto last_point = mantle_map.lanes[0]->centerline.rbegin();
    const auto half_lane_width = 1.5;
    EXPECT_NEAR(0, first_point->x(), kAbsError);
    EXPECT_NEAR(0 - half_lane_width, first_point->y(), kAbsError);
    EXPECT_NEAR(arc_radius() + half_lane_width, last_point->x(), kAbsError);
    EXPECT_NEAR(arc_radius(), last_point->y(), kAbsError);
}

/// @verbatim
/// Expected Lane IDs (MapAPI), direction and center line y-values for the six-lane road. (Checked on 12.06.2023)
/// ╓─────────────────────╖
/// ║ <-    Lane 11       ║ y=7.5
/// ╟─────────────────────╢
/// ║ <-    Lane 10       ║ y=4.5
/// ╟─────────────────────╢
/// ║ <-    Lane 5        ║ y=1.5
/// ╠═════════════════════╣
/// ║       Lane 3     -> ║ y=-1.5
/// ╟─────────────────────╢
/// ║       Lane 4     -> ║ y=-4.5
/// ╟─────────────────────╢
/// ║       Lane 6     -> ║ y=-7.5
/// ╙─────────────────────╜
/// Bottom lanes are called eastward_lanes and top lanes called westward_lanes in some tests.
/// @endverbatim
auto CreateSixLaneRoad()
{
    road_logic_suite::Road road{
        .id = "0",
        .length = units::length::meter_t(100),
        .index_geometries_start = 0,
        .index_geometries_end = 1,
    };
    road_logic_suite::types::LaneSection section{
        .s_start = units::length::meter_t(0),
        .s_end = units::length::meter_t(100),
        .left_lanes = {CreateSimpleDrivingLane(1), CreateSimpleDrivingLane(2), CreateSimpleDrivingLane(3)},
        .right_lanes = {CreateSimpleDrivingLane(-1), CreateSimpleDrivingLane(-2), CreateSimpleDrivingLane(-3)},
        .center_lane = CreateSimpleCenterLane(),
    };
    road.lane_sections.Insert(section.s_start, section);
    return road;
}

auto CreateStraightSixLaneRoad()
{
    road_logic_suite::OpenDriveData data{};
    data.roads.insert_or_assign("0", CreateSixLaneRoad());
    data.geometries.push_back(CreateSimpleLineGeometry("0"));
    return data;
}

// Matcher to check if a vector<reference_wrapper<Lane>> contains a Lane*
MATCHER_P(ContainsLane, lane_ptr, "")
{
    *result_listener << "where the expected lane had a mantle_api::UniqueId of " << lane_ptr->id;
    return std::any_of(
        arg.begin(), arg.end(), [this](auto related_lane) { return (&(related_lane.get()) == lane_ptr); });
}

TEST(MantleConverterTest, GivenRoadWithRightAndLeftLanes_WhenConverting_ThenLanesShouldHaveProperRightAndLeftNeighbours)
{
    road_logic_suite::OpenDriveData data = CreateStraightSixLaneRoad();
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data);

    const auto& eastward_left_lane = mantle_map.lanes[0];
    const auto& eastward_middle_lane = mantle_map.lanes[1];
    const auto& eastward_right_lane = mantle_map.lanes[2];
    const auto& westward_left_lane = mantle_map.lanes[3];
    const auto& westward_middle_lane = mantle_map.lanes[4];
    const auto& westward_right_lane = mantle_map.lanes[5];

    // check each lane
    EXPECT_THAT(westward_right_lane->left_adjacent_lanes, ContainsLane(westward_middle_lane.get()));
    EXPECT_THAT(westward_right_lane->right_adjacent_lanes, ::testing::IsEmpty());

    EXPECT_THAT(westward_middle_lane->left_adjacent_lanes, ContainsLane(westward_left_lane.get()));
    EXPECT_THAT(westward_middle_lane->right_adjacent_lanes, ContainsLane(westward_right_lane.get()));

    EXPECT_THAT(westward_left_lane->left_adjacent_lanes, ContainsLane(eastward_left_lane.get()));
    EXPECT_THAT(westward_left_lane->right_adjacent_lanes, ContainsLane(westward_middle_lane.get()));

    EXPECT_THAT(eastward_left_lane->left_adjacent_lanes, ContainsLane(westward_left_lane.get()));
    EXPECT_THAT(eastward_left_lane->right_adjacent_lanes, ContainsLane(eastward_middle_lane.get()));

    EXPECT_THAT(eastward_middle_lane->left_adjacent_lanes, ContainsLane(eastward_left_lane.get()));
    EXPECT_THAT(eastward_middle_lane->right_adjacent_lanes, ContainsLane(eastward_right_lane.get()));

    EXPECT_THAT(eastward_right_lane->left_adjacent_lanes, ContainsLane(eastward_middle_lane.get()));
    EXPECT_THAT(eastward_right_lane->right_adjacent_lanes, ::testing::IsEmpty());
}

/// @verbatim
/// ╠══════════╤══════════╣╠══════════╤══════════╣
/// ║ Lane 12  │ Lane 4 ->║║ Lane 3   │ Lane 8 ->║
/// ╟──────────┴──────────╢╟──────────┴──────────╢
/// @endverbatim
auto CreateTwoRoadsInSameDirection()
{
    const auto center_lane = CreateSimpleCenterLane();
    auto first_driving_lane = CreateSimpleDrivingLane(-1);
    first_driving_lane.successors.push_back(-1);
    const auto first_road_first_lane_section = CreateLaneSection(0, 500, center_lane, {first_driving_lane}, {});
    const auto first_road_second_lane_section = CreateLaneSection(500, 500, center_lane, {first_driving_lane}, {});
    auto first_road = CreateRoadDefinition("0", {first_road_first_lane_section, first_road_second_lane_section});
    first_road.road_links.push_back(road_logic_suite::RoadLink{
        .target_id = "1",
        .link_type = road_logic_suite::RoadLinkType::kSuccessor,
        .contact_type = road_logic_suite::LinkContactType::kStart,
    });
    auto second_driving_lane = CreateSimpleDrivingLane(-1);
    second_driving_lane.predecessors.push_back(-1);
    const auto second_road_first_lane_section = CreateLaneSection(0, 500, center_lane, {second_driving_lane}, {});
    const auto second_road_second_lane_section = CreateLaneSection(500, 500, center_lane, {second_driving_lane}, {});
    auto second_road = CreateRoadDefinition("1", {second_road_first_lane_section, second_road_second_lane_section});
    second_road.road_links.push_back(road_logic_suite::RoadLink{
        .target_id = "0",
        .link_type = road_logic_suite::RoadLinkType::kPredecessor,
        .contact_type = road_logic_suite::LinkContactType::kEnd,
    });
    second_road.index_geometries_start = 1;
    second_road.index_geometries_end = 2;
    road_logic_suite::OpenDriveData data{};
    data.geometries.push_back(CreateSimpleLineGeometry("0", units::length::meter_t(1000)));
    data.geometries.push_back(CreateSimpleLineGeometry("1", units::length::meter_t(1000)));
    data.roads.insert_or_assign("0", first_road);
    data.roads.insert_or_assign("1", second_road);
    return data;
}

TEST(MantleConverterTest, GivenTwoConnectedSameDirectionRoads_WhenConverting_ThenMantleLanesMustBeLinked)
{
    // Ref lines: --⁞-->|--⁞-->
    road_logic_suite::OpenDriveData data = CreateTwoRoadsInSameDirection();
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data);

    ASSERT_EQ(4, mantle_map.lanes.size());
    const auto& road_antecessor_lane = mantle_map.lanes[3];
    const auto& road_successor_lane = mantle_map.lanes[0];
    EXPECT_THAT(road_antecessor_lane->successor_lanes, ContainsLane(road_successor_lane.get()));
    EXPECT_THAT(road_successor_lane->antecessor_lanes, ContainsLane(road_antecessor_lane.get()));
}

TEST(MantleConverterTest, GivenMapWithProjectionString_WhenConverting_ThenProjectionStringMustBeSet)
{
    const auto proj =
        "+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs";
    road_logic_suite::OpenDriveData data{.projection = proj};
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data);

    ASSERT_EQ(mantle_map.projection_string, proj);
}

TEST(MantleConverterTest, GivenMapWithSingleStraightLane_WhenConvertingWithDownsampling_ThenLaneMustHaveOnlyTwoPoints)
{
    const auto data = CreateStraightOneWayRoad();
    MantleMapConverter converter({.sampling_distance = units::length::meter_t(1),
                                  .downsampling = true,
                                  .downsampling_epsilon = units::length::meter_t(0.05)});

    const auto mantle_map = converter.Convert(data);

    for (const auto& lane : mantle_map.lanes)
    {
        EXPECT_EQ(lane->centerline.size(), 2) << "Failing Lane ID= " << lane->id;
    }

    for (const auto& lane_boundary : mantle_map.lane_boundaries)
    {
        ASSERT_EQ(lane_boundary->boundary_line.size(), 2) << "Failing Lane Boundary ID= " << lane_boundary->id;
    }
}

TEST(MantleConverterTest, GivenMapWithAnyObject_WhenConverting_ThenObjectMustBeConverted)
{
    // Note: Detail tests are in the mantle road object converter tests. This just makes sure it is called..
    const road_logic_suite::OpenDriveData data{.objects = {{}}};
    MantleMapConverter converter{};
    const auto mantle_map = converter.Convert(data);
    EXPECT_EQ(mantle_map.stationary_objects.size(), 1);
}

TEST(MantleConverterTest, GivenDoubleLinedRoad_WhenConvertingToMantleMap_ThenLaneBoundariesShouldSplitCorrectly)
{
    const auto data_storage = CreateDoubleLinedOneWayRoad();
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    auto& left_lane = mantle_map.lanes.at(0);
    EXPECT_EQ(left_lane->left_lane_boundaries.size(), 2);
    EXPECT_EQ(left_lane->right_lane_boundaries.size(), 2);

    auto& left_center_boundary = left_lane->left_lane_boundaries.at(0);
    auto& right_center_boundary = left_lane->left_lane_boundaries.at(1);

    EXPECT_EQ(left_center_boundary.get().type, map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(right_center_boundary.get().type, map_api::LaneBoundary::Type::kDashedLine);

    EXPECT_EQ(left_center_boundary.get().color, map_api::LaneBoundary::Color::kRed);
    EXPECT_EQ(right_center_boundary.get().color, map_api::LaneBoundary::Color::kRed);

    auto left_outer_boundary = left_lane->right_lane_boundaries.at(0);
    auto right_outer_boundary = left_lane->right_lane_boundaries.at(1);

    EXPECT_EQ(left_outer_boundary.get().type, map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(right_outer_boundary.get().type, map_api::LaneBoundary::Type::kDashedLine);

    EXPECT_EQ(left_outer_boundary.get().color, map_api::LaneBoundary::Color::kRed);
    EXPECT_EQ(right_outer_boundary.get().color, map_api::LaneBoundary::Color::kRed);
}

class MantleConverterLaneTypeTestFixture : public ::testing::TestWithParam<std::string>
{
};

TEST_P(MantleConverterLaneTypeTestFixture,
       GivenRoadWithLaneTypes_WhenConvertingToMantleMap_ThenLaneTypeIsCorrectlyConverted)
{
    const auto lane_type = GetParam();

    const auto data_storage = CreateStraightOneWayRoad("0", lane_type);
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 1);
    const auto& lane = mantle_map.lanes.at(0);

    if (lane_type == "driving" ||  //
        lane_type == "exit" ||     //
        lane_type == "entry" ||    //
        lane_type == "onRamp" ||   //
        lane_type == "offRamp" ||  //
        lane_type == "biking" ||   //
        lane_type == "connectingRamp")
    {
        EXPECT_EQ(lane->type, map_api::Lane::Type::kDriving);
    }

    if (lane_type == "shoulder" ||    //
        lane_type == "parking" ||     //
        lane_type == "stop" ||        //
        lane_type == "restricted" ||  //
        lane_type == "median" ||      //
        lane_type == "sidewalk" ||    //
        lane_type == "curb")
    {
        EXPECT_EQ(lane->type, map_api::Lane::Type::kNonDriving);
    }
}

TEST_P(MantleConverterLaneTypeTestFixture,
       GivenJunctionRoadWithLaneTypes_WhenConvertingToMantleMap_ThenLaneTypeIsCorrectlyConverted)
{
    const auto lane_type = GetParam();

    const auto data_storage = CreateStraightOneWayJunctionRoad("0", "1", lane_type);
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 1);
    const auto& lane = mantle_map.lanes.at(0);

    if (lane_type == "driving" ||         //
        lane_type == "exit" ||            //
        lane_type == "entry" ||           //
        lane_type == "onRamp" ||          //
        lane_type == "offRamp" ||         //
        lane_type == "biking" ||          //
        lane_type == "connectingRamp" ||  //
        lane_type == "shoulder" ||        //
        lane_type == "parking" ||         //
        lane_type == "stop" ||            //
        lane_type == "restricted" ||      //
        lane_type == "median" ||          //
        lane_type == "sidewalk" ||        //
        lane_type == "curb")
    {
        EXPECT_EQ(lane->type, map_api::Lane::Type::kIntersection);
    }
}

INSTANTIATE_TEST_CASE_P(MantleConverterLaneTypeTest,
                        MantleConverterLaneTypeTestFixture,
                        ::testing::Values("driving",
                                          "shoulder",
                                          "stop",
                                          "exit",
                                          "entry",
                                          "onramp",
                                          "offRamp",
                                          "connectingRamp",
                                          "none",
                                          "parking",
                                          "biking",
                                          "restricted",
                                          "median",
                                          "sidewalk",
                                          "curb"));

class MantleConverterLaneSubtypeTestFixture : public ::testing::TestWithParam<std::string>
{
};

TEST_P(MantleConverterLaneSubtypeTestFixture,
       GivenRoadWithLaneSubtypes_WhenConvertingToMantleMap_ThenLaneSubtypeIsCorrectlyConverted)
{
    const auto lane_subtype = GetParam();

    const auto data_storage = CreateStraightOneWayRoad("0", lane_subtype);
    MantleMapConverter converter{};

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 1);
    const auto& lane = mantle_map.lanes.at(0);

    const auto lane_subtype_str = road_logic_suite::utils::ToLower(lane_subtype);
    if (lane_subtype_str == "shoulder")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kShoulder);
    }
    else if (lane_subtype_str == "border")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kBorder);
    }
    else if (lane_subtype_str == "driving")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kNormal);
    }
    else if (lane_subtype_str == "stop")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kStop);
    }
    else if (lane_subtype_str == "none")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kOther);
    }
    else if (lane_subtype_str == "restricted")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kRestricted);
    }
    else if (lane_subtype_str == "parking")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kParking);
    }
    else if (lane_subtype_str == "median")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kOther);
    }
    else if (lane_subtype_str == "biking")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kBiking);
    }
    else if (lane_subtype_str == "sidewalk")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kSidewalk);
    }
    else if (lane_subtype_str == "curb")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kOther);
    }
    else if (lane_subtype_str == "exit")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kExit);
    }
    else if (lane_subtype_str == "entry")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kEntry);
    }
    else if (lane_subtype_str == "onramp")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kOnRamp);
    }
    else if (lane_subtype_str == "offramp")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kOffRamp);
    }
    else if (lane_subtype_str == "connectingramp")
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kConnectingRamp);
    }
    else
    {
        EXPECT_EQ(lane->sub_type, map_api::Lane::Subtype::kUnknown);
    }
}

INSTANTIATE_TEST_CASE_P(MantleConverterLaneSubtypeTest,
                        MantleConverterLaneSubtypeTestFixture,
                        ::testing::Values("driving",
                                          "shoulder",
                                          "border",
                                          "stop",
                                          "exit",
                                          "entry",
                                          "onramp",
                                          "offRamp",
                                          "connectingRamp",
                                          "none",
                                          "parking",
                                          "biking",
                                          "restricted",
                                          "median",
                                          "sidewalk",
                                          "curb"));

TEST(IMapConverterTest, GivenRoadWithBarrier_WhenConvertingToIMap_ThenBarrierBoundaryMustBeCorrectlySet)
{
    auto data_storage = CreateStraightOneWayRoad();
    const units::length::meter_t s0(0);
    const road_logic_suite::RoadBarrier barrier{.s_start = units::length::meter_t(2),
                                                .s_end = units::length::meter_t(97),
                                                .t_start = units::length::meter_t(0),
                                                .t_end = units::length::meter_t(0.04)};
    data_storage.roads["0"].lane_sections.Find(s0)->second.center_lane.road_barriers.push_back(barrier);

    road_logic_suite::MapConverterConfig converter_config{};
    road_logic_suite::map_conversion::MantleMapConverter converter(converter_config);

    const auto mantle_map = converter.Convert(data_storage);

    ASSERT_EQ(mantle_map.lanes.size(), 1);
    ASSERT_EQ(mantle_map.lanes.at(0)->left_lane_boundaries.size(), 2);
}

TEST(IMapConverterTest,
     GivenRoadWithConvertedRoadmarkContainsOnlyOnePoint_WhenConvertingToIMap_ThenDownsamplingIsSkippedAndPrintWarning)
{
    std::vector<double> roadmark_soffsets_to_add = {0, 100};
    // soffset 100 m is in edge case here and contains only one point because length of the default lane created in the
    // following function is 100 m.
    auto data_storage = CreateStraightOneWayRoadWithMultiRoadmarks("0", roadmark_soffsets_to_add);

    MantleMapConverter converter({.sampling_distance = units::length::meter_t(1.0),
                                  .downsampling = true,
                                  .downsampling_epsilon = units::length::meter_t(0.01)});

    testing::internal::CaptureStdout();

    const auto expected_warning_output =
        "[RoadLogicSuite] WARN: Cannot simplify a polyline with less than two points. Downsampling is skipped!\n";

    EXPECT_NO_THROW(converter.Convert(data_storage));

    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_warning_output);
}

TEST(MantleConverterTest, GivenDataStorageWithAnySignal_WhenConverting_ThenSignalMustBeConverted)
{
    // Note: Detail tests are in the mantle road signal converter tests. This just makes sure it is called.
    const road_logic_suite::OpenDriveData data{.signals = {{"1", road_logic_suite::Signal{.type = "274"}}}};
    MantleMapConverter converter{};
    const auto mantle_map = converter.Convert(data);
    EXPECT_EQ(mantle_map.traffic_signs.size(), 1);
}

TEST(MantleConverterTest, GivenDataStorageWithSimpleLineGeometry_WhenConverting_ThenReferenceLineIsCorrectlyConverted)
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
        lane_sections{};
    road_logic_suite::types::LaneSection lane_section{.s_start = units::length::meter_t(0),
                                                      .s_end = units::length::meter_t(100)};
    lane_sections.Insert(units::length::meter_t(0), lane_section);

    const auto road_id = "1";
    const auto road_length = units::length::meter_t(100);
    road_logic_suite::Road road{
        .id = road_id,
        .length = road_length,
        .index_geometries_start = 0,
        .index_geometries_end = 1,
        .lane_sections = lane_sections,
    };
    road_logic_suite::OpenDriveData data;
    data.roads.insert_or_assign(road_id, road);
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                                       road_logic_suite::Vec2<units::length::meter_t>(),
                                                                       units::angle::radian_t(0),
                                                                       road_length,
                                                                       road_id));
    MantleMapConverter converter{
        road_logic_suite::MapConverterConfig{.sampling_distance = units::length::meter_t(0.5)}};
    const auto mantle_map = converter.Convert(data);
    ASSERT_EQ(mantle_map.reference_lines.size(), 1);
    ASSERT_EQ(mantle_map.reference_lines.at(0)->poly_line.size(), 201);
}

TEST(MantleConverterTest,
     GivenDataStorageWithSimpleLineGeometry_WhenConverting_ThenLogicLaneBoundaryIsCorrectlyConverted)
{
    const auto data_storage = CreateDoubleLinedOneWayRoad();
    MantleMapConverter converter{};

    const auto& mantle_map = converter.Convert(data_storage);
    const auto& logic_lane_boundaries = mantle_map.logical_lane_boundaries;

    ASSERT_EQ(logic_lane_boundaries.size(), 2);
    const auto& lane_minus_1_right_logic_boundary = logic_lane_boundaries.at(0);
    const auto& lane_minus_1_left_logic_boundary = logic_lane_boundaries.at(1);

    EXPECT_EQ(lane_minus_1_left_logic_boundary->reference_line->poly_line.size(), 101);
    EXPECT_EQ(lane_minus_1_right_logic_boundary->reference_line->poly_line.size(), 101);

    EXPECT_EQ(lane_minus_1_left_logic_boundary->boundary_line.size(), 101);
    EXPECT_EQ(lane_minus_1_right_logic_boundary->boundary_line.size(), 101);

    EXPECT_EQ(lane_minus_1_left_logic_boundary->passing_rule, map_api::LogicalLaneBoundary::PassingRule::kIncreasingT);
    EXPECT_EQ(lane_minus_1_right_logic_boundary->passing_rule, map_api::LogicalLaneBoundary::PassingRule::kIncreasingT);

    ASSERT_EQ(lane_minus_1_left_logic_boundary->physical_boundaries.size(), 2);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->physical_boundaries.at(0).get().type,
              map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->physical_boundaries.at(1).get().type,
              map_api::LaneBoundary::Type::kDashedLine);
    ASSERT_EQ(lane_minus_1_right_logic_boundary->physical_boundaries.size(), 2);
    EXPECT_EQ(lane_minus_1_right_logic_boundary->physical_boundaries.at(0).get().type,
              map_api::LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_minus_1_right_logic_boundary->physical_boundaries.at(1).get().type,
              map_api::LaneBoundary::Type::kDashedLine);
}

TEST(MantleConverterTest, GivenDataStorageWithSimpleLineGeometry_WhenConverting_ThenLogicalLaneIsCorrectlyConverted)
{
    const auto data_storage = CreateDoubleLinedOneWayRoad();
    MantleMapConverter converter{};

    const auto& mantle_map = converter.Convert(data_storage);
    const auto& logical_lanes = mantle_map.logical_lanes;

    ASSERT_EQ(logical_lanes.size(), 1);
    const auto& logical_lane = logical_lanes[0];
    const auto& lane_minus_1_right_logical_boundaries = logical_lane->right_boundaries;
    ASSERT_EQ(lane_minus_1_right_logical_boundaries.size(), 1);
    const auto& lane_minus_1_right_logical_boundary = lane_minus_1_right_logical_boundaries[0];
    const auto& lane_minus_1_left_logical_boundaries = logical_lane->right_boundaries;
    ASSERT_EQ(lane_minus_1_left_logical_boundaries.size(), 1);
    const auto& lane_minus_1_left_logical_boundary = lane_minus_1_left_logical_boundaries[0];
    ASSERT_EQ(logical_lane->physical_lane_references.size(), 1);
    const auto& physical_lane_reference = logical_lane->physical_lane_references.at(0);

    EXPECT_EQ(logical_lane->move_direction, map_api::LogicalLane::MoveDirection::kIncreasingS);

    EXPECT_EQ(lane_minus_1_left_logical_boundary.get().reference_line->poly_line.size(), 101);
    EXPECT_EQ(lane_minus_1_right_logical_boundary.get().reference_line->poly_line.size(), 101);

    EXPECT_EQ(lane_minus_1_left_logical_boundary.get().boundary_line.size(), 101);
    EXPECT_EQ(lane_minus_1_right_logical_boundary.get().boundary_line.size(), 101);

    EXPECT_EQ(physical_lane_reference.start_s.value(), 0);
    EXPECT_EQ(physical_lane_reference.end_s.value(), 100);
    EXPECT_EQ(physical_lane_reference.physical_lane.local_id, -1);
}

TEST(MantleConverterTest,
     GivenDataStorageWithLaneOffset_WhenConverting_ThenLaneAndLogicalLanePointsAreCorrectlyConverted)
{
    // Here default Lane offset parameter is a = 0.1, b = 0.0, c = 0.0, d = 0.0.
    const auto data_storage = CreateStraightOneWayRoadWithLaneOffset();
    MantleMapConverter converter{};

    const auto expected_centerline_start_position_y = -1.4;
    const auto expected_right_lane_boundary_start_position_y = -2.9;
    const auto expected_left_lane_boundary_start_position_y = 0.1;
    const auto expected_logical_lane_right_boundary_start_position_y = -2.9;

    const auto& mantle_map = converter.Convert(data_storage);

    const auto& lanes = mantle_map.lanes;
    ASSERT_EQ(lanes.size(), 1);
    const auto& lane = lanes[0];
    EXPECT_EQ(lane->centerline.front().y(), expected_centerline_start_position_y);

    ASSERT_EQ(lane->right_lane_boundaries.size(), 1);
    const auto& right_lane_boundary = lane->right_lane_boundaries[0].get();
    EXPECT_EQ(right_lane_boundary.boundary_line.front().position.y.value(),
              expected_right_lane_boundary_start_position_y);

    ASSERT_EQ(lane->left_lane_boundaries.size(), 1);
    const auto& left_lane_boundary = lane->left_lane_boundaries[0].get();
    EXPECT_EQ(left_lane_boundary.boundary_line.front().position.y.value(),
              expected_left_lane_boundary_start_position_y);

    const auto& logical_lanes = mantle_map.logical_lanes;
    ASSERT_EQ(logical_lanes.size(), 1);
    const auto& logical_lane = logical_lanes[0];
    EXPECT_EQ(logical_lane->right_boundaries.front().get().boundary_line.front().position.y.value(),
              expected_logical_lane_right_boundary_start_position_y);
}

TEST(MantleConverterTest, GivenDataStorageWithBarrierOutOfRange_WhenConverting_ThenOutputWarning)
{
    // The road length is 100 meters. A barrier with s_end at 150 meters exceeds the valid range.
    auto data_storage = CreateStraightOneWayRoad();
    const units::length::meter_t s0(0);
    const road_logic_suite::RoadBarrier barrier{.s_start = units::length::meter_t(2),
                                                .s_end = units::length::meter_t(150),
                                                .t_start = units::length::meter_t(0),
                                                .t_end = units::length::meter_t(0.04)};
    data_storage.roads["0"].lane_sections.Find(s0)->second.center_lane.road_barriers.push_back(barrier);

    // The warning message is printed twice because the barrier lies on the centerline, which includes both the left and
    // right centerlines.
    const auto expected_warning_output =
        "[RoadLogicSuite: Barrier Conversion] WARNING: The s_end of the road barrier is larger than the road length "
        "(ID: 0). Points outside the range will be discarded during conversion!\n[RoadLogicSuite: Barrier Conversion] "
        "WARNING: The s_end of the road barrier is larger than the road length (ID: 0). Points outside the range will "
        "be discarded during conversion!\n";

    road_logic_suite::MapConverterConfig converter_config{};
    road_logic_suite::map_conversion::MantleMapConverter converter(converter_config);

    testing::internal::CaptureStdout();
    EXPECT_NO_THROW(converter.Convert(data_storage));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_warning_output);
}

}  // namespace
