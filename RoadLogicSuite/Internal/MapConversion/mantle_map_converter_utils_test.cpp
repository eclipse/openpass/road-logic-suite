/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace
{
using namespace road_logic_suite::map_conversion::utils;
using units::literals::operator""_m;

TEST(ConvertRoadMarkingsTypeTest,
     GivenValidRoadMarkingsTypeOnLeftSideRoad_WhenConverting_ThenParseRoadMarkingsTypeCorrectly)
{
    const bool is_left_side = true;

    EXPECT_EQ(map_api::LaneBoundary::Type::kUnknown,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::UNKNOWN, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kBottsDots,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::BOTTS_DOTS, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kDashedLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::BROKEN_BROKEN, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kDashedLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::BROKEN_SOLID, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kDashedLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::BROKEN, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kCurb,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::CURB, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kOther,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::CUSTOM, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kRoadEdge,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::EDGE, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kGrassEdge,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::GRASS, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kNoLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::NONE, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kSolidLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::SOLID_BROKEN, is_left_side));
}

TEST(ConvertRoadMarkingsTypeTest,
     GivenValidRoadMarkingsTypeOnRightSideRoad_WhenConverting_ThenParseRoadMarkingsTypeCorrectly)
{

    const bool is_left_side = false;

    EXPECT_EQ(map_api::LaneBoundary::Type::kSolidLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::BROKEN_SOLID, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kDashedLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::SOLID_BROKEN, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kSolidLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::SOLID_SOLID, is_left_side));
    EXPECT_EQ(map_api::LaneBoundary::Type::kSolidLine,
              ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType::SOLID, is_left_side));
}

TEST(ConvertRoadMarkingsTypeTest, GivenInvalidRoadMarkingsType_WhenConverting_ThenThrowError)
{
    const auto& invalid_road_mark_type = static_cast<road_logic_suite::types::RoadMarkType>(100);

    EXPECT_THROW(ConvertRoadMarkingsType(invalid_road_mark_type), std::runtime_error);
}

TEST(ConvertRoadMarkingsColorTest, GivenValidRoadMarkingsColorType_WhenConverting_ThenParseRoadMarkingsColorCorrectly)
{
    EXPECT_EQ(map_api::LaneBoundary::Color::kUnknown,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::UNKNOWN));
    EXPECT_EQ(map_api::LaneBoundary::Color::kBlue,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::BLUE));
    EXPECT_EQ(map_api::LaneBoundary::Color::kGreen,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::GREEN));
    EXPECT_EQ(map_api::LaneBoundary::Color::kOrange,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::ORANGE));
    EXPECT_EQ(map_api::LaneBoundary::Color::kRed,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::RED));
    EXPECT_EQ(map_api::LaneBoundary::Color::kStandard,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::STANDARD));
    EXPECT_EQ(map_api::LaneBoundary::Color::kViolet,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::VIOLET));
    EXPECT_EQ(map_api::LaneBoundary::Color::kWhite,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::WHITE));
    EXPECT_EQ(map_api::LaneBoundary::Color::kYellow,
              ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor::YELLOW));
}

TEST(ConvertRoadMarkingsColorTest, GivenInvalidRoadMarkingsColorType_WhenConverting_ThenThrowError)
{
    const auto& invalid_road_mark_color = static_cast<road_logic_suite::types::RoadMarkColor>(100);

    EXPECT_THROW(ConvertRoadMarkingsColor(invalid_road_mark_color), std::runtime_error);
}

TEST(ConvertStationaryObjectTypeTest,
     GivenValidStationaryObjectType_WhenConverting_ThenParseStationaryObjectTypeCorrectly)
{
    // lower case
    EXPECT_EQ(map_api::StationaryObject::Type::kBarrier, ConvertStationaryObjectType("barrier"));
    EXPECT_EQ(map_api::StationaryObject::Type::kBuilding, ConvertStationaryObjectType("building"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("crosswalk"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOverheadStructure, ConvertStationaryObjectType("gantry"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("none"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("obstacle"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("parkingspace"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("patch"));
    EXPECT_EQ(map_api::StationaryObject::Type::kPole, ConvertStationaryObjectType("pole"));
    EXPECT_EQ(map_api::StationaryObject::Type::kBarrier, ConvertStationaryObjectType("railing"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("roadmark"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("trafficisland"));
    EXPECT_EQ(map_api::StationaryObject::Type::kTree, ConvertStationaryObjectType("tree"));
    EXPECT_EQ(map_api::StationaryObject::Type::kVegetation, ConvertStationaryObjectType("vegetation"));
    EXPECT_EQ(map_api::StationaryObject::Type::kWall, ConvertStationaryObjectType("soundbarrier"));
    EXPECT_EQ(map_api::StationaryObject::Type::kPole, ConvertStationaryObjectType("streetlamp"));
    // upper case
    EXPECT_EQ(map_api::StationaryObject::Type::kBarrier, ConvertStationaryObjectType("BARRIER"));
    EXPECT_EQ(map_api::StationaryObject::Type::kBuilding, ConvertStationaryObjectType("BUILDING"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("CROSSWALK"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOverheadStructure, ConvertStationaryObjectType("GANTRY"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("NONE"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("OBSTACLE"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("PARKINGSPACE"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("PATCH"));
    EXPECT_EQ(map_api::StationaryObject::Type::kPole, ConvertStationaryObjectType("POLE"));
    EXPECT_EQ(map_api::StationaryObject::Type::kBarrier, ConvertStationaryObjectType("RAILING"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("ROADMARK"));
    EXPECT_EQ(map_api::StationaryObject::Type::kOther, ConvertStationaryObjectType("TRAFFICISLAND"));
    EXPECT_EQ(map_api::StationaryObject::Type::kTree, ConvertStationaryObjectType("TREE"));
    EXPECT_EQ(map_api::StationaryObject::Type::kVegetation, ConvertStationaryObjectType("VEGETATION"));
    EXPECT_EQ(map_api::StationaryObject::Type::kWall, ConvertStationaryObjectType("SOUNDBARRIER"));
    EXPECT_EQ(map_api::StationaryObject::Type::kPole, ConvertStationaryObjectType("STREETLAMP"));
}

TEST(ConvertStationaryObjectTypeTest,
     GivenInvalidStationaryObjectType_WhenConverting_ThenReturnsUnknownAndPrintsWarningMessage)
{
    testing::internal::CaptureStderr();
    const std::string& invalid_stationary_object_type = "invalid_object_type";
    const std::string& expected_string_output =
        "[RoadLogicSuite: StationaryObjectType Conversion] WARNING: Invalid StationaryObjectType input: " +
        invalid_stationary_object_type + ", please check your map file.\n";

    EXPECT_EQ(map_api::StationaryObject::Type::kUnknown, ConvertStationaryObjectType(invalid_stationary_object_type));
    std::string console_output = testing::internal::GetCapturedStderr();
    EXPECT_EQ(expected_string_output, console_output);
}

TEST(IsRoadMarkTypeDoubleLinedTest, GivenValidRoadMarkType_WhenCheckingIfDoubleLined_ThenRoadMarkTypeIsCheckedCorrectly)
{
    // Testing for false cases
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::UNKNOWN));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::BOTTS_DOTS));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::BROKEN));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::CURB));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::CUSTOM));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::EDGE));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::GRASS));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::NONE));
    EXPECT_EQ(false, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::SOLID));

    // Testing for true cases
    EXPECT_EQ(true, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::BROKEN_BROKEN));
    EXPECT_EQ(true, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::BROKEN_SOLID));
    EXPECT_EQ(true, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::SOLID_BROKEN));
    EXPECT_EQ(true, IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType::SOLID_SOLID));
}

TEST(IsRoadMarkTypeDoubleLinedTest, GivenInvalidRoadMarkType_WhenCheckingIfDoubleLined_ThenThrowError)
{
    const auto& invalid_road_mark_type = static_cast<road_logic_suite::types::RoadMarkType>(100);

    EXPECT_THROW(IsRoadMarkTypeDoubleLined(invalid_road_mark_type), std::runtime_error);
}

class GetMantleLaneIdsFromLaneIdsTest : public ::testing::Test
{
  protected:
    road_logic_suite::OpenDriveData data;
    std::string road_id;
    std::vector<road_logic_suite::types::LaneId> lane_ids;
    road_logic_suite::map_conversion::MantleIdProvider id_provider;
};

TEST_F(GetMantleLaneIdsFromLaneIdsTest, GivenNonExistedRoadId_WhenFunctionCalled_ThenReturnEmpty)
{
    const road_logic_suite::Road road{.id = "1"};
    data.roads.insert_or_assign("1", road);
    lane_ids = {1, 2, 3};
    road_id = "nonexistent_road_id";
    auto result = GetMantleMapLaneIdFromOpenDriveLaneIds(data, road_id, lane_ids, id_provider);
    EXPECT_TRUE(result.empty());
}

TEST_F(GetMantleLaneIdsFromLaneIdsTest, GivenExistedRoadButNoLaneSections_WhenFunctionCalled_ThenReturnEmpty)
{
    road_id = "1";
    const road_logic_suite::Road road{.id = road_id};
    data.roads.insert_or_assign(road_id, road);

    lane_ids = {1, 2, 3};

    testing::internal::CaptureStderr();
    auto result = GetMantleMapLaneIdFromOpenDriveLaneIds(data, road_id, lane_ids, id_provider);
    std::string output = testing::internal::GetCapturedStderr();

    EXPECT_TRUE(result.empty());
    EXPECT_NE(output.find("WARNING: No lane section exists in open drive data"), std::string::npos);
}

TEST_F(GetMantleLaneIdsFromLaneIdsTest,
       GivenValidRoadAndLaneSectionButNoLaneIdsMapToMantleIds_WhenFunctionCalled_ThenReturnEmpty)
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
        lane_sections{};
    road_logic_suite::types::LaneSection lane_section{};
    lane_sections.Insert(units::length::meter_t(0), lane_section);
    road_id = "1";
    const road_logic_suite::Road road{
        .id = road_id,
        .lane_sections = lane_sections,
    };
    data.roads.insert_or_assign(road_id, road);

    lane_ids = {1, 2, 3};

    auto result = GetMantleMapLaneIdFromOpenDriveLaneIds(data, road_id, lane_ids, id_provider);

    EXPECT_TRUE(result.empty());
}

TEST_F(GetMantleLaneIdsFromLaneIdsTest, GivenValidInputs_WhenFunctionCalled_ThenReturnValidMantleLaneIds)
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
        lane_sections{};
    road_logic_suite::types::LaneSection lane_section{};
    lane_sections.Insert(units::length::meter_t(0), lane_section);
    road_id = "1";
    const road_logic_suite::Road road{
        .id = road_id,
        .lane_sections = lane_sections,
    };
    data.roads.insert_or_assign(road_id, road);
    lane_ids = {1, 2, 3};
    for (const auto ele : lane_ids)
    {
        const road_logic_suite::map_conversion::LaneInfo lane_info{
            .road_id = "1", .lane_section_id = 0, .lane_id = ele};
        id_provider.GetOrGenerateLaneId(lane_info);
    }

    std::vector<mantle_api::UniqueId> expected_result = {1, 2, 3};

    auto result = GetMantleMapLaneIdFromOpenDriveLaneIds(data, road_id, lane_ids, id_provider);

    EXPECT_EQ(result, expected_result);
}

class CreateAssignedLanesTest : public ::testing::Test
{
  protected:
    map_api::Map map;
    std::vector<mantle_api::UniqueId> mantle_lane_ids;
};

TEST_F(CreateAssignedLanesTest, GivenNoLanesFoundInMap_WhenFunctionCalled_ThenReturnEmptyAndOutputWarning)
{
    mantle_lane_ids = {1, 2, 3};

    testing::internal::CaptureStderr();
    auto result = CreateAssignedLanes(map, mantle_lane_ids);
    std::string output = testing::internal::GetCapturedStderr();

    EXPECT_TRUE(result.empty());
    EXPECT_NE(output.find("WARNING: An expected lane with id 1 was not found"), std::string::npos);
    EXPECT_NE(output.find("WARNING: An expected lane with id 2 was not found"), std::string::npos);
    EXPECT_NE(output.find("WARNING: An expected lane with id 3 was not found"), std::string::npos);
}

TEST_F(CreateAssignedLanesTest, GivenAllMantleLaneIdsFoundInMap_WhenFunctionCalled_ThenReturnEmptyAndOutputWarning)
{
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{1}));
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{2}));
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{3}));
    mantle_lane_ids = {1, 2, 3};

    auto result = CreateAssignedLanes(map, mantle_lane_ids);

    ASSERT_EQ(result.size(), 3);
    EXPECT_EQ(result[0].get().id, 1);
    EXPECT_EQ(result[1].get().id, 2);
    EXPECT_EQ(result[2].get().id, 3);
}

TEST_F(CreateAssignedLanesTest,
       GivenSomeMantleLaneIdsFoundInMap_WhenFunctionCalled_ThenReturnValidResultAndOutputWarning)
{
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{1}));
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{3}));
    mantle_lane_ids = {1, 2, 3};

    testing::internal::CaptureStderr();
    auto result = CreateAssignedLanes(map, mantle_lane_ids);
    std::string output = testing::internal::GetCapturedStderr();

    ASSERT_EQ(result.size(), 2);
    EXPECT_EQ(result[0].get().id, 1);
    EXPECT_EQ(result[1].get().id, 3);
    EXPECT_NE(output.find("WARNING: An expected lane with id 2 was not found"), std::string::npos);
}

TEST_F(CreateAssignedLanesTest,
       GivenNoneOfTheMantleLaneIdsFoundInMap_WhenFunctionCalled_ThenReturnEmptyAndOutputWarning)
{
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{4}));
    map.lanes.emplace_back(std::make_unique<map_api::Lane>(map_api::Lane{5}));
    mantle_lane_ids = {1, 2, 3};

    testing::internal::CaptureStderr();
    auto result = CreateAssignedLanes(map, mantle_lane_ids);
    std::string output = testing::internal::GetCapturedStderr();

    EXPECT_TRUE(result.empty());
    EXPECT_NE(output.find("WARNING: An expected lane with id 1 was not found"), std::string::npos);
    EXPECT_NE(output.find("WARNING: An expected lane with id 2 was not found"), std::string::npos);
    EXPECT_NE(output.find("WARNING: An expected lane with id 3 was not found"), std::string::npos);
}

class CreateReferenceLinePointAtTest : public ::testing::Test
{
  protected:
    std::unique_ptr<road_logic_suite::CoordinateConverter> coordinate_converter;
    road_logic_suite::OpenDriveData data;
    std::string road_id;
    units::length::meter_t road_length = 0_m;

    void InitializeTestDataWithLineGeometry()
    {
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
            lane_sections{};
        road_logic_suite::types::LaneSection lane_section{};
        lane_sections.Insert(units::length::meter_t(0), lane_section);

        road_id = "1";
        road_length = 100_m;
        road_logic_suite::Road road{
            .id = road_id,
            .length = road_length,
            .index_geometries_start = 0,
            .index_geometries_end = 1,
            .lane_sections = lane_sections,
        };
        data.roads.insert_or_assign(road_id, road);

        data.geometries.push_back(
            std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                     road_logic_suite::Vec2<units::length::meter_t>{0_m, 0_m},
                                                     units::angle::radian_t(0),
                                                     road_length,
                                                     road_id));

        coordinate_converter = std::make_unique<road_logic_suite::CoordinateConverter>(data);
    }

    void InitializeTestDataWithoutLineGeometry()
    {
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
            lane_sections{};
        road_logic_suite::types::LaneSection lane_section{};
        lane_sections.Insert(units::length::meter_t(0), lane_section);

        road_id = "1";
        road_length = 100_m;
        road_logic_suite::Road road{
            .id = road_id,
            .length = road_length,
            .lane_sections = lane_sections,
        };
        data.roads.insert_or_assign(road_id, road);

        coordinate_converter = std::make_unique<road_logic_suite::CoordinateConverter>(data);
    }
};

TEST_F(CreateReferenceLinePointAtTest, GivenNoGeometry_WhenCreating_ThenReturnsNullopt)
{
    InitializeTestDataWithoutLineGeometry();
    const auto queried_s_position = 50_m;
    const auto created_point = CreateReferenceLinePointAt(*coordinate_converter, queried_s_position, road_id);

    EXPECT_FALSE(created_point.has_value());
}

TEST_F(CreateReferenceLinePointAtTest,
       GivenValidLineGeometry_WhenCreatingWhenCreatingPointsOutsideGeometryRange_ThenReturnsNullopt)
{
    InitializeTestDataWithLineGeometry();

    const auto queried_s_position_before = -1_m;
    const auto created_point_before =
        CreateReferenceLinePointAt(*coordinate_converter, queried_s_position_before, road_id);
    EXPECT_FALSE(created_point_before.has_value());

    const auto queried_s_position_after = 101_m;
    const auto created_point_after =
        CreateReferenceLinePointAt(*coordinate_converter, queried_s_position_after, road_id);
    EXPECT_FALSE(created_point_after.has_value());

    // Test with invalid road_id
    const auto created_point_invalid_road = CreateReferenceLinePointAt(*coordinate_converter, 50_m, "invalid_road_id");
    EXPECT_FALSE(created_point_invalid_road.has_value());
}

TEST_F(CreateReferenceLinePointAtTest,
       GivenValidLineGeometry_WhenCreatingPointsInsideGeometryRange_ThenReturnCorrectReferenceLinePoint)
{
    InitializeTestDataWithLineGeometry();

    const auto queried_s_position_1 = 0_m;
    const auto queried_s_position_2 = 10_m;
    const auto queried_s_position_3 = 100_m;
    const auto created_point_1 = CreateReferenceLinePointAt(*coordinate_converter, queried_s_position_1, road_id);
    const auto created_point_2 = CreateReferenceLinePointAt(*coordinate_converter, queried_s_position_2, road_id);
    const auto created_point_3 = CreateReferenceLinePointAt(*coordinate_converter, queried_s_position_3, road_id);

    ASSERT_TRUE(created_point_1.has_value());
    EXPECT_EQ(created_point_1->world_position.x.value(), 0);
    EXPECT_EQ(created_point_1->world_position.y.value(), 0);
    EXPECT_EQ(created_point_1->world_position.z.value(), 0);
    EXPECT_EQ(created_point_1->s_position, queried_s_position_1);
    EXPECT_EQ(created_point_1->t_axis_yaw, units::angle::radian_t(M_PI_2));

    ASSERT_TRUE(created_point_2.has_value());
    EXPECT_EQ(created_point_2->world_position.x.value(), 10);
    EXPECT_EQ(created_point_2->world_position.y.value(), 0);
    EXPECT_EQ(created_point_2->world_position.z.value(), 0);
    EXPECT_EQ(created_point_2->s_position, queried_s_position_2);
    EXPECT_EQ(created_point_2->t_axis_yaw, units::angle::radian_t(M_PI_2));

    ASSERT_TRUE(created_point_3.has_value());
    EXPECT_EQ(created_point_3->world_position.x.value(), 100);
    EXPECT_EQ(created_point_3->world_position.y.value(), 0);
    EXPECT_EQ(created_point_3->world_position.z.value(), 0);
    EXPECT_EQ(created_point_3->s_position, queried_s_position_3);
    EXPECT_EQ(created_point_3->t_axis_yaw, units::angle::radian_t(M_PI_2));
}

class CreateLogicBoundaryPointAtTest : public ::testing::Test
{
  protected:
    std::unique_ptr<road_logic_suite::CoordinateConverter> coordinate_converter;
    road_logic_suite::OpenDriveData data;
    road_logic_suite::types::LaneSection lane_section;
    std::string road_id;
    units::length::meter_t road_length = 0_m;

    auto CreateSimpleCenterLane() { return road_logic_suite::types::Lane{.id = 0}; }

    auto CreateSimpleDrivingLane(const road_logic_suite::types::Lane::LaneId& id, const std::string& lane_type = "")
    {
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::Poly3> constant_width{};
        constant_width.Insert(units::length::meter_t(0), road_logic_suite::types::Poly3(3, 0, 0, 0));
        return road_logic_suite::types::Lane{
            .id = id,
            .width_polys = constant_width,
            .type = lane_type,
        };
    }

    void InitializeTestDataWithLineGeometry()
    {
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
            lane_sections{};
        lane_section = road_logic_suite::types::LaneSection{
            .s_start = 0_m,
            .s_end = 100_m,
            .right_lanes = {CreateSimpleDrivingLane(-1)},
            .center_lane = CreateSimpleCenterLane(),
        };
        lane_sections.Insert(units::length::meter_t(0), lane_section);

        road_id = "1";
        road_length = 100_m;
        road_logic_suite::Road road{
            .id = road_id,
            .length = road_length,
            .index_geometries_start = 0,
            .index_geometries_end = 1,
            .lane_sections = lane_sections,
        };
        data.roads.insert_or_assign(road_id, road);

        data.geometries.push_back(
            std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                     road_logic_suite::Vec2<units::length::meter_t>{0_m, 0_m},
                                                     units::angle::radian_t(0),
                                                     road_length,
                                                     road_id));

        coordinate_converter = std::make_unique<road_logic_suite::CoordinateConverter>(data);
    }

    void InitializeTestDataWithoutLineGeometry()
    {
        road_logic_suite::types::RangeBasedMap<units::length::meter_t, road_logic_suite::types::LaneSection>
            lane_sections{};
        lane_section = road_logic_suite::types::LaneSection{
            .s_start = 0_m,
            .s_end = 100_m,
            .right_lanes = {CreateSimpleDrivingLane(-1)},
            .center_lane = CreateSimpleCenterLane(),
        };
        lane_sections.Insert(units::length::meter_t(0), lane_section);

        road_id = "1";
        road_length = 100_m;
        road_logic_suite::Road road{
            .id = road_id,
            .length = road_length,
            .lane_sections = lane_sections,
        };
        data.roads.insert_or_assign(road_id, road);

        coordinate_converter = std::make_unique<road_logic_suite::CoordinateConverter>(data);
    }
};

TEST_F(CreateLogicBoundaryPointAtTest, GivenNoGeometry_WhenCreating_ThenReturnsNullopt)
{
    InitializeTestDataWithoutLineGeometry();
    const auto queried_s_position = 50_m;
    const auto created_point =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position, 0_m, road_id, lane_section, -1);

    EXPECT_FALSE(created_point.has_value());
}

TEST_F(CreateLogicBoundaryPointAtTest,
       GivenValidLineGeometry_WhenCreatingWhenCreatingPointsOutsideGeometryRange_ThenReturnsNullopt)
{
    InitializeTestDataWithLineGeometry();

    const auto queried_s_position_before = -1_m;
    const auto created_point_before =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position_before, 0_m, road_id, lane_section, -1);
    EXPECT_FALSE(created_point_before.has_value());

    const auto queried_s_position_after = 101_m;
    const auto created_point_after =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position_after, 0_m, road_id, lane_section, -1);
    EXPECT_FALSE(created_point_after.has_value());

    // Test with invalid road_id
    const auto created_point_invalid_road =
        CreateLogicBoundaryPointAt(*coordinate_converter, 50_m, 0_m, "invalid_road_id", lane_section, -1);
    EXPECT_FALSE(created_point_invalid_road.has_value());
}

TEST_F(CreateLogicBoundaryPointAtTest,
       GivenValidLineGeometry_WhenCreatingPointsInsideGeometryRange_ThenReturnCorrectLogicBoundaryPoint)
{
    InitializeTestDataWithLineGeometry();

    const auto queried_s_position_1 = 0_m;
    const auto queried_s_position_2 = 10_m;
    const auto queried_s_position_3 = 100_m;
    const auto created_point_1 =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position_1, 0_m, road_id, lane_section, -1);
    const auto created_point_2 =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position_2, 0_m, road_id, lane_section, -1);
    const auto created_point_3 =
        CreateLogicBoundaryPointAt(*coordinate_converter, queried_s_position_3, 0_m, road_id, lane_section, -1);

    ASSERT_TRUE(created_point_1.has_value());
    EXPECT_EQ(created_point_1->position.x.value(), 0);
    EXPECT_EQ(created_point_1->position.y.value(), -3);
    EXPECT_EQ(created_point_1->position.z.value(), 0);
    EXPECT_EQ(created_point_1->s_position, queried_s_position_1);
    EXPECT_EQ(created_point_1->t_position, -3_m);

    ASSERT_TRUE(created_point_2.has_value());
    EXPECT_EQ(created_point_2->position.x.value(), 10);
    EXPECT_EQ(created_point_2->position.y.value(), -3);
    EXPECT_EQ(created_point_2->position.z.value(), 0);
    EXPECT_EQ(created_point_2->s_position, queried_s_position_2);
    EXPECT_EQ(created_point_2->t_position, -3_m);

    ASSERT_TRUE(created_point_3.has_value());
    EXPECT_EQ(created_point_3->position.x.value(), 100);
    EXPECT_EQ(created_point_3->position.y.value(), -3);
    EXPECT_EQ(created_point_3->position.z.value(), 0);
    EXPECT_EQ(created_point_3->s_position, queried_s_position_3);
    EXPECT_EQ(created_point_3->t_position, -3_m);
}

TEST(ConvertPassingRuleTest, GivenValidRoadMarkType_WhenConverting_ThenReturnExpectedPassingRule)
{
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::UNKNOWN, 0),
              map_api::LogicalLaneBoundary::PassingRule::kUnknown);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BOTTS_DOTS, 0),
              map_api::LogicalLaneBoundary::PassingRule::kBothAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BROKEN_BROKEN, 0),
              map_api::LogicalLaneBoundary::PassingRule::kBothAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BROKEN, 0),
              map_api::LogicalLaneBoundary::PassingRule::kBothAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::CURB, 0),
              map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::CUSTOM, 0),
              map_api::LogicalLaneBoundary::PassingRule::kOther);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::EDGE, 0),
              map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::GRASS, 0),
              map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::NONE, 0),
              map_api::LogicalLaneBoundary::PassingRule::kBothAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::SOLID_SOLID, 0),
              map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::SOLID, 0),
              map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed);
}

TEST(ConvertPassingRuleTest, GivenValidRoadMarkTypeWithDifferentLaneId_WhenConverting_ThenReturnExpectedPassingRule)
{
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BROKEN_SOLID, 1),
              map_api::LogicalLaneBoundary::PassingRule::kIncreasingT);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BROKEN_SOLID, 0),
              map_api::LogicalLaneBoundary::PassingRule::kDecreasingT);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::BROKEN_SOLID, -1),
              map_api::LogicalLaneBoundary::PassingRule::kDecreasingT);

    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::SOLID_BROKEN, 1),
              map_api::LogicalLaneBoundary::PassingRule::kDecreasingT);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::SOLID_BROKEN, 0),
              map_api::LogicalLaneBoundary::PassingRule::kIncreasingT);
    EXPECT_EQ(ConvertPassingRule(road_logic_suite::types::RoadMarkType::SOLID_BROKEN, -1),
              map_api::LogicalLaneBoundary::PassingRule::kIncreasingT);
}

TEST(ConvertPassingRuleTest, GivenInvalidRoadMarkType_WhenConverting_ThenThrowError)
{
    EXPECT_THROW(ConvertPassingRule(static_cast<road_logic_suite::types::RoadMarkType>(100), 0), std::runtime_error);
}

TEST(SetLogicalLaneTypeTest, GivenValidLaneSubtypes_WhenSettingType_ThenReturnExpectedLogicalLaneType)
{
    map_api::LogicalLane logical_lane;

    SetLogicalLaneType(logical_lane, "unknown");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kUnknown);

    SetLogicalLaneType(logical_lane, "other");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kOther);

    SetLogicalLaneType(logical_lane, "normal");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kNormal);

    SetLogicalLaneType(logical_lane, "biking");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kBiking);

    SetLogicalLaneType(logical_lane, "sidewalk");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kSidewalk);

    SetLogicalLaneType(logical_lane, "parking");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kParking);

    SetLogicalLaneType(logical_lane, "stop");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kStop);

    SetLogicalLaneType(logical_lane, "restricted");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kRestricted);

    SetLogicalLaneType(logical_lane, "border");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kBorder);

    SetLogicalLaneType(logical_lane, "shoulder");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kShoulder);

    SetLogicalLaneType(logical_lane, "exit");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kExit);

    SetLogicalLaneType(logical_lane, "entry");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kEntry);

    SetLogicalLaneType(logical_lane, "median");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kMedian);

    SetLogicalLaneType(logical_lane, "curb");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kCurb);

    SetLogicalLaneType(logical_lane, "rail");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kRail);

    SetLogicalLaneType(logical_lane, "tram");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kTram);
}

TEST(SetLogicalLaneTypeTest, GivenAlternativeSpellings_WhenSettingType_ThenReturnExpectedLogicalLaneType)
{
    map_api::LogicalLane logical_lane;

    SetLogicalLaneType(logical_lane, "onramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kOnramp);
    SetLogicalLaneType(logical_lane, "on_ramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kOnramp);

    SetLogicalLaneType(logical_lane, "offramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kOfframp);
    SetLogicalLaneType(logical_lane, "off_ramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kOfframp);

    SetLogicalLaneType(logical_lane, "connectingramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kConnectingramp);
    SetLogicalLaneType(logical_lane, "connecting_ramp");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kConnectingramp);
}

TEST(SetLogicalLaneTypeTest, GivenUnknownString_WhenSettingType_ThenReturnUnknown)
{
    map_api::LogicalLane logical_lane;

    SetLogicalLaneType(logical_lane, "some_unknown_type");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kUnknown);
}

TEST(SetLogicalLaneTypeTest, GivenDifferentCases_WhenSettingType_ThenReturnExpectedLogicalLaneType)
{
    map_api::LogicalLane logical_lane;

    SetLogicalLaneType(logical_lane, "NORMAL");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kNormal);

    SetLogicalLaneType(logical_lane, "BiKiNg");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kBiking);

    SetLogicalLaneType(logical_lane, "sIdEwAlK");
    EXPECT_EQ(logical_lane.type, map_api::LogicalLane::Type::kSidewalk);
}

TEST(SetLogicalLaneMovingDirectionTest, GivenDifferentSideInputs_WhenSettingDirection_ThenReturnExpectedMoveDirection)
{
    map_api::LogicalLane logical_lane;

    // Test for right-side driving (default behavior)
    SetLogicalLaneMovingDirection(logical_lane);
    EXPECT_EQ(logical_lane.move_direction, map_api::LogicalLane::MoveDirection::kIncreasingS);

    SetLogicalLaneMovingDirection(logical_lane, false);
    EXPECT_EQ(logical_lane.move_direction, map_api::LogicalLane::MoveDirection::kIncreasingS);

    SetLogicalLaneMovingDirection(logical_lane, true);
    EXPECT_EQ(logical_lane.move_direction, map_api::LogicalLane::MoveDirection::kDecreasingS);
}

class CreateLogicalLaneRefWithIdTest : public ::testing::Test
{
  protected:
    map_api::Map::LogicalLanes logical_lanes;

    void SetUp() override
    {
        // Create some test logical lanes
        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 1}));
        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 2}));
        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 3}));
    }
};

TEST_F(CreateLogicalLaneRefWithIdTest, GivenEmptyLogicalLanes_WhenCreatingRef_ThenReturnNullopt)
{
    map_api::Map::LogicalLanes empty_lanes;
    auto result = CreateLogicalLaneRefWithId(empty_lanes, static_cast<map_api::Identifier>(1));
    EXPECT_FALSE(result.has_value());
}

TEST_F(CreateLogicalLaneRefWithIdTest, GivenExistingId_WhenCreatingRef_ThenReturnValidReference)
{
    auto result = CreateLogicalLaneRefWithId(logical_lanes, static_cast<map_api::Identifier>(2));
    EXPECT_TRUE(result.has_value());
    EXPECT_EQ(result->get().id, map_api::Identifier(2));
}

TEST_F(CreateLogicalLaneRefWithIdTest, GivenNonExistingId_WhenCreatingRef_ThenReturnNullopt)
{
    auto result = CreateLogicalLaneRefWithId(logical_lanes, static_cast<map_api::Identifier>(4));
    EXPECT_FALSE(result.has_value());
}

class CreateLogicalLaneRelationWithIdTest : public ::testing::Test
{
  protected:
    map_api::Map::LogicalLanes logical_lanes;
    std::unique_ptr<map_api::LogicalLane> test_lane;

    void SetUp() override
    {
        // Create some test logical lanes
        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 1}));
        logical_lanes.back()->start_s = 0.0_m;
        logical_lanes.back()->end_s = 100.0_m;

        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 2}));
        logical_lanes.back()->start_s = 50.0_m;
        logical_lanes.back()->end_s = 150.0_m;

        logical_lanes.push_back(std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 3}));
        logical_lanes.back()->start_s = 100.0_m;
        logical_lanes.back()->end_s = 200.0_m;

        // Create a test lane for input
        test_lane = std::make_unique<map_api::LogicalLane>(map_api::LogicalLane{.id = 4});
        test_lane->start_s = 0.0_m;
        test_lane->end_s = 50.0_m;
    }
};

TEST_F(CreateLogicalLaneRelationWithIdTest, GivenEmptyLogicalLanes_WhenCreatingRelation_ThenReturnNullopt)
{
    map_api::Map::LogicalLanes empty_lanes;
    auto result = CreateLogicalLaneRelationWithId(empty_lanes, *test_lane, static_cast<map_api::Identifier>(1));
    EXPECT_FALSE(result.has_value());
}

TEST_F(CreateLogicalLaneRelationWithIdTest, GivenNonExistingId_WhenCreatingRelation_ThenReturnNullopt)
{
    auto result = CreateLogicalLaneRelationWithId(logical_lanes, *test_lane, static_cast<map_api::Identifier>(5));
    EXPECT_FALSE(result.has_value());
}

TEST_F(CreateLogicalLaneRelationWithIdTest, GivenDifferentIds_WhenCreatingRelation_ThenReturnCorrectRelations)
{
    auto result1 = CreateLogicalLaneRelationWithId(logical_lanes, *test_lane, static_cast<map_api::Identifier>(1));
    auto result2 = CreateLogicalLaneRelationWithId(logical_lanes, *test_lane, static_cast<map_api::Identifier>(2));
    auto result3 = CreateLogicalLaneRelationWithId(logical_lanes, *test_lane, static_cast<map_api::Identifier>(3));

    EXPECT_TRUE(result1.has_value());
    EXPECT_TRUE(result2.has_value());
    EXPECT_TRUE(result3.has_value());

    EXPECT_EQ(result1.value().other_lane.id, static_cast<map_api::Identifier>(1));
    EXPECT_EQ(result1.value().start_s, 0.0_m);
    EXPECT_EQ(result1.value().end_s, 50.0_m);
    EXPECT_EQ(result1.value().start_s_other, 0.0_m);
    EXPECT_EQ(result1.value().end_s_other, 100.0_m);

    EXPECT_EQ(result2.value().other_lane.id, static_cast<map_api::Identifier>(2));
    EXPECT_EQ(result2.value().start_s, 0.0_m);
    EXPECT_EQ(result2.value().end_s, 50.0_m);
    EXPECT_EQ(result2.value().start_s_other, 50.0_m);
    EXPECT_EQ(result2.value().end_s_other, 150.0_m);

    EXPECT_EQ(result3.value().other_lane.id, static_cast<map_api::Identifier>(3));
    EXPECT_EQ(result3.value().start_s, 0.0_m);
    EXPECT_EQ(result3.value().end_s, 50.0_m);
    EXPECT_EQ(result3.value().start_s_other, 100.0_m);
    EXPECT_EQ(result3.value().end_s_other, 200.0_m);
}

TEST(CalculateLaneOffsetAtTest, GivenRoadWithLaneOffsets_WhenCalculatingOffset_ThenReturnExpectedOffset)
{
    road_logic_suite::Road road;
    road.length = 100.0_m;

    const auto laneoffset_param_a = 0.1 - 0.0 * 50 + 3.9e-03 * 50 * 50 - (-5.2e-05) * 50 * 50 * 50;
    const auto laneoffset_param_b = 0.0 - 2 * 3.9e-03 * 50 + 3 * (-5.2e-05) * 50 * 50;
    const auto laneoffset_param_c = 3.9e-03 - 3 * (-5.2e-05) * 50;
    const auto laneoffset_param_d = -5.2e-05;

    road.laneoffsets = {{0.0_m, road_logic_suite::types::Poly3{0, 0, 0, 0}},
                        {50.0_m,
                         road_logic_suite::types::Poly3{
                             laneoffset_param_a, laneoffset_param_b, laneoffset_param_c, laneoffset_param_d}}};

    const auto s_25 = 25.0_m;
    const auto expected_laneoffset_s25 = 0.0_m;
    units::length::meter_t actual_laneoffset_s25 =
        road_logic_suite::map_conversion::utils::CalculateLaneOffsetAt(road, s_25);
    EXPECT_EQ(actual_laneoffset_s25, expected_laneoffset_s25);

    // Calculate the lane offset at s = 50
    const auto s_50 = 50.0_m;
    const auto expected_laneoffset_s50 = 0.1_m;
    units::length::meter_t actual_laneoffset_s50 =
        road_logic_suite::map_conversion::utils::CalculateLaneOffsetAt(road, s_50);
    EXPECT_NEAR(actual_laneoffset_s50.value(), expected_laneoffset_s50.value(), 1e-6);

    // Calculate the lane offset at s = 100
    const auto s_100 = 100.0_m;
    const auto expected_laneoffset_s100 = 3.35_m;
    units::length::meter_t actual_laneoffset_s100 =
        road_logic_suite::map_conversion::utils::CalculateLaneOffsetAt(road, s_100);
    EXPECT_NEAR(actual_laneoffset_s100.value(), expected_laneoffset_s100.value(), 1e-6);
}

TEST(CalculateLaneOffsetAtTest, GivenInvalidSValue_WhenCalculatingOffset_ThenThrowError)
{
    road_logic_suite::Road road;
    road.length = 100.0_m;

    road.laneoffsets = {{0.0_m, road_logic_suite::types::Poly3{0, 0, 0, 0}},
                        {50.0_m, road_logic_suite::types::Poly3{0, 0, 0, 0}}};

    // Calculate the lane offset at a given s position greater than the road length
    units::length::meter_t s(150);
    EXPECT_THROW(road_logic_suite::map_conversion::utils::CalculateLaneOffsetAt(road, s), std::runtime_error);
}

}  // namespace
