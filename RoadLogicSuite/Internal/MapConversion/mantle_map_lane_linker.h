/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_MAP_LANE_LINKER_H
#define ROADLOGICSUITE_MANTLE_MAP_LANE_LINKER_H

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "RoadLogicSuite/Internal/road.h"

#include <MapAPI/lane.h>

namespace road_logic_suite::map_conversion
{
class MantleMapLaneLinker
{
  public:
    /// @brief Adds predecessor and successor IDs to the \p lane_relation_ids. This needs roads, lane sections and lanes
    /// in order to properly map internal lanes to lane IDs.
    /// @param data_storage the data storage, provided by the road logic suite.
    /// @param road the currently converted road
    /// @param lane_section the currently converted lane section
    /// @param lane the current converted lane
    /// @param id_provider the ID provider, used to reserve IDs
    /// @param lane_relation_ids the struct of the lane's predecessor, successor, left- and right-adjacent lane IDs
    static void StoreLinks(const OpenDriveData& data_storage,
                           const road_logic_suite::Road& road,
                           const road_logic_suite::types::LaneSection& lane_section,
                           const road_logic_suite::types::Lane& lane,
                           road_logic_suite::map_conversion::MantleIdProvider& id_provider,
                           road_logic_suite::map_conversion::utils::LaneRelationIds& lane_relation_ids);

  private:
    MantleMapLaneLinker(const OpenDriveData& data_storage,
                        const Road& road,
                        const types::LaneSection& lane_section,
                        const types::Lane& lane,
                        MantleIdProvider& id_provider,
                        utils::LaneRelationIds& lane_relation_ids)
        : data_storage_(data_storage),
          road_(road),
          lane_section_(lane_section),
          lane_(lane),
          id_provider_(id_provider),
          lane_relation_ids_(lane_relation_ids)
    {
    }
    void HandleOpenDrivePredecessors();
    void HandleOpenDriveSuccessors();
    void HandleRoadPredecessors();
    void HandleRoadSuccessors();
    void AddPredecessorsTowardsSection(const std::string& road_id, std::int64_t section_id);
    void AddSuccessorsTowardsSection(const std::string& road_id, std::int64_t section_id);

    const OpenDriveData& data_storage_;
    const road_logic_suite::Road& road_;
    const road_logic_suite::types::LaneSection& lane_section_;
    const road_logic_suite::types::Lane& lane_;
    road_logic_suite::map_conversion::MantleIdProvider& id_provider_;
    road_logic_suite::map_conversion::utils::LaneRelationIds& lane_relation_ids_;
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_MAP_LANE_LINKER_H
