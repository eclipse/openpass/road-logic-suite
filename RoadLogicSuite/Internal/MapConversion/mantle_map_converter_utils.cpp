/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"

namespace road_logic_suite::map_conversion::utils
{
std::vector<mantle_api::UniqueId> GetMantleMapLaneIdFromOpenDriveLaneIds(
    const road_logic_suite::OpenDriveData& data,
    const std::string& road_id,
    const std::vector<road_logic_suite::types::LaneId>& lane_ids,
    road_logic_suite::map_conversion::MantleIdProvider& id_provider)
{
    std::vector<mantle_api::UniqueId> result;

    auto it = data.roads.find(road_id);
    if (it == data.roads.end())
    {
        std::cerr << "[RoadLogicSuite: Signal/Object Conversion] WARNING: Road ID not found in open drive data."
                  << std::endl;
        return result;
    }

    const auto& lane_sections = it->second.lane_sections;
    if (lane_sections.size() == 0)
    {
        std::cerr << "[RoadLogicSuite: Signal/Object Conversion] WARNING: No lane section exists in open drive data. "
                  << "The conversion from signal/object valid lanes ids to mantle lanes ids failed." << std::endl;
        return result;
    }

    result.reserve(lane_sections.size() * lane_ids.size());

    for (const auto& lane_id : lane_ids)
    {
        for (uint32_t i = 0; i < lane_sections.size(); ++i)
        {
            const road_logic_suite::map_conversion::LaneInfo lane_info{
                .road_id = road_id, .lane_section_id = i, .lane_id = lane_id};
            if (const auto mantle_lane_id = id_provider.QueryLaneId(lane_info); mantle_lane_id.has_value())
            {
                result.emplace_back(mantle_lane_id.value());
            }
        }
    }

    return result;
}

std::vector<map_api::RefWrapper<map_api::Lane>> CreateAssignedLanes(
    const map_api::Map& map,
    const std::vector<mantle_api::UniqueId>& mantle_lane_ids)
{
    std::vector<map_api::RefWrapper<map_api::Lane>> result;
    result.reserve(mantle_lane_ids.size());

    for (const auto& lane_id : mantle_lane_ids)
    {
        if (auto lane_ref = utils::FindLaneWithId(map.lanes, lane_id))
        {
            result.push_back(lane_ref.value());
        }
        else
        {
            std::cerr << "[RoadLogicSuite: Signal/Object AssignedLanes Conversion] WARNING: An expected lane with id " +
                             std::to_string(lane_id) + " was not found in the mantle map lanes vector."
                      << std::endl;
        }
    }
    return result;
}

}  // namespace road_logic_suite::map_conversion::utils
