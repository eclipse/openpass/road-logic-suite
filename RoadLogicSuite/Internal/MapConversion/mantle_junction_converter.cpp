/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "RoadLogicSuite/Internal/MapConversion/mantle_junction_converter.h"

namespace road_logic_suite::map_conversion
{

void MantleJunctionConverter::DeduceLaneRelationFromJunctions(
    const OpenDriveData& data_storage,
    MantleIdProvider& id_provider,
    std::map<mantle_api::UniqueId, utils::LaneRelationIds>& id_to_lane_relation)
{
    MantleJunctionConverter junction_converter(data_storage, id_provider, id_to_lane_relation);

    for (const auto& junction : data_storage.junctions)
    {
        junction_converter.DeduceLaneRelationFromJunction(junction.second);
    }
}

void MantleJunctionConverter::DeduceLaneRelationFromJunction(const types::Junction& junction)
{
    for (const auto& junction_connection : junction.junction_connections)
    {
        const auto incoming_road_id = junction_connection.incoming_road;
        const auto connecting_road_id = junction_connection.connecting_road;

        if (junction_connection.junction_lane_links.empty())
        {
            std::cout
                << "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID " << junction.id
                << " and connection ID " << junction_connection.id
                << " has no laneLink elements. Lane relations (successor/predecessor) cannot be deduced. Please check "
                   "the map file!"
                << std::endl;
            continue;
        }

        for (const auto& junction_lane_link : junction_connection.junction_lane_links)
        {
            DeduceLaneRelationFromJunctionLaneLink(incoming_road_id, connecting_road_id, junction_lane_link);
        }
    }
}

void MantleJunctionConverter::DeduceLaneRelationFromJunctionLaneLink(const std::string& incoming_road_id,
                                                                     const std::string& connecting_road_id,
                                                                     const types::JunctionLaneLink& junction_lane_link)
{
    auto handleWarning = [](const std::string& road_id, const std::string& road_type) {
        std::cout << "[RoadLogicSuite: Junction Conversion] WARNING: " << road_type << " Road with ID " << road_id
                  << " not found. Lane relations (successor/predecessor) cannot be deduced. Please check the map file!"
                  << std::endl;
    };

    auto createLaneRelation = [&](const LaneInfo& lane_info_from, const LaneInfo& lane_info_to) {
        const auto mantle_lane_id_from = id_provider_.GetOrGenerateLaneId(lane_info_from);
        id_to_lane_relation_.insert({mantle_lane_id_from, {}});
        id_to_lane_relation_.at(mantle_lane_id_from)
            .successor_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info_to));
    };

    auto section_id_from = (junction_lane_link.from < 0) ? QueryRoadLastSectionIndex(incoming_road_id) : 0;
    auto section_id_to = (junction_lane_link.to < 0) ? 0 : QueryRoadLastSectionIndex(connecting_road_id);

    if (!section_id_from.has_value())
    {
        handleWarning(incoming_road_id, "Incoming");
        return;
    }

    if (!section_id_to.has_value())
    {
        handleWarning(connecting_road_id, "Connecting");
        return;
    }

    const LaneInfo lane_info_from{
        .road_id = incoming_road_id, .lane_section_id = section_id_from.value(), .lane_id = junction_lane_link.from};

    const LaneInfo lane_info_to{
        .road_id = connecting_road_id, .lane_section_id = section_id_to.value(), .lane_id = junction_lane_link.to};

    createLaneRelation(lane_info_from, lane_info_to);
}

std::optional<std::int64_t> MantleJunctionConverter::QueryRoadLastSectionIndex(const std::string& queried_road_id)
{
    std::optional<std::int64_t> queried_result = std::nullopt;
    if (const auto queried_road_it = data_storage_.roads.find(queried_road_id);
        queried_road_it != data_storage_.roads.end())
    {
        const auto& queried_road = queried_road_it->second;
        const auto lane_section_id = queried_road.lane_sections.FindIndex(queried_road.length);

        queried_result = lane_section_id;
    }
    return queried_result;
}

}  // namespace road_logic_suite::map_conversion
