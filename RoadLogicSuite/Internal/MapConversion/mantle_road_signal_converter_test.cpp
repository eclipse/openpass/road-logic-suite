/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_road_signal_converter.h"

#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <MapAPI/map.h>
#include <gmock/gmock.h>

namespace
{

using road_logic_suite::map_conversion::MantleIdProvider;
using units::literals::operator""_m;
using units::literals::operator""_rad;

constexpr double kAbsError = 1e-6;

auto CreateMainSignal()
{
    road_logic_suite::Signal dummy_signal;

    // Assign dummy values
    dummy_signal.name = "dummy_traffic_sign";
    dummy_signal.id = "1";
    dummy_signal.is_dynamic = false;
    dummy_signal.country = "DE";
    dummy_signal.country_revision = "2023";
    dummy_signal.type = "274";
    dummy_signal.subtype = "56";

    dummy_signal.value = 60.0;
    dummy_signal.unit = road_logic_suite::SignalUnit::kKilometerPerHour;

    dummy_signal.length = 0.1_m;
    dummy_signal.width = 2.0_m;
    dummy_signal.height = 3.0_m;

    dummy_signal.yaw = 1.0_rad;
    dummy_signal.pitch = 2.0_rad;
    dummy_signal.roll = 3.0_rad;

    dummy_signal.orientation = road_logic_suite::SignalOrientation::kNone;

    dummy_signal.text = "speed limit";

    dummy_signal.position = {10.0_m, 5.0_m, 0.0_m};

    dummy_signal.shape = {{0.05_m, 1.0_m}, {-0.05_m, 1.0_m}, {-0.05_m, -1.0_m}, {0.05_m, -1.0_m}};

    dummy_signal.valid_lanes = {};

    dummy_signal.dependencies = {};

    return dummy_signal;
}

auto CreateMainAndSupplementarySign()
{
    road_logic_suite::Signal main_sign;
    main_sign.id = "1";
    main_sign.type = "274";
    main_sign.dependencies = {"2"};

    road_logic_suite::Signal supplementary_sign;

    // Assign dummy values
    supplementary_sign.name = "supplementary_sign";
    supplementary_sign.id = "2";
    supplementary_sign.is_dynamic = false;
    supplementary_sign.type = "1010";
    supplementary_sign.subtype = "51";

    supplementary_sign.length = 0.1_m;
    supplementary_sign.width = 2.0_m;
    supplementary_sign.height = 3.0_m;

    supplementary_sign.yaw = 1.0_rad;
    supplementary_sign.pitch = 2.0_rad;
    supplementary_sign.roll = 3.0_rad;

    supplementary_sign.position = {10.0_m, 5.0_m, 0.0_m};

    supplementary_sign.shape = {{0.05_m, 1.0_m}, {-0.05_m, 1.0_m}, {-0.05_m, -1.0_m}, {0.05_m, -1.0_m}};

    return std::unordered_map<std::string, road_logic_suite::Signal>{{"1", main_sign}, {"2", supplementary_sign}};
}

auto CreateRoadMarking()
{
    road_logic_suite::Signal dummy_road_marking;

    dummy_road_marking.name = "dummy_road_marking";
    dummy_road_marking.id = "1";
    dummy_road_marking.country = "DE";
    dummy_road_marking.country_revision = "2023";
    dummy_road_marking.type = "293";
    dummy_road_marking.subtype = "";

    dummy_road_marking.length = 1.0_m;
    dummy_road_marking.width = 2.0_m;
    dummy_road_marking.height = 0.0_m;

    dummy_road_marking.yaw = 1.0_rad;
    dummy_road_marking.pitch = 0.2_rad;
    dummy_road_marking.roll = 0.3_rad;

    dummy_road_marking.position = {10.0_m, 5.0_m, 0.0_m};

    dummy_road_marking.shape = {{0.5_m, 1.0_m}, {-0.5_m, 1.0_m}, {-0.5_m, -1.0_m}, {0.5_m, -1.0_m}};
    return dummy_road_marking;
}

TEST(MantleRoadSignalConverterTest,
     GivenDataStorageWithOnlyMainSingal_WhenConvertingToIMap_ThenSingalMainSignIsCorrectlyConverted)
{
    const auto& signal = CreateMainSignal();
    road_logic_suite::OpenDriveData data{.signals = {{"1", signal}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    const auto expected_position_x = signal.position.x();
    const auto expected_position_y = signal.position.y();
    const auto expected_position_z = signal.position.z() + signal.height.value() * 0.5;
    const auto expected_rotation_roll = signal.roll.value();
    const auto expected_rotation_pitch = signal.pitch.value();
    const auto expected_rotation_yaw = signal.yaw.value();
    const auto expected_length = 0.1;
    const auto expected_width = 2.0;
    const auto expected_height = 3.0;
    const auto expected_variability = map_api::TrafficSignVariability::kFixed;
    const auto expected_main_sign_type = map_api::MainSignType::kSpeedLimitBegin;
    const auto expected_sign_value = 60.0;
    const auto expected_sign_value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    const auto expected_sign_text = "speed limit";
    const auto expected_type_str = "274";
    const auto expected_subtype_str = "56";
    const auto expected_country_str = "DE";
    const auto expected_country_revision_str = "2023";

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);

    // test position.
    ASSERT_EQ(map.traffic_signs.size(), 1);
    const auto& main_sign = map.traffic_signs[0]->main_sign;
    const auto& main_sign_base = map.traffic_signs[0]->main_sign.base;

    EXPECT_NEAR(main_sign_base.position.x(), expected_position_x, kAbsError);
    EXPECT_NEAR(main_sign_base.position.y(), expected_position_y, kAbsError);
    EXPECT_NEAR(main_sign_base.position.z(), expected_position_z, kAbsError);

    // test rotation.
    EXPECT_NEAR(main_sign_base.orientation.yaw(), expected_rotation_yaw, kAbsError);
    EXPECT_NEAR(main_sign_base.orientation.pitch(), expected_rotation_pitch, kAbsError);
    EXPECT_NEAR(main_sign_base.orientation.roll(), expected_rotation_roll, kAbsError);

    // test base polygon.
    ASSERT_EQ(main_sign_base.base_polygon.size(), 4);
    for (int i = 0; i < 4; i++)
    {
        EXPECT_NEAR(main_sign_base.base_polygon[i].x(), signal.shape[i].x(), kAbsError);
        EXPECT_NEAR(main_sign_base.base_polygon[i].y(), signal.shape[i].y(), kAbsError);
    }

    // test dimension
    ASSERT_NEAR(main_sign_base.dimension.length(), expected_length, kAbsError);
    ASSERT_NEAR(main_sign_base.dimension.width(), expected_width, kAbsError);
    ASSERT_NEAR(main_sign_base.dimension.height(), expected_height, kAbsError);

    EXPECT_EQ(main_sign.variability, expected_variability);
    EXPECT_EQ(main_sign.type, expected_main_sign_type);
    EXPECT_EQ(main_sign.value.value, expected_sign_value);
    EXPECT_EQ(main_sign.value.value_unit, expected_sign_value_unit);
    EXPECT_EQ(main_sign.value.text, expected_sign_text);
    EXPECT_TRUE(main_sign.assigned_lanes.empty());
    EXPECT_EQ(main_sign.vertically_mirrored, false);
    EXPECT_EQ(main_sign.code, expected_type_str);
    EXPECT_EQ(main_sign.sub_code, expected_subtype_str);
    EXPECT_EQ(main_sign.country, expected_country_str);
    EXPECT_EQ(main_sign.country_revision, expected_country_revision_str);
}

TEST(
    MantleRoadSignalConverterTest,
    GivenDataStorageWithMainSingalAndSupplementarySignal_WhenConvertingToIMap_ThenMainAndSupplementarySignalIsCorrectlyConverted)
{
    const auto& signal_map = CreateMainAndSupplementarySign();
    road_logic_suite::OpenDriveData data{.signals = signal_map};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    const auto expected_position_x = signal_map.at("2").position.x();
    const auto expected_position_y = signal_map.at("2").position.y();
    const auto expected_position_z = signal_map.at("2").position.z() + signal_map.at("2").height.value() * 0.5;
    const auto expected_rotation_roll = signal_map.at("2").roll.value();
    const auto expected_rotation_pitch = signal_map.at("2").pitch.value();
    const auto expected_rotation_yaw = signal_map.at("2").yaw.value();
    const auto expected_length = 0.1;
    const auto expected_width = 2.0;
    const auto expected_height = 3.0;
    const auto expected_type_str = "1010";
    const auto expected_subtype_str = "51";
    const auto expected_type = map_api::SupplementarySignType::kConstrainedTo;

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);

    // signal with id 2 is converted as supplementary sign, so the size of traffic_sign is one only containing the main
    // sign with id 1.
    ASSERT_EQ(map.traffic_signs.size(), 1);
    const auto& supplementary_signs = map.traffic_signs[0]->supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);
    const auto& supplementary_sign = supplementary_signs[0];
    const auto& supplementary_sign_base = supplementary_sign.base;

    EXPECT_NEAR(supplementary_sign_base.position.x(), expected_position_x, kAbsError);
    EXPECT_NEAR(supplementary_sign_base.position.y(), expected_position_y, kAbsError);
    EXPECT_NEAR(supplementary_sign_base.position.z(), expected_position_z, kAbsError);

    EXPECT_NEAR(supplementary_sign_base.orientation.yaw(), expected_rotation_yaw, kAbsError);
    EXPECT_NEAR(supplementary_sign_base.orientation.pitch(), expected_rotation_pitch, kAbsError);
    EXPECT_NEAR(supplementary_sign_base.orientation.roll(), expected_rotation_roll, kAbsError);

    ASSERT_EQ(supplementary_sign_base.base_polygon.size(), 4);
    for (int i = 0; i < 4; i++)
    {
        EXPECT_NEAR(supplementary_sign_base.base_polygon[i].x(), signal_map.at("2").shape[i].x(), kAbsError);
        EXPECT_NEAR(supplementary_sign_base.base_polygon[i].y(), signal_map.at("2").shape[i].y(), kAbsError);
    }

    ASSERT_NEAR(supplementary_sign_base.dimension.length(), expected_length, kAbsError);
    ASSERT_NEAR(supplementary_sign_base.dimension.width(), expected_width, kAbsError);
    ASSERT_NEAR(supplementary_sign_base.dimension.height(), expected_height, kAbsError);

    EXPECT_EQ(supplementary_sign.type, expected_type);
    EXPECT_THAT(supplementary_sign.actors, ::testing::ElementsAre(map_api::SupplementarySignActor::kTrucks));
    ASSERT_EQ(supplementary_sign.arrows.size(), 1);
    EXPECT_THAT(supplementary_sign.arrows[0].direction,
                ::testing::ElementsAre(map_api::SupplementarySignArrow::Direction::kUnknown));
    EXPECT_EQ(supplementary_sign.code, expected_type_str);
    EXPECT_EQ(supplementary_sign.sub_code, expected_subtype_str);
}

TEST(MantleRoadSignalConverterTest,
     GivenDataStorageWithRoadMarking_WhenConvertingToIMap_ThenRoadMarkingIsCorrectlyConverted)
{
    const auto& signal = CreateRoadMarking();
    road_logic_suite::OpenDriveData data{.signals = {{"1", signal}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    const auto expected_position_x = signal.position.x();
    const auto expected_position_y = signal.position.y();
    const auto expected_position_z = signal.position.z() + signal.height.value() * 0.5;
    const auto expected_rotation_roll = signal.roll.value();
    const auto expected_rotation_pitch = signal.pitch.value();
    const auto expected_rotation_yaw = signal.yaw.value();
    const auto expected_length = signal.length.value();
    const auto expected_width = signal.width.value();
    const auto expected_height = signal.height.value();
    const auto expected_type = map_api::RoadMarking::Type::kSymbolicTrafficSign;
    const auto expected_main_sign_type = map_api::MainSignType::kZebraCrossing;
    const auto expected_type_str = "293";
    const auto expected_subtype_str = "";
    const auto expected_country_str = "DE";
    const auto expected_country_revision_str = "2023";

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);

    // test position.
    ASSERT_EQ(map.road_markings.size(), 1);
    const auto& road_marking = map.road_markings[0];
    const auto& road_marking_base = road_marking->base;

    EXPECT_NEAR(road_marking_base.position.x(), expected_position_x, kAbsError);
    EXPECT_NEAR(road_marking_base.position.y(), expected_position_y, kAbsError);
    EXPECT_NEAR(road_marking_base.position.z(), expected_position_z, kAbsError);

    // test rotation.
    EXPECT_NEAR(road_marking_base.orientation.yaw(), expected_rotation_yaw, kAbsError);
    EXPECT_NEAR(road_marking_base.orientation.pitch(), expected_rotation_pitch, kAbsError);
    EXPECT_NEAR(road_marking_base.orientation.roll(), expected_rotation_roll, kAbsError);

    // test base polygon.
    ASSERT_EQ(road_marking_base.base_polygon.size(), 4);
    for (int i = 0; i < 4; i++)
    {
        EXPECT_NEAR(road_marking_base.base_polygon[i].x(), signal.shape[i].x(), kAbsError);
        EXPECT_NEAR(road_marking_base.base_polygon[i].y(), signal.shape[i].y(), kAbsError);
    }

    // test dimension
    ASSERT_NEAR(road_marking_base.dimension.length(), expected_length, kAbsError);
    ASSERT_NEAR(road_marking_base.dimension.width(), expected_width, kAbsError);
    ASSERT_NEAR(road_marking_base.dimension.height(), expected_height, kAbsError);

    EXPECT_EQ(road_marking->type, expected_type);
    EXPECT_EQ(road_marking->traffic_main_sign_type, expected_main_sign_type);
    EXPECT_TRUE(road_marking->assigned_lanes.empty());
    EXPECT_EQ(road_marking->code, expected_type_str);
    EXPECT_EQ(road_marking->sub_code, expected_subtype_str);
    EXPECT_EQ(road_marking->country, expected_country_str);
    EXPECT_EQ(road_marking->country_revision, expected_country_revision_str);
}

class MantleRoadSignalConverterMainSignTypeTest
    : public ::testing::TestWithParam<std::pair<std::string, map_api::MainSignType>>
{
};

TEST_P(MantleRoadSignalConverterMainSignTypeTest, GivenMainSignWithType_WhenConvertingToIMap_ThenCorrectTypeIsSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{.signals = {{"1", road_logic_suite::Signal{.type = param.first}}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);
    ASSERT_EQ(map.traffic_signs.size(), 1);
    EXPECT_EQ(map.traffic_signs[0]->main_sign.type, param.second);
}

const std::vector<std::pair<std::string, map_api::MainSignType>> kMainSignTestCases{
    {"101", map_api::MainSignType::kDangerSpot},
    {"101-11", map_api::MainSignType::kZebraCrossing},
    {"101-21", map_api::MainSignType::kZebraCrossing},
    {"350-10", map_api::MainSignType::kZebraCrossing},
    {"350-20", map_api::MainSignType::kZebraCrossing},
    {"101-10", map_api::MainSignType::kFlight},
    {"101-20", map_api::MainSignType::kFlight},
    {"101-12", map_api::MainSignType::kCattle},
    {"101-22", map_api::MainSignType::kCattle},
    {"101-13", map_api::MainSignType::kHorseRiders},
    {"101-23", map_api::MainSignType::kHorseRiders},
    {"101-14", map_api::MainSignType::kAmphibians},
    {"101-24", map_api::MainSignType::kAmphibians},
    {"101-15", map_api::MainSignType::kFallingRocks},
    {"101-25", map_api::MainSignType::kFallingRocks},
    {"101-51", map_api::MainSignType::kSnowOrIce},
    {"101-52", map_api::MainSignType::kLooseGravel},
    {"101-53", map_api::MainSignType::kWaterside},
    {"101-54", map_api::MainSignType::kClearance},
    {"101-55", map_api::MainSignType::kMovableBridge},
    {"102", map_api::MainSignType::kRightBeforeLeftNextIntersection},
    {"103-10", map_api::MainSignType::kTurnLeft},
    {"103-20", map_api::MainSignType::kTurnRight},
    {"105-10", map_api::MainSignType::kDoubleTurnLeft},
    {"105-20", map_api::MainSignType::kDoubleTurnRight},
    {"108", map_api::MainSignType::kHillDownwards},
    {"110", map_api::MainSignType::kHillUpwards},
    {"112", map_api::MainSignType::kUnevenRoad},
    {"114", map_api::MainSignType::kRoadSlipperyWetOrDirty},
    {"117-10", map_api::MainSignType::kSideWinds},
    {"117-20", map_api::MainSignType::kSideWinds},
    {"120", map_api::MainSignType::kRoadNarrowing},
    {"121-10", map_api::MainSignType::kRoadNarrowingRight},
    {"121-20", map_api::MainSignType::kRoadNarrowingLeft},
    {"123", map_api::MainSignType::kRoadWorks},
    {"124", map_api::MainSignType::kTrafficQueues},
    {"125", map_api::MainSignType::kTwoWayTraffic},
    {"131", map_api::MainSignType::kAttentionTrafficLight},
    {"133-10", map_api::MainSignType::kPedestrians},
    {"133-20", map_api::MainSignType::kPedestrians},
    {"136-10", map_api::MainSignType::kChildrenCrossing},
    {"136-20", map_api::MainSignType::kChildrenCrossing},
    {"138-10", map_api::MainSignType::kCycleRoute},
    {"138-20", map_api::MainSignType::kCycleRoute},
    {"142-10", map_api::MainSignType::kDeerCrossing},
    {"142-20", map_api::MainSignType::kDeerCrossing},
    {"151", map_api::MainSignType::kUngatedLevelCrossing},
    {"157-10", map_api::MainSignType::kLevelCrossingMarker},
    {"159-10", map_api::MainSignType::kLevelCrossingMarker},
    {"161-10", map_api::MainSignType::kLevelCrossingMarker},
    {"157-20", map_api::MainSignType::kLevelCrossingMarker},
    {"159-20", map_api::MainSignType::kLevelCrossingMarker},
    {"162-20", map_api::MainSignType::kLevelCrossingMarker},
    {"201-50", map_api::MainSignType::kRailwayTrafficPriority},
    {"201-52", map_api::MainSignType::kRailwayTrafficPriority},
    {"205", map_api::MainSignType::kGiveWay},
    {"206", map_api::MainSignType::kStop},
    {"208", map_api::MainSignType::kPriorityToOppositeDirection},
    {"209-10", map_api::MainSignType::kPrescribedLeftTurn},
    {"209-20", map_api::MainSignType::kPrescribedRightTurn},
    {"209-30", map_api::MainSignType::kPrescribedStraight},
    {"211", map_api::MainSignType::kPrescribedRightWay},
    {"211-10", map_api::MainSignType::kPrescribedLeftWay},
    {"214", map_api::MainSignType::kPrescribedRightTurnAndStraight},
    {"214-10", map_api::MainSignType::kPrescribedLeftTurnAndStraight},
    {"214-30", map_api::MainSignType::kPrescribedLeftTurnAndRightTurn},
    {"215", map_api::MainSignType::kRoundabout},
    {"220-10", map_api::MainSignType::kOnewayLeft},
    {"220-20", map_api::MainSignType::kOnewayRight},
    {"222", map_api::MainSignType::kPassLeft},
    {"222-10", map_api::MainSignType::kPassRight},
    {"223.1-50", map_api::MainSignType::kSideLaneOpenForTraffic},
    {"223.1-51", map_api::MainSignType::kSideLaneOpenForTraffic},
    {"223.1-52", map_api::MainSignType::kSideLaneOpenForTraffic},
    {"223.2-50", map_api::MainSignType::kSideLaneClosedForTraffic},
    {"223.2-51", map_api::MainSignType::kSideLaneClosedForTraffic},
    {"223.2-52", map_api::MainSignType::kSideLaneClosedForTraffic},
    {"223.3-50", map_api::MainSignType::kSideLaneClosingForTraffic},
    {"223.3-51", map_api::MainSignType::kSideLaneClosingForTraffic},
    {"223.3-52", map_api::MainSignType::kSideLaneClosingForTraffic},
    {"224", map_api::MainSignType::kBusStop},
    {"237", map_api::MainSignType::kBicyclesOnly},
    {"238", map_api::MainSignType::kHorseRidersOnly},
    {"239", map_api::MainSignType::kPedestriansOnly},
    {"240", map_api::MainSignType::kBicyclesPedestriansSharedOnly},
    {"241-30", map_api::MainSignType::kBicyclesPedestriansSeparatedLeftOnly},
    {"241-31", map_api::MainSignType::kBicyclesPedestriansSeparatedRightOnly},
    {"242.1", map_api::MainSignType::kPedestrianZoneBegin},
    {"242.2", map_api::MainSignType::kPedestrianZoneEnd},
    {"244.1", map_api::MainSignType::kBicycleRoadBegin},
    {"244.2", map_api::MainSignType::kBicycleRoadEnd},
    {"245", map_api::MainSignType::kBusLane},
    {"250", map_api::MainSignType::kAllProhibited},
    {"251", map_api::MainSignType::kMotorizedMultitrackProhibited},
    {"253", map_api::MainSignType::kTrucksProhibited},
    {"254", map_api::MainSignType::kBicyclesProhibited},
    {"255", map_api::MainSignType::kMotorcyclesProhibited},
    {"257-50", map_api::MainSignType::kMopedsProhibited},
    {"257-51", map_api::MainSignType::kHorseRidersProhibited},
    {"257-52", map_api::MainSignType::kHorseCarriagesProhibited},
    {"257-53", map_api::MainSignType::kCattleProhibited},
    {"257-54", map_api::MainSignType::kBusesProhibited},
    {"257-55", map_api::MainSignType::kCarsProhibited},
    {"257-56", map_api::MainSignType::kCarsTrailersProhibited},
    {"257-57", map_api::MainSignType::kTrucksTrailersProhibited},
    {"257-58", map_api::MainSignType::kTractorsProhibited},
    {"259", map_api::MainSignType::kPedestriansProhibited},
    {"260", map_api::MainSignType::kMotorVehiclesProhibited},
    {"261", map_api::MainSignType::kHazardousGoodsVehiclesProhibited},
    {"262", map_api::MainSignType::kOverWeightVehiclesProhibited},
    {"263", map_api::MainSignType::kVehiclesAxleOverWeightProhibited},
    {"264", map_api::MainSignType::kVehiclesExcessWidthProhibited},
    {"265", map_api::MainSignType::kVehiclesExcessHeightProhibited},
    {"266", map_api::MainSignType::kVehiclesExcessLengthProhibited},
    {"267", map_api::MainSignType::kDoNotEnter},
    {"268", map_api::MainSignType::kSnowChainsRequired},
    {"269", map_api::MainSignType::kWaterPollutantVehiclesProhibited},
    {"270.1", map_api::MainSignType::kEnvironmentalZoneBegin},
    {"270.2", map_api::MainSignType::kEnvironmentalZoneEnd},
    {"272", map_api::MainSignType::kNoUTurnLeft},
    {"273", map_api::MainSignType::kMinimumDistanceForTrucks},
    {"274", map_api::MainSignType::kSpeedLimitBegin},
    {"274.1", map_api::MainSignType::kSpeedLimitZoneBegin},
    {"274.2", map_api::MainSignType::kSpeedLimitZoneEnd},
    {"275", map_api::MainSignType::kMinimumSpeedBegin},
    {"276", map_api::MainSignType::kOvertakingBanBegin},
    {"277", map_api::MainSignType::kOvertakingBanForTrucksBegin},
    {"278", map_api::MainSignType::kSpeedLimitEnd},
    {"279", map_api::MainSignType::kMinimumSpeedEnd},
    {"280", map_api::MainSignType::kOvertakingBanEnd},
    {"281", map_api::MainSignType::kOvertakingBanForTrucksEnd},
    {"282", map_api::MainSignType::kAllRestrictionsEnd},
    {"283", map_api::MainSignType::kNoStopping},
    {"286", map_api::MainSignType::kNoParking},
    {"290.1", map_api::MainSignType::kNoParkingZoneBegin},
    {"290.2", map_api::MainSignType::kNoParkingZoneEnd},
    {"301", map_api::MainSignType::kRightOfWayNextIntersection},
    {"306", map_api::MainSignType::kRightOfWayBegin},
    {"307", map_api::MainSignType::kRightOfWayEnd},
    {"308", map_api::MainSignType::kPriorityOverOppositeDirection},
    {"310", map_api::MainSignType::kTownBegin},
    {"311", map_api::MainSignType::kTownEnd},
    {"314", map_api::MainSignType::kCarParking},
    {"314-50", map_api::MainSignType::kCarParking},
    {"316", map_api::MainSignType::kCarParking},
    {"317", map_api::MainSignType::kCarParking},
    {"318", map_api::MainSignType::kCarParking},
    {"314-10", map_api::MainSignType::kCarParking},
    {"314-20", map_api::MainSignType::kCarParking},
    {"314-30", map_api::MainSignType::kCarParking},
    {"314.1", map_api::MainSignType::kCarParkingZoneBegin},
    {"314.2", map_api::MainSignType::kCarParkingZoneEnd},
    {"315-50", map_api::MainSignType::kSidewalkHalfParkingLeft},
    {"315-55", map_api::MainSignType::kSidewalkHalfParkingRight},
    {"315-60", map_api::MainSignType::kSidewalkParkingLeft},
    {"315-65", map_api::MainSignType::kSidewalkParkingRight},
    {"315-70", map_api::MainSignType::kSidewalkPerpendicularHalfParkingLeft},
    {"315-75", map_api::MainSignType::kSidewalkPerpendicularHalfParkingRight},
    {"315-80", map_api::MainSignType::kSidewalkPerpendicularParkingLeft},
    {"315-85", map_api::MainSignType::kSidewalkPerpendicularParkingRight},
    {"325.1", map_api::MainSignType::kLivingStreetBegin},
    {"325.2", map_api::MainSignType::kLivingStreetEnd},
    {"327", map_api::MainSignType::kTunnel},
    {"328", map_api::MainSignType::kEmergencyStoppingRight},
    {"330.1", map_api::MainSignType::kHighwayBegin},
    {"330.2", map_api::MainSignType::kHighwayEnd},
    {"331.1", map_api::MainSignType::kExpresswayBegin},
    {"331.2", map_api::MainSignType::kExpresswayEnd},
    {"332", map_api::MainSignType::kNamedHighwayExit},
    {"332.1", map_api::MainSignType::kNamedExpresswayExit},
    {"332.1-20", map_api::MainSignType::kNamedRoadExit},
    {"333", map_api::MainSignType::kHighwayExit},
    {"333.1", map_api::MainSignType::kExpresswayExit},
    {"353", map_api::MainSignType::kOnewayStreet},
    {"356", map_api::MainSignType::kCrossingGuards},
    {"357", map_api::MainSignType::kDeadend},
    {"357-50", map_api::MainSignType::kDeadendExcludingDesignatedActors},
    {"357-51", map_api::MainSignType::kDeadendExcludingDesignatedActors},
    {"357-52", map_api::MainSignType::kDeadendExcludingDesignatedActors},
    {"358", map_api::MainSignType::kFirstAidStation},
    {"363", map_api::MainSignType::kPoliceStation},
    {"365-50", map_api::MainSignType::kTelephone},
    {"365-51", map_api::MainSignType::kTelephone},
    {"365-52", map_api::MainSignType::kFillingStation},
    {"365-53", map_api::MainSignType::kFillingStation},
    {"365-54", map_api::MainSignType::kFillingStation},
    {"365-65", map_api::MainSignType::kFillingStation},
    {"365-66", map_api::MainSignType::kFillingStation},
    {"365-55", map_api::MainSignType::kHotel},
    {"365-56", map_api::MainSignType::kInn},
    {"365-57", map_api::MainSignType::kKiosk},
    {"365-58", map_api::MainSignType::kToilet},
    {"365-59", map_api::MainSignType::kChapel},
    {"365-61", map_api::MainSignType::kTouristInfo},
    {"365-62", map_api::MainSignType::kRepairService},
    {"365-63", map_api::MainSignType::kPedestrianUnderpass},
    {"365-64", map_api::MainSignType::kPedestrianBridge},
    {"365-67", map_api::MainSignType::kCamperPlace},
    {"365-68", map_api::MainSignType::kCamperPlace},
    {"380", map_api::MainSignType::kAdvisorySpeedLimitBegin},
    {"381", map_api::MainSignType::kAdvisorySpeedLimitEnd},
    {"385", map_api::MainSignType::kPlaceName},
    {"386.1", map_api::MainSignType::kTouristAttraction},
    {"386.2", map_api::MainSignType::kTouristRoute},
    {"386.3", map_api::MainSignType::kTouristArea},
    {"388", map_api::MainSignType::kShoulderNotPassableMotorVehicles},
    {"389", map_api::MainSignType::kShoulderUnsafeTrucksTractors},
    {"390", map_api::MainSignType::kTollBegin},
    {"390.2", map_api::MainSignType::kTollEnd},
    {"391", map_api::MainSignType::kTollRoad},
    {"392", map_api::MainSignType::kCustoms},
    {"393", map_api::MainSignType::kInternationalBorderInfo},
    {"401", map_api::MainSignType::kFederalHighwayRouteNumber},
    {"405", map_api::MainSignType::kHighwayRouteNumber},
    {"410", map_api::MainSignType::kEuropeanRouteNumber},
    {"415-10", map_api::MainSignType::kFederalHighwayDirectionLeft},
    {"415-20", map_api::MainSignType::kFederalHighwayDirectionRight},
    {"418-10", map_api::MainSignType::kPrimaryRoadDirectionLeft},
    {"418-20", map_api::MainSignType::kPrimaryRoadDirectionRight},
    {"419-10", map_api::MainSignType::kSecondaryRoadDirectionLeft},
    {"419-20", map_api::MainSignType::kSecondaryRoadDirectionRight},
    {"421-20", map_api::MainSignType::kDirectionDesignatedActorsRight},
    {"421-21", map_api::MainSignType::kDirectionDesignatedActorsRight},
    {"421-22", map_api::MainSignType::kDirectionDesignatedActorsRight},
    {"422-20", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-22", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-24", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-26", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-21", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-23", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-25", map_api::MainSignType::kRoutingDesignatedActors},
    {"422-27", map_api::MainSignType::kRoutingDesignatedActors},
    {"442-20", map_api::MainSignType::kRoutingDesignatedActors},
    {"442-22", map_api::MainSignType::kRoutingDesignatedActors},
    {"442-23", map_api::MainSignType::kRoutingDesignatedActors},
    {"430-10", map_api::MainSignType::kDirectionToHighwayLeft},
    {"430-20", map_api::MainSignType::kDirectionToHighwayRight},
    {"432-10", map_api::MainSignType::kDirectionToLocalDestinationLeft},
    {"432-20", map_api::MainSignType::kDirectionToLocalDestinationRight},
    {"437", map_api::MainSignType::kStreetName},
    {"438", map_api::MainSignType::kDirectionPreannouncement},
    {"439", map_api::MainSignType::kDirectionPreannouncementLaneConfig},
    {"440", map_api::MainSignType::kDirectionPreannouncementHighwayEntries},
    {"448", map_api::MainSignType::kHighwayAnnouncement},
    {"448-50", map_api::MainSignType::kOtherRoadAnnouncement},
    {"448.1", map_api::MainSignType::kHighwayAnnouncementTruckStop},
    {"449", map_api::MainSignType::kHighwayPreannouncementDirections},
    {"450", map_api::MainSignType::kPoleExit},
    {"454-10", map_api::MainSignType::kDetourLeft},
    {"454-20", map_api::MainSignType::kDetourRight},
    {"455.1", map_api::MainSignType::kNumberedDetour},
    {"457.1", map_api::MainSignType::kDetourBegin},
    {"457.2", map_api::MainSignType::kDetourEnd},
    {"458", map_api::MainSignType::kDetourRoutingBoard},
    {"460-50", map_api::MainSignType::kOptionalDetour},
    {"460-10", map_api::MainSignType::kOptionalDetour},
    {"460-11", map_api::MainSignType::kOptionalDetour},
    {"460-12", map_api::MainSignType::kOptionalDetour},
    {"460-20", map_api::MainSignType::kOptionalDetour},
    {"460-21", map_api::MainSignType::kOptionalDetour},
    {"460-22", map_api::MainSignType::kOptionalDetour},
    {"460-30", map_api::MainSignType::kOptionalDetour},
    {"466", map_api::MainSignType::kOptionalDetourRouting},
    {"467.1-10", map_api::MainSignType::kRouteRecommendation},
    {"467.1-20", map_api::MainSignType::kRouteRecommendation},
    {"467.2", map_api::MainSignType::kRouteRecommendationEnd},
    {"501-10", map_api::MainSignType::kAnnounceLaneTransitionLeft},
    {"501-11", map_api::MainSignType::kAnnounceLaneTransitionLeft},
    {"501-12", map_api::MainSignType::kAnnounceLaneTransitionLeft},
    {"501-20", map_api::MainSignType::kAnnounceLaneTransitionRight},
    {"501-21", map_api::MainSignType::kAnnounceLaneTransitionRight},
    {"501-22", map_api::MainSignType::kAnnounceLaneTransitionRight},
    {"531-10", map_api::MainSignType::kAnnounceRightLaneEnd},
    {"531-20", map_api::MainSignType::kAnnounceLeftLaneEnd},
    {"551-20", map_api::MainSignType::kAnnounceLaneConsolidation},
    {"551-21", map_api::MainSignType::kAnnounceLaneConsolidation},
    {"551-22", map_api::MainSignType::kAnnounceLaneConsolidation},
    {"551-23", map_api::MainSignType::kAnnounceLaneConsolidation},
    {"551-24", map_api::MainSignType::kAnnounceLaneConsolidation},
    {"590-10", map_api::MainSignType::kDetourCityBlock},
    {"590-11", map_api::MainSignType::kDetourCityBlock},
    {"600", map_api::MainSignType::kGate},
    {"605", map_api::MainSignType::kPoleWarning},
    {"610", map_api::MainSignType::kTrafficCone},
    {"615", map_api::MainSignType::kMobileLaneClosure},
    {"616-30", map_api::MainSignType::kMobileLaneClosure},
    {"616-31", map_api::MainSignType::kMobileLaneClosure},
    {"620-40", map_api::MainSignType::kReflectorPost},
    {"621-40", map_api::MainSignType::kReflectorPost},
    {"625-1", map_api::MainSignType::kDirectionalBoardWarning},
    {"625-2", map_api::MainSignType::kDirectionalBoardWarning},
    {"626-10", map_api::MainSignType::kGuidingPlate},
    {"626-20", map_api::MainSignType::kGuidingPlate},
    {"626-30", map_api::MainSignType::kGuidingPlateWedges},
    {"630-10", map_api::MainSignType::kParkingHazard},
    {"630-20", map_api::MainSignType::kParkingHazard},
    {"720", map_api::MainSignType::kTrafficLightGreenArrow}};

INSTANTIATE_TEST_SUITE_P(MantleRoadSignalConverterTest,
                         MantleRoadSignalConverterMainSignTypeTest,
                         ::testing::ValuesIn(kMainSignTestCases));

class MantleRoadSignalConverterSupplementarySignTypeTest
    : public ::testing::TestWithParam<std::tuple<std::string, std::string, map_api::SupplementarySignType>>
{
};

TEST_P(MantleRoadSignalConverterSupplementarySignTypeTest,
       GivenSupplementarySignWithType_WhenConvertingToIMap_ThenCorrectTypeIsSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{
        .signals = {
            {"1", road_logic_suite::Signal{.type = "274", .dependencies = {"2"}}},
            {"2", road_logic_suite::Signal{.id = "2", .type = std::get<0>(param), .subtype = std::get<1>(param)}}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);
    ASSERT_EQ(map.traffic_signs.size(), 1);
    ASSERT_EQ(map.traffic_signs[0]->supplementary_signs.size(), 1);
    EXPECT_EQ(map.traffic_signs[0]->supplementary_signs[0].type, std::get<2>(param));
}

const std::vector<std::tuple<std::string, std::string, map_api::SupplementarySignType>> kSupplementarySignTypeTestCases{
    {"1007", "30", map_api::SupplementarySignType::kText},
    {"1007", "31", map_api::SupplementarySignType::kText},
    {"1007", "32", map_api::SupplementarySignType::kText},
    {"1007", "33", map_api::SupplementarySignType::kText},
    {"1007", "34", map_api::SupplementarySignType::kText},
    {"1007", "35", map_api::SupplementarySignType::kText},
    {"1007", "36", map_api::SupplementarySignType::kText},
    {"1007", "37", map_api::SupplementarySignType::kText},
    {"1007", "38", map_api::SupplementarySignType::kText},
    {"1007", "39", map_api::SupplementarySignType::kText},
    {"1007", "50", map_api::SupplementarySignType::kText},
    {"1007", "51", map_api::SupplementarySignType::kText},
    {"1007", "52", map_api::SupplementarySignType::kText},
    {"1007", "53", map_api::SupplementarySignType::kText},
    {"1007", "54", map_api::SupplementarySignType::kText},
    {"1007", "57", map_api::SupplementarySignType::kText},
    {"1007", "58", map_api::SupplementarySignType::kText},
    {"1007", "60", map_api::SupplementarySignType::kText},
    {"1007", "61", map_api::SupplementarySignType::kText},
    {"1007", "62", map_api::SupplementarySignType::kText},
    {"1008", "30", map_api::SupplementarySignType::kText},
    {"1008", "31", map_api::SupplementarySignType::kText},
    {"1008", "32", map_api::SupplementarySignType::kText},
    {"1008", "33", map_api::SupplementarySignType::kText},
    {"1008", "34", map_api::SupplementarySignType::kText},
    {"1012", "30", map_api::SupplementarySignType::kText},
    {"1012", "31", map_api::SupplementarySignType::kText},
    {"1012", "34", map_api::SupplementarySignType::kText},
    {"1012", "35", map_api::SupplementarySignType::kText},
    {"1012", "36", map_api::SupplementarySignType::kText},
    {"1012", "37", map_api::SupplementarySignType::kText},
    {"1012", "38", map_api::SupplementarySignType::kText},
    {"1012", "50", map_api::SupplementarySignType::kText},
    {"1012", "51", map_api::SupplementarySignType::kText},
    {"1012", "52", map_api::SupplementarySignType::kText},
    {"1012", "53", map_api::SupplementarySignType::kText},
    {"1013", "50", map_api::SupplementarySignType::kText},
    {"1013", "51", map_api::SupplementarySignType::kText},
    {"1014", "50", map_api::SupplementarySignType::kText},
    {"1028", "31", map_api::SupplementarySignType::kText},
    {"1053", "30", map_api::SupplementarySignType::kText},
    {"1053", "31", map_api::SupplementarySignType::kText},
    {"1053", "32", map_api::SupplementarySignType::kText},
    {"1053", "34", map_api::SupplementarySignType::kText},
    {"1053", "36", map_api::SupplementarySignType::kText},
    {"1053", "52", map_api::SupplementarySignType::kText},
    {"1053", "53", map_api::SupplementarySignType::kText},
    {"1004", "32", map_api::SupplementarySignType::kSpace},
    {"1005", "30", map_api::SupplementarySignType::kSpace},
    {"1007", "59", map_api::SupplementarySignType::kSpace},
    {"1013", "52", map_api::SupplementarySignType::kSpace},
    {"1028", "33", map_api::SupplementarySignType::kSpace},
    {"1040", "30", map_api::SupplementarySignType::kTime},
    {"1040", "31", map_api::SupplementarySignType::kTime},
    {"1040", "34", map_api::SupplementarySignType::kTime},
    {"1040", "35", map_api::SupplementarySignType::kTime},
    {"1040", "36", map_api::SupplementarySignType::kTime},
    {"1042", "30", map_api::SupplementarySignType::kTime},
    {"1042", "30", map_api::SupplementarySignType::kTime},
    {"1042", "31", map_api::SupplementarySignType::kTime},
    {"1042", "32", map_api::SupplementarySignType::kTime},
    {"1042", "33", map_api::SupplementarySignType::kTime},
    {"1042", "34", map_api::SupplementarySignType::kTime},
    {"1042", "35", map_api::SupplementarySignType::kTime},
    {"1042", "36", map_api::SupplementarySignType::kTime},
    {"1042", "37", map_api::SupplementarySignType::kTime},
    {"1042", "38", map_api::SupplementarySignType::kTime},
    {"1042", "51", map_api::SupplementarySignType::kTime},
    {"1042", "53", map_api::SupplementarySignType::kTime},
    {"1000", "13", map_api::SupplementarySignType::kArow},
    {"1000", "23", map_api::SupplementarySignType::kArow},
    {"1000", "30", map_api::SupplementarySignType::kArow},
    {"1000", "31", map_api::SupplementarySignType::kArow},
    {"1000", "34", map_api::SupplementarySignType::kArow},
    {"1000", "12", map_api::SupplementarySignType::kConstrainedTo},
    {"1000", "22", map_api::SupplementarySignType::kConstrainedTo},
    {"1000", "32", map_api::SupplementarySignType::kConstrainedTo},
    {"1000", "33", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "50", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "51", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "52", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "53", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "54", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "55", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "56", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "57", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "58", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "59", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "60", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "61", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "62", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "63", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "64", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "65", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "66", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "67", map_api::SupplementarySignType::kConstrainedTo},
    {"1012", "32", map_api::SupplementarySignType::kConstrainedTo},
    {"1049", "11", map_api::SupplementarySignType::kConstrainedTo},
    {"1050", "30", map_api::SupplementarySignType::kConstrainedTo},
    {"1050", "31", map_api::SupplementarySignType::kConstrainedTo},
    {"1050", "32", map_api::SupplementarySignType::kConstrainedTo},
    {"1050", "33", map_api::SupplementarySignType::kConstrainedTo},
    {"1060", "32", map_api::SupplementarySignType::kConstrainedTo},
    {"1044", "10", map_api::SupplementarySignType::kConstrainedTo},
    {"1044", "11", map_api::SupplementarySignType::kConstrainedTo},
    {"1044", "12", map_api::SupplementarySignType::kConstrainedTo},
    {"1044", "30", map_api::SupplementarySignType::kConstrainedTo},
    {"1048", "14", map_api::SupplementarySignType::kConstrainedTo},
    {"1048", "15", map_api::SupplementarySignType::kConstrainedTo},
    {"1048", "18", map_api::SupplementarySignType::kConstrainedTo},
    {"1048", "20", map_api::SupplementarySignType::kConstrainedTo},
    {"1049", "12", map_api::SupplementarySignType::kConstrainedTo},
    {"1049", "13", map_api::SupplementarySignType::kConstrainedTo},
    {"1052", "30", map_api::SupplementarySignType::kConstrainedTo},
    {"1052", "31", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "10", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "11", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "12", map_api::SupplementarySignType::kConstrainedTo},
    {"1010", "13", map_api::SupplementarySignType::kConstrainedTo},
    {"1040", "10", map_api::SupplementarySignType::kConstrainedTo},
    {"1007", "55", map_api::SupplementarySignType::kConstrainedTo},
    {"1007", "56", map_api::SupplementarySignType::kConstrainedTo},
    {"1006", "30", map_api::SupplementarySignType::kConstrainedTo},
    {"1012", "33", map_api::SupplementarySignType::kConstrainedTo},
    {"1020", "11", map_api::SupplementarySignType::kExcept},
    {"1020", "12", map_api::SupplementarySignType::kExcept},
    {"1020", "14", map_api::SupplementarySignType::kExcept},
    {"1020", "30", map_api::SupplementarySignType::kExcept},
    {"1020", "31", map_api::SupplementarySignType::kExcept},
    {"1020", "32", map_api::SupplementarySignType::kExcept},
    {"1022", "10", map_api::SupplementarySignType::kExcept},
    {"1022", "11", map_api::SupplementarySignType::kExcept},
    {"1022", "12", map_api::SupplementarySignType::kExcept},
    {"1022", "13", map_api::SupplementarySignType::kExcept},
    {"1022", "14", map_api::SupplementarySignType::kExcept},
    {"1022", "15", map_api::SupplementarySignType::kExcept},
    {"1024", "10", map_api::SupplementarySignType::kExcept},
    {"1024", "11", map_api::SupplementarySignType::kExcept},
    {"1024", "12", map_api::SupplementarySignType::kExcept},
    {"1024", "13", map_api::SupplementarySignType::kExcept},
    {"1024", "14", map_api::SupplementarySignType::kExcept},
    {"1024", "15", map_api::SupplementarySignType::kExcept},
    {"1024", "16", map_api::SupplementarySignType::kExcept},
    {"1024", "17", map_api::SupplementarySignType::kExcept},
    {"1024", "18", map_api::SupplementarySignType::kExcept},
    {"1024", "19", map_api::SupplementarySignType::kExcept},
    {"1024", "20", map_api::SupplementarySignType::kExcept},
    {"1026", "31", map_api::SupplementarySignType::kExcept},
    {"1026", "32", map_api::SupplementarySignType::kExcept},
    {"1026", "33", map_api::SupplementarySignType::kExcept},
    {"1026", "34", map_api::SupplementarySignType::kExcept},
    {"1026", "35", map_api::SupplementarySignType::kExcept},
    {"1026", "36", map_api::SupplementarySignType::kExcept},
    {"1026", "37", map_api::SupplementarySignType::kExcept},
    {"1026", "38", map_api::SupplementarySignType::kExcept},
    {"1026", "39", map_api::SupplementarySignType::kExcept},
    {"1026", "60", map_api::SupplementarySignType::kExcept},
    {"1026", "61", map_api::SupplementarySignType::kExcept},
    {"1026", "62", map_api::SupplementarySignType::kExcept},
    {"1026", "63", map_api::SupplementarySignType::kExcept},
    {"1028", "30", map_api::SupplementarySignType::kExcept},
    {"1028", "32", map_api::SupplementarySignType::kExcept},
    {"1028", "34", map_api::SupplementarySignType::kExcept},
    {"1031", "50", map_api::SupplementarySignType::kExcept},
    {"1031", "51", map_api::SupplementarySignType::kExcept},
    {"1031", "52", map_api::SupplementarySignType::kExcept},
    {"1001", "30", map_api::SupplementarySignType::kValidForDistance},
    {"1001", "32", map_api::SupplementarySignType::kValidForDistance},
    {"1001", "33", map_api::SupplementarySignType::kValidForDistance},
    {"1001", "34", map_api::SupplementarySignType::kValidForDistance},
    {"1001", "35", map_api::SupplementarySignType::kValidForDistance},
    {"1002", "10", map_api::SupplementarySignType::kPriorityRoadBottomLeftFourWay},
    {"1002", "11", map_api::SupplementarySignType::kPriorityRoadTopLeftFourWay},
    {"1002", "12", map_api::SupplementarySignType::kPriorityRoadBottomLeftThreeWayStraight},
    {"1002", "13", map_api::SupplementarySignType::kPriorityRoadBottomLeftThreeWaySideways},
    {"1002", "14", map_api::SupplementarySignType::kPriorityRoadTopLeftThreeWayStraight},
    {"1002", "20", map_api::SupplementarySignType::kPriorityRoadBottomRightFourWay},
    {"1002", "21", map_api::SupplementarySignType::kPriorityRoadTopRightFourWay},
    {"1002", "22", map_api::SupplementarySignType::kPriorityRoadBottomRightThreeWayStraight},
    {"1002", "23", map_api::SupplementarySignType::kPriorityRoadBottomRightThreeWaySideway},
    {"1002", "24", map_api::SupplementarySignType::kPriorityRoadTopRightThreeWayStraight},
    {"1004", "30", map_api::SupplementarySignType::kValidInDistance},
    {"1004", "31", map_api::SupplementarySignType::kValidInDistance},
    {"1000", "10", map_api::SupplementarySignType::kLeftArrow},
    {"1000", "20", map_api::SupplementarySignType::kRightArrow},
    {"1000", "21", map_api::SupplementarySignType::kRightBendArrow},
    {"1006", "31", map_api::SupplementarySignType::kAccident},
    {"1010", "14", map_api::SupplementarySignType::kRollingHighwayInformation},
    {"1010", "15", map_api::SupplementarySignType::kServices},
    {"1040", "32", map_api::SupplementarySignType::kParkingDiscTimeRestriction},
    {"1040", "33", map_api::SupplementarySignType::kParkingDiscTimeRestriction},
    {"1053", "33", map_api::SupplementarySignType::kWeight},
    {"1053", "37", map_api::SupplementarySignType::kWeight},
    {"1060", "33", map_api::SupplementarySignType::kWeight},
    {"1053", "35", map_api::SupplementarySignType::kWet},
    {"1053", "38", map_api::SupplementarySignType::kParkingConstraint},
    {"1053", "39", map_api::SupplementarySignType::kParkingConstraint},
    {"1060", "31", map_api::SupplementarySignType::kNoWaitingSideStripes},
};

INSTANTIATE_TEST_SUITE_P(MantleRoadSignalConverterTest,
                         MantleRoadSignalConverterSupplementarySignTypeTest,
                         ::testing::ValuesIn(kSupplementarySignTypeTestCases));

class MantleRoadSignalConverterSupplementarySignActorTest
    : public ::testing::TestWithParam<
          std::tuple<std::string, std::string, std::vector<map_api::SupplementarySignActor>>>
{
};

TEST_P(MantleRoadSignalConverterSupplementarySignActorTest,
       GivenSupplementarySignWithActor_WhenConvertingToIMap_ThenCorrectActorIsSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{
        .signals = {
            {"1", road_logic_suite::Signal{.type = "274", .dependencies = {"2"}}},
            {"2", road_logic_suite::Signal{.id = "2", .type = std::get<0>(param), .subtype = std::get<1>(param)}}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);
    ASSERT_EQ(map.traffic_signs.size(), 1);
    ASSERT_EQ(map.traffic_signs[0]->supplementary_signs.size(), 1);
    EXPECT_EQ(map.traffic_signs[0]->supplementary_signs[0].actors, std::get<2>(param));
}

const std::vector<std::tuple<std::string, std::string, std::vector<map_api::SupplementarySignActor>>>
    kSupplementarySignActorTestCases{
        {"1000", "12", {map_api::SupplementarySignActor::kPedestrians}},
        {"1000", "22", {map_api::SupplementarySignActor::kPedestrians}},
        {"1000", "32", {map_api::SupplementarySignActor::kBicycles}},
        {"1000", "33", {map_api::SupplementarySignActor::kBicycles}},
        {"1010", "50", {map_api::SupplementarySignActor::kMotorizedMultitrackVehicles}},
        {"1010", "51", {map_api::SupplementarySignActor::kTrucks}},
        {"1010", "52", {map_api::SupplementarySignActor::kBicycles}},
        {"1010", "53", {map_api::SupplementarySignActor::kPedestrians}},
        {"1010", "54", {map_api::SupplementarySignActor::kHorseRiders}},
        {"1010", "55", {map_api::SupplementarySignActor::kCattle}},
        {"1010", "56", {map_api::SupplementarySignActor::kTrams}},
        {"1010", "57", {map_api::SupplementarySignActor::kBuses}},
        {"1010", "58", {map_api::SupplementarySignActor::kCars}},
        {"1010", "59", {map_api::SupplementarySignActor::kCarsWithTrailers}},
        {"1010", "60", {map_api::SupplementarySignActor::kTrucksWithTrailers}},
        {"1010", "61", {map_api::SupplementarySignActor::kTractors}},
        {"1010", "62", {map_api::SupplementarySignActor::kMotorcycles}},
        {"1010", "63", {map_api::SupplementarySignActor::kMopeds}},
        {"1010", "64", {map_api::SupplementarySignActor::kHorseCarriages}},
        {"1010", "65", {map_api::SupplementarySignActor::kEbikes}},
        {"1010", "66", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1010", "67", {map_api::SupplementarySignActor::kCampers}},
        {"1012", "32", {map_api::SupplementarySignActor::kBicycles}},
        {"1049", "11", {map_api::SupplementarySignActor::kTractors}},
        {"1050", "30", {map_api::SupplementarySignActor::kTaxis}},
        {"1050", "31", {map_api::SupplementarySignActor::kTaxis}},
        {"1050", "32", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1050", "33", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1060", "32", {map_api::SupplementarySignActor::kBuses, map_api::SupplementarySignActor::kCarsWithTrailers}},
        {"1044", "10", {map_api::SupplementarySignActor::kDisabledPersons}},
        {"1044", "11", {map_api::SupplementarySignActor::kDisabledPersons}},
        {"1044", "12", {map_api::SupplementarySignActor::kDisabledPersons}},
        {"1044", "30", {map_api::SupplementarySignActor::kResidents}},
        {"1048", "14", {map_api::SupplementarySignActor::kTrucksWithSemitrailers}},
        {"1048",
         "15",
         {map_api::SupplementarySignActor::kTrucksWithSemitrailers,
          map_api::SupplementarySignActor::kTrucksWithSemitrailers}},
        {"1048", "18", {map_api::SupplementarySignActor::kRailroadTraffic}},
        {"1048", "20", {map_api::SupplementarySignActor::kCarsWithTrailers, map_api::SupplementarySignActor::kTrucks}},
        {"1049", "12", {map_api::SupplementarySignActor::kMilitaryVehicles}},
        {"1049",
         "13",
         {map_api::SupplementarySignActor::kBuses,
          map_api::SupplementarySignActor::kCarsWithTrailers,
          map_api::SupplementarySignActor::kTrucks}},
        {"1052", "30", {map_api::SupplementarySignActor::kHazardousGoodsVehicles}},
        {"1052", "31", {map_api::SupplementarySignActor::kWaterPollutantVehicles}},
        {"1010", "10", {map_api::SupplementarySignActor::kChildren}},
        {"1010", "11", {map_api::SupplementarySignActor::kWinterSportspeople}},
        {"1010", "12", {map_api::SupplementarySignActor::kTrailers}},
        {"1010", "13", {map_api::SupplementarySignActor::kCaravans}},
        {"1040", "10", {map_api::SupplementarySignActor::kWinterSportspeople}},
        {"1007", "55", {map_api::SupplementarySignActor::kWinterSportspeople}},
        {"1007", "56", {map_api::SupplementarySignActor::kWinterSportspeople}},
        {"1006", "30", {map_api::SupplementarySignActor::kCarsWithCaravans}},
        {"1012", "33", {map_api::SupplementarySignActor::kMopeds}},
        {"1020", "11", {map_api::SupplementarySignActor::kDisabledPersons}},
        {"1020", "12", {map_api::SupplementarySignActor::kBicycles, map_api::SupplementarySignActor::kResidents}},
        {"1020", "14", {map_api::SupplementarySignActor::kWinterSportspeople}},
        {"1020", "30", {map_api::SupplementarySignActor::kResidents}},
        {"1020", "31", {map_api::SupplementarySignActor::kResidents}},
        {"1020", "32", {map_api::SupplementarySignActor::kResidents}},
        {"1022", "10", {map_api::SupplementarySignActor::kBicycles}},
        {"1022", "11", {map_api::SupplementarySignActor::kMopeds}},
        {"1022", "12", {map_api::SupplementarySignActor::kMotorcycles}},
        {"1022", "13", {map_api::SupplementarySignActor::kEbikes}},
        {"1022", "14", {map_api::SupplementarySignActor::kBicycles, map_api::SupplementarySignActor::kMopeds}},
        {"1022", "15", {map_api::SupplementarySignActor::kEbikes, map_api::SupplementarySignActor::kMopeds}},
        {"1024", "10", {map_api::SupplementarySignActor::kCars}},
        {"1024", "11", {map_api::SupplementarySignActor::kCarsWithTrailers}},
        {"1024", "12", {map_api::SupplementarySignActor::kTrucks}},
        {"1024", "13", {map_api::SupplementarySignActor::kTrucksWithTrailers}},
        {"1024", "14", {map_api::SupplementarySignActor::kBuses}},
        {"1024", "15", {map_api::SupplementarySignActor::kRailroadTraffic}},
        {"1024", "16", {map_api::SupplementarySignActor::kTrams}},
        {"1024", "17", {map_api::SupplementarySignActor::kTractors}},
        {"1024", "18", {map_api::SupplementarySignActor::kHorseCarriages}},
        {"1024", "19", {map_api::SupplementarySignActor::kCampers}},
        {"1024", "20", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1026", "31", {map_api::SupplementarySignActor::kBuses}},
        {"1026", "32", {map_api::SupplementarySignActor::kPublicTransportVehicles}},
        {"1026", "33", {map_api::SupplementarySignActor::kEmergencyVehicles}},
        {"1026", "34", {map_api::SupplementarySignActor::kMedicalVehicles}},
        {"1026", "35", {map_api::SupplementarySignActor::kDeliveryVehicles}},
        {"1026", "36", {map_api::SupplementarySignActor::kAgriculturalVehicles}},
        {"1026", "37", {map_api::SupplementarySignActor::kForestryVehicles}},
        {"1026",
         "38",
         {map_api::SupplementarySignActor::kForestryVehicles, map_api::SupplementarySignActor::kAgriculturalVehicles}},
        {"1026", "39", {map_api::SupplementarySignActor::kOperationalAndUtilityVehicles}},
        {"1026", "60", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1026", "61", {map_api::SupplementarySignActor::kElectricVehicles}},
        {"1026", "62", {map_api::SupplementarySignActor::kSlurryTransport}},
        {"1026", "63", {map_api::SupplementarySignActor::kEbikes}},
        {"1028", "30", {map_api::SupplementarySignActor::kConstructionVehicles}},
        {"1028", "32", {map_api::SupplementarySignActor::kResidents}},
        {"1028", "34", {map_api::SupplementarySignActor::kFerryUsers}},
        {"1031",
         "50",
         {map_api::SupplementarySignActor::kVehiclesWithRedBadges,
          map_api::SupplementarySignActor::kVehiclesWithYellowBadges,
          map_api::SupplementarySignActor::kVehiclesWithGreenBadges}},
        {"1031",
         "51",
         {map_api::SupplementarySignActor::kVehiclesWithYellowBadges,
          map_api::SupplementarySignActor::kVehiclesWithGreenBadges}},
        {"1031", "52", {map_api::SupplementarySignActor::kVehiclesWithGreenBadges}},
    };

INSTANTIATE_TEST_SUITE_P(MantleRoadSignalConverterTest,
                         MantleRoadSignalConverterSupplementarySignActorTest,
                         ::testing::ValuesIn(kSupplementarySignActorTestCases));

class MantleRoadSignalConverterSupplementarySignArrowDirectionTest
    : public ::testing::TestWithParam<
          std::tuple<std::string, std::string, std::vector<map_api::SupplementarySignArrow::Direction>>>
{
};

TEST_P(MantleRoadSignalConverterSupplementarySignArrowDirectionTest,
       GivenSupplementarySignWithArrowDirection_WhenConvertingToIMap_ThenCorrectArrowDirectionIsSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{
        .signals = {
            {"1", road_logic_suite::Signal{.type = "274", .dependencies = {"2"}}},
            {"2", road_logic_suite::Signal{.id = "2", .type = std::get<0>(param), .subtype = std::get<1>(param)}}}};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);
    ASSERT_EQ(map.traffic_signs.size(), 1);
    ASSERT_EQ(map.traffic_signs[0]->supplementary_signs.size(), 1);
    ASSERT_EQ(map.traffic_signs[0]->supplementary_signs[0].arrows.size(), 1);
    EXPECT_EQ(map.traffic_signs[0]->supplementary_signs[0].arrows[0].direction, std::get<2>(param));
}

const std::vector<std::tuple<std::string, std::string, std::vector<map_api::SupplementarySignArrow::Direction>>>
    kSupplementarySignArrowDirectionTestCases{
        {"1000", "13", {map_api::SupplementarySignArrow::Direction::kCircle90DegLeft}},
        {"1000", "23", {map_api::SupplementarySignArrow::Direction::kCircle90DegRight}},
        {"1000",
         "30",
         {map_api::SupplementarySignArrow::Direction::kDirect90DegRight,
          map_api::SupplementarySignArrow::Direction::kDirect90DegLeft}},
        {"1000",
         "31",
         {map_api::SupplementarySignArrow::Direction::kDirect0Deg,
          map_api::SupplementarySignArrow::Direction::kDirect180Deg}},
        {"1000", "34", {map_api::SupplementarySignArrow::Direction::kCircle0Deg}},
        {"1000", "12", {map_api::SupplementarySignArrow::Direction::kDirect90DegLeft}},
        {"1000", "22", {map_api::SupplementarySignArrow::Direction::kDirect90DegRight}},
        {"1000",
         "32",
         {map_api::SupplementarySignArrow::Direction::kDirect90DegLeft,
          map_api::SupplementarySignArrow::Direction::kDirect90DegRight}},
        {"1000",
         "33",
         {map_api::SupplementarySignArrow::Direction::kDirect0Deg,
          map_api::SupplementarySignArrow::Direction::kDirect180Deg}},
    };

INSTANTIATE_TEST_SUITE_P(MantleRoadSignalConverterTest,
                         MantleRoadSignalConverterSupplementarySignArrowDirectionTest,
                         ::testing::ValuesIn(kSupplementarySignArrowDirectionTestCases));

class MantleRoadSignalConverterRoadMarkingTypeAndMainSignTypeTest
    : public ::testing::TestWithParam<std::tuple<std::string, map_api::RoadMarking::Type, map_api::MainSignType>>
{
};

TEST_P(MantleRoadSignalConverterRoadMarkingTypeAndMainSignTypeTest,
       GivenRoadMarkingWithTypeAndMainSignType_WhenConvertingToIMap_ThenCorrectTypeAndMainSignTypeAreSet)
{
    const auto param = GetParam();
    road_logic_suite::OpenDriveData data{.signals = {
                                             {"1", road_logic_suite::Signal{.type = std::get<0>(param)}},
                                         }};
    map_api::Map map{};
    MantleIdProvider id_provider{};

    road_logic_suite::map_conversion::MantleRoadSignalConverter::ConvertSignals(data, map, id_provider);
    ASSERT_EQ(map.road_markings.size(), 1);
    EXPECT_EQ(map.road_markings[0]->type, std::get<1>(param));
    EXPECT_EQ(map.road_markings[0]->traffic_main_sign_type, std::get<2>(param));
}

const std::vector<std::tuple<std::string, map_api::RoadMarking::Type, map_api::MainSignType>>
    kRoadMarkingTypeAndMainSignTypeTestCases{
        {"293", map_api::RoadMarking::Type::kSymbolicTrafficSign, map_api::MainSignType::kZebraCrossing},
        {"294", map_api::RoadMarking::Type::kSymbolicTrafficSign, map_api::MainSignType::kStop},
        {"299", map_api::RoadMarking::Type::kSymbolicTrafficSign, map_api::MainSignType::kNoParking},
        {"341", map_api::RoadMarking::Type::kSymbolicTrafficSign, map_api::MainSignType::kGiveWay},
    };

INSTANTIATE_TEST_SUITE_P(MantleRoadSignalConverterTest,
                         MantleRoadSignalConverterRoadMarkingTypeAndMainSignTypeTest,
                         ::testing::ValuesIn(kRoadMarkingTypeAndMainSignTypeTestCases));
}  // namespace
