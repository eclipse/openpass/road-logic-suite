/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_JUNCTION_CONVERTER_H
#define ROADLOGICSUITE_MANTLE_JUNCTION_CONVERTER_H

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

namespace road_logic_suite::map_conversion
{
class MantleJunctionConverter
{
  public:
    /// @brief Converts junctions from the internal representation to the mantle map format.
    /// @param data_storage internal data representation.
    /// @param id_provider to update the IDs.
    /// @param id_to_lane_relation map from mantle lane id to lane relation.
    static void DeduceLaneRelationFromJunctions(
        const road_logic_suite::OpenDriveData& data_storage,
        MantleIdProvider& id_provider,
        std::map<mantle_api::UniqueId, utils::LaneRelationIds>& id_to_lane_relation);

  private:
    MantleJunctionConverter(const OpenDriveData& data_storage,
                            MantleIdProvider& id_provider,
                            std::map<mantle_api::UniqueId, utils::LaneRelationIds>& id_to_lane_relation)
        : data_storage_(data_storage), id_provider_(id_provider), id_to_lane_relation_(id_to_lane_relation)
    {
    }

    void DeduceLaneRelationFromJunction(const types::Junction& junction);
    void DeduceLaneRelationFromJunctionLaneLink(const std::string& incoming_road_id,
                                                const std::string& connecting_road_id,
                                                const types::JunctionLaneLink& junction_lane_link);
    std::optional<std::int64_t> QueryRoadLastSectionIndex(const std::string& queried_road_id);

    const OpenDriveData& data_storage_;
    MantleIdProvider& id_provider_;
    std::map<mantle_api::UniqueId, utils::LaneRelationIds>& id_to_lane_relation_;
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_JUNCTION_CONVERTER_H
