/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "mantle_id_provider.h"
namespace road_logic_suite::map_conversion
{
mantle_api::UniqueId MantleIdProvider::GetOrGenerateLaneId(const LaneInfo& info)
{
    const auto it = lane_info_to_mantle_id_.find(info);
    if (it != lane_info_to_mantle_id_.end())
    {
        return it->second;
    }
    return lane_info_to_mantle_id_[info] = GetNewId();
}

std::optional<mantle_api::UniqueId> MantleIdProvider::QueryLaneId(const LaneInfo& info)
{
    const auto it = lane_info_to_mantle_id_.find(info);
    if (it != lane_info_to_mantle_id_.end())
    {
        return it->second;
    }
    return std::nullopt;
}

mantle_api::UniqueId MantleIdProvider::GetNewId()
{
    return internal_id_count_++;
}
}  // namespace road_logic_suite::map_conversion
