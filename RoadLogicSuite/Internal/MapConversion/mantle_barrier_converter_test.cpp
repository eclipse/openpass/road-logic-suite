/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_barrier_converter.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <gtest/gtest.h>

namespace
{
constexpr double kAbsError = 1e-6;

TEST(MantleBarrierConverterTest, GivenSimpleBarrier_WhenConverting_ThenGeometryMustBeCorrect)
{
    // arrange
    road_logic_suite::map_conversion::MantleIdProvider id_provider{};
    const std::string road_id = "0";
    const road_logic_suite::RoadBarrier barrier{
        .s_start = units::length::meter_t(2),
        .s_end = units::length::meter_t(97),
        .t_start = units::length::meter_t(0),
        .t_end = units::length::meter_t(0.04),
        .z_offset_start = units::length::meter_t(0.5),
        .z_offset_end = units::length::meter_t(0.2),
        .height_start = units::length::meter_t(0.5),
        .height_end = units::length::meter_t(0.9),
        .width_start = units::length::meter_t(0.5),
        .width_end = units::length::meter_t(1.0),
    };
    road_logic_suite::OpenDriveData data_storage{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data_storage.geometries.push_back(
        std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                 road_logic_suite::Vec2<units::length::meter_t>{},
                                                 units::angle::radian_t(0),
                                                 units::length::meter_t(100),
                                                 road_id));
    const road_logic_suite::CoordinateConverter converter(data_storage);
    const road_logic_suite::MapConverterConfig config{
        .sampling_distance = units::length::meter_t(50.0),
    };

    // act
    const auto result = road_logic_suite::map_conversion::MantleBarrierConverter::Convert(
        road_id, barrier, converter, config, id_provider);

    // assert
    ASSERT_EQ(result->boundary_line.size(), 3);
    EXPECT_NEAR(result->boundary_line[0].position.x(), 2, kAbsError);
    EXPECT_NEAR(result->boundary_line[0].position.y(), 0.0, kAbsError);
    EXPECT_NEAR(result->boundary_line[0].position.z(), 0.5, kAbsError);
    EXPECT_NEAR(result->boundary_line[0].height(), 0.5, kAbsError);
    EXPECT_NEAR(result->boundary_line[0].width(), 0.5, kAbsError);

    EXPECT_NEAR(result->boundary_line[1].position.x(), 52, kAbsError);
    EXPECT_NEAR(result->boundary_line[1].position.y(), 0.021053, kAbsError);
    EXPECT_NEAR(result->boundary_line[1].position.z(), 0.342105, kAbsError);
    EXPECT_NEAR(result->boundary_line[1].height(), 0.710526, kAbsError);
    EXPECT_NEAR(result->boundary_line[1].width(), 0.763158, kAbsError);

    EXPECT_NEAR(result->boundary_line[2].position.x(), 97, kAbsError);
    EXPECT_NEAR(result->boundary_line[2].position.y(), 0.04, kAbsError);
    EXPECT_NEAR(result->boundary_line[2].position.z(), 0.2, kAbsError);
    EXPECT_NEAR(result->boundary_line[2].height(), 0.9, kAbsError);
    EXPECT_NEAR(result->boundary_line[2].width(), 1.0, kAbsError);
}

TEST(MantleBarrierConverterTest, GivenSimpleBarrier_WhenConvertingWithDownSampling_ThenGeometryMustBeCorrect)
{
    // arrange
    road_logic_suite::map_conversion::MantleIdProvider id_provider{};
    const std::string road_id = "0";
    const road_logic_suite::RoadBarrier barrier{.s_start = units::length::meter_t(2),
                                                .s_end = units::length::meter_t(97),
                                                .t_start = units::length::meter_t(0),
                                                .t_end = units::length::meter_t(0.04)};
    road_logic_suite::OpenDriveData data_storage{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data_storage.geometries.push_back(
        std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                 road_logic_suite::Vec2<units::length::meter_t>{},
                                                 units::angle::radian_t(0),
                                                 units::length::meter_t(100),
                                                 road_id));
    const road_logic_suite::CoordinateConverter converter(data_storage);
    const road_logic_suite::MapConverterConfig config{
        .sampling_distance = units::length::meter_t(1.0),
        .downsampling = true,
        .downsampling_epsilon = units::length::meter_t(0.001),
    };

    // act
    const auto result = road_logic_suite::map_conversion::MantleBarrierConverter::Convert(
        road_id, barrier, converter, config, id_provider);

    // assert
    ASSERT_EQ(result->boundary_line.size(), 2);
    ASSERT_NEAR(result->boundary_line[0].position.x(), 2, kAbsError);
    ASSERT_NEAR(result->boundary_line[0].position.y(), 0.0, kAbsError);
    ASSERT_NEAR(result->boundary_line[1].position.x(), 97, kAbsError);
    ASSERT_NEAR(result->boundary_line[1].position.y(), 0.04, kAbsError);
}

TEST(MantleBarrierConverterTest, GivenBarrierWithGuardRailType_WhenConverting_ThenTypeMustBeCorrect)
{
    // arrange
    road_logic_suite::map_conversion::MantleIdProvider id_provider{};
    const std::string road_id = "0";
    const road_logic_suite::RoadBarrier barrier{
        .s_start = units::length::meter_t(0),
        .s_end = units::length::meter_t(50),
        .t_start = units::length::meter_t(0),
        .t_end = units::length::meter_t(0),
        .name = "guardRail",
    };
    road_logic_suite::OpenDriveData data_storage{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data_storage.geometries.push_back(
        std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                 road_logic_suite::Vec2<units::length::meter_t>{},
                                                 units::angle::radian_t(0),
                                                 units::length::meter_t(100),
                                                 road_id));
    const road_logic_suite::CoordinateConverter converter(data_storage);
    const road_logic_suite::MapConverterConfig config{
        .sampling_distance = units::length::meter_t(50.0),
    };

    // act
    const auto result = road_logic_suite::map_conversion::MantleBarrierConverter::Convert(
        road_id, barrier, converter, config, id_provider);

    // assert
    ASSERT_EQ(result->type, map_api::LaneBoundary::Type::kGuardRail);
}

TEST(MantleBarrierConverterTest, GivenBarrierWithInvalidSEnd_WhenConverting_ThenInvalidPointIsDiscarded)
{
    // arrange
    road_logic_suite::map_conversion::MantleIdProvider id_provider{};
    const std::string road_id = "0";
    const road_logic_suite::RoadBarrier barrier{
        .s_start = units::length::meter_t(2),
        .s_end = units::length::meter_t(100.00001),
        .t_start = units::length::meter_t(0),
        .t_end = units::length::meter_t(0.04),
        .z_offset_start = units::length::meter_t(0.5),
        .z_offset_end = units::length::meter_t(0.2),
        .height_start = units::length::meter_t(0.5),
        .height_end = units::length::meter_t(0.9),
        .width_start = units::length::meter_t(0.5),
        .width_end = units::length::meter_t(1.0),
    };
    road_logic_suite::OpenDriveData data_storage{
        .roads = {{"0", {.index_geometries_start = 0, .index_geometries_end = 1}}},
    };
    data_storage.geometries.push_back(
        std::make_unique<road_logic_suite::Line>(units::length::meter_t(0),
                                                 road_logic_suite::Vec2<units::length::meter_t>{},
                                                 units::angle::radian_t(0),
                                                 units::length::meter_t(100),
                                                 road_id));
    const road_logic_suite::CoordinateConverter converter(data_storage);
    const road_logic_suite::MapConverterConfig config{
        .sampling_distance = units::length::meter_t(50.0),
    };

    const auto expected_boundary_line_size = 2;

    // act
    const auto result = road_logic_suite::map_conversion::MantleBarrierConverter::Convert(
        road_id, barrier, converter, config, id_provider);

    // assert
    ASSERT_EQ(result->boundary_line.size(), expected_boundary_line_size);
    EXPECT_NEAR(result->boundary_line[0].position.x(), 2, kAbsError);
    EXPECT_NEAR(result->boundary_line[1].position.x(), 52, kAbsError);
}

}  // namespace
