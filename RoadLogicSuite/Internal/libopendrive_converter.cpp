/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_converter.h"

#include "RoadLogicSuite/Internal/Geometry/arc.h"
#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/Geometry/param_poly_3.h"
#include "RoadLogicSuite/Internal/Geometry/spiral.h"
#include "RoadLogicSuite/Internal/Types/road_mark_utils.h"
#include "RoadLogicSuite/Internal/libopendrive_converter_helpers.h"
#include "RoadLogicSuite/Internal/libopendrive_road_object_converter.h"
#include "RoadLogicSuite/Internal/libopendrive_road_signal_converter.h"

#include <Geometries/Arc.h>
#include <Geometries/Line.h>
#include <Geometries/ParamPoly3.h>
#include <Geometries/Spiral.h>
#include <OpenDriveMap.h>
#include <pugixml/pugixml.hpp>

#include <string>

namespace road_logic_suite
{

using namespace helpers;

namespace
{

// libopendrive converter: internal constants
constexpr auto kXmlElementNameLink = "link";
constexpr auto kXmlElementNamePredecessor = "predecessor";
constexpr auto kXmlElementNameSuccessor = "successor";
constexpr auto kXmlAttributeNameId = "id";
}  // namespace

// public methods
bool LibOpenDriveConverter::Convert(const odr::OpenDriveMap& map)
{
    if (!data_storage_reference_.IsEmpty())
    {
        return false;
    }
    data_storage_reference_.projection = map.proj4;
    for (auto const& xml_road : map.get_roads())
    {
        auto ref_line_geometries = ConvertGeometries(xml_road.ref_line.get_geometries(), xml_road.id);
        const units::length::meter_t road_length(xml_road.length);
        const auto road_links = CreateRoadLinks(xml_road);
        const auto lanes = ConvertLaneSections(xml_road.s_to_lanesection, road_length);
        const auto elevations = ConvertPoly3AlongSAxis(xml_road.ref_line.elevation_profile.s0_to_poly);
        const auto superelevations = ConvertPoly3AlongSAxis(xml_road.superelevation.s0_to_poly);
        const auto laneoffsets = ConvertPoly3AlongSAxis(xml_road.lane_offset.s0_to_poly);
        AddToStorage(xml_road, ref_line_geometries, lanes, road_links, elevations, superelevations, laneoffsets);
        LibOpenDriveRoadObjectConverter::ConvertForRoad(data_storage_reference_, xml_road);
        LibOpenDriveRoadSignalConverter::ConvertForRoad(data_storage_reference_, xml_road);
    }

    for (auto const& xml_junction : map.get_junctions())
    {
        const auto junction_type = ConvertJunctionType(xml_junction);
        const auto junction_connections = ConvertJunctionConnections(xml_junction.id_to_connection);
        const auto junction_cross_paths = ConvertJunctionCrossPaths(xml_junction);
        const auto junction_controllers = ConvertJunctionControllers(xml_junction.id_to_controller);
        const auto junction_priorities = ConvertJunctionPriorities(xml_junction);
        const auto junction_road_sections = ConvertJunctionRoadSections(xml_junction);
        AddJunctionToStorage(xml_junction,
                             junction_type,
                             junction_connections,
                             junction_cross_paths,
                             junction_controllers,
                             junction_priorities,
                             junction_road_sections);
    }

    return true;
}

types::RangeBasedMap<units::length::meter_t, types::Poly3> LibOpenDriveConverter::ConvertPoly3AlongSAxis(
    const std::map<double, odr::Poly3>& xml_poly3)
{
    types::RangeBasedMap<units::length::meter_t, types::Poly3> poly3s{};
    for (auto it = xml_poly3.rbegin(); it != xml_poly3.rend(); ++it)
    {
        const auto s0 = units::length::meter_t(it->first);
        const auto poly = types::Poly3(it->second.a, it->second.b, it->second.c, it->second.d);
        poly3s.Insert(s0, poly);
    }
    return poly3s;
}

// private methods
void LibOpenDriveConverter::AddToStorage(
    const odr::Road& xml_road,
    std::vector<std::unique_ptr<IGeometry>>& geometries,
    const types::RangeBasedMap<units::length::meter_t, types::LaneSection>& lane_sections,
    const std::vector<RoadLink>& road_links,
    const types::RangeBasedMap<units::length::meter_t, types::Poly3>& elevations,
    const types::RangeBasedMap<units::length::meter_t, types::Poly3>& superelevations,
    const types::RangeBasedMap<units::length::meter_t, types::Poly3>& laneoffsets)
{
    // road
    std::uint32_t start_index = this->data_storage_reference_.geometries.size();
    std::uint32_t end_index = start_index + geometries.size();
    Road current_road{
        .id = xml_road.id,
        .road_assigned_junction_id = xml_road.junction,
        .length = units::length::meter_t(xml_road.length),
        .index_geometries_start = start_index,
        .index_geometries_end = end_index,
        .lane_sections = lane_sections,
        .road_links = road_links,
        .elevations = elevations,
        .superelevations = superelevations,
        .laneoffsets = laneoffsets,
    };
    this->data_storage_reference_.roads[xml_road.id] = current_road;

    // geometries - sorted by s
    std::sort(geometries.begin(),
              geometries.end(),
              [](const std::unique_ptr<IGeometry>& lhs, const std::unique_ptr<IGeometry>& rhs) -> bool {
                  return lhs->s < rhs->s;
              });

    // move the sorted geometries to the destination storage
    std::move(begin(geometries), end(geometries), std::back_inserter(this->data_storage_reference_.geometries));
    geometries.clear();  // after we move the uniques to the destination storage the source vector only contains nullptr
                         // and should be cleared
}

std::unique_ptr<Arc> CreateFrom(const odr::Arc* xml_geometry, const std::string& road_id)
{
    return std::make_unique<Arc>(
        units::length::meter_t(xml_geometry->s0),
        Vec2(units::length::meter_t(xml_geometry->x0), units::length::meter_t(xml_geometry->y0)),
        units::angle::radian_t(xml_geometry->hdg0),
        units::length::meter_t(xml_geometry->length),
        custom_units::curvature_t(xml_geometry->curvature),
        road_id);
}

std::unique_ptr<Spiral> CreateFrom(const odr::Spiral* xml_geometry, const std::string& road_id)
{
    return std::make_unique<Spiral>(
        units::length::meter_t(xml_geometry->s0),
        Vec2(units::length::meter_t(xml_geometry->x0), units::length::meter_t(xml_geometry->y0)),
        units::angle::radian_t(xml_geometry->hdg0),
        units::length::meter_t(xml_geometry->length),
        road_id,
        custom_units::curvature_t(xml_geometry->curv_start),
        custom_units::curvature_t(xml_geometry->curv_end));
}

std::unique_ptr<Line> CreateFrom(const odr::Line* xml_geometry, const std::string& road_id)
{
    return std::make_unique<Line>(
        units::length::meter_t(xml_geometry->s0),
        Vec2(units::length::meter_t(xml_geometry->x0), units::length::meter_t(xml_geometry->y0)),
        units::angle::radian_t(xml_geometry->hdg0),
        units::length::meter_t(xml_geometry->length),
        road_id);
}

std::unique_ptr<ParamPoly3> CreateFrom(const odr::ParamPoly3* xml_geometry, const std::string& road_id)
{
    return std::make_unique<ParamPoly3>(
        units::length::meter_t(xml_geometry->s0),
        Vec2(units::length::meter_t(xml_geometry->x0), units::length::meter_t(xml_geometry->y0)),
        units::angle::radian_t(xml_geometry->hdg0),
        units::length::meter_t(xml_geometry->length),
        road_id,
        // Poly3(xml_geometry->aU, xml_geometry->bU, xml_geometry->cU, xml_geometry->dU),
        types::Poly3(xml_geometry->xml_node.attribute("aU").as_double(),
                     xml_geometry->xml_node.attribute("bU").as_double(),
                     xml_geometry->xml_node.attribute("cU").as_double(),
                     xml_geometry->xml_node.attribute("dU").as_double()),
        // Poly3(xml_geometry->aV, xml_geometry->bV, xml_geometry->cV, xml_geometry->dV),
        types::Poly3(xml_geometry->xml_node.attribute("aV").as_double(),
                     xml_geometry->xml_node.attribute("bV").as_double(),
                     xml_geometry->xml_node.attribute("cV").as_double(),
                     xml_geometry->xml_node.attribute("dV").as_double()),
        xml_geometry->pRange_normalized);
}

std::vector<std::unique_ptr<IGeometry>> LibOpenDriveConverter::ConvertGeometries(
    const std::set<const odr::RoadGeometry*>& xml_geometries,
    const std::string& road_id)
{
    std::vector<std::unique_ptr<IGeometry>> geometries;
    for (auto const& xml_geometry : xml_geometries)
    {
        switch (xml_geometry->type)
        {
            case odr::GeometryType_Arc:
            {
                const auto* xml_arc = dynamic_cast<const odr::Arc*>(xml_geometry);
                geometries.push_back(CreateFrom(xml_arc, road_id));
                break;
            }
            case odr::GeometryType_Line:
            {
                const auto* xml_line = dynamic_cast<const odr::Line*>(xml_geometry);
                geometries.push_back(CreateFrom(xml_line, road_id));
                break;
            }
            case odr::GeometryType_ParamPoly3:
            {
                const auto* xml_param_poly = dynamic_cast<const odr::ParamPoly3*>(xml_geometry);
                geometries.push_back(CreateFrom(xml_param_poly, road_id));
                break;
            }
            case odr::GeometryType_Spiral:
            {
                const auto* xml_spiral = dynamic_cast<const odr::Spiral*>(xml_geometry);
                geometries.push_back(CreateFrom(xml_spiral, road_id));
                break;
            }
            default:
                throw std::runtime_error("Found a not supported geometry type in XODR");
        }
    }
    return geometries;
}

types::RangeBasedMap<units::length::meter_t, types::RoadMark> CreateRoadMarks(
    const std::set<odr::RoadMarkGroup>& xml_road_marks,
    const double roadmark_s_start)
{
    types::RangeBasedMap<units::length::meter_t, types::RoadMark> map{};

    auto s_offset_min = std::numeric_limits<double>::max();
    for (const auto& xml_road_mark : xml_road_marks)
    {
        s_offset_min = std::min(s_offset_min, xml_road_mark.s_offset);

        map.Insert(units::length::meter_t(xml_road_mark.s_offset + roadmark_s_start),
                   types::RoadMark{.type = types::utils::ParseRoadMarkType(xml_road_mark.type),
                                   .weight = types::utils::ParseRoadMarkWeight(xml_road_mark.weight),
                                   .color = types::utils::ParseRoadMarkColor(xml_road_mark.color),
                                   .laneChange = types::utils::ParseRoadMarkLaneChange(xml_road_mark.lane_change),
                                   .height = units::length::meter_t(xml_road_mark.height),
                                   .width = units::length::meter_t(xml_road_mark.width)});
    }
    if (map.Empty() || s_offset_min > 0.01)
    {
        // make sure there is always an item present to prevent errors
        map.Insert(units::length::meter_t(roadmark_s_start),  // sOffset
                   types::RoadMark{.type = types::RoadMarkType::NONE,
                                   .weight = types::RoadMarkWeight::STANDARD,
                                   .color = types::RoadMarkColor::STANDARD,
                                   .laneChange = types::RoadMarkLaneChange::BOTH,
                                   .height = units::length::meter_t(0),
                                   .width = units::length::meter_t(0)});
    }

    return map;
}

types::RangeBasedMap<units::length::meter_t, types::Poly3> CreateWidthPolys(const odr::CubicSpline& spline)
{
    types::RangeBasedMap<units::length::meter_t, types::Poly3> map{};
    for (const auto& pair : spline.s0_to_poly)
    {
        map.Insert(units::length::meter_t(pair.first),
                   types::Poly3(pair.second.a, pair.second.b, pair.second.c, pair.second.d));
    }
    return map;
}

types::RangeBasedMap<units::length::meter_t, types::LaneSection> LibOpenDriveConverter::ConvertLaneSections(
    const std::map<double, odr::LaneSection>& xml_lane_sections,
    units::length::meter_t road_length)
{
    types::RangeBasedMap<units::length::meter_t, types::LaneSection> lane_sections{};
    auto last_s0 = road_length;
    for (auto xml_lane_section_it = xml_lane_sections.rbegin(); xml_lane_section_it != xml_lane_sections.rend();
         ++xml_lane_section_it)
    {
        types::LaneSection lane_section{};

        lane_section.s_start = units::length::meter_t(xml_lane_section_it->first);
        lane_section.s_end = last_s0;

        ConvertLanes(xml_lane_sections, xml_lane_section_it, lane_section);
        lane_sections.Insert(lane_section.s_start, lane_section);
        last_s0 = lane_section.s_start;
    }
    return lane_sections;
}

auto ParseLinks(const pugi::xml_node xml_link_node, const std::string& link_type)
{
    std::vector<types::Lane::LaneId> links{};
    const auto xml_links = xml_link_node.children(link_type.c_str());
    for (const auto& xml_link : xml_links)
    {
        links.push_back(xml_link.attribute(kXmlAttributeNameId).as_int());
    }
    return links;
}

void AutoDeducePredecessor(
    const std::map<double, odr::LaneSection>& xml_lane_sections,
    const std::map<double, odr::LaneSection>::const_reverse_iterator& xml_current_lane_section_it,
    std::vector<types::Lane::LaneId>& links,
    int lane_id)
{
    // reverse iterator => next element is the predecessor
    const auto xml_next_lane_section = std::next(xml_current_lane_section_it);
    if (xml_next_lane_section == xml_lane_sections.rend())
    {
        return;
    }
    if (xml_next_lane_section->second.id_to_lane.count(lane_id))
    {
        links.push_back(lane_id);
    }
}

void AutoDeduceSuccessor(const std::map<double, odr::LaneSection>& xml_lane_sections,
                         const std::map<double, odr::LaneSection>::const_reverse_iterator& xml_current_lane_section_it,
                         std::vector<types::Lane::LaneId>& links,
                         int lane_id)
{
    // reverse iterator => previous element is the successor
    if (xml_current_lane_section_it == xml_lane_sections.rbegin())
    {
        return;
    }
    const auto xml_prev_lane_section = std::prev(xml_current_lane_section_it);
    if (xml_prev_lane_section->second.id_to_lane.count(lane_id))
    {
        links.push_back(lane_id);
    }
}

void LibOpenDriveConverter::ConvertLanes(
    const std::map<double, odr::LaneSection>& xml_lane_sections,
    const std::map<double, odr::LaneSection>::const_reverse_iterator& xml_lane_section_it,
    types::LaneSection& lane_section)
{
    for (const auto& xml_lane : xml_lane_section_it->second.get_lanes())
    {
        const auto xml_lane_link = xml_lane.xml_node.child(kXmlElementNameLink);
        auto predecessors = ParseLinks(xml_lane_link, kXmlElementNamePredecessor);
        if (predecessors.empty())
        {
            AutoDeducePredecessor(xml_lane_sections, xml_lane_section_it, predecessors, xml_lane.id);
        }
        auto successors = ParseLinks(xml_lane_link, kXmlElementNameSuccessor);
        if (successors.empty())
        {
            AutoDeduceSuccessor(xml_lane_sections, xml_lane_section_it, successors, xml_lane.id);
        }

        const auto roadmark_s_start = xml_lane_section_it->first;

        auto new_lane = types::Lane{
            .id = xml_lane.id,
            .road_mark = CreateRoadMarks(xml_lane.roadmark_groups, roadmark_s_start),
            .width_polys = CreateWidthPolys(xml_lane.lane_width),
            .predecessors = predecessors,
            .successors = successors,
            .type = xml_lane.type,
        };

        if (xml_lane.id == 0)
        {
            lane_section.center_lane = new_lane;
        }
        else if (xml_lane.id > 0)
        {
            lane_section.left_lanes.push_back(new_lane);
        }
        else
        {
            lane_section.right_lanes.push_back(new_lane);
        }
    }

    auto comparer = [](const auto& lhs, const auto& rhs) { return std::abs(lhs.id) < std::abs(rhs.id); };
    sort(lane_section.left_lanes.begin(), lane_section.left_lanes.end(), comparer);
    sort(lane_section.right_lanes.begin(), lane_section.right_lanes.end(), comparer);
}

void LibOpenDriveConverter::AddJunctionToStorage(const odr::Junction& xml_junction,
                                                 const types::JunctionType& junction_type,
                                                 const std::vector<types::JunctionConnection>& junction_connections,
                                                 const std::vector<types::JunctionCrossPath>& junction_cross_paths,
                                                 const std::vector<types::JunctionController>& junction_controllers,
                                                 const std::vector<types::JunctionPriority>& junction_priorities,
                                                 const std::vector<types::JunctionRoadSection>& junction_road_sections)
{
    types::Junction current_junction{
        .id = xml_junction.id,
        .name = xml_junction.name,
        .type = junction_type,
        .junction_connections = junction_connections,
        .junction_cross_paths = junction_cross_paths,
        .junction_controllers = junction_controllers,
        .junction_priorities = junction_priorities,
        .junction_road_sections = junction_road_sections,
    };

    this->data_storage_reference_.junctions[xml_junction.id] = current_junction;
}

types::JunctionType LibOpenDriveConverter::ConvertJunctionType(const odr::Junction& xml_junction)
{
    const auto& xml_node = xml_junction.xml_node;
    const std::string junction_type_str = xml_node.attribute("type").as_string("");
    if (junction_type_str == "crossing")
    {
        return types::JunctionType::kCrossing;
    }
    if (junction_type_str == "")
    {
        return types::JunctionType::kStandard;
    }
    return types::JunctionType::kOther;
}

std::vector<types::JunctionConnection> LibOpenDriveConverter::ConvertJunctionConnections(
    const std::map<std::string, odr::JunctionConnection>& xml_junction_connections)
{
    std::vector<types::JunctionConnection> junction_connections{};
    for (const auto& pair : xml_junction_connections)
    {
        const auto& xml_connection = pair.second;
        types::JunctionConnection junction_connection{
            .id = xml_connection.id,
            .incoming_road = xml_connection.incoming_road,
            .connecting_road = xml_connection.connecting_road,
            .contact_type = GetContactType(xml_connection.contact_point),
            .junction_lane_links = CreateJunctionLinks(xml_connection),
        };
        junction_connections.push_back(junction_connection);
    }
    return junction_connections;
}

std::vector<types::JunctionCrossPath> LibOpenDriveConverter::ConvertJunctionCrossPaths(
    const odr::Junction& xml_junction)
{
    std::vector<types::JunctionCrossPath> junction_cross_paths;
    const auto& xml_node = xml_junction.xml_node;

    for (pugi::xml_node cross_path_node : xml_node.children("crossPath"))
    {
        const std::string cross_path_id = cross_path_node.attribute("id").as_string("");
        const std::string crossing_road_str = cross_path_node.attribute("crossingRoad").as_string("");
        const std::string road_at_start_str = cross_path_node.attribute("roadAtStart").as_string("");
        const std::string road_at_end_str = cross_path_node.attribute("roadAtEnd").as_string("");

        const auto xml_start_lane_link = cross_path_node.child("startLaneLink");
        const auto xml_end_lane_link = cross_path_node.child("endLaneLink");

        types::StartLaneLink start_lane_link{
            .s = xml_start_lane_link.attribute("s").as_double(0.0),
            .from = xml_start_lane_link.attribute("from").as_int(0),
            .to = xml_start_lane_link.attribute("to").as_int(0),
        };

        types::EndLaneLink end_lane_link{
            .s = xml_end_lane_link.attribute("s").as_double(0.0),
            .from = xml_end_lane_link.attribute("from").as_int(0),
            .to = xml_end_lane_link.attribute("to").as_int(0),
        };

        types::JunctionCrossPath junction_cross_path{.id = cross_path_id,
                                                     .crossing_road = crossing_road_str,
                                                     .road_at_start = road_at_start_str,
                                                     .road_at_end = road_at_end_str,
                                                     .start_lane_link = start_lane_link,
                                                     .end_lane_link = end_lane_link};

        junction_cross_paths.push_back(junction_cross_path);
    }
    return junction_cross_paths;
}

std::vector<types::JunctionController> LibOpenDriveConverter::ConvertJunctionControllers(
    const std::map<std::string, odr::JunctionController>& xml_junction_controllers)
{
    std::vector<types::JunctionController> junction_controllers{};
    for (const auto& pair : xml_junction_controllers)
    {
        const auto& xml_controller = pair.second;
        types::JunctionController junction_controller{
            .id = xml_controller.id, .type = xml_controller.type, .sequence = xml_controller.sequence};
        junction_controllers.push_back(junction_controller);
    }
    return junction_controllers;
}

std::vector<types::JunctionPriority> LibOpenDriveConverter::ConvertJunctionPriorities(const odr::Junction& xml_junction)
{
    std::vector<types::JunctionPriority> junction_priorities{};

    const auto& xml_node = xml_junction.xml_node;

    for (pugi::xml_node priority_node : xml_node.children("priority"))
    {
        types::JunctionPriority junction_priority{.high = priority_node.attribute("high").as_string(""),
                                                  .low = priority_node.attribute("low").as_string("")};
        junction_priorities.push_back(junction_priority);
    }

    return junction_priorities;
}

std::vector<types::JunctionRoadSection> LibOpenDriveConverter::ConvertJunctionRoadSections(
    const odr::Junction& xml_junction)
{
    std::vector<types::JunctionRoadSection> junction_road_sections;
    const auto& xml_node = xml_junction.xml_node;

    for (pugi::xml_node road_section_node : xml_node.children("roadSection"))
    {
        const auto road_section_id = road_section_node.attribute("id").as_string("");
        const auto road_section_road_id = road_section_node.attribute("roadId").as_string("");
        const auto road_section_s_start = road_section_node.attribute("sStart").as_double(0.0);
        const auto road_section_s_end = road_section_node.attribute("sEnd").as_double(0.0);

        types::JunctionRoadSection junction_road_section{.id = road_section_id,
                                                         .road_id = road_section_road_id,
                                                         .s_start = road_section_s_start,
                                                         .s_end = road_section_s_end};

        junction_road_sections.push_back(junction_road_section);
    }
    return junction_road_sections;
}

}  // namespace road_logic_suite
