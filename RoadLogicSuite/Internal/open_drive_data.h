/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_OPEN_DRIVE_DATA_H
#define ROADLOGICSUITE_OPEN_DRIVE_DATA_H

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"
#include "RoadLogicSuite/Internal/Types/junction.h"
#include "RoadLogicSuite/Internal/Types/object.h"
#include "RoadLogicSuite/Internal/Types/signal.h"
#include "RoadLogicSuite/Internal/road.h"

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace road_logic_suite
{
/// @brief This data storage is used throughout the RoadLogicSuite and holds all information about the OpenDRIVE map
/// with the goal to have a data structure that is as efficient as possible for the different use cases.
/// @note An external application using the RoadLogicSuite as a dependency should usually use the public API, i.e. the
/// converters to access the data in here, rather than this data structure itself.
struct OpenDriveData
{
    /// @brief This maps the OpenDRIVE road identifiers to their respective road representation. According to the
    /// OpenDRIVE standard (from at least 1.4 to 1.7), road identifiers must be a unique string. It is recommended, but
    /// not required to use a std::uint32_t compatible integer, however the RoadLogicSuite does not enforce this
    /// recommendation.
    std::unordered_map<std::string, Road> roads{};

    /// @brief This maps the OpenDRIVE junction identifiers to their respective junction representation. According to
    /// the OpenDRIVE standard (from at least 1.4 to 1.7), junction identifiers must be a unique string. It is
    /// recommended, but not required to use a std::uint32_t compatible integer, however the RoadLogicSuite does not
    /// enforce this recommendation.
    std::unordered_map<std::string, types::Junction> junctions{};

    /// @brief This is a collection of geometries. This can eventually be changed to be a different data structure or a
    /// small collection of data structures if that ends up being more performant. Geometries for reference lines are
    /// ordered by road and then by the s-coordinate of the starting position.
    ///
    /// @code
    /// | Geometries for road "42"
    /// | - Geometry @ s=0
    /// | - Geometry @ s=40
    /// | - Geometry @ s=80
    /// | Geometries for road "43"
    /// | - Geometry @ s=0
    /// | - Geometry @ s=30
    /// @endcode
    std::vector<std::unique_ptr<IGeometry>> geometries{};

    /// @brief List of all objects that were declared in the OpenDrive file, including repeating and continuous objects.
    std::vector<Object> objects{};

    /// @brief map from signal id to corresponding signal that were declared in the OpenDrive file.
    std::unordered_map<std::string, Signal> signals{};

    /// @brief Holds a projection string. See <a
    /// href=https://releases.asam.net/OpenDRIVE/1.6.0/ASAM_OpenDRIVE_BS_V1-6-0.html#_georeferencing_in_opendrive>Documentation</a>
    std::string projection{};

    /// @brief Returns whether this data storage is empty. Some operations are only allowed on an empty storage.
    /// @return true, when the data storage is empty, false otherwise.
    bool IsEmpty() const;
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_OPEN_DRIVE_DATA_H
