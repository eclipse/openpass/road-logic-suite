/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_converter.h"

#include "RoadLogicSuite/Internal/Types/link_contact_type.h"
#include "RoadLogicSuite/Internal/Types/road_link_type.h"
#include "RoadLogicSuite/Internal/Utils/math_utils.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "RoadLogicSuite/Tests/Utils/assertions.h"
#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <cmath>

namespace road_logic_suite::test
{

using namespace units::length;
using namespace units::literals;

static const double kAbsErrorMax = 1e-6;

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithLinearGeometry_WhenConverting_ThenGeometryMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto absoluteXodrPath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");
    const odr::OpenDriveMap map(absoluteXodrPath);

    converter.Convert(map);

    // check parsing of roads
    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].id, "0");  // same id in road data storage for convenience.
    EXPECT_EQ(data.roads["0"].index_geometries_start, 0);
    EXPECT_EQ(data.roads["0"].index_geometries_end, 2);
    // check parsing of geometries
    ASSERT_EQ(data.geometries.size(), 2);
    EXPECT_EQ(data.geometries[0]->s, 0_m);
    EXPECT_EQ(data.geometries[1]->s, 500_m);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.x, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.x, units::length::meter_t(500 / sqrt(2)), kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.y, units::length::meter_t(500 / sqrt(2)), kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->hdg, math_utils::kPiOver4, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->hdg, math_utils::kPiOver4, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->length, 500_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->length, 250_m, kAbsErrorMax);
    EXPECT_EQ(data.geometries[0]->road_id, "0");
    EXPECT_EQ(data.geometries[1]->road_id, "0");
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithArcGeometry_WhenConverting_ThenGeometryMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto absoluteXodrPath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_arc.xodr");
    const odr::OpenDriveMap map(absoluteXodrPath);

    converter.Convert(map);

    // check parsing of roads
    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].index_geometries_start, 0);
    EXPECT_EQ(data.roads["0"].index_geometries_end, 2);
    // check parsing of geometries
    ASSERT_EQ(data.geometries.size(), 2);
    EXPECT_EQ(data.geometries[0]->s, 0_m);
    EXPECT_EQ(data.geometries[1]->s, 500_m);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.x, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.x, 500_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->length, 500_m, kAbsErrorMax);
    // arc length: s = r * theta with r = 1/k (k: curvature) and theta = pi/2
    EXPECT_NEAR_UNITS(data.geometries[1]->length, meter_t((1 / 0.01) * math_utils::kPiOver2()), kAbsErrorMax);
    // for straight lines only a curvature of 0 is correct.
    EXPECT_EQ(data.geometries[0]->CalcCurvatureAt(0_m), custom_units::curvature_t(0));
    // for straight lines only a curvature of 0 is correct.
    EXPECT_NEAR_UNITS(data.geometries[1]->CalcCurvatureAt(0_m), custom_units::curvature_t(0.01), kAbsErrorMax);
    EXPECT_EQ(data.geometries[0]->road_id, "0");
    EXPECT_EQ(data.geometries[1]->road_id, "0");
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithSpiralGeometry_WhenConverting_ThenGeometryMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto absoluteXodrPath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_spiral.xodr");
    const odr::OpenDriveMap map(absoluteXodrPath);

    converter.Convert(map);

    // check parsing of roads
    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].index_geometries_start, 0);
    EXPECT_EQ(data.roads["0"].index_geometries_end, 2);
    // check parsing of geometries
    ASSERT_EQ(data.geometries.size(), 2);
    EXPECT_EQ(data.geometries[0]->s, 0_m);
    EXPECT_EQ(data.geometries[1]->s, 500_m);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.x, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.x, 500_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->length, 500_m, kAbsErrorMax);
    // arc length: s = r * theta with r = 1/k (k: curvature) and theta = pi/2
    // EXPECT_NEAR(data.geometries[1].length, (1 / 0.01) * M_PI_2, kAbsErrorMax);
    // for straight lines only a curvature of 0 is correct.
    EXPECT_EQ(data.geometries[0]->CalcCurvatureAt(0_m), custom_units::curvature_t(0));
    // for straight lines only a curvature of 0 is correct.
    // EXPECT_NEAR(data.geometries[1].curvature, 0.01, kAbsErrorMax);
    EXPECT_EQ(data.geometries[0]->road_id, "0");
    EXPECT_EQ(data.geometries[1]->road_id, "0");
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithParamPolyGeometry_WhenConverting_ThenGeometryMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto absoluteXodrPath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_param_poly.xodr");
    const odr::OpenDriveMap map(absoluteXodrPath);

    converter.Convert(map);

    // check parsing of roads
    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].index_geometries_start, 0);
    EXPECT_EQ(data.roads["0"].index_geometries_end, 2);
    // check parsing of geometries
    ASSERT_EQ(data.geometries.size(), 2);
    EXPECT_EQ(data.geometries[0]->s, 0_m);
    EXPECT_EQ(data.geometries[1]->s, 500_m);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.x, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.x, 500_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->position.y, 0_m, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[1]->hdg, 0_rad, kAbsErrorMax);
    EXPECT_NEAR_UNITS(data.geometries[0]->length, 500_m, kAbsErrorMax);
    // arc length: s = r * theta with r = 1/k (k: curvature) and theta = pi/2
    // EXPECT_NEAR(data.geometries[1].length, (1 / 0.01) * M_PI_2, kAbsErrorMax);
    // for straight lines only a curvature of 0 is correct.
    EXPECT_EQ(data.geometries[0]->CalcCurvatureAt(0_m), custom_units::curvature_t(0));
    // the curvature should be near on the connecting point.
    EXPECT_NEAR_UNITS(data.geometries[1]->CalcCurvatureAt(0_m), custom_units::curvature_t(0), 0.001);
    EXPECT_EQ(data.geometries[0]->road_id, "0");
    EXPECT_EQ(data.geometries[1]->road_id, "0");
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithSimpleRoad_WhenConverting_ThenLaneMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].lane_sections.At(units::length::meter_t(721)).GetLaneCount(), 5);
    // just check one lane
    const auto lane = data.roads["0"].lane_sections.At(units::length::meter_t(721)).GetLaneById(-1);
    EXPECT_EQ(lane.GetWidthAt(units::length::meter_t(808)), units::length::meter_t(3));
    const auto roadMark = lane.road_mark.At(units::length::meter_t(462));
    EXPECT_EQ(roadMark.type, types::RoadMarkType::BROKEN);
    EXPECT_EQ(roadMark.weight, types::RoadMarkWeight::STANDARD);
    EXPECT_EQ(roadMark.color, types::RoadMarkColor::STANDARD);
    EXPECT_EQ(roadMark.laneChange, types::RoadMarkLaneChange::BOTH);
    EXPECT_EQ(roadMark.height, units::length::meter_t(0.02));
    EXPECT_EQ(roadMark.width, units::length::meter_t(0.2));
}

TEST(LibOpenDriveConverterTest,
     GivenRoadWithMultipleLaneSections_WhenConverting_ThenLaneSectionsMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/lanesections_example.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    ASSERT_EQ(data.roads.size(), 1);
    EXPECT_EQ(data.roads["0"].lane_sections.At(units::length::meter_t(60)).GetLaneCount(), 2);
    const auto first_lane_section = data.roads["0"].lane_sections.At(units::length::meter_t(22.68));
    const auto second_lane_section = data.roads["0"].lane_sections.At(units::length::meter_t(53.64));
    EXPECT_EQ(second_lane_section.GetLaneCount(), 2);
    EXPECT_NEAR(first_lane_section.s_start(), 0, kAbsErrorMax);
    EXPECT_NEAR(first_lane_section.s_end(), 50, kAbsErrorMax);
    EXPECT_NEAR(second_lane_section.s_start(), 50, kAbsErrorMax);
    EXPECT_NEAR(second_lane_section.s_end(), 90, kAbsErrorMax);
}

TEST(LibOpenDriveConverterTest,
     GivenRoadWithExplicitLaneLinks_WhenConverting_ThenLaneLinksMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/lanesections_example_explicit_links.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto first_lane = data.roads["0"].lane_sections.At(units::length::meter_t(0)).GetLaneById(-1);
    const auto second_lane = data.roads["0"].lane_sections.At(units::length::meter_t(50)).GetLaneById(-1);
    ASSERT_EQ(first_lane.successors.size(), 1);
    ASSERT_EQ(second_lane.predecessors.size(), 1);
    EXPECT_EQ(-1, first_lane.successors[0]);
    EXPECT_EQ(-1, second_lane.predecessors[0]);
}

TEST(LibOpenDriveConverterTest,
     GivenRoadWithoutExplicitLaneLinks_WhenConverting_ThenImplicitLinkageShouldBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/lanesections_example.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto lane_with_successor = data.roads["0"].lane_sections.At(units::length::meter_t(0)).GetLaneById(-1);
    const auto lane_with_predecessor = data.roads["0"].lane_sections.At(units::length::meter_t(50)).GetLaneById(-1);
    ASSERT_EQ(1, lane_with_successor.successors.size());
    EXPECT_EQ(-1, lane_with_successor.successors[0]);
    EXPECT_EQ(0, lane_with_successor.predecessors.size());
    ASSERT_EQ(1, lane_with_predecessor.predecessors.size());
    EXPECT_EQ(-1, lane_with_predecessor.predecessors[0]);
    EXPECT_EQ(0, lane_with_predecessor.successors.size());
}

TEST(LibOpenDriveConverterTest, GivenRoadWithMultipleLinks_WhenConverting_TheMultipleLinksShouldBeConvertedCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/lanesections_example_multiple_links.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto lane_with_multiple_links = data.roads["0"].lane_sections.At(units::length::meter_t(50)).GetLaneById(-1);
    ASSERT_EQ(2, lane_with_multiple_links.predecessors.size());
}

TEST(LibOpenDriveConverterTest, GivenTwoLinkedRoads_WhenConverting_ThenLinkInformationShouldBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/two_roads.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto first_road = data.roads["0"];
    const auto second_road = data.roads["1"];
    ASSERT_EQ(1, first_road.road_links.size());
    ASSERT_EQ(1, second_road.road_links.size());
    EXPECT_EQ(second_road.id, first_road.road_links[0].target_id);
    EXPECT_EQ(RoadLinkType::kSuccessor, first_road.road_links[0].link_type);
    EXPECT_EQ(LinkContactType::kStart, first_road.road_links[0].contact_type);
    EXPECT_EQ(first_road.id, second_road.road_links[0].target_id);
    EXPECT_EQ(RoadLinkType::kPredecessor, second_road.road_links[0].link_type);
    EXPECT_EQ(LinkContactType::kEnd, second_road.road_links[0].contact_type);
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithProjectionString_WhenConverting_ThenStringMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/projection_string.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    EXPECT_EQ(data.projection,
              "+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs");
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithObjects_WhenConverting_ThenObjectsMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road_with_objects.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto emergency_call_box = std::find_if(
        data.objects.begin(), data.objects.end(), [](const auto& obj) { return obj.name == "emergencyCallBox"; });
    ASSERT_TRUE(emergency_call_box != data.objects.end()) << "Emergency Call Box was not found in data storage!";
    const auto delineator_count = std::count_if(
        data.objects.begin(), data.objects.end(), [](const auto& obj) { return obj.name == "delineator"; });
    ASSERT_EQ(40, delineator_count);
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithoutRoadmark_WhenConverting_DefaultRoadmarkMustBeSet)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/lanesections_example.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto& road = data.roads["0"];
    const auto& lane_section = road.lane_sections.At(units::length::meter_t(0));
    EXPECT_FALSE(lane_section.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section.center_lane.road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::NONE);

    EXPECT_TRUE(lane_section.left_lanes.empty());
    EXPECT_EQ(lane_section.right_lanes.size(), 2);
    for (const auto& lane : lane_section.right_lanes)
    {
        EXPECT_FALSE(lane.road_mark.Empty());
        EXPECT_EQ(lane.road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::NONE);
        EXPECT_EQ(lane.road_mark.At(units::length::meter_t(50)).type, types::RoadMarkType::NONE);
        EXPECT_EQ(lane.road_mark.At(units::length::meter_t(0)).laneChange, types::RoadMarkLaneChange::BOTH);
        EXPECT_EQ(lane.road_mark.At(units::length::meter_t(0)).color, types::RoadMarkColor::STANDARD);
        EXPECT_EQ(lane.road_mark.At(units::length::meter_t(0)).height, units::length::meter_t(0));
    }
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithRoadmark_WhenConverting_CorrectRoadmarkMustBeSet)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto& road = data.roads["0"];
    const auto& lane_section = road.lane_sections.At(units::length::meter_t(0));
    EXPECT_FALSE(lane_section.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section.center_lane.road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::SOLID);

    EXPECT_EQ(lane_section.left_lanes.size(), 2);
    EXPECT_EQ(lane_section.right_lanes.size(), 2);

    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::BROKEN);
    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).weight,
              types::RoadMarkWeight::STANDARD);
    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).height, units::length::meter_t(0.02));
    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).width, units::length::meter_t(0.2));
    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).color, types::RoadMarkColor::STANDARD);
    EXPECT_EQ(lane_section.left_lanes[0].road_mark.At(units::length::meter_t(0)).laneChange,
              types::RoadMarkLaneChange::BOTH);
    EXPECT_EQ(lane_section.left_lanes[1].road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::SOLID);

    EXPECT_EQ(lane_section.right_lanes[0].road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::BROKEN);
    EXPECT_EQ(lane_section.right_lanes[1].road_mark.At(units::length::meter_t(0)).type, types::RoadMarkType::SOLID);
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithLaneType_WhenConverting_ThenLaneTypeIsConvertedCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    ASSERT_EQ(data.roads.size(), 1);
    const auto lane = data.roads["0"].lane_sections.At(units::length::meter_t(721)).GetLaneById(-1);
    EXPECT_EQ(lane.type, "driving");
}

TEST(LibOpenDriveConverterTest,
     GivenNonEmptyDataStorage_WhenConverting_ThenReturnFalseDueToExistedUnexpectedDataStorage)
{
    OpenDriveData data{};
    road_logic_suite::Road road{};
    data.roads.emplace("0", road);
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    const odr::OpenDriveMap map(path);

    const auto result = converter.Convert(map);

    EXPECT_FALSE(result);
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithJunction_WhenConverting_ThenJunctionMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/junction_example.xodr");
    const odr::OpenDriveMap map(path);
    const auto expected_junctions_size = 1;
    const auto expected_junction_id = "1";
    const auto expected_junction_name = "";
    const auto expected_junction_connections_size = 2;
    const auto expected_first_junction_connection_id = "0";
    const auto expected_first_junction_connection_incoming_road = "0";
    const auto expected_first_junction_connection_connecting_road = "1";
    const auto expected_first_junction_connection_contact_type = types::JunctionContactType::kStart;
    const auto expected_second_junction_connection_id = "1";
    const auto expected_second_junction_connection_incoming_road = "0";
    const auto expected_second_junction_connection_connecting_road = "2";
    const auto expected_second_junction_connection_contact_type = types::JunctionContactType::kStart;
    const auto expected_first_junction_lane_links_size = 3;
    const auto expected_second_junction_lane_links_size = 1;

    converter.Convert(map);

    const auto& junctions = data.junctions;
    ASSERT_EQ(junctions.size(), expected_junctions_size);
    EXPECT_EQ(junctions.begin()->first, expected_junction_id);

    const auto& junction = junctions.begin()->second;
    EXPECT_EQ(junction.name, expected_junction_name);
    EXPECT_EQ(junction.id, expected_junction_id);

    const auto& junction_connections = junction.junction_connections;
    ASSERT_EQ(junction_connections.size(), expected_junction_connections_size);

    const auto& first_junction_connection = junction_connections[0];
    EXPECT_EQ(first_junction_connection.id, expected_first_junction_connection_id);
    EXPECT_EQ(first_junction_connection.incoming_road, expected_first_junction_connection_incoming_road);
    EXPECT_EQ(first_junction_connection.connecting_road, expected_first_junction_connection_connecting_road);
    EXPECT_EQ(first_junction_connection.contact_type, expected_first_junction_connection_contact_type);

    const auto& second_junction_connection = junction_connections[1];
    EXPECT_EQ(second_junction_connection.id, expected_second_junction_connection_id);
    EXPECT_EQ(second_junction_connection.incoming_road, expected_second_junction_connection_incoming_road);
    EXPECT_EQ(second_junction_connection.connecting_road, expected_second_junction_connection_connecting_road);
    EXPECT_EQ(second_junction_connection.contact_type, expected_second_junction_connection_contact_type);

    const auto& first_junction_lane_links = first_junction_connection.junction_lane_links;
    ASSERT_EQ(first_junction_lane_links.size(), expected_first_junction_lane_links_size);
    EXPECT_EQ(first_junction_lane_links[0].from, 1);
    EXPECT_EQ(first_junction_lane_links[0].to, -1);
    EXPECT_EQ(first_junction_lane_links[1].from, 2);
    EXPECT_EQ(first_junction_lane_links[1].to, -2);
    EXPECT_EQ(first_junction_lane_links[2].from, 3);
    EXPECT_EQ(first_junction_lane_links[2].to, -3);

    const auto& second_junction_lane_links = second_junction_connection.junction_lane_links;
    ASSERT_EQ(second_junction_lane_links.size(), expected_second_junction_lane_links_size);
    // compare each link in a group of lane links
    EXPECT_EQ(second_junction_lane_links[0].from, 1);
    EXPECT_EQ(second_junction_lane_links[0].to, -1);
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithMultiSectionsAndRoadmarks_WhenConverting_RoadmarkMustHaveCorrectStartValue)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_multisections.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto expected_section_0_roadmark_s_start = 0.0;
    const auto expected_section_1_roadmark_s_start = 142.9609375569712;
    const auto expected_section_2_roadmark_s_start = 614.56952837498488;
    const auto expected_section_3_roadmark_s_start = 1274.57;

    const auto& road = data.roads["5"];
    const auto& lane_section_0 = road.lane_sections.At(units::length::meter_t(0));
    const auto& lane_section_1 = road.lane_sections.At(units::length::meter_t(142.9609375569712));
    const auto& lane_section_2 = road.lane_sections.At(units::length::meter_t(614.56952837498488));
    const auto& lane_section_3 = road.lane_sections.At(units::length::meter_t(1274.57));
    EXPECT_FALSE(lane_section_0.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_0.center_lane.road_mark.begin()->first.value(), expected_section_0_roadmark_s_start);
    EXPECT_FALSE(lane_section_1.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_1.center_lane.road_mark.begin()->first.value(), expected_section_1_roadmark_s_start);
    EXPECT_FALSE(lane_section_2.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_2.center_lane.road_mark.begin()->first.value(), expected_section_2_roadmark_s_start);
    EXPECT_FALSE(lane_section_3.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_3.center_lane.road_mark.begin()->first.value(), expected_section_3_roadmark_s_start);
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithMultiSectionsWithoutRoadmarks_WhenConverting_DefaultRoadmarkMustHaveCorrectStartValue)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_multisections_without_roadmark.xodr");
    const odr::OpenDriveMap map(path);

    converter.Convert(map);

    const auto expected_section_0_roadmark_s_start = 0.0;
    const auto expected_section_1_roadmark_s_start = 142.9609375569712;
    const auto expected_section_2_roadmark_s_start = 614.56952837498488;
    const auto expected_section_3_roadmark_s_start = 1274.57;

    const auto& road = data.roads["5"];
    const auto& lane_section_0 = road.lane_sections.At(units::length::meter_t(0));
    const auto& lane_section_1 = road.lane_sections.At(units::length::meter_t(142.9609375569712));
    const auto& lane_section_2 = road.lane_sections.At(units::length::meter_t(614.56952837498488));
    const auto& lane_section_3 = road.lane_sections.At(units::length::meter_t(1274.57));
    EXPECT_FALSE(lane_section_0.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_0.center_lane.road_mark.begin()->first.value(), expected_section_0_roadmark_s_start);
    EXPECT_FALSE(lane_section_1.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_1.center_lane.road_mark.begin()->first.value(), expected_section_1_roadmark_s_start);
    EXPECT_FALSE(lane_section_2.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_2.center_lane.road_mark.begin()->first.value(), expected_section_2_roadmark_s_start);
    EXPECT_FALSE(lane_section_3.center_lane.road_mark.Empty());
    EXPECT_EQ(lane_section_3.center_lane.road_mark.begin()->first.value(), expected_section_3_roadmark_s_start);
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithCrossPaths_WhenConverting_ThenCrossPathsMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/cross_path_example.xodr");
    const odr::OpenDriveMap map(path);
    const auto expected_junctions_size = 1;
    const auto expected_junction_connections_size = 4;
    const auto expected_junction_cross_paths_size = 3;
    const auto expected_first_junction_cross_path_id = "6";
    const auto expected_first_junction_cross_path_crossing_road = "75";
    const auto expected_first_junction_cross_path_road_at_start = "46";
    const auto expected_first_junction_cross_path_road_at_end = "45";

    converter.Convert(map);

    const auto& junctions = data.junctions;
    ASSERT_EQ(junctions.size(), expected_junctions_size);

    const auto& junction = junctions.begin()->second;
    const auto& junction_connections = junction.junction_connections;
    const auto& junction_cross_paths = junction.junction_cross_paths;
    ASSERT_EQ(junction_connections.size(), expected_junction_connections_size);
    ASSERT_EQ(junction_cross_paths.size(), expected_junction_cross_paths_size);

    const auto& first_junction_cross_path = junction_cross_paths[0];
    EXPECT_EQ(first_junction_cross_path.id, expected_first_junction_cross_path_id);
    EXPECT_EQ(first_junction_cross_path.crossing_road, expected_first_junction_cross_path_crossing_road);
    EXPECT_EQ(first_junction_cross_path.road_at_start, expected_first_junction_cross_path_road_at_start);
    EXPECT_EQ(first_junction_cross_path.road_at_end, expected_first_junction_cross_path_road_at_end);

    const auto& first_junction_cross_path_start_lane_link = first_junction_cross_path.start_lane_link;
    const auto& first_junction_cross_path_end_lane_link = first_junction_cross_path.end_lane_link;
    EXPECT_EQ(first_junction_cross_path_start_lane_link.s, 0.500);
    EXPECT_EQ(first_junction_cross_path_start_lane_link.from, 3);
    EXPECT_EQ(first_junction_cross_path_start_lane_link.to, 1);
    EXPECT_EQ(first_junction_cross_path_end_lane_link.s, 0.2484163);
    EXPECT_EQ(first_junction_cross_path_end_lane_link.from, -3);
    EXPECT_EQ(first_junction_cross_path_end_lane_link.to, 1);
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithCrossing_WhenConverting_ThenCrossingMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/crossing_example.xodr");
    const odr::OpenDriveMap map(path);
    const auto expected_junctions_size = 1;
    const auto expected_junction_type = types::JunctionType::kCrossing;
    const auto expected_junction_priorities_size = 1;
    const auto expected_road_id_with_high_priority = "2";
    const auto expected_road_id_with_low_priority = "1";
    const auto expected_junction_road_sections_size = 2;
    const auto expected_first_junction_road_section_id = "0";
    const auto expected_first_junction_road_section_road_id = "1";
    const auto expected_first_junction_road_section_s_start = 50.0;
    const auto expected_first_junction_road_section_s_end = 60.0;
    const auto expected_second_junction_road_section_id = "1";
    const auto expected_second_junction_road_section_road_id = "2";
    const auto expected_second_junction_road_section_s_start = 0.0;
    const auto expected_second_junction_road_section_s_end = 10.0;

    converter.Convert(map);

    const auto& junctions = data.junctions;
    ASSERT_EQ(junctions.size(), expected_junctions_size);

    const auto& junction = junctions.begin()->second;
    EXPECT_EQ(junction.type, expected_junction_type);

    const auto& junction_priorities = junction.junction_priorities;
    ASSERT_EQ(junction_priorities.size(), expected_junction_priorities_size);

    const auto& junction_priority = junction_priorities.front();
    EXPECT_EQ(junction_priority.high, expected_road_id_with_high_priority);
    EXPECT_EQ(junction_priority.low, expected_road_id_with_low_priority);

    const auto& junction_road_sections = junction.junction_road_sections;
    ASSERT_EQ(junction_road_sections.size(), expected_junction_road_sections_size);

    const auto& first_junction_road_section = junction_road_sections[0];
    EXPECT_EQ(first_junction_road_section.id, expected_first_junction_road_section_id);
    EXPECT_EQ(first_junction_road_section.road_id, expected_first_junction_road_section_road_id);
    EXPECT_EQ(first_junction_road_section.s_start, expected_first_junction_road_section_s_start);
    EXPECT_EQ(first_junction_road_section.s_end, expected_first_junction_road_section_s_end);

    const auto& second_junction_road_section = junction_road_sections[1];
    EXPECT_EQ(second_junction_road_section.id, expected_second_junction_road_section_id);
    EXPECT_EQ(second_junction_road_section.road_id, expected_second_junction_road_section_road_id);
    EXPECT_EQ(second_junction_road_section.s_start, expected_second_junction_road_section_s_start);
    EXPECT_EQ(second_junction_road_section.s_end, expected_second_junction_road_section_s_end);
}

TEST(LibOpenDriveConverterTest, GivenOpenDriveMapWithSignals_WhenConverting_ThenSignalsMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_traffic_signs.xodr");
    const odr::OpenDriveMap map(path);

    const mantle_api::Vec3<units::length::meter_t> expected_position_signal_1(
        units::length::meter_t(50), units::length::meter_t(-6.5), units::length::meter_t(1.90));
    const mantle_api::Vec3<units::length::meter_t> expected_position_signal_2(
        units::length::meter_t(50), units::length::meter_t(-6.5), units::length::meter_t(1.56));
    const auto expected_signal_1_dynamic = false;
    const auto expected_signal_2_dynamic = false;
    const auto expected_signal_1_type = "274";
    const auto expected_signal_2_type = "1010";
    const auto expected_signal_1_subtype = "56";
    const auto expected_signal_2_subtype = "51";
    const auto expected_signal_1_country = "DE";
    const auto expected_signal_1_country_revision = "2013";
    const auto expected_signal_1_width = units::length::meter_t(0.61);
    const auto expected_signal_1_height = units::length::meter_t(0.61);
    const auto expected_signal_2_width = units::length::meter_t(0.60);
    const auto expected_signal_2_height = units::length::meter_t(0.33);
    const auto expected_signal_1_value = 60.0;
    const auto expected_signal_1_unit = road_logic_suite::SignalUnit::kKilometerPerHour;
    const auto expected_signal_2_unit = road_logic_suite::SignalUnit::kNoUnit;
    const auto expected_signal_1_yaw = units::angle::radian_t(M_PI);
    const auto expected_signal_1_pitch = units::angle::radian_t(0.0);
    const auto expected_signal_1_roll = units::angle::radian_t(0.0);
    const auto expected_signal_2_yaw = units::angle::radian_t(M_PI);
    const auto expected_signal_2_pitch = units::angle::radian_t(0.0);
    const auto expected_signal_2_roll = units::angle::radian_t(0.0);

    converter.Convert(map);

    const auto& signals = data.signals;

    auto it_1 = signals.find("1");
    ASSERT_TRUE(it_1 != signals.end()) << "The signal was not found in data storage!";
    const auto signal_1 = it_1->second;

    auto it_2 = signals.find("2");
    ASSERT_TRUE(it_2 != signals.end()) << "The signal was not found in data storage!";
    const auto signal_2 = it_2->second;

    ASSERT_THAT(signal_1.position, road_logic_suite::test::InertialPositionIsNear(expected_position_signal_1));
    ASSERT_THAT(signal_2.position, road_logic_suite::test::InertialPositionIsNear(expected_position_signal_2));

    EXPECT_EQ(expected_signal_1_dynamic, signal_1.is_dynamic);
    EXPECT_EQ(expected_signal_2_dynamic, signal_2.is_dynamic);
    EXPECT_EQ(expected_signal_1_type, signal_1.type);
    EXPECT_EQ(expected_signal_2_type, signal_2.type);
    EXPECT_EQ(expected_signal_1_subtype, signal_1.subtype);
    EXPECT_EQ(expected_signal_2_subtype, signal_2.subtype);
    EXPECT_EQ(expected_signal_1_country, signal_1.country);
    EXPECT_EQ(expected_signal_1_country_revision, signal_1.country_revision);
    EXPECT_EQ(expected_signal_1_width, signal_1.width);
    EXPECT_EQ(expected_signal_1_height, signal_1.height);
    EXPECT_EQ(expected_signal_2_width, signal_2.width);
    EXPECT_EQ(expected_signal_2_height, signal_2.height);
    EXPECT_EQ(expected_signal_1_value, signal_1.value);
    EXPECT_EQ(expected_signal_1_unit, signal_1.unit);
    EXPECT_EQ(expected_signal_2_unit, signal_2.unit);
    EXPECT_EQ(expected_signal_1_yaw, signal_1.yaw);
    EXPECT_EQ(expected_signal_1_roll, signal_1.roll);
    EXPECT_EQ(expected_signal_1_pitch, signal_1.pitch);
    EXPECT_EQ(expected_signal_2_yaw, signal_2.yaw);
    EXPECT_EQ(expected_signal_2_roll, signal_2.roll);
    EXPECT_EQ(expected_signal_2_pitch, signal_2.pitch);

    EXPECT_THAT(signal_1.valid_lanes, ::testing::ElementsAre(-2, -1));
    EXPECT_THAT(signal_2.valid_lanes, ::testing::ElementsAre(-2, -1));
    EXPECT_THAT(signal_1.dependencies, ::testing::ElementsAre("2"));
}

TEST(LibOpenDriveConverterTest,
     GivenOpenDriveMapWithLaneOffsets_WhenConverting_ThenLaneOffsetsMustBeAddedToStorageCorrectly)
{
    OpenDriveData data{};
    LibOpenDriveConverter converter(data);
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/road_with_laneoffset.xodr");
    const odr::OpenDriveMap map(path);

    // expect_a = a - b * s0 + c * s0 * s0 - d * s0 * s0 * s0;
    // expect_b = b - 2 * c * s0 + 3 * d * s0 * s0;
    // expect_c = c - 3 * d * s0;
    // expect_d = d;
    const auto expected_laneoffset_param_a = 0.1 - 0.0 * 50 + 3.9e-03 * 50 * 50 - (-5.2e-05) * 50 * 50 * 50;
    const auto expected_laneoffset_param_b = 0.0 - 2 * 3.9e-03 * 50 + 3 * (-5.2e-05) * 50 * 50;
    const auto expected_laneoffset_param_c = 3.9e-03 - 3 * (-5.2e-05) * 50;
    const auto expected_laneoffset_param_d = -5.2e-05;

    converter.Convert(map);

    ASSERT_EQ(data.roads.size(), 2);
    const auto& laneoffsets_0 = data.roads["0"].laneoffsets;
    const auto& laneoffsets_1 = data.roads["1"].laneoffsets;

    ASSERT_EQ(laneoffsets_0.size(), 1);
    ASSERT_EQ(laneoffsets_1.size(), 2);
    const auto& laneoffset_road_0 = laneoffsets_0.At(0_m);
    const auto& laneoffset_road_1_s0 = laneoffsets_1.At(0_m);
    const auto& laneoffset_road_1_s50 = laneoffsets_1.At(50_m);

    EXPECT_DOUBLE_EQ(laneoffset_road_0.a, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_0.b, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_0.c, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_0.d, 0);

    EXPECT_DOUBLE_EQ(laneoffset_road_1_s0.a, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s0.b, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s0.c, 0);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s0.d, 0);

    EXPECT_DOUBLE_EQ(laneoffset_road_1_s50.a, expected_laneoffset_param_a);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s50.b, expected_laneoffset_param_b);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s50.c, expected_laneoffset_param_c);
    EXPECT_DOUBLE_EQ(laneoffset_road_1_s50.d, expected_laneoffset_param_d);
}

}  // namespace road_logic_suite::test
