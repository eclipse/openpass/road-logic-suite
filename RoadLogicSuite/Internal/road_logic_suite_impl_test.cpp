/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/road_logic_suite_impl.h"

#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <gtest/gtest.h>
#include <units.h>

namespace road_logic_suite::test
{
using namespace units::literals;

class RoadLogicSuiteImplTestFixture : public ::testing::Test
{
  public:
    RoadLogicSuiteImplTestFixture() = default;
};

TEST_F(RoadLogicSuiteImplTestFixture, GivenRoadLogicSuiteImpl_WhenConstructed_ThenNoExceptions)
{
    EXPECT_NO_THROW(RoadLogicSuiteImpl unit);
}

/**
 * Simple test. If the file does not exist, the return value should be false.
 */
TEST_F(RoadLogicSuiteImplTestFixture, GivenDummyPath_WhenLoadNonExistingFile_ThenReturnFailed)
{
    RoadLogicSuiteImpl unit;
    EXPECT_FALSE(unit.LoadFile("any_path.xodr"));
}

/**
 * Simple test. If the file exists, the return value should be true.
 */
TEST_F(RoadLogicSuiteImplTestFixture, GivenExistingPath_WhenLoadFile_ThenReturnSucceds)
{
    RoadLogicSuiteImpl unit;
    const auto path = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    ASSERT_TRUE(unit.LoadFile(path));
}

/**
 * This is also a basic usage example for the validators
 */
TEST_F(RoadLogicSuiteImplTestFixture, GivenValidMap_WhenValidatingMap_ThenNoErrorsShouldExist)
{
    RoadLogicSuiteImpl impl;
    const auto simpleXodrFile = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");
    impl.LoadFile(simpleXodrFile);
    const auto runner = impl.GetMapValidator();

    const auto result = runner.Validate();

    EXPECT_EQ(0, result.size());
}

}  // namespace road_logic_suite::test
