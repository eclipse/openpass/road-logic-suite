/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LIBOPENDRIVE_ROAD_OBJECT_CONVERTER_H
#define ROADLOGICSUITE_LIBOPENDRIVE_ROAD_OBJECT_CONVERTER_H

#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <OpenDriveMap.h>

namespace road_logic_suite
{
class LibOpenDriveRoadObjectConverter
{
  public:
    /// @brief Converts all road objects (singular, repeating and continuous) into the internal data structure for this
    /// road.
    /// @param data_storage the data storage.
    /// @param xml_road the input road.
    static void ConvertForRoad(OpenDriveData& data_storage, const odr::Road& xml_road);
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_LIBOPENDRIVE_ROAD_OBJECT_CONVERTER_H
