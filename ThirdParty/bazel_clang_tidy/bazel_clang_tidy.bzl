"""
This module contains rule to pull erenon/bazel_clang_tidy
"""

load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

# LICENSE: MIT license
def bazel_clang_tidy():
    maybe(
        git_repository,
        name = "bazel_clang_tidy",
        commit = "674fac7640ae0469b7a58017018cb1497c26e2bf",
        remote = "https://github.com/erenon/bazel_clang_tidy.git",
    )
