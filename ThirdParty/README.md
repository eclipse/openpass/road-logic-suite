# Third Party Libraries in use

## Open Source

## Release build dependencies

You can find the list of third-party libraries used in this project in the [NOTICE.md](https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite/-/blob/main/NOTICE.md?ref_type=heads). 


# Licenses in use

| License                                         |    Type     |                           Additional information/comment                           |
| ----------------------------------------------- | :---------: | :--------------------------------------------------------------------------------: |
| Hedron Vision Incorporated                      | Open Source |                                  customer license                                  |
| Apache-2.0                                      | Open Source |                        free license, compatible with GPLv3                         |
| MIT License                                     | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| University of Illinois/NCSA Open Source License | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 3-Clause "New" or "Revised" License         | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| The Happy Bunny License and MIT License         | Open Source |                     Happy Bunny License (modified MIT License)                     |
| ISC                                             | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| Eclipse Public License 2.0                      | Open Source | free license, **weak copyleft**, additions to the same module have to be published |
