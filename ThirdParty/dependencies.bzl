load("@road_logic_suite//ThirdParty/libOpenDRIVE:libOpenDrive.bzl", "lib_open_drive")
load("@road_logic_suite//ThirdParty/units:units.bzl", "units_nhh")
load("@road_logic_suite//ThirdParty/rtree:rtree.bzl", "rtree")
load("@road_logic_suite//ThirdParty/bazel_clang_tidy:bazel_clang_tidy.bzl", "bazel_clang_tidy")
load("@road_logic_suite//ThirdParty/mantle_api:mantle_api.bzl", "mantle_api")
load("@road_logic_suite//ThirdParty/map_sdk:map_sdk.bzl", "map_sdk")

def _non_module_dependencies_impl(_ctx):
    """
    Load third party libraries
    """
    lib_open_drive()
    units_nhh()
    rtree()
    mantle_api()
    map_sdk()

non_module_dependencies = module_extension(
    implementation = _non_module_dependencies_impl,
)

def _non_module_dev_dependencies_impl(_ctx):
    bazel_clang_tidy()

non_module_dev_dependencies = module_extension(
    implementation = _non_module_dev_dependencies_impl,
)
