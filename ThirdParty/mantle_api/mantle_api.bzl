load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v2.0.0"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = "https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "4b3da5d1d9f00e96745f9f307c5d179d00e57df996b388f20119429d8ad2a46c",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
